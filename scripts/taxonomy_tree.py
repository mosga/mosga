#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import re
import random
import time
import json
import argparse
from argparse import ArgumentParser

TAX_LEVELS = {"kingdom": 1.0,
              "phylum": 1.0,
              "class": 1.0,
              "order": 1.0,
              "family": 1.0,
              "genus": 1.0,
              "species": 1.0,
              "clade": 0.25}


class TaxonomyTree:
    def __init__(self, mode: str, in_file: str, out_file: str, tax_file: str, do_test=False):
        self.mode = mode
        self.in_file = in_file
        self.out_file = out_file
        self.tax_file = tax_file
        self._run()
        if do_test:
            self._run_test()

    @staticmethod
    def _read_dump(path):
        """
        Opens a NCBI taxonomy dump file and reads the rows of the file.

        :param path: Path to dump file which contains tax id, parent tax id, ...
        :returns: returns all lines of the file as list
        """
        if not os.path.isfile(path):
            exit(0)
        else:
            with open(path) as csv:
                out = [[e for e in re.split('\t', line.rstrip(' \n\r')) if e and e != ' '] for line in csv.readlines()][1:]
                return out

    def _build_taxonomy_tree(self):
        """
        Builds a dictionary that has all the links between the individual taxes.

        :note: There can be 'unknown' nodes inside, which have the ID 0 with a weight of 1
        and are so to speak origin nodes.
        """
        self.tree = dict()
        for line in self.tax:

            try:
                weight = TAX_LEVELS[line[3]] if line[3] in TAX_LEVELS else 0.0      # if the rank appears in the list, the node gets a weighting
                current_tax = int(line[0])
                next_tax = int(line[1])
            except ValueError:
                continue

            if current_tax not in self.tree:
                # node weight -> 0, parent -> 1, children -> 2
                self.tree[current_tax] = {0: weight, 1: next_tax, 2: []}
                if next_tax not in self.tree:
                    self.tree[next_tax] = {0: 1, 1: 0, 2: [current_tax]}
                else:
                    self.tree[next_tax][2].append(current_tax)
            else:
                self.tree[current_tax][0] = weight
                self.tree[current_tax][1] = next_tax
                if next_tax not in self.tree:
                    self.tree[next_tax] = {0: 1, 1: 0, 2: [current_tax]}
                else:
                    self.tree[next_tax][2].append(current_tax)

    def _build_taxonomy_paths(self):
        """
        Creates a dictionary that outputs the paths from given ids to the origin node.
        """
        self.paths = dict()
        for target in self.tax_targets:
            lst = target
            try:
                nxt = self.tree[lst][1]
            except KeyError: # not connected in the tree
                continue
            search = True
            path = [list(), list()]
            while search:
                if nxt == 0:
                    search = False
                else:
                    path[0].append(self.tree[nxt][0])
                    path[1].append(nxt)
                    lst = nxt
                    nxt = self.tree[lst][1]
            self.paths[target] = path

    @staticmethod
    def _read_json(path):
        """
        Reads a JSON file with the taxonomy tree.

        :param path: Path and name of the JSON file with the taxonomy tree.
        :returns: Taxonomy tree as dict.
        """
        with open(path, 'r', encoding='utf-8') as f:
            return json.load(f, object_hook=lambda d: {int(k): v for k, v in d.items()})

    @staticmethod
    def _write_json(tree, path):
        """
        Writes a JSON file with the taxonomy tree.

        :param tree: Taxonomy tree as dict.
        :param path: Path and name of the JSON file.
        """
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(tree, f, ensure_ascii=False, indent=4)

    def _run(self):
        """
        Calls the single steps of the script one after the other.
        """
        if self.mode == "path":
            self.tree = TaxonomyTree._read_json(self.in_file)
            with open(self.tax_file, 'r', encoding='utf-8') as f:
                self.tax_targets = [int(x) for x in f.readlines()]
            self._build_taxonomy_paths()
            TaxonomyTree._write_json(self.paths, self.out_file)
        else:
            self.tax = TaxonomyTree._read_dump(self.in_file)
            self._build_taxonomy_tree()
            TaxonomyTree._write_json(self.tree, self.out_file)

    def _run_test(self, k=5):
        print("--- distance test ---")
        start_time = time.time()
        a = random.choice(list(self.tree.keys()))
        for i in range(k):
            b_time = time.time()
            if self.mode == "path":         # TESTING DISTANCE BETWEEN TWO NODES WITH GIVEN PATH
                b = random.choice(self.tax_targets)
                dist = self._dist_fast(a, b)
            else:                       # TESTING DISTANCE BETWEEN TWO NODES
                b = random.choice(list(self.tree.keys()))
                dist = self._dist(a, b)
            print(str(a) + "," + str(b) + " distance: " + str(dist) + " time: " + str(time.time() - b_time))
        print("total time: %s" % (time.time() - start_time))

    def _dist_fast(self, uid, target):  # TESTING DISTANCE BETWEEN TWO NODES WITH GIVEN PATH
        path = self.paths[target]
        lst = uid
        nxt = self.tree[lst][1]
        dist = 0
        while True:
            dist += self.tree[nxt][0]
            if nxt == target:
                break
            if nxt in path[1]:
                idx = path[1].index(nxt)
                for i in range(idx - 1, -1, -1):
                    dist += path[0][i]
                dist += self.tree[target][0]
                break
            lst = nxt
            nxt = self.tree[lst][1]
        return dist

    def _dist(self, uid, target):       # TESTING DISTANCE BETWEEN TWO NODES
        lst = uid
        nxt = self.tree[lst][1]
        search = True
        dist = 0
        while search:
            dist += self.tree[nxt][0]
            if nxt == target:
                break
            for child in self.tree[nxt][2]:
                if child == lst:      # dont check origin id again
                    continue
                d, f = self._dist_childs(child, target, self.tree[child][0])
                if f:
                    search = False
                    dist += d
                    break
            lst = nxt
            nxt = self.tree[lst][1]
        return dist

    def _dist_childs(self, child, target, dist=1):         # TESTING DISTANCE BETWEEN TWO NODES
        if child == target:
            return dist, True
        if len(self.tree[child][2]) > 0:
            c = []
            for child in self.tree[child][2]:
                d, f = self._dist_childs(child, target, dist + self.tree[child][0])
                if f:
                    return d, f
                else:
                    c.append(d)
            return min(c), False
        else:
            return sys.maxsize, False


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="JSON taxonomy tree",
                                     description="Builds a taxonomic tree and puts it in JSON format,\r\n " \
                                                 "can also create the paths from certain taxonomies to the root\r\n " \
                                                 "Copyright © 2020 Hagen Dreßler",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-m", "--mode", help="choose between the two builder modes: tree / path", choices=["tree", "path"], metavar="", default="tree", type=str)
    parser.add_argument("-i", "--input", help="path to NCBI taxonomy dump file / path to taxonomy tree JSON file", metavar="path", type=str)
    parser.add_argument("-t", "--tax", help="path to tax id list (only for path mode)", metavar="path", type=str)
    parser.add_argument("-o", "--output", help="path and name for JSON file", metavar="path", type=str)
    parser.add_argument("--test", help="test random taxes for connections", action="store_true")
    args = parser.parse_args()
    TaxonomyTree(args.mode, args.input, args.output, args.tax, do_test=args.test)


if __name__ == '__main__':
    main()
