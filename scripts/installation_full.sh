#!/bin/bash
# run as root user

echo DEBIAN_FRONTEND=noninteractive >> /etc/environment
echo TZ="Europe/Berlin" >> /etc/environment
echo TOOLDIR="/opt/mosga/tools" >> /etc/environment
echo PATH=$PATH:/opt/mosga/tools/BRAKER:/opt/mosga/tools/augustus/bin:/opt/mosga/tools/augustus/scripts:/opt/mosga/tools/BRAKER/scripts:/usr/local/RepeatMasker:/opt/mosga/tools/GeneMark/gmes_petap:/opt/mosga/tools/diamond:/opt/mosga/tools/perl5lib-Fasta/lib:/opt/mosga/tools/BUSCO/bin:/opt/mosga/tools/miniconda/bin:/opt/mosga/tools/miniconda/condabin:/opt/mosga/tools/bbmap:/opt/mosga/tools/metaeuk/bin >> /etc/environment
echo AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config >> /etc/environment
echo GENEMARK_PATH=/opt/mosga/tools/GeneMark/ >> /etc/environment
echo BAMTOOLS_PATH=/usr/local/bin/ >> /etc/environment
echo ALIGNMENT_TOOL_PATH=/opt/mosga/tools/gth/bin/ >> /etc/environment
echo SAMTOOLS_PATH=/opt/mosga/tools/samtools/ >> /etc/environment
echo DIAMOND_PATH=/opt/mosga/tools/diamond/ >> /etc/environment
echo CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta/ >> /etc/environment
echo SSUALIGNDIR=/usr/local/share/ssu-align-0.1.1 >> /etc/environment

echo PERL_MM_USE_DEFAULT=1 >> /etc/environment
export PERL_MM_USE_DEFAULT=1

adduser --disabled-password --gecos '' mosga
chown mosga:root /opt/
sudo -u mosga git clone https://gitlab.com/mosga/mosga.git /opt/mosga
mkdir /opt/mosga/tools
chmod 775 /opt/mosga/tools
chown -R mosga:mosga /opt/mosga/tools
chown mosga:www-data /opt/mosga/gui /opt/mosga/snakemake

DEBIAN_FRONTEND=noninteractive bash /opt/mosga/scripts/install/basics_root.sh

cd /opt/mosga/gui/ && sudo -u mosga composer install
sudo -u mosga echo ';<?php exit(0); ?>' > /opt/mosga/gui/config.ini.php
sudo -u mosga bash /opt/mosga/scripts/install/prepare_tools.sh
sudo -u mosga bash /opt/mosga/scripts/install/eggnog.sh
sudo -u mosga bash /opt/mosga/scripts/install/idtaxa.sh
sudo -u mosga bash /opt/mosga/scripts/install/swissprot.sh
sudo -u mosga cp /opt/mosga/data/*.faa /opt/mosga/tools/diamond
sudo -u mosga bash /opt/mosga/scripts/install/organelles_db.sh
sudo -u mosga bash /opt/mosga/scripts/install/silva.sh

sudo -u mosga echo "export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8
export PYTHONIOENCODING=utf-8" > /home/mosga/env.sh
chown mosga:mosga /home/mosga/env.sh

sudo -u mosga touch /home/mosga/cron.log
(crontab -l -u mosga; echo "* * * * * /bin/bash /home/mosga/env.sh && cd /opt/mosga/gui && php scheduler.php >> /home/mosga/cron.log 2>&1") | sudo -u mosga crontab

cd /opt/mosga/tools/augustus/
sudo -u mosga make
make install

bash /opt/mosga/scripts/install/tools.sh
cp /opt/mosga/scripts/docker_apache.conf /etc/apache2/sites-enabled/000-default.conf

systemctl restart apache2

# If you would like to use BRAKER, GeneMark and EukCC fully, you require to install GeneMark as well. Please store the gmes_linux64.tar.gz binary archive to /home/mosga/ and continue:

if [ -f "/home/mosga/gmes_linux_64.tar.gz" ] ; then
  sudo -u mosga tar -xzf /home/mosga/gmes_linux_64.tar.gz -C /opt/mosga/tools;
  sudo -u mosga mv /opt/mosga/tools/gmes_* /opt/mosga/tools/GeneMark
  cp -r /opt/mosga/tools/GeneMark/* /usr/local/bin/
  rm -rf /opt/mosga/tools/ProtHint/dependencies/GeneMarkES/ 2> /dev/null
  ln -s /opt/mosga/tools/GeneMark /opt/mosga/tools/ProtHint/dependencies/GeneMarkES
  cp -f /opt/mosga/tools/spaln/bin/spaln /opt/mosga/tools/ProtHint/dependencies/spaln
else
  jq 'del(.annotation.genes.content.prediction.tools.braker, .annotation.genes.content.prediction.tools.genemark, .annotation.validation.content.completeness.tools.eukcc, .comparative.completeness.content.orthologs.tools.eukcc)' /opt/mosga/gui/gui-rules.json > /opt/mosga/gui/gui-rules.alt.json
fi

echo "DONE! You should be able to access the MOSGA page under http://localhost/";
