#!/usr/bin/env python3
import argparse
import gzip


class MatchestoGFF3:

    def __init__(self, input_path: str, output_path: str, console: bool = False, gff_type: str = None,
                 stype: str = None, source: str = None):
        self.__input: str = input_path
        self.__output: str = output_path
        self.__console: bool = console
        self.__stype: str = stype
        self.__type: str = gff_type
        self.__source: str = source
        self.__converts()

    def __converts(self):
        self.gene_ids = {}
        gene_cnt = 1

        if not self.__console:
            write = open(self.__output, "w")

        feature_bucket = []
        identifier = 1

        with gzip.open(self.__input, "rb") as fp:
            out = ["##gff-version 3"]

            for line in fp:
                line = line.decode("utf-8").rstrip()

                if line[:1] == "#":
                    continue

                line_split = line.split("\t")
                source = self.__source
                gff_type = self.__type
                attr = ""

                # SILVA
                if self.__type == "rRNA" and self.__source == "SILVA":
                    if self.__stype == "LSU":
                        names = [" LSU", "large subunit rRNA"]
                    elif self.__stype == "SSU":
                        names = [" SSU", "small subunit rRNA"]
                    else:
                        names = ["", ""]

                    source = "SILVA%s" % names[0]

                    if self.__stype is not None:
                        attr = "ID=r%i; Name=%s; Notice=Match with %s;" % (identifier, names[1],
                                                                           line_split[17].replace(";", "/"))
                    else:
                        attr = "ID=%i" % identifier

                # Mito / Plastid
                if self.__type == "Match" and (self.__source == "Mito" or self.__source == "Plastid"):
                    last_split = line_split[-1]
                    name = last_split[last_split.find(" ")+1:]
                    name2 = name[name.rfind(")")+1:]
                    name = name[:name.rfind("(")-1]
                    notice = "Match with %s (%s)" % (name2[2:-1], line_split[1])
                    attr = "ID=o%i; Name=%s; Notice=%s" % (identifier, name.replace("=", ""), notice)

                out.append("\t".join([
                    line_split[0], source, gff_type, line_split[2], line_split[3], line_split[8], ".", ".",
                    attr]))
                identifier += 1
            
        if len(out):
            for o in out:
                if self.__console:
                    print(o)
                else:
                    write.write(o+"\r\n")

        fp.close()
        if not self.__console:
            write.close()


def main():
    """
    Converts BLAST/Diamond matches to proper GFF3.
    """
    parser = argparse.ArgumentParser(prog="BLAST/Diamond matches to GFF3 Converter",
                                     description="Converts BLAST/Diamondt matches to GFF3\r\n "
                                                 "Copyright © 2022 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", type=str, help="path of the gzipped input file")
    parser.add_argument("-o", "--output", type=str, default="",
                        help="path to output file. if not exist output will be printed.")
    parser.add_argument("-st", "--specific-type", type=str, default=None, help="more specific than the type para.")
    parser.add_argument("-t", "--type", type=str, default="Match")
    parser.add_argument("-s", "--source", type=str, default="BLAST")
    args = parser.parse_args()

    console = False
    if not len(args.output):
        console = True

    MatchestoGFF3(args.input, args.output, console, args.type, args.specific_type, args.source)


if __name__ == '__main__':
    main()