#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import altair as alt
import argparse
from argparse import ArgumentParser


HEATMAP_HTML = "heatmap.html"
HEATMAP_SVG = "heatmap.svg"


class ANIVisual:
    def __init__(self, in_file: str, out_dir: str, text_overlay: bool = False):
        self.in_file = in_file
        self.out_dir = out_dir
        self.text_overlay = text_overlay
        alt.renderers.set_embed_options(scaleFactor=3)
        self.__run()

    def _read_table(self):
        """
        Reads the specified ANI summary/table file.
        """
        self.table = []
        if not os.path.isfile(self.in_file):
            print(f"ERROR: Specified file could not be found: {self.in_file}")
            exit(0)
        else:
            with open(self.in_file) as f:
                self.table = [re.split(',', line.rstrip('\n')) for line in f.readlines()]

    def _plot_heatmap(self):
        """
        Creates & save a heatmap from the ANI values.
        """
        data = {"x": [], "y": [], "z": [], "ignore": []}
        names = list()
        for x in self.table:        # prepare dataset
            data["x"].append(x[0])
            data["y"].append(x[1])
            data["z"].append(x[2])
            data["ignore"].append(False)
            names.append(x[0])
        names = list(dict.fromkeys(names))
        amount = len(names)
        for i in range(0, amount):       # fix missing data
            data["x"].insert((i * amount) + i, names[i])
            data["y"].insert((i * amount) + i, names[i])
            data["z"].insert((i * amount) + i, "100.0")
            data["ignore"].insert((i * amount) + i, True)
        source = alt.pd.DataFrame(data)

        fig = alt.Chart(source).mark_rect().encode(
            x=alt.X('x:O', axis=alt.Axis(title="")),
            y=alt.Y('y:O', axis=alt.Axis(title="")),
            color=alt.Color('z:Q', legend=alt.Legend(title="ANI"))
        ).properties(width=256, height=256)

        if self.text_overlay:
            text = fig.mark_text(baseline="middle").encode(
                text=alt.Text("z:Q", format=".2f"),
                color=alt.condition(
                    alt.datum.z > 90,
                    alt.value("white"),
                    alt.value("black")
                ),
                opacity=alt.condition(
                    alt.datum.ignore,
                    alt.value(0.0),
                    alt.value(1.0)
                )
            )
            fig += text

        fig.save(self.out_dir + HEATMAP_HTML, embed_options={"renderer": "svg"}, webdriver='firefox')
        fig.save(self.out_dir + HEATMAP_SVG, webdriver='firefox')

    def __run(self):
        """
        Calls the single steps of the script one after the other.
        """
        self._read_table()  # read ANI summary/table
        self._plot_heatmap()  # plotting heatmap


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="ANI Visual",
                                     description="Visualizes the results of ANI of the comparative pipeline.\r\n " \
                                                 "Copyright © 2020 Roman Martin, Hagen Dreßler",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", help="Path to ANI summary file.", metavar="<PATH>", type=str, required=True)
    parser.add_argument("-o", "--output", help="Path to output directory.", metavar="<PATH>", type=str, required=True)
    parser.add_argument("-t", "--text", help="Enable text overlay.", action="store_true", default=False)
    args = parser.parse_args()
    ANIVisual(args.input, args.output, args.text)


if __name__ == '__main__':
    main()
