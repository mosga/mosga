#!/bin/bash
# check for long running spaln instances
PIDS=`ps aux | grep -e 'R.*ProtHint.*spaln' | grep -v "grep" | awk '{split($10,a,":");} a[1] > 9 {print $2}'`
# log and kill these instances
[ ! -z "$PIDS" ] && echo $PIDS | tee -a ~/kill_spaln.log | xargs kill
