#!/bin/bash
if [[ 6 -gt $# ]] ; then
    echo 'Arguments are missing'
    exit 1
fi

INPUT=$1
THREADS=$2
PDIR=$3
SPECIE=$4
LOG=$5
TAX=$6

# Check if the species was manually set
if [ "$SPECIE" = "default" ]; then
# Default specie?
  /usr/local/RepeatMasker/RepeatMasker -q -xsmall -no_is $INPUT -e hmmer -pa $2 -dir $PDIR >$LOG 2>&1
# Species was set
else
  python3 /usr/local/RepeatMasker/famdb.py -i /usr/local/RepeatMasker/Libraries/RepeatMaskerLib.h5 lineage "$SPECIE" > $TAX 2>&1
  tax=`head -n1 $TAX | awk '{print $1}'`
  if [ "$tax" = "No" ]; then
    # Default
    echo 'Unknown species for RepeatMasker database, fallback to undefined';
    echo 'Please check the repeats/repeatmasker/taxonomy.txt file';
    echo 'Or check manually for existing species:';
    echo 'python3 /usr/local/RepeatMasker/famdb.py -i /usr/local/RepeatMasker/Libraries/RepeatMaskerLib.h5 lineage "YOUR SPECIES NAME"';
    /usr/local/RepeatMasker/RepeatMasker -q -xsmall -no_is $INPUT -e hmmer -pa $THREADS -dir $PDIR >$LOG 2>&1
  else
    cat $TAX;
    /usr/local/RepeatMasker/RepeatMasker -q -species "$SPECIE" -xsmall -no_is $INPUT -e hmmer -pa $THREADS -dir $PDIR >$LOG 2>&1
  fi
fi
