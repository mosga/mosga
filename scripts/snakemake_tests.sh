#!/bin/bash

function test_snakemake {
  cd /opt/mosga/snakemake
  conf=$1
  dirname=`grep -Po '"dir":.*",' $conf | awk -F": "  '{print $2'} | cut -d '"' -f 2`
  dataname=`grep -Po '"path": .*"' $conf | awk -F": "  '{print $2'} | cut -d '"' -f 2`
  datapath=`echo $dataname | sed 's#[^ ]* *#tests/data/&#g'`
  extension="${datapath##*.}"
  rm  -r -f .snakemake/log .snakemake/locks tests/$dirname 2> /dev/null
  mkdir tests/$dirname 2> /dev/null
  cp $datapath tests/$dirname/
  snakemake -p --configfile $conf --cores 8 --conda-frontend conda --use-conda
  if ! [ $? -eq 0 ] ; then
    echo "An error occurred in the Snakemake test run!"
    exit 1;
  fi
}

cd /opt/mosga/snakemake ;
for conf in tests/conf_*.json; do
  echo $conf;
  test_snakemake $conf
done;
