#!/usr/bin/env Rscript
write_ids_to_csv <- function(ids, path) {
  func_data <- data.frame(matrix(ncol = 4, nrow = 0 ))
  #colnames(func_data) <- c("id", "level", "confidence", "function")
  for(n in names(ids)) {
    if( length(ids[[n]]$taxon) > 2 ) {
      for(i in 2:length(ids[[n]]$taxon)) {
        func_data[nrow(func_data)+1, ] = c(n, i-1, ids[[n]]$confidence[i],  ids[[n]]$taxon[i])
      }
    }
  }
  write.table(func_data, row.names=FALSE, quote=FALSE, file=gzfile(path), sep="\t", col.names = FALSE)
}

args = commandArgs(trailingOnly = TRUE)
if (length(args) != 6) {
  stop("Database, FASTA AA, fullLength, threshold, processors, results", call.=FALSE)
}

training_db = args[1]
fas = args[2]
full_length = as.numeric(args[3])
threshold = as.integer(args[4])
threads = as.integer(args[5])
output = args[6]

library("DECIPHER")
print("Load training set")
load(training_db)
print("Prepare sequences")
seqs <- readAAStringSet(fas)
seqs <- RemoveGaps(seqs)
print("Annotate")
ids <- IdTaxa(seqs, trainingSet, fullLength = full_length, threshold=threshold, processors=threads) # use all available processors
print("Write results")
write_ids_to_csv(ids, output)
