#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import gzip


class BlobToolsDiamondResult:

    @staticmethod
    def convert(result_path: str, output_path: str):
        """

        """
        out = open(output_path, 'wt')

        with gzip.open(result_path, 'rt') as f:
            for line in f:
                v = line.rstrip().split("\t")
                name = v[0][:v[0].rfind("_")]
                score = float(v[1])
                tax_temp = v[3][v[3].find("OX=")+3:]
                tax_id = int(tax_temp[:tax_temp.find(" ")])
                out.write("\t".join([name, str(tax_id), str(score)]) + "\r\n")
        out.close()


def main():
    """
    Manages the arguments and initiates the TaxonomyDatabase class.
    """
    parser = argparse.ArgumentParser(prog="BlobToolsDiamondResult",
                                     description="Converts Diamond results against SwissProt database to"
                                                 "BlobTools compliant hints files. \r\n "
                                                 "Copyright © 2021 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", type=str)
    parser.add_argument("output", type=str)
    args = parser.parse_args()
    blob = BlobToolsDiamondResult()
    blob.convert(args.input, args.output)


if __name__ == '__main__':
    main()
