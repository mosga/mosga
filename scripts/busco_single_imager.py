#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import re
import pandas as pd
import altair as alt


class BuscoImager:

    def create_image(self, input_path: str, output_path: str, title: str = ""):
        """
        Generates the pie chart image from a truncated BUSCO summary file.
        """
        res = self.parse_results(input_path)
        data = pd.DataFrame(columns=["Value", "Percentage", "Title", "Category", "Order"])
        data = data.append({"Value": res["s"], "Percentage": "%.2f" % (res["s"]/res["n"]*100),
                            "Title": "%d Complete (single-copy)" % res["s"], "Category": "Complete (single-copy)",
                            "Order": 3}, ignore_index=True)
        data = data.append({"Value": res["d"], "Percentage": "%.2f" % (res["d"]/res["n"]*100),
                            "Title": "%d Complete (duplicated)" % res["d"], "Category": "Complete (duplicated)",
                            "Order": 2}, ignore_index=True)
        data = data.append({"Value": res["f"], "Percentage": "%.2f" % (res["f"]/res["n"]*100),
                            "Title": "%d Fragmented" % res["f"], "Category": "Fragmented", "Order": 1},
                           ignore_index=True)
        data = data.append({"Value": res["m"], "Percentage": "%.2f" % (res["m"]/res["n"]*100),
                            "Title": "%d Missing" % res["m"], "Category": "Missing", "Order": 0}, ignore_index=True)
        t2 = data.loc[:, data.columns != "Title"]
        t1 = t2.loc[:, t2.columns != "Order"]
        if len(title):
            print("Source: %s" % title)
        print(t1)

        bar = alt.Chart(data).mark_bar().encode(
            x=alt.X("Value", title=title, scale=alt.Scale(domain=[0, res["n"]])),
            color=alt.Color("Title", sort=alt.EncodingSortField("Order", order="descending"),
                            title="BUSCO (n=" + str(res["n"]) + ")"),
            order=alt.Order("Order", sort="descending")
        )
        bar.save(output_path, webdriver='firefox')

    @staticmethod
    def parse_results(path: str) -> dict:
        """
        Parses the first line of BUSCO summary result.
        """
        r = []
        with open(path, "r") as tsv:
            out = [re.split(r'\t+', line.rstrip('\n')) for line in tsv.readlines()][1:]
            for o in out:
                try:
                    r.append(int(o[0]))
                except ValueError:
                    break

        res = {
            "c": r[0],
            "s": r[1],
            "d": r[2],
            "f": r[3],
            "m": r[4],
            "n": r[5]
        }
        return res


def main():
    """
    Manages the arguments and initiates the TaxonomyDatabase class.
    """
    parser = argparse.ArgumentParser(prog="BUSCO Single Imager",
                                     description="Creates BUSCO visualization. \r\n "
                                                 "Copyright © 2022 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", type=str, help="BUSCO MOSGA result file path")
    parser.add_argument("output", type=str, help="output svg file path")
    parser.add_argument("--title", type=str, default="")
    args = parser.parse_args()
    img = BuscoImager()
    img.create_image(args.input, args.output, args.title)


if __name__ == '__main__':
    main()
