#!/bin/bash

WORKDIR=$1
FASTA_FILE=$2
GLIMMERHMM=$3
SPECIE=$4
OUTPUT=$5

mkdir $WORKDIR && cd $WORKDIR

# Split multi sequence fasta files into separate files
awk 'BEGIN {O="";} /^>/ { O=sprintf("%s.fna",substr($0,2));} {if(O!="") print >> O;}' $FASTA_FILE


# Iterate over single new fasta file
for file in $WORKDIR*.fna
do
  filename=$(basename -- "$file")
  extension="${filename##*.}"
  filename="${filename%.*}"
  $3 $file -d $4 -g > $filename.gff
done

# Merge all GFF files
touch $5
gt gff3 -sort $WORKDIR/*.gff >> $5
rm -rf *.gff *.fna

exit 0;
