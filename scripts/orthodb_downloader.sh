#!/bin/bash

DB=$1
DIR=$2

URL1="https://v100.orthodb.org/download/odb10_"
URL2="_fasta.tar.gz"

if [ -z "$DB" ]; then
  echo "At least the database argument have to be passed";
fi

if [ -z "$DIR" ]; then
  DIR=/opt/mosga/data/
fi


DBpath=${DIR}orthodb_${DB}.faa
if test -f "$DBpath"; then
  echo $DBpath;
  exit 0;
else
  mkdir ${DIR}tmp
  wget -qO- ${URL1}${DB}${URL2} | tar xz -C ${DIR}tmp
  cat ${DIR}tmp/${DB}/Rawdata/* > ${DIR}tmp.all.faa
  # removes duplicated sequence ids
  awk '/^>/{f=!d[$1];d[$1]=1}f' ${DIR}tmp.all.faa > ${DIR}tmp.uniq_id.faa
  # removes duplicated sequences
  cat ${DIR}tmp.uniq_id.faa | paste - -  | cut -c2- | sort -t $'\t' -k2,2 | datamash  collapse 1 -g 2  | tr "," "_" | awk '{printf(">%s\n%s\n",$2,$1);}' > $DBpath
  rm -rf ${DIR}tmp ${DIR}tmp.*.* 2> /dev/null
  echo $DBpath;
  exit 0;
fi

exit 1;
