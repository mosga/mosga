#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import altair as alt
import argparse
from glob import glob
from argparse import ArgumentParser


class GenomeCompletenessComparator:

    def __init__(self, args: ArgumentParser):
        self.inputs = args.input
        self.output = args.output
        self.options = vars(args)


class GenomeCompletenessVisualizer:

    def __init__(self, args: ArgumentParser):
        alt.renderers.set_embed_options(scaleFactor=3)
        self.name_filters = ("fa", "faa", "fna", "fa", "fasta", "gbk", "gbff", "gbf", "nt", "nuc")
        self.names = self._clean_names(args.names)
        self.in_dirs = args.input
        self.out_dir = args.output
        self.names = [x for x in range(1, len(args.input) + 1)] if len(args.names) <= 0 else \
            self._clean_names(args.names)

    def run(self):
        pass

    def collect_data(self):
        pass

    def _clean_names(self, names: list) -> list:
        new_names: list = []
        for name in names:
            name_lower = name.lower()
            found_extension = False
            for extension in self.name_filters:
                extension_in_name = name_lower.rfind(extension)
                if extension_in_name >= 0:
                    found_extension = True
                    new_names.append(name[:extension_in_name-1])
                    break
            if not found_extension:
                new_names.append(name)
        return new_names


class BUSCOcomparer(GenomeCompletenessComparator):

    def __init__(self, args: ArgumentParser):
        super().__init__(args)
        if len(self.options["names"]) <= 0:     # check genome names
            self.options["names"] = [str(x) for x in range(1, len(self.inputs) + 1)]
        self.version = args.version
        self.TSV_FILE = "full_table.tsv"
        self.SC_FAA_PATH = "busco_sequences/single_copy_busco_sequences/"
        self.MC_FAA_PATH = "busco_sequences/multi_copy_busco_sequences/"

    def _evaluate_status(self):
        """
        Checks if the duplicates should be considered.

        :returns: returns the statuses to be taken into account
        """
        dups = []
        for path in self.inputs:
            if not os.path.isfile(path + self.TSV_FILE):
                print(f"ERROR: Specified file could not be found: {path + self.TSV_FILE}")
                exit(0)
            else:
                with open(path + self.TSV_FILE) as tsv:
                    n = 0
                    lines = tsv.read().splitlines()
                    for line in lines:
                        row = re.split(r'\t+', line)
                        if len(row) > 1 and row[1] == "Duplicated":
                            n += 1
                    dups.append(n)

        if (sum(dups) / len(dups)) > self.options["min_duplicates"]:
            return ["Complete", "Duplicated"]
        else:
            return ["Complete"]

    def _filter_tsv(self, path, name, status: list = None):
        """
        Opens the BUSCO full table file and filters the rows of the file according to the criteria.

        :param path: path to BUSCO output directory
        :param name: name of the genome
        :param status: the status which the gene should have
        :returns: returns all BUSCO ids as list that match the criteria
        """
        if status is None:
            status = ["Complete", "Duplicated"]

        score_column = 6 if self.version == 5 else 5

        if not os.path.isfile(path + self.TSV_FILE):
            print(f"ERROR: Specified file could not be found: {path + self.TSV_FILE}")
            exit(0)
        else:
            with open(path + self.TSV_FILE) as tsv:
                out = []
                lines = tsv.read().splitlines()
                for line in lines:
                    row = re.split(r'\t+', line)
                    if len(row) > 1 and row[0][:1] != "#":
                        if row[0] not in self.all_ids:
                            self.all_ids[row[0]] = [(name, row[1])]
                        else:
                            self.all_ids[row[0]].append((name, row[1]))
                    if len(row) > 1 and row[1] in status and float(row[score_column]) >= self.options["min_score"] \
                            and float(row[score_column+1]) >= self.options["min_length"]:
                        out.append(row[0])
                return set(out)

    def _compare_tsv(self):
        """
        Compares a list of BUSCO ids to see if any occur more than once, or if they occur in all files.
        """
        reg = {}
        out = []
        for x in self.ids:
            if x not in reg:
                reg[x] = 1
            else:
                if reg[x] == len(self.inputs) - 1:
                    out.append(x)
                reg[x] += 1
        self.uids = out

    def _join_faa(self, path):
        """
        Merges all single faa files to one genome using the BUSCO ids.

        :param path: path to BUSCO output directory
        :returns: returns the genome as string (amino acid sequence -> faa format)
        """
        out = ''
        for uid in self.uids:
            full_path = None
            if os.path.isfile(path + self.SC_FAA_PATH + uid + ".faa"):
                full_path = path + self.SC_FAA_PATH + uid + ".faa"
            elif os.path.isfile(path + self.MC_FAA_PATH + uid + ".faa"):
                full_path = path + self.MC_FAA_PATH + uid + ".faa"

            if full_path is not None:
                with open(full_path) as faa:
                    lines = faa.read().splitlines()[1:]
                    for line in lines:
                        if line[:1] == ">":
                            break
                        else:
                            out += line
            else:
                print("WARNING: FASTA file could not be found.")
        return out

    def _write_summary(self):
        """
        Writes a short summary about common genes.
        """
        complete = []
        missing = []
        for k, v in self.all_ids.items():
            m = [x[1] for x in v]
            if all(x == "Complete" or x == "Duplicated" for x in m):
                complete.append(k)
            elif all(x == "Missing" for x in m):
                missing.append(k)

        with open(self.output + "summary.csv", 'w') as f:
            f.write("# " + str(len(complete)) + " common complete BUSCO genes identified:\n")
            f.write("# " + ", ".join(complete) + "\n")
            f.write("# " + str(len(missing)) + " common missing BUSCO genes identified:\n")
            f.write("# " + ", ".join(missing) + "\n\n")

            # create a table about the individual statuses
            f.write("IDs\t" + "\t".join(self.options["names"]) + "\n")
            for k, v in self.all_ids.items():
                f.write(k + "\t")
                for i, name in enumerate(self.options["names"]):
                    status = [x[1] for x in v if x[0] == name]
                    f.write(status[0] + ("\t" if i < len(self.inputs) - 1 else "\n"))

    def _write_seq(self, n_sep=60):
        """
        Writes a file with the individual genomes.

        :param n_sep: number after how many characters a line break should happen
        """
        with open(self.output + "genes.fa", 'w') as f:
            for name, seq in self.genomes.items():
                f.write('>' + name + "\n")
                for i in range(0, len(seq), n_sep):
                    f.write(seq[i:i + n_sep] + "\n")

    def run(self):
        """
        Calls the single steps of the script one after the other.
        """
        if self.options["min_duplicates"] < 0:
            status = ["Complete"]
        elif self.options["min_duplicates"] == 0:
            status = ["Complete', 'Duplicated"]
        else:
            status = self._evaluate_status()

        self.all_ids = {}
        self.ids = []
        for i, path in enumerate(self.inputs):    # filter & collect BUSCO ids
            self.ids = [*self.ids, *self._filter_tsv(path, self.options["names"][i], status=status)]
        self._compare_tsv()    # compare BUSCO ids
        self.genomes = {}
        for i in range(0, len(self.inputs)):    # collect genomes
            self.genomes[self.options["names"][i]] = self._join_faa(self.inputs[i])
        self._write_seq()  # write file
        self._write_summary()   # summarize the commonalities


class EukCCcomparer(GenomeCompletenessComparator):
    pass


class BUSCOvisualizer(GenomeCompletenessVisualizer):

    def __init__(self, args: ArgumentParser):
        super().__init__(args)
        self.title = args.title
        self.version = args.version
        self.ids = []
        self.BAR_CHART_HTML = "bar_chart.html"
        self.BAR_CHART_SVG = "bar_chart.svg"
        self.STRIP_CHART_HTML = "strip_chart.html"
        self.STRIP_CHART_SVG = "strip_chart.svg"

    def run(self):
        """
        Calls the single steps of the script one after the other.
        """
        self._collect_data()
        self._read_summary_files()
        self._plot_bar_chart()  # plotting bar chart (category)
        self._plot_strip_chart()  # plotting strip chart (missings)

    def _collect_data(self):
        """
        Reads the specified BUSCO tsv files.
        """
        for path in self.in_dirs:
            if not os.path.isfile(path + "full_table.tsv"):
                print(f"ERROR: Specified file could not be found: {path}full_table.tsv")
                exit(0)
            else:
                with open(path + "full_table.tsv") as tsv:
                    out = [re.split(r'\t+', line.rstrip('\n')) for line in tsv.readlines()][3:]
                    self.ids.append({x[0]: x[1:] for x in out})

    def _read_summary_files(self):
        """
        Reads the specified BUSCO summary files.
        """
        self.frequency = []
        line_parsing_start = 9 if self.version == 5 else 8
        line_parsing_end = 15 if self.version == 5 else 14

        for i, path in enumerate(self.in_dirs):
            try:
                file = glob(path + "../short_summary*.txt")[0]  # supports only one summary
            except IndexError:
                print(f"ERROR: Specified file could not be found: {path}../short_summary*.txt")
                exit(0)
            else:
                with open(file) as summary:
                    out = [re.split(r'\s+', line.rstrip('\n')) for line in summary.readlines()][line_parsing_start:line_parsing_end]
                    self.frequency.append([x[1] for x in out])

    def _plot_bar_chart(self):
        """
        Creates & save a bar chart from the categories of the individual BUSCO runs.
        """
        data = []
        for i, entry in enumerate(self.frequency):  # get the distribution for each tsv file
            labels = ["Complete (single-copy)", "Complete (duplicated)", "Fragmented", "Missing"]
            reg = {}
            for j in range(len(labels)):
                reg[labels[j]] = entry[1:][j]

            portion = 0
            for k, v in reg.items():
                percentage = float(v) * 100.0 / float(entry[5])
                data.append({
                    "name": self.names[i],
                    "type": k,
                    "amount": v,
                    "percentage": percentage,
                    "portion_start": portion,
                    "portion_end": portion + int(v)
                })
                portion += int(v)
        source = alt.pd.DataFrame(data)

        fig = alt.Chart(source).mark_bar().encode(
            x=alt.X('portion_start:Q', axis=alt.Axis(title="Number of BUSCOs")),
            x2=alt.X2('portion_end:Q'),
            y=alt.Y('name:N', axis=alt.Axis(
                title="",
                offset=5,
                ticks=False,
                minExtent=60,
                domain=False
            )),
            color=alt.Color('type:N', legend=alt.Legend(title='BUSCOs category'), scale=alt.Scale(
                domain=["Complete (single-copy)", "Complete (duplicated)", "Fragmented", "Missing"]
            ))
        ).properties(width=512, height=30 * len(self.in_dirs))

        fig.save(self.out_dir + self.BAR_CHART_HTML, embed_options={"renderer": "svg"}, webdriver='firefox')
        fig.save(self.out_dir + self.BAR_CHART_SVG, webdriver='firefox')

    def _plot_strip_chart(self):
        """
        Creates & save a strip chart from the BUSCOs missing in at least one strain.
        """
        missing_categories: list = ["All genomes", "Multiple genomes", "A single genome"]
        data = []
        for i in range(0, len(self.ids)):
            for p, (k, v) in enumerate(self.ids[i].items()):
                if v[0] == "Missing":
                    t = missing_categories[2]  # check the frequency of missing
                    c = 0
                    for j in range(len(self.ids)):
                        if self.ids[j][k][0] == "Missing":
                            c += 1
                    if c == len(self.ids):
                        t = missing_categories[0]
                    elif c > 1:
                        t = missing_categories[1]

                    data.append({
                        "name": self.names[i],
                        "missing": t,
                        "id": k,
                        "position": p,
                    })
        source = alt.pd.DataFrame(data)
        datasource_title = " (" + self.title + ")" if len(self.title) else ""
        fig = alt.Chart(source).mark_tick(size=25, opacity=1.0).encode(
            x=alt.X('position:Q', axis=alt.Axis(
                title="BUSCOs" + datasource_title,
                grid=False
            )),
            y=alt.Y('name:O', axis=alt.Axis(
                title="",
                offset=5,
                ticks=False,
                minExtent=60,
                domain=False,
                grid=False
            )),
            color=alt.Color('missing:N', legend=alt.Legend(title='Missing BUSCOs in ', symbolType='square'),
                            scale=alt.Scale(domain=missing_categories))
        ).properties(width=512, height=30 * len(self.in_dirs))

        fig.save(self.out_dir + self.STRIP_CHART_HTML, embed_options={"renderer": "svg"}, webdriver='firefox')
        fig.save(self.out_dir + self.STRIP_CHART_SVG, webdriver='firefox')


class EukCCvisualizer(GenomeCompletenessVisualizer):

    def __init__(self, args: ArgumentParser):
        super().__init__(args)
        self.tsvs = {}
        self.DOT_CHART_HTML = "scatter_plot.html"
        self.DOT_CHART_SVG = "scatter_plot.svg"

    def run(self):
        """
        Calls the single steps of the script one after the other.
        """
        self._collect_data()
        self._create_scatter_plot()    # create scatter plot (completeness)

    def _collect_data(self):
        """
        Reads the specified EukCC tsv files.
        """
        for i, path in enumerate(self.in_dirs):
            if not os.path.isfile(path + "eukcc.tsv"):
                print(f"ERROR: Specified file could not be found: {path}eukcc.tsv")
                exit(0)
            else:
                with open(path + "eukcc.tsv") as tsv:
                    out = [re.split(r'\t+', line.rstrip('\n')) for line in tsv.readlines()][1:]
                    self.tsvs[self.names[i]] = out

    def _create_scatter_plot(self):
        """
        Creates & save a scatter plot from the completeness and contamination of the individual EukCC runs.
        """
        data = []
        for k, v in self.tsvs.items():
            entry = v[0]    # consider only the first entry of the tsv
            data.append({
                "name": k,
                "type": "Completeness",
                "value": entry[0]
            })
            data.append({
                "name": k,
                "type": "Contamination",
                "value": entry[1]
            })
            data.append({
                "name": k,
                "type": "Max silent contamination",
                "value": entry[2]
            })
        source = alt.pd.DataFrame(data)

        fig = alt.Chart(source).mark_circle(size=60).encode(
            x=alt.X('value:Q', axis=alt.Axis(title="%"), scale=alt.Scale(domain=[0, 100])),
            y=alt.Y('name:N', axis=alt.Axis(title="")),
            color=alt.Color('type:N', legend=alt.Legend(title='EukCC'), scale=alt.Scale(
                domain=["Completeness", "Contamination", "Max silent contamination"]
            ))
        ).properties(width=512, height=30*len(self.in_dirs))

        fig.save(self.out_dir + self.DOT_CHART_HTML, embed_options={"renderer": "svg"}, webdriver='firefox')
        fig.save(self.out_dir + self.DOT_CHART_SVG, webdriver='firefox')


def main():
    parser = argparse.ArgumentParser(
        prog="MOSGA Genome Completenesses Script\r\nCopyright 2022. Roman Martin & Hagen Dreßler",
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False)

    parser.add_argument("action", type=str, choices=("compare", "visualize"))
    parser.add_argument("source", type=str, choices=("busco", "eukcc"))
    tmp_arguments = parser.parse_known_args()[0]

    classes: dict = {
        "compare": {
            "busco": BUSCOcomparer,
            "eukcc": EukCCcomparer
        },
        "visualize": {
            "busco": BUSCOvisualizer,
            "eukcc": EukCCvisualizer
        }
    }

    if tmp_arguments.action == "visualize":
        # All visualizer
        parser.add_argument("-i", "--input", help="Paths to source directories.", nargs='+', metavar="<PATHS>",
                            required=True)
        parser.add_argument("-o", "--output", help="Path to output directory.", metavar="<PATH>", type=str,
                            required=True)
        parser.add_argument("-n", "--names", help="Names of the individual genomes.", nargs='+', metavar="<LIST>",
                            default=[], required=False)

        # BUSCO specific
        if tmp_arguments.source == "busco":
            parser.add_argument("--title", help="data source and x-Axis graph title", type=str, default="")
            parser.add_argument("-v", "--version", help="the BUSCO version", type=int, default=5)

    elif tmp_arguments.action == "compare":
        parser.add_argument("-i", "--input", nargs='+', metavar="paths", help="paths to BUSCO directories")
        parser.add_argument("-o", "--output", type=str, metavar="path", help="path to output directory")
        parser.add_argument("-n", "--names", nargs='+', metavar="list", default=[], required=False,
                            help="names of the individual genomes (optional)")

        # BUSCO specific
        if tmp_arguments.source == "busco":
            parser.add_argument("-ms", "--min_score", type=float, metavar="value", default=0.0, required=False,
                                help="the minimum score which the gene should have (optional)")
            parser.add_argument("-ml", "--min_length", type=float, metavar="value", default=0.0, required=False,
                                help="the minimum length which the gene should have (optional)")
            parser.add_argument("-md", "--min_duplicates", type=int, metavar="value", default=-1, required=False,
                                help="the minimum number of duplicates to be taken into account (-1 = duplicates are ignored)")
            parser.add_argument("-v", "--version", help="the BUSCO version", type=int, default=5)

    args = parser.parse_args()
    target_class = classes[tmp_arguments.action][tmp_arguments.source]
    open_target_class = target_class(args)
    open_target_class.run()


if __name__ == '__main__':
    main()
