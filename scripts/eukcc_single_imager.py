#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import argparse
import altair as alt
import pandas as pd
from argparse import ArgumentParser


class EukCCSingleVisual:

    def __init__(self, input_path: str, output_path: str):
        self.input = input_path
        self.output = output_path
        alt.renderers.set_embed_options(scaleFactor=3)
        self._run()

    @staticmethod
    def _parse_tsv(path: str) -> list:
        fp = open(path, "r")
        lines = fp.readlines()
        fp.close()

        if len(lines) > 1:
            lines = [line.rstrip().split("\t") for line in lines[1:]]

        return lines

    def _create_bar_chart(self, result: list):
        # Take the first hit
        r = result[0]

        df = pd.DataFrame(columns=["Percentage", "CategoryTitle", "Category", "Order"])
        df = df.append({"Percentage": float(r[0]), "CategoryTitle": "%.2f %% Completeness" % float(r[0]),
                        "Category": "Completeness", "Order": 3},
                       ignore_index=True)
        df = df.append({"Percentage": float(r[1]), "CategoryTitle": "%.2f %% Contamination" % float(r[1]),
                        "Category": "Contamination", "Order": 2},
                       ignore_index=True)
        df = df.append({"Percentage": float(r[2]), "CategoryTitle": "%.2f %% Max silent Contamination" % float(r[2]),
                        "Category": "Max silent Contamination", "Order": 1},
                       ignore_index=True)

        print("Source: %s" % r[9])
        t1 = df.loc[:, df.columns != "Order"]
        print(t1.loc[:, t1.columns != "CategoryTitle"])

        data: list = []
        data.append({
            "name": "",
            "type": "Completeness",
            "value": float(r[0])
        })
        data.append({
            "name": "",
            "type": "Contamination",
            "value": float(r[1])
        })
        data.append({
            "name": "",
            "type": "Max silent contamination",
            "value": float(r[2])
        })
        source = alt.pd.DataFrame(data)

        fig = alt.Chart(source).mark_circle(size=60).encode(
            x=alt.X('value:Q', axis=alt.Axis(title="%"), scale=alt.Scale(domain=[0, 100])),
            y=alt.Y('name', axis=None),
            color=alt.Color('type:N', legend=alt.Legend(title='EukCC'), scale=alt.Scale(
                domain=["Completeness", "Contamination", "Max silent contamination"]
            ))
        ).properties(width=512, height=30)

        fig.save(self.output, webdriver='firefox')

    def _run(self):
        result = self._parse_tsv(self.input)
        self._create_bar_chart(result)


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="EukCC Visual",
                                     description="Visualizes the results of EukCC of the comparative pipeline.\r\n " \
                                                 "Copyright © 2021 Roman Martin, codebase by Hagen Dreßler",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", help="Paths to EukCC file.", metavar="<PATHS>", type=str, required=True)
    parser.add_argument("-o", "--output", help="Path to output file.", metavar="<PATH>", type=str, required=True)
    args = parser.parse_args()
    EukCCSingleVisual(args.input, args.output)


if __name__ == '__main__':
    main()
