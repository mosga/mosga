#!/bin/bash
mkdir /opt/mosga/tools/silva/
wget -qO- https://www.arb-silva.de/fileadmin/silva_databases/release_132/Exports/SILVA_132_LSURef_tax_silva.fasta.gz | gunzip -c > /opt/mosga/tools/silva/lsu.fasta
wget -qO- https://www.arb-silva.de/fileadmin/silva_databases/release_132/Exports/SILVA_132_SSURef_tax_silva.fasta.gz | gunzip -c > /opt/mosga/tools/silva/ssu.fasta

cd /opt/mosga/tools/silva
makeblastdb -in lsu.fasta -parse_seqids -dbtype nucl -title "SILVA LSU" -out lsu
makeblastdb -in ssu.fasta -parse_seqids -dbtype nucl -title "SILVA SSU" -out ssu
rm /opt/mosga/tools/silva/lsu.fasta /opt/mosga/tools/silva/ssu.fasta
