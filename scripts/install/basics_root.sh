#!/bin/bash
# Update the machine
apt-get update && apt-get upgrade -y -q

## Install software
# Update R v4.0.5+
apt-get install -y -q wget lsb-release software-properties-common apt-utils gnupg curl
wget -qO- https://cloud.r-project.org/bin/linux/ubuntu/marutter_pubkey.asc | tee -a /etc/apt/trusted.gpg.d/cran_ubuntu_key.asc
add-apt-repository -y "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
apt-get install -y -q --no-install-recommends  dirmngr
apt-get install -y -q r-base r-base-core r-recommended
apt-get update && apt-get upgrade -y

# Prepare NodeJS
curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
apt-get install -y nodejs

# Basics
apt-get -y -q install build-essential wget \
    git autoconf g++ gzip make cmake cron libidn-dev libidn12 libidn11-dev \
    libboost-iostreams-dev libboost-system-dev libboost-filesystem-dev \
    gcc-multilib zlib1g-dev python3 python3-pip tcsh  perl cpanminus \
    snakemake graphviz libcurl3-dev jq sendmail libzip-dev libsqlite3-dev \
    apache2 libapache2-mod-php php php-sqlite3 php-zip php-mysql sqlite3 \
    ncbi-blast+ unzip time python-setuptools libgsl-dev libboost-all-dev \
    libsuitesparse-dev liblpsolve55-dev libbamtools-dev libbz2-dev liblzma-dev \
    libncurses5-dev libmysql++-dev cdbfasta diamond-aligner libfile-which-perl \
    libhdf5-dev libyaml-perl libdbd-mysql-perl libfontconfig1-dev prodigal \
    libparallel-forkmanager-perl libxml2-dev libexpat-dev postgresql-client \
    libpq-dev nano locales hisat2 bowtie2 bedtools emboss aegean genometools \
    composer datamash python3 python3-pip libcairo2-dev muscle  mafft clustalw \
    clamav bzip2 pbzip2 tabix ncbi-blast+ libjpeg-dev libpango-1.0-0 \
    libpango1.0-dev sudo default-jre libgif-dev unrar

apt-get install -y --no-install-recommends python3-biopython

# Download Python3 dependencies
pip install biopython argparse PrettyTable gprofiler-official numpy pandas matplotlib seaborn
pip install pandas h5py stripe requests
pip install docopt matplotlib tqdm pysam anybadge coverage
pip install "pyyaml>=4.2b1" # blobtools
pip install sklearn
pip install --upgrade pip
pip install awscli

# Perl dependencies
cpanm --force File::Spec::Functions Hash::Merge List::Util Logger::Simple \
      Module::Load::Conditional Parallel::ForkManager POSIX Scalar::Util::Numeric \
    	Heap::Simple Heap::Simple::XS List::MoreUtils Exception::Class Test::Warn Bio::Perl \
    	Bio::DB::SeqFeature::Store File::Next Bio::DB::Das::Chado File::HomeDir Math::Utils \
      MCE::Mutex \
      Math::Random # seq-scripts
cpanm --force File::HomeDir # BRAKER/Augustus
cpanm --force inc::Module::Install::DSL FindBin Test::Pod YAML # EukCC
cpanm --force Text::Soundex # RepeatMasker

locale-gen en_US.UTF-8

# Configure PHP
# Update the PHP.ini file, enable <? ?> tags and quieten logging.
sed -i "s/post_max_size = 8M/post_max_size = 24M/" /etc/php/8.1/apache2/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 24M/" /etc/php/8.1/apache2/php.ini

# Setup sendmail
RUN echo "sendmail_path=/usr/sbin/sendmail -t -i" >> /etc/php/8.1/cli/conf.d/sendmail.ini

# Enable apache mods.
a2enmod php8.1
a2enmod rewrite
a2enmod headers
a2enmod deflate
a2enmod setenvif
a2enmod rewrite

## JBROWSE ##
# Install NodeJS Yarn
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install -y yarn
npm install -g @jbrowse/cli@2.0.1

# Git issues with ownership
git config --system --add safe.directory '/opt/mosga'

# Altair & Altair Saver
npm install --unsafe-perm -g vega-lite@5.1.0 vega-cli@5.20.2 npm@6.14.18 vega@5.20.2
pip install altair altair_saver
pip install selenium==4.2.0 --force-reinstall
