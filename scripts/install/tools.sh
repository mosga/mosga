#!/bin/bash

# HMMer 3
cd /opt/mosga/tools/hmmer/ && make install

# RepeatMasker
cd /usr/local/bin
wget -q https://raw.githubusercontent.com/guyleonard/gene_prediction_pipeline/master/external_programs/trf409.linux64 &&  mv trf*.linux64 trf && chmod +x trf
cd /usr/local
wget -qO- http://repeatmasker.org/RepeatMasker-4.1.1.tar.gz | tar -xz -C . && rm -f RepeatMasker*.tar.gz
cd /usr/local/RepeatMasker
perl ./configure -trf_prgm=/usr/local/bin/trf -hmmer_dir=/usr/local/bin/

# tRNAScan-SE 2.0
cd /opt/mosga/tools/tRNAscan && make install

# Infernal
cd /opt/mosga/tools/infernal && make install

# perl5lib-fasta
ln -s /opt/mosga/tools/perl5lib-Fasta/lib/Fasta/ /usr/share/perl5/Fasta

# BAMtools
cd /opt/mosga/tools/bamtools/build && make install
cd /usr/lib/ && ln -f -s /usr/local/lib/bamtools/libbamtools.* .

# Spaln
cd /opt/mosga/tools/spaln/src/ && make install 2>/dev/null

# HTSLIB
cd /opt/mosga/tools/htslib/ && make install

# BCFTools
cd /opt/mosga/tools/bcftools && make install

# SAMTools
cd /opt/mosga/tools/samtools && make install

# Augustus
cd /opt/mosga/tools/augustus/ && sudo -u mosga make && make install

# ClipKIT
cd /opt/mosga/tools/ClipKIT && python3 setup.py install

# FastANI
cd /opt/mosga/tools/FastANI && make install

# FastME
cd /opt/mosga/tools/FastME && make install

# BUSCO
cd /opt/mosga/tools/BUSCO && python3 setup.py install

# EukCC
if [ -d "/opt/mosga/tools/EukCC/" ]; then
  cd /opt/mosga/tools/EukCC && python3 setup.py install
fi
cp -r /opt/mosga/tools/pplacer/* /usr/local/bin/

# BlobTools
cd /opt/mosga/tools/blobtools && python setup.py install

# R packages
Rscript /opt/mosga/scripts/install/Rpackages.R

# ssu-align
cd /opt/mosga/tools/ssu-align && make install

# tbl2asn fix
ln -s /usr/lib/x86_64-linux-gnu/libidn.so /usr/lib/x86_64-linux-gnu/libidn.so.11

# Update clamav database
freshclam
