#!/bin/bash
cd /opt/mosga/tools/diamond
./diamond makedb --in plastid80.faa -d plastid
./diamond makedb --in mito80.faa -d mito
rm /opt/mosga/tools/diamond/plastid80.faa /opt/mosga/tools/diamond/mito80.faa
