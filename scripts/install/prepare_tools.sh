#!/bin/bash

# Miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /opt/mosga/tools/miniconda.sh
bash /opt/mosga/tools/miniconda.sh -p /opt/mosga/tools/miniconda -b && rm /opt/mosga/tools/miniconda.sh

# HMMer 3
cd /opt/mosga/tools/ && wget -qO- http://eddylab.org/software/hmmer/hmmer-3.3.2.tar.gz | tar xz -C .
mv /opt/mosga/tools/hmmer-* /opt/mosga/tools/hmmer
cd /opt/mosga/tools/hmmer
./configure
make

# tRNAScan-SE 2.0
git clone -b v2.0.8 https://github.com/UCSC-LoweLab/tRNAscan-SE.git --depth 1 /opt/mosga/tools/tRNAscan
cd /opt/mosga/tools/tRNAscan
./configure
autoreconf -vfi
make

# Infernal
wget -qO- http://eddylab.org/infernal/infernal-1.1.4.tar.gz | tar xz -C /opt/mosga/tools
mv /opt/mosga/tools/infernal-* /opt/mosga/tools/infernal
cd /opt/mosga/tools/infernal
./configure
make
make check

# GlimmerHMM
wget -qO- http://ccb.jhu.edu/software/glimmerhmm/dl/GlimmerHMM-3.0.4.tar.gz | tar xz -C /opt/mosga/tools/

# EggNogg
cd /opt/mosga/tools/ && git clone -b 2.1.9 https://github.com/eggnogdb/eggnog-mapper.git --depth 1

# GlimmerHMM
wget -qO- http://ccb.jhu.edu/software/glimmerhmm/dl/GlimmerHMM-3.0.4.tar.gz | tar xz -C /opt/mosga/tools/

# SNAP
cd /opt/mosga/tools/
git clone https://github.com/KorfLab/SNAP.git --depth 1
cd /opt/mosga/tools/SNAP
make

# Diamond
mkdir /opt/mosga/tools/diamond
wget -qO- https://github.com/bbuchfink/diamond/releases/download/v2.0.15/diamond-linux64.tar.gz | tar xz -C /opt/mosga/tools/diamond

# BAMTools
git clone https://github.com/pezmaster31/bamtools.git /opt/mosga/tools/bamtools/ && cd /opt/mosga/tools/bamtools/ && git checkout 2391b1a1275816ad89c624586fa02b1a621924f5
cd /opt/mosga/tools/bamtools/
mkdir build
cd /opt/mosga/tools/bamtools/build
cmake ..
make

# HTSLIB
git clone -b 1.15.1 https://github.com/samtools/htslib.git --depth 1 /opt/mosga/tools/htslib
cd /opt/mosga/tools/htslib/
git submodule update --init --recursive
autoheader
autoconf
./configure
make

# BCFTools
git clone -b 1.15.1 --depth 1 https://github.com/samtools/bcftools.git /opt/mosga/tools/bcftools
cd /opt/mosga/tools/bcftools
autoheader
autoconf
./configure
make

# SAMTools
git clone -b 1.15.1 --depth 1 https://github.com/samtools/samtools.git /opt/mosga/tools/samtools
cd /opt/mosga/tools/samtools
autoheader
autoconf -Wno-syntax
./configure
make

# Augustus
git clone https://github.com/Gaius-Augustus/Augustus.git /opt/mosga/tools/augustus
cd /opt/mosga/tools/augustus/
git checkout b69e6bccfd46b4c7452407aafb2d6a6077e60ab8
chmod a+w -R /opt/mosga/tools/augustus/config

# BRAKER
cd /opt/mosga/tools
git clone https://github.com/Gaius-Augustus/BRAKER/ /opt/mosga/tools/BRAKER
cd /opt/mosga/tools/BRAKER
git checkout 34ed06111b5da296762c9ac0862ca912f71777d3
ln -s /opt/mosga/tools/augustus/scripts/gtf2gff.pl /opt/mosga/tools/BRAKER/scripts/gtf2gff.pl
ln -s /opt/mosga/tools/BRAKER/scripts/findGenesInIntrons.pl /opt/mosga/tools/augustus/scripts/findGenesInIntrons.pl
ln -s /opt/mosga/tools/BRAKER/scripts/filterIntronsFindStrand.pl /opt/mosga/tools/augustus/scripts/filterIntronsFindStrand.pl
ln -s /opt/mosga/tools/BRAKER/scripts/filterGenemark.pl /opt/mosga/tools/augustus/scripts/filterGenemark.pl
chmod a+w -R /opt/mosga/tools/BRAKER

# Red
wget -qO- http://toolsmith.ens.utulsa.edu/red/data/DataSet2Unix64.tar.gz | tar xz -C /opt/mosga/tools/; mv /opt/mosga/tools/redUnix64/ /opt/mosga/tools/Red

# Spaln
git clone https://github.com/ogotoh/spaln.git -b ver2.4.12 --depth 1 /opt/mosga/tools/spaln
cd /opt/mosga/tools/spaln/src/ && ./configure && make

# tbl2asn
wget -qO- https://ftp.ncbi.nih.gov/toolbox/ncbi_tools/converters/versions/2022-06-14/by_program/tbl2asn/linux64.tbl2asn.gz | gunzip > /opt/mosga/tools/tbl2asn
chmod u+x /opt/mosga/tools/tbl2asn

# barrnap
git clone --depth 1 -b 0.9 https://github.com/tseemann/barrnap.git /opt/mosga/tools/barrnap

# GenomeThreader
wget -qO- http://genomethreader.org/distributions/gth-1.7.3-Linux_x86_64-64bit.tar.gz | tar xz -C /opt/mosga/tools/
mv /opt/mosga/tools/gth-* /opt/mosga/tools/gth

# Clone CDBtools
git clone https://github.com/gpertea/cdbfasta.git --depth 1 /opt/mosga/tools/cdbfasta
cd /opt/mosga/tools/cdbfasta
make

# perl5lib-Fasta
cd /opt/mosga/tools/ && git clone --depth 1 https://github.com/BioInf-Wuerzburg/perl5lib-Fasta.git

# seq-scripts
cd /opt/mosga/tools/ && git clone --depth 1 https://github.com/thackl/seq-scripts

# ProtHint
git clone --depth 1 -b v2.6.0 https://github.com/gatech-genemark/ProtHint.git /opt/mosga/tools/ProtHint

# BUSCO
git clone --depth 1 -b 5.4.1 https://gitlab.com/ezlab/busco.git /opt/mosga/tools/BUSCO

# metaeuk
cd /opt/mosga/tools && wget https://mmseqs.com/metaeuk/metaeuk-linux-sse41.tar.gz && tar xzvf metaeuk-linux-sse41.tar.gz

# bbmap
wget -qO- "https://altushost-swe.dl.sourceforge.net/project/bbmap/BBMap_38.96.tar.gz" | tar xz -C /opt/mosga/tools/

# sepp
git clone --depth 1 -b 4.5.1 https://github.com/smirarab/sepp.git /opt/mosga/tools/sepp

# EukCC
git clone --depth 1 -b v.2.1.1 https://github.com/Finn-Lab/EukCC.git /opt/mosga/tools/EukCC
wget -qO- "http://ftp.ebi.ac.uk/pub/databases/metagenomics/eukcc/eukcc_db_v1.1.tar.gz" | tar xz -C /opt/mosga/tools/EukCC/
cd /opt/mosga/tools/EukCC && mv eukcc_db_* eukccdb

# pplacer
cd /opt/mosga/tools/
wget -q "https://github.com/matsen/pplacer/releases/download/v1.1.alpha19/pplacer-linux-v1.1.alpha19.zip" -O pplacer.zip
unzip pplacer.zip && rm -f pplacer.zip && mv pplacer-Linux-v1.1.alpha19 pplacer

# ClipKIT
git clone --depth 1 https://github.com/JLSteenwyk/ClipKIT.git /opt/mosga/tools/ClipKIT

# trimAl
git clone --depth 1 -b v1.4.1 https://github.com/scapella/trimal.git /opt/mosga/tools/trimAl
cd /opt/mosga/tools/trimAl/source/ && make

# FastANI
git clone --depth 1 -b v1.33 https://github.com/ParBLiSS/FastANI.git /opt/mosga/tools/FastANI
cd /opt/mosga/tools/FastANI && ./bootstrap.sh && ./configure && make

# RAxML
git clone --depth 1 -b v8.2.12 https://github.com/stamatak/standard-RAxML.git /opt/mosga/tools/RAxML
cd  /opt/mosga/tools/RAxML && make -f Makefile.AVX.PTHREADS.gcc && rm -f *.o

# FastME
git clone --depth 1 https://gite.lirmm.fr/atgc/FastME.git /opt/mosga/tools/FastME
cd /opt/mosga/tools/FastME && autoreconf -i && ./configure && make

# Vecscreen DB
mkdir -p /opt/mosga/tools/vecscreen && wget "https://ftp.ncbi.nlm.nih.gov/pub/UniVec/UniVec" -P /opt/mosga/tools/vecscreen
cd /opt/mosga/tools/vecscreen && makeblastdb -in UniVec -input_type fasta -dbtype nucl -out UniVec && rm UniVec

# NCBI taxonomy DB
mkdir /opt/mosga/data/tax/ && wget -qO- https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz | tar xz -C /opt/mosga/data/tax/
# Removes unrelated devisions
awk -F "|" '{if ($5!=0 && $5!=3 && $5!=7 && $5!=8 && $5!=9 && $5!=11) print $1,$2,$5,$3}' /opt/mosga/data/tax/nodes.dmp > /opt/mosga/data/tax/nodes_shorted.dmp
# Keep only scientific names
grep 'scientific name' /opt/mosga/data/tax/names.dmp | awk -F "|" '{print $1";"$2}' > /opt/mosga/data/tax/names_shorted.dmp
python3 /opt/mosga/scripts/taxonomy_tree.py -i /opt/mosga/data/tax/nodes_shorted.dmp -o /opt/mosga/data/tax/taxonomy.json
python3 /opt/mosga/scripts/taxonomy_database_creation.py /opt/mosga/data/tax/names_shorted.dmp /opt/mosga/gui/taxonomy.db --nodes /opt/mosga/data/tax/nodes_shorted.dmp
grep -h '"tax":' /opt/mosga/gui/gui-rules.json /opt/mosga/snakemake/rules/tax-search.smk | awk '{print $2}' | sort -u > /opt/mosga/data/tax/tax-tools.txt
python3 /opt/mosga/scripts/taxonomy_tree.py -m path -i /opt/mosga/data/tax/taxonomy.json -t /opt/mosga/data/tax/tax-tools.txt -o /opt/mosga/data/tax/tax-tools.json

# blobtools
git clone -b blobtools_v1.1.1 --depth 1 https://github.com/DRL/blobtools.git /opt/mosga/tools/blobtools
/opt/mosga/tools/blobtools/blobtools nodesdb --nodes /opt/mosga/data/tax/nodes.dmp --names /opt/mosga/data/tax/names.dmp

# Spliceator
git clone --depth 1 https://git.unistra.fr/nscalzitti/spliceator.git /opt/mosga/tools/spliceator && rm -rf /opt/mosga/tools/spliceator/Data

# CpGIScan
cd /opt/mosga/tools/
wget -q https://github.com/jzuoyi/cpgiscan/raw/master/Build/cpgiscan-1.0-linux-x86_64.zip -O /opt/mosga/tools/cpgiscan.zip
unzip /opt/mosga/tools/cpgiscan.zip && rm -f /opt/mosga/tools/cpgiscan.zip

# ssu-align
wget -qO- http://eddylab.org/software/ssu-align/ssu-align-0.1.1.tar.gz | tar xz -C /opt/mosga/tools/
mv /opt/mosga/tools/ssu-align-* /opt/mosga/tools/ssu-align
cd /opt/mosga/tools/ssu-align && ./configure && make
wget -q https://raw.githubusercontent.com/EddyRivasLab/ssu-align/develop/trunk/src/ssu.pm -O /opt/mosga/tools/ssu-align/src/ssu.pm

# Deori6.0 # 10.1093/bioinformatics/bts151
wget -q https://tubic.org/deori/deori6.0.rar -O ./deorci6.0.rar
mkdir -p /opt/mosga/tools/deori/ 2> /dev/null
unrar x ./deorci6.0.rar /opt/mosga/tools/deori && rm ./deorci6.0.rar
cd /opt/mosga/tools/deori && makeblastdb -in deori6.0.fasta -input_type fasta -dbtype nucl -out DeOri && rm deori6.0.fasta

# JBrowse 2
/usr/bin/jbrowse create /opt/mosga/gui/jbrowse2 -f --url https://github.com/GMOD/jbrowse-components/releases/download/v2.0.1/jbrowse-web-v2.0.1.zip
mkdir /opt/mosga/gui/jbrowse2/mosga
#rm -rf test_data/
cp /opt/mosga/scripts/jbrowse_htaccess.conf /opt/mosga/gui/jbrowse2/.htaccess

# Setup cron job
(crontab -l ; echo "* * * * * /opt/mosga/scripts/kill_prothint_child.sh") | crontab
