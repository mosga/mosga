#!/bin/bash
wget -qO- ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz | gunzip -c > /opt/mosga/tools/diamond/uniprot_sprot.fasta
cd /opt/mosga/tools/diamond/
./diamond makedb --in uniprot_sprot.fasta -d swissprot
rm uniprot_sprot.fasta
