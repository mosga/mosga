#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re
import glob
import argparse
from argparse import ArgumentParser

TSV_FILE = "eukcc.tsv"  # is not used
FAA_PATH = "workfiles/pplacer/tmp/"


class EukCCComparer:
    def __init__(self, inputs: list, output: str, options: dict):
        self.inputs = inputs
        self.output = output
        self.options = options
        if len(self.options["names"]) <= 0:     # check genome names
            self.options["names"] = [str(x) for x in range(1, len(self.inputs) + 1)]
        self._run()

    def _filter_ids(self, path, name):
        """
        Opens the EukCC output directory and filters the gene files.

        :param path: path to EukCC output directory
        :param name: name of the genome
        :returns: returns all EukCC ids as list that occur only once
        """
        reg = {}
        for file in glob.glob(path + FAA_PATH + "*.faa"):
            n = re.split(r'_+', os.path.basename(file))[0]
            if n not in reg:
                reg[n] = 1
            else:
                reg[n] += 1
        out = [k for k, v in reg.items() if v == 1]     # filter duplicates
        for x in out:
            if x not in self.all_ids:
                self.all_ids[x] = [name]
            else:
                self.all_ids[x].append(name)
        return out

    def _compare_ids(self):
        """
        Compares a list of EukCC ids to see if any occur more than once, or if they occur in all runs.
        """
        reg = {}
        out = []
        for x in self.ids:
            if x not in reg:
                reg[x] = 1
            else:
                if reg[x] == len(self.inputs) - 1:
                    out.append(x)
                reg[x] += 1
        self.uids = out

    def _join_faa(self, path):
        """
        Merges all single faa files to one genome using the EukCC ids.

        :param path: path to EukCC output directory
        :returns: returns the genome as string (amino acid sequence -> faa format)
        """
        out = ""
        for x in self.uids:
            full_path = glob.glob(path + FAA_PATH + x + "*.faa")
            if len(full_path) == 1:
                with open(full_path[0]) as faa:
                    lines = faa.read().splitlines()[1:]
                    for line in lines:
                        out += line
            else:
                exit(0)
        return out

    def _write_summary(self):
        """
        Writes a short summary about common genes.
        """
        complete = []
        for k, v in self.all_ids.items():
            if len(v) == len(self.inputs):
                complete.append(k)

        with open(self.output + "summary.csv", 'w') as f:
            f.write("# " + str(len(complete)) + " common complete EukCC genes identified:\n")
            f.write("# " + ", ".join(complete) + "\n\n")

            # create a table about the individual statuses
            f.write("IDs\t" + "\t".join(self.options["names"]) + "\n")
            for k, v in self.all_ids.items():
                f.write(k + "\t")
                for i, name in enumerate(self.options["names"]):
                    status = "Complete" if name in v else "Missing"
                    f.write(status + ("\t" if i < len(self.inputs) - 1 else "\n"))

    def _write_seq(self, n_sep=60):
        """
        Writes one file with the individual genomes.

        :param n_sep: number after how many characters a line break should happen
        """
        with open(self.output + "genes.fa", 'w') as f:
            for name, seq in self.genomes.items():
                f.write(">" + name + "\n")
                for i in range(0, len(seq), n_sep):
                    f.write(seq[i:i + n_sep] + "\n")

    def _run(self):
        """
        Calls the single steps of the script one after the other.
        """
        self.all_ids = {}
        self.ids = []
        for i, path in enumerate(self.inputs):    # filter & collect ids from EukCC
            self.ids = [*self.ids, *self._filter_ids(path, self.options["names"][i])]
        self._compare_ids()  # compare EukCC ids
        self.genomes = {}
        for i in range(0, len(self.inputs)):    # collect genomes for each run
            self.genomes[self.options["names"][i]] = self._join_faa(self.inputs[i])
        self._write_seq()  # write file
        self._write_summary()   # summarize the commonalities


def main() -> ArgumentParser:
    parser = argparse.ArgumentParser(prog="EukCC_CompareTSVFiles",
                                     description="Compares & filters the results of EukCC of the comparative pipeline.\r\n " \
                                                 "Copyright © 2021 Hagen Dreßler",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-i", "--input", help="paths to EukCC's directories", nargs='+', metavar="paths")
    parser.add_argument("-o", "--output", help="path to output directory", metavar="path", type=str)
    parser.add_argument("-n", "--names", help="names of the individual genomes (optional)", nargs='+', metavar="list", default=[], required=False)
    args = parser.parse_args()
    EukCCComparer(args.input, args.output, vars(args))


if __name__ == '__main__':
    main()
