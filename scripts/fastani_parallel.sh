#!/usr/bin/env bash
QUERY_FILE=$1
REF_FILE=$2
PARAM_DIR=$3
PARAM_KMER=$4
PARAM_FLEN=$5
PARAM_MIN=$6
LOG=$7
NAME_QUERY=$8
NAME_REF=$9
OUTPUT=${10}

REF_REL_FILE="$(basename -- $REF_FILE)"
QUERY_REL_FILE="$(basename -- $QUERY_FILE)"
TMP_NAME="${PARAM_DIR}${REF_REL_FILE}_${QUERY_REL_FILE}_temp"

/opt/mosga/tools/FastANI/fastANI -q $QUERY_FILE -r $REF_FILE -o ${TMP_NAME}.ani --kmer $PARAM_KMER --fragLen $PARAM_FLEN --minFraction $PARAM_MIN --visualize --threads 2 >>$LOG 2>&1
sed -e "s/\s/,/g" -e "s#$QUERY_FILE#$NAME_QUERY#" -e "s#$REF_FILE#$NAME_REF#" ${TMP_NAME}.ani >> $OUTPUT
sed -e "s#$QUERY_FILE#$NAME_QUERY#" -e "s#$REF_FILE#$NAME_REF#" ${TMP_NAME}.ani >> ${OUTPUT}.visual
rm ${TMP_NAME}.*
