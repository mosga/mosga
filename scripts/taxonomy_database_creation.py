#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sqlite3
import os.path

from argparse import ArgumentParser


class TaxonomyDatabase:
    """
    Generates a SQLite database with all eukaryotic species names and NCBI taxonomy identifier.
    """

    def __init__(self, args: ArgumentParser):
        """
        Initiates the generation.
        :param args: Passes over all parameter.
        """
        self.database_file: str = args.output
        self.names_file: str = args.names
        self.nodes_file: str = args.nodes
        self.db: sqlite3.Connection = None

        self.nodes = self._get_nodes() if self.nodes_file is not None else []
        self.names = self._get_names()
        print("Found " + str(len(self.names)) + " names.")
        self._build_database()
        self._fill_database()

    def _get_names(self) -> list:
        """
        Isolate all species names. If self.nodes exists, only names that are referenced in the nodes file will be kept.
        :return: List of tuples with: NCBI ID, Species name
        """
        names: list = []
        with open(self.names_file) as fp:
            line = fp.readline()
            while line:
                delimiter_position = line.find(";")
                ncbi_id = int(line[:delimiter_position].strip())
                species_name = line[delimiter_position+1:].strip()

                # keep only Linné style names: Drosophila melanogstar,
                # but not families like "Drosophila" or hybrids like "Drosophilia melanogaster x Drosophila elegans"
                linne = species_name.split(" ")
                if len(linne) < 2 or len(linne) > 2:
                    line = fp.readline()
                    continue

                if len(self.nodes):
                    try:
                        self.nodes[ncbi_id]
                    except KeyError:
                        line = fp.readline()
                        continue

                names.append((ncbi_id, species_name))
                line = fp.readline()

        self.nodes = None
        return names

    def _get_nodes(self) -> dict:
        """
        Creates a simple dictionary with the NCBI ID as keys if they are referenced inside the nodes.dmp.
        :return: Dictionary with all NCBI ID as keys (=> allows fast random access).
        """
        nodes: dict = {}
        with open(self.nodes_file) as fp:
            line = fp.readline()
            while line:
                entries = line.strip("\t").split()
                nodes.update({int(entries[0]): True})
                line = fp.readline()
        return nodes

    def _build_database(self) -> bool:
        """
        Build the database with tables.
        Removes previous version of t he database.
        :return True if functions runs through.
        """
        if os.path.isfile(self.database_file):
            os.remove(self.database_file)
            print("Removed old taxonomy database file.")

        self.db = sqlite3.connect(self.database_file)
        query = """CREATE TABLE `ncbi_taxonomy` (
                    `ncbi_id`	INTEGER NOT NULL,
                    `name`	TEXT NOT NULL,
                    PRIMARY KEY(`ncbi_id`,`name`)
                );"""
        self.db.execute(query)
        self.db.commit()
        return True

    def _fill_database(self) -> bool:
        """
        Insert the NCBI id and species name into the new database.
        :return: True if functions runs through.
        """
        if self.db is None or not len(self.names):
            return False
        cursor = self.db.cursor()

        # executemany fails...
        for i in range(len(self.names)):
            entry = self.names.pop()
            cursor.execute("INSERT INTO ncbi_taxonomy (ncbi_id, name) VALUES (?,?)", entry)
        self.db.commit()
        return True


def main():
    """
    Manages the arguments and initiates the TaxonomyDatabase class.
    """
    parser = argparse.ArgumentParser(prog="TaxonomyDatabase",
                                     description="Creates a SQLite database with taxnomy IDs and species names. \r\n " \
                                                 "Copyright © 2021 Roman Martin",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("names", type=str)
    parser.add_argument("--nodes", type=str, default=None)
    parser.add_argument("output", type=str)
    args = parser.parse_args()
    TaxonomyDatabase(args)


if __name__ == '__main__':
    main()
