from argparse import ArgumentParser

import argparse
import subprocess


class ProtHintOrthoDBPreparer:

    def __init__(self, args: ArgumentParser):
        db_file = self.__download_orthodb(args.database, args.datadir)
        self.__prothint(args.prothint_directory, args.genome, args.dir, db_file, args.cores)

    @staticmethod
    def __download_orthodb(database: str, datadir: str) -> str:
        """
        Invokes the bash OrthoDB downloader script.
        :param database: the database.
        :param datadir: the directory for the downloaded database.
        :return: the full path of the downloaded and stored database.
        """
        download = subprocess.run(["bash", "/opt/mosga/scripts/orthodb_downloader.sh", database.lower(), datadir], stdout=subprocess.PIPE)
        download_path = download.stdout.decode("utf-8").rstrip()

        if download.returncode == 0:
            return download_path

        return None

    @staticmethod
    def __prothint(prothint: str, genome: str, directory: str, database_file: str, cores: int):
        """
        Executes the ProtHint run
        :param prothint: the directory of ProtHint.
        :param genome: the fasta genome file.
        :param directory: the target directory.
        :param database_file: the OrthoDB downloaded database file.
        :param cores: number of used cores/threads.
        """
        prothint_bin = prothint + "/bin/prothint.py"
        prothint = subprocess.run(["python3", prothint_bin, genome, database_file, "--workdir", directory,
                                   "--threads", str(cores)], stdout=subprocess.PIPE)
        if prothint.returncode == 0:
            print(prothint.stdout.decode('utf-8'))
            exit(0)
        else:
            print(prothint.stderr.decode('utf-8'))
            exit(1)


def main():
    parser = argparse.ArgumentParser(prog="ProtHintOrthoDBPreparer", description="ProtHint OrthoDB Hints Preparer",
        formatter_class=argparse.RawTextHelpFormatter)

    # Execution parameter
    parser.add_argument("-c", "--cores", help="Number of threads", type=int, default=1)
    parser.add_argument("-dd", "--datadir", help="download directory", type=str, default="/opt/mosga/data/")
    parser.add_argument("-p", "--prothint_directory", help="path to the ProtHint directory",
                        default="/opt/mosga/tools/ProtHint/")
    parser.add_argument("genome", help="path to genome fasta file", type=str)
    parser.add_argument("database", help="name of the OrthoDB v10 database")
    parser.add_argument("dir", help="target directory", type=str)
    args = parser.parse_args()
    ProtHintOrthoDBPreparer(args)


if __name__ == '__main__':
    main()
