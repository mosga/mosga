#!/usr/bin/env python3
import multiprocessing
import shutil
import sys
import argparse
from multiprocessing import Semaphore
import subprocess

sys.path.insert(1, '/opt/mosga/accumulator/')
from lib.misc.file_io import FileIO
from lib.misc.fasta import FASTA
from lib.exporter import Exporter


class AugustusParrallelization:

    def __init__(self, args: argparse.ArgumentParser):
        self.__args = args

        if self.__args.output.rfind("/") >= 0:
            self.__output_directory = self.__args.output[:self.__args.output.rfind("/") ]
        else:
            self.__output_directory = "."
        self.__extrinsic = self.__args.extrinsic if self.__args.extrinsic is not None and len(self.__args.extrinsic) else None
        self.__hints = self.__args.hints if self.__args.hints is not None and len(self.__args.hints) else None

        self.__split_dir = "augustus_split"
        self.__split_ext = ".fa"
        self.__result_ext = ".gff"
        self.__log_ext = ".log"

        self.__headers = FASTA.split_fasta_file(self.__args.genome, self.__output_directory, self.__split_dir, self.__split_ext)
        self.__manage_augustus()
        all_results_path = ["%s/%s/%s%s" % (self.__output_directory, self.__split_dir, str(x), self.__result_ext) for x in self.__headers.values()]
        FileIO.cat_files(all_results_path, self.__args.output)
        shutil.rmtree("%s/%s" % (self.__output_directory, self.__split_dir))

    def __manage_augustus(self):
        sema = Semaphore(self.__args.threads)
        # workaround: avoiding too many open processes
        sublist = Exporter.chunks(list(self.__headers.keys()), 64)
        for sub in sublist:
            jobs = []
            for i in sub:
                p = multiprocessing.Process(target=self.__run_augustus, args=(i, sema))
                jobs.append(p)
                p.start()
            for proc in jobs:
                proc.join()

    def __run_augustus(self, header_key: str, sema: Semaphore):
        sema.acquire()
        genome_file_path = "%s/%s/%s%s" % (self.__output_directory, self.__split_dir, str(self.__headers[header_key]), self.__split_ext)
        result_file_path = "%s/%s/%s%s" % (self.__output_directory, self.__split_dir, str(self.__headers[header_key]), self.__result_ext)
        log_file_path =    "%s/%s/%s%s" % (self.__output_directory, self.__split_dir, str(self.__headers[header_key]), self.__log_ext)
        result_file = open(result_file_path, "w")
        log_file = open(log_file_path, "w")
        execution_cmds = ["/opt/mosga/tools/augustus/bin/augustus",
                          genome_file_path,
                          "--species=%s" % self.__args.model,
                          "--genemodel=complete",
                          "--protein=on",
                          "--codingseq=1",
                          "--uniqueGeneId=true",
                          "--gff3=on"]
        if self.__extrinsic is not None:
            execution_cmds.append("--extrinsicCfgFile=%s" % self.__extrinsic)
        if self.__args.masked:
            execution_cmds.append("--softmasking=1")
        if self.__hints:
            execution_cmds.append("--hintsfile=%s" % self.__hints)
        subprocess.run(execution_cmds, stdout=result_file, stderr=log_file)
        result_file.close()
        log_file.close()
        sema.release()
        return True


def main():
    parser = argparse.ArgumentParser(
        prog="Parallelize Augustus",
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False)

    parser.add_argument("genome", type=str, metavar="path", help="path to input directory")
    parser.add_argument("threads", type=int, metavar="number", help="number of used threads")
    parser.add_argument("model", type=str, metavar="name", help="augustus model")
    parser.add_argument("output", type=str, metavar="name", help="augustus model")

    parser.add_argument("--masked", action="store_true", default=False)
    parser.add_argument("--hints", type=str, metavar="path", default=None)
    parser.add_argument("--extrinsic", type=str, metavar="path", default=None)

    args = parser.parse_args()
    AugustusParrallelization(args)


if __name__ == '__main__':
    main()
