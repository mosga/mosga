#!/bin/bash

# download EggNog
if test -f "/opt/mosga/tools/eggnog-mapper/data/eggnog.db"; then
  echo 'EggNog found';
else
  bash /opt/mosga/scripts/install/eggnog.sh > /dev/null 2>&1
fi

# download IDXATA
if test -f "/opt/mosga/tools/idtaxa/KEGG_AllEukaryotes_r95.RData"; then
  echo 'IDTAXA found';
else
  bash /opt/mosga/scripts/install/idtaxa.sh > /dev/null 2>&1
fi

# download SILVA database
if test -d "/opt/mosga/tools/silva/"; then
  echo 'SILVA database found'
else
  bash /opt/mosga/scripts/install/silva.sh > /dev/null 2>&1
fi

# Download Swiss-Prot database
if test -f "/opt/mosga/tools/diamond/swissprot.dmnd"; then
  echo "Swiss-Prot found"
else
  bash /opt/mosga/scripts/install/swissprot.sh > /dev/null 2>&1
fi

# Peform perfomance tests
# Check defined core numbers
corenr=`grep -m 1 cores gui/config.ini.php | awk '{print $3}'`
# SNAP
if test ! -f "/opt/mosga/snakemake/tests/yeast/snap.faa"; then
  ts=`( time /opt/mosga/tools/SNAP/snap /opt/mosga/tools/SNAP/HMM/Acanium.hmm /opt/mosga/snakemake/tests/yeast/yeast -gff -quiet -lcmask -tx /dev/null -aa /opt/mosga/snakemake/tests/yeast/snap.faa)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_snap = \"$ts\"" >> /opt/mosga/gui/config.ini.php
fi
# Augustus
if test ! -f "/opt/mosga/snakemake/tests/yeast/augustus.gff"; then
  ta=`( time /opt/mosga/tools/augustus/bin/augustus /opt/mosga/snakemake/tests/yeast/yeast --species=schistosoma --genemodel=complete --protein=on --codingseq=1 --gff3=off > /opt/mosga/snakemake/tests/yeast/augustus.gff)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_augustus = \"$ta\"" >> /opt/mosga/gui/config.ini.php
  /opt/mosga/tools/augustus/scripts/getAnnoFasta.pl /opt/mosga/snakemake/tests/yeast/augustus.gff
fi
# RepeatMasker
if test ! -f "/opt/mosga/snakemake/tests/yeast/yeast.masked"; then
  tr=`( time /usr/local/RepeatMasker/RepeatMasker -q default -xsmall -no_is /opt/mosga/snakemake/tests/yeast/yeast -e hmmer -pa 1 -dir /opt/mosga/snakemake/tests/yeast/)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_repeatmasker = \"$tr\"" >> /opt/mosga/gui/config.ini.php
fi
# WindowMasker
if test ! -f "/opt/mosga/snakemake/tests/yeast/wm"; then
tw=`( time (windowmasker -mk_counts -in /opt/mosga/snakemake/tests/yeast/yeast -infmt fasta -out /opt/mosga/snakemake/tests/yeast/wo -sformat ascii -checkdup true && windowmasker -in /opt/mosga/snakemake/tests/yeast/yeast -infmt fasta -ustat /opt/mosga/snakemake/tests/yeast/wo -out /opt/mosga/snakemake/tests/yeast/wm -outfmt fasta) )  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_windowmasker = \"$tw\"" >> /opt/mosga/gui/config.ini.php
fi
# tRNA-Scan 2
if test ! -f "/opt/mosga/snakemake/tests/yeast/trnascan"; then
  tt=`( time /opt/mosga/tools/tRNAscan/tRNAscan-SE -Q -q -E /opt/mosga/snakemake/tests/yeast/yeast -o /opt/mosga/snakemake/tests/yeast/trnascan --detail --thread 1)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_trnascan2 = \"$tt\"" >> /opt/mosga/gui/config.ini.php
fi
# Barrnap
if test ! -f "/opt/mosga/snakemake/tests/yeast/barrnap"; then
  tb=`( time /opt/mosga/tools/barrnap/bin/barrnap /opt/mosga/snakemake/tests/yeast/yeast --kingdom euk --threads 1 --quiet > /opt/mosga/snakemake/tests/yeast/barrnap)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_barrnap = \"$tb\"" >> /opt/mosga/gui/config.ini.php
fi
# SILVA LSU
if test ! -f "/opt/mosga/snakemake/tests/yeast/silvalsu"; then
  ts=`( time BLASTDB=/opt/mosga/tools/silva/ blastn -db lsu -query /opt/mosga/snakemake/tests/yeast/yeast -out /opt/mosga/snakemake/tests/yeast/silvalsu -num_threads 1)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_silvalsu = \"$ts\"" >> /opt/mosga/gui/config.ini.php
fi
# EggNog
if test ! -f "/opt/mosga/snakemake/tests/yeast/eggnog" -a -f "/opt/mosga/snakemake/tests/yeast/augustus.codingseq"; then
  te=`( time (cd /opt/mosga/tools/eggnog-mapper && python3 emapper.py -i /opt/mosga/snakemake/tests/yeast/augustus.codingseq --output /opt/mosga/snakemake/tests/yeast/eggnog -m diamond --cpu 1 --translate) ) 2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_eggnog = \"$te\"" >> /opt/mosga/gui/config.ini.php
fi
#Swissprot
if test ! -f "/opt/mosga/snakemake/tests/yeast/swissprot" -a -f "/opt/mosga/snakemake/tests/yeast/augustus.codingseq"; then
  ts=`( time /opt/mosga/tools/diamond/diamond blastx -d /opt/mosga/tools/diamond/swissprot.dmnd -q /opt/mosga/snakemake/tests/yeast/yeast -o /opt/mosga/snakemake/tests/yeast/swissprot --threads 1)  2>&1 > /dev/null | grep real | awk '{print $2}' | cut -d "m" -f2 | cut -d "s" -f1`; echo "time_estimator_1m_swissprot = \"$ts\"" >> /opt/mosga/gui/config.ini.php
fi

if [ -z "$corenr" ]
then
  # Single threadings only
  echo 'Singlecore';
else
  # Multi threading
  echo 'Multicore threading'
fi

# setup sendmail
if test -f "/opt/mosga/tools/sendmail"; then
  service sendmail restart
  echo "$(hostname -i)\t$(hostname) $(hostname).localhost" >> /etc/hosts
fi

echo "export LANG=en_US.UTF-8
export LANGUAGE=en_US:en
export LC_ALL=en_US.UTF-8
export PYTHONIOENCODING=utf-8" > /home/mosga/env.sh

chmod u+x /home/mosga/env.sh

# start cron
cron && tail -f /home/mosga/cron.log &

# start apache
/usr/sbin/apache2ctl -D FOREGROUND
