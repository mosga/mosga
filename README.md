[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5121228.svg)](https://doi.org/10.5281/zenodo.5121228)
[![Coverage](https://gitlab.com/mosga/mosga/-/raw/master/coverage.svg?inline=false)](https://gitlab.com/mosga/mosga/-/raw/master/coverage.svg)

---

![MOSGA Logo](https://gitlab.com/mosga/mosga/-/raw/master/gui/images/mosga-logo-full.svg?inline=false)

[Check out our wiki page](https://gitlab.com/mosga/mosga/-/wikis/home)

### Docker
Easiest way to install the pipeline is to use our last released Docker image:

```bash
docker pull registry.gitlab.com/mosga/mosga:latest
docker run --publish 8000:80 --detach --name mosga registry.gitlab.com/mosga/mosga:latest
```
You can open the pipeline by opening a web browser and type in ``http://localhost:8000`` as the address. If you run the docker container you have to use the IP address or hostname instead `localhost`.

**The first run will cause the download of the EggNog 5, SILVA and Swiss-Prot database. In total it requires ~60 GB disk space. Please be patient, since MOSGA will only starts after the downloads are finnished.**
The database downloads are skipped by our builds due to the huge image size.
