## [2.1.6] - 2023-03-25
### Added
- misc wrapper
- ClassifySeqFromBlast module
- DiscreteSequencesFromPositions module
- Add experimental Benchmark module

### Fixed
- Missing unrar package (Deori6)
- Avoid error during single-core exporting
- tbl2asn fixed version
- vega-lite fixed version

## [2.1.5] - 2022-11-14
### Added
- ssu-align for 18S rRNA detection.
- Deori6 database for searches of origin of replications.
- JBrowse2 GFF MOSGA wrapper.
- newcpgreport in JBrowse.
- Comprehensive benchmark module to compare genome annotations.
- Abstract MOSGADB reader that reads in MOSGA annotation database.

### Fixed
- All rRNAs were not written in the final output (filter error).
- Wrong mapping of genes to sequences during quality control (#196).
- Adapt to eggmapper changed result column order.

### Changed
- CpGisland unit incorporation in the organelle scanner.

### Removed
- scripts/snap_to_gff3.py
- scripts/barrnap_to_gff3.py
- scripts/cpgiscan_to_gff3.py
- Remove the CpGIScan reader in the organelles module. Use the general one.

### Fixed
- Due to a wrong filter, every rRNA was removed during the export procedure.

## [2.1.4-3] - 2022-09-02
### Fixed
- Remove add-apt-repository question during full installation.

## [2.1.4-2] - 2022-08-19 Hotfix
### Fixed
- Missing GlimmerHMM installation.

## [2.1.4] - 2022-08-06
### Added
- Compatibility to Ubuntu 22.04.
- Compatibility to PHP 8.1.

### Fixed
- Error during the quality check if genes overlap (#194).
- Fixed error that blockades the upload process.

### Changed
- Switch docker image from Ubuntu 20.04 image to Ubuntu 22.04.
- Update multiple versions of dependencies.
- Update visualizer since BUSCO was upgraded from version 4 to version 5.
- Rewrite the "map_genes_with_file" function, and allow multiple partial or fully overlapping.
- Refactor and harmonize annotation exporter for single- and multi-threaded classes.
- Enabled multi-level verbose mode with extensive output.

### Removed
- Removed debug as an option for some classes. Use verbose mode (level >1) instead.

## [2.1.3] - 2022-07-19
### Added
- GUI table showing the overall genome characteristics (#182).
- Minimal mobile phone support.
- Search for matching genetic code (nuclear, mitochondrial, plastid).
- General GFF3 reader for RefSeq or other files.

### Fixed
- Detect .genbank file as GBFF extension.
- Empty GBFF files with only sequences but no annotations are failed (#189).

### Changed
- Remove log/genome_chopping.txt file genome_chopping.
- Move parallelized augustus error log (removed from run.log).

## [2.1.2] - 2022-04-07
### Added
- Message of the Day embedding (place a message in gui/motd.html).
- Warning if the service runs with publicly visible listed jobs.
- Include CpG Island as a partial basic annotation (sequin still does not support it).
- Integrate newcpgreport
- newcpgreport and CpGIScan results are now stored inside the annotation.db and will be exported in the GFF output.
- First version of an EFF (EMBL Flat File) reading support.
- Unit tests for CpG Island reader.
- Twitter link.
- Basic modular summary file support, including assembly stats (#182).
- Parallel Augustus support through a python3 wrapper (#186)

### Fixed
- UploadHandler renames files with dots.
- FASTA Header normalization failed (#183).
- Remove non-ASCII characters from FASTA headers.
- GBFF was wrong initialized if no annotations are present (#185).
- GBFF reading failed on pseudo tRNAs.
- RAxML bootstrap iterations configuration was used instead of the parsimony iterations.
- match_to_gff3.py fails (#188).

### Changed
- Refactor the GUI a bit.
- Catch error for the writer if an unsupported unit appears.
- Catch error from VecScreen analysis (#184).

## [2.1.1] - 2022-02-08
### Added
- Categorizes and colors for JBrowse 2.
- Mitochondrial and plastid gene matches are now visualized in JBrowse 2.
- modules.php that manages different optional MOSGA modules that can be loaded.
- Add PHP interface for the different template/modules views (separate webpages) and result sidebar reference.
- BlobTools results are now showed on the results page (validation box).
- Multi-tab support for parallel job submissions.
- New job status: Re-queued. Manually jobs that have to be rerun will be use this status.

### Changed
- Template views and result sidebar references (refactor).
- Merge busco_visual.py and eukcc_visual.py into genome_completeness.py

## [2.1.0] - 2022-01-28
### Added
- CpGIScan for CpG island detection.
- Reference to Zenodo.
- Notice about the aprox. execution time.

### Fixed
- Spliceator will not work if no gene prediction was selected.
- StringDB crashes the pipeline when receiving an empty list (#175).

### Changed
- Use an external ncbl-blast+ package from ubuntu (#178).

## [2.0.11.1] - 2022-01-22 Hotfix
### Fixed
- Wrong path for JBrowse BRAKER file.
## [2.0.11] - 2022-01-20
### Added
- Separate log files for BRAKER.
- VecScreen results now JBrowse.
- SILVA LSU results now in JBrowse.
- Spliceator to prediction of accurate splicing sites (#172).
- First Anaconda support for some snakemake rules (#60).

### Fixed
- Wrong interpretation of VecScreen's result due to an shift in the positions (#171).
- SILVA LSU wrong sorting.
- spaln installation (our binaries sources is not reachable anymore).
- Some gzipped files were not correctly recognized (#168).
- Augusts result now again integrated into JBrowse (#161).

### Changed
- SILVA LSU searches use chopped genome and supports gzip (#163).
- Replace IDTAXA's database from KEGG_Eukaryotes_r95.RData tp KEGG_AllEukaryotes_r95.RData (Thanks to Erik Scott Wright!).
- Updated spaln from 2.4.0 to 2.4.6
- Moved installation scripts into another sub-directory

## [2.0.10] - 2021-12-23
### Added
- Wrapper for RepeatMasker to safely catch unknown species configuration.

### Fixed
- Too many aruguments error for the MOSGA writer due too many scaffolds.
- Too long summary.html results caused overflow of PHP memory limit resulting in a empty page.

## [2.0.9] - 2021-12-02
### Added
- Improve parsing of EggNog classifications.

### Fixed
- Broken GFF3 files due to not supported symbols from the EggNog classifier.
- Redundant GFF3 gene notes.

## [2.0.8] - 2021-11-25
### Added
- IDTAXA for functional protein classification.

### Fixed
- Red repeating regions start shifted to +1.
- EggNog output classification filters wrong columns (due to some EggNog changes..).
- EggNog python3 version (deprecated for Ubuntu 18.04).
- Backwards compatiblity for functional gene annotation (idtaxa).
- Patch (system-dependent) wrong fastme path in Snakemake rule.

### Changed
- Refactor the functional annotation parts that is important for the accumulator genes import.
- Change GFF3 output for better JBrowse visualization.
- Update JBrowse2 to 1.5.1

## [2.0.7] - 2021-11-10
### Added
- Units tests for SNAP genes reading.

### Fixed
- GeneBank flat files without explicit transcripts could not be imported #155.
- Red repeating regions starts with 0 (changed to 1).
- SNAP reader sorts the gene into the wrong sequence bucket #157.
- Docker build ignores missing GeneMark binaries #156.
- Sometimes snakemake rules files could not be found (nested directory issues).

### Changed
- Update citations.
- Restructure text output file. Use more the logs directory.
- Introduce control directory that contains empty files, which are important for the workflow control.
- Rename Docker user from dockeruser to mosga.

## [2.0.6] - 2021-10-10
### Fixed
- Docker: MOSGA starts before the databases are ready.

## [2.0.5.1] - 2021-10-08
### Fixed
- Ubuntu docker image lost some important packages by default.

### Changed
- Update citations.

## [2.0.5] - 2021-08-26
### Added
- A log viewer presents reads all existing log files.

### Fixed
- Severe security issue, that could allow to execute external tools.
- Prevente comparitve genome job submission with a single job.
- Remove white spaces from FASTA file inside the sequence.
- Rewrote completly SNAP GFF2 to GFF3 converter.
- EggNog failed due a recent for Ubuntu 20 update which replaced python3.7 with python3.8
- EggNog mapper requires an additional paramter for translation mode

## [2.0.4] - 2021-08-11
### Added
- Multithreading support for MOSGA Writer. Single-core mode is still possible as a fallback.
- Unify GFF/FeatureTable writing process.
- More RAxML parameters for the GUI.

### Changed
- Simplify workflows that remove redundancy and make independent workflows more robust.
- Limit StringDB to 2000 proteins for a network analysis request.

### Removed
- MOSGA GFF export execution (as a standalone call).

## [2.0.3] - 2021-08-03
### Added
- The PHP "Config" class (default values) for Snakemake that manages the default config values other multiple classes.

### Fixed
- GBFF fails due missing ACESSION/VERSION values; fallback implemented.
- Reduce workflow complexity by rerouting rules.

## [2.0.2] - 2021-08-02
### Added
- MOSGA 2 manuscript BioRxiv reference.
- Auto gzip decompression.
- Auto antivirus scan.

### Fixed
- Replace old JBrowse logo with the newer.
- Fixe missing function (rrmdir) for job cleaner.

## [2.0.1] - 2021-07-21
### Added
- Snakemake GBFF test.
- Extend mode for organelle scanner.

### Fixed
- Organelle scanner on mod_rewrite URLs.
- Organelle scanner will be not invoked multiple times.
- zenodo.json syntax error.
- Red (Repeat detector) fails genomes with carriage retuern symbols from GBFF import.

## [2.0.0] - 2021-07-20

### Added
- A new mode: Comparative Genomics
- Comparative Genomic workflows.
  - BUSCO
  - EukCC
  - clipKit
  - traimAl
  - FastME
  - RAxML
  - FastANI
  - MOSGA Gene Comparison
- Validation tools.
  - BUSCO
  - EukCC
  - VecScreen
  - BlobTools
- Repeats: Red
- Visualization for comparative genomics and validation tools.
- Possibility to sort gui-rules groupes.
- Introduce taxonomy search.
- GBFF import function for annotation and comparatige genomics mode.
- Server usage stats-
- Enable mod_rewrite for URL-

### Fixed
- Fix organelle scan for very large genome files (> 1 GiB).

## [1.2.4] - 2021-06-01

### Fixed
- Exporter fails due to unknown strand direction of two repeats in a row.
- EggNog fails in combination with GlimmerHMM prediction.

## [1.2.3] - 2021-04-27

### Fixed
- QC: Internal stop-codons errors
- QC: Overlapping features with gapped sequences
- QC: Doubled tailing stop-codons (Augustus/BRAKER differentiation)

## [1.2.2] - 2021-03-09

### Fixed
- User interface Conflicting Resolution menu.

## [1.2.1] - 2021-03-02

### Added
- Protein-coding nucleotide sequence analysis and filters.
- Filters for the final output writing.

### Fixed
- Filter for intron lenghts fallback could discard wrong genes.

## [1.2.0] - 2021-02-26

### Added
- Supporting graph shows the mode of action for reading and writing processes.

### Fixed
- Sequin repeating region were wrong formatted.
- Fatal errors leads that tbl2asn does not check the feature table.

## [1.1.9] - 2021-02-05

### Added
- An IID API implementation.
- A STRING DB implementation.
- Minimal CLI tool to rerun single jobs.

### Fixed
- Made output genome writer class more robust.
- MOSGA GFF exporter writes None as score value.
- Organelle scan summary table crashed #94.

### Changed
- Modified Snakemake pipeline graph modal popup.
- Externalize g:GOSt g:Profiler module.

## [1.1.8] - 2021-01-19

### Added
- New unittest for tRNAScan-SE 2 reading.

### Fixed
- RepeatMasker can not handle with FASTA files that has headers with more than 50 characters.
- SILVA LSU input error

## [1.1.7] - 2021-01-14

### Added
- New unittest test design and test data.
- New Snakemake test and test data.
- Initial genome FASTA file initialization.

### Fixed
- Last gene missed transcripts from Augustus gene reader.
- EggNog reader does only remove too long descriptions but keeps short names.

### Changed
- Switched partially to a new FileIO reader that supports GZIP files.
- Docker cronjob times for killing hanging spaln instances.
- Coordinator loads modules locally, improve massively execution time and reduce dependency errors.

## [1.1.6] - 2021-01-08

### Added
- Class that maps the abstract gene objects back to FASTA files and returns the sequences.
- Class that allows comparison of multiple sets of genes from multiple FASTA files.
- Class to allow the use of gzipped files.
- A index file with all generated final and intermediate results.
- Preparation for central genome initialization process including new database columns.
- Added new Augustus HMM models to the options.

### Fixed
- RepeatMasker species argument passing.
- JBrowse linkage fails due to race conditions.
- Docker container was not able to execute jobs due to encoding issues.

### Changed
- Enable RepeatMasker quick mode as default.
- collector.py and writer.py are now integrated into the central mosga.py
- repeatmasker_to_gff.py and windowmasker_to_gff.py callable through mosga.py
- Small improvments for the oraganelle scan score.
- Minimized required parameters for database initialiazion.
- Updated database structure (version. 0->1).
- Updated AUGUSTUS >=3.4.0 und BRAKER.
- Updated HTSlib to 1.11.0.
- Updated BCFtools and samtools to 1.11
- Updated RepeatMasker to 4.1.1.
- Updated HMMer to 3.3.2.

## [1.1.5] - 2020-12-04

### Added
- A workflow for orthology based prediction with BRAKER and ProtHints.
- Fallback for settings radio menu entries if no titles are given.
- File index with all files.

### Fixed
- Render HTML formated publications output

### Changed
- Moved files from snakemake/data/ to data/
- Use only an integer for the oragnelle scan scoring

## [1.1.4] - 2020-11-23

### Added
- Docker building argument for skipping sendmail setup.

### Fixed
- Missing sticky group bit for project directories.
- Modal title did not render HTML code.
- Prediction results were not display due the gui-rules.json change from v.1.1.2.
- Flag not existing projects as deleted during the deletion routine.
- Summary table did not count the first row.
- Organelle scan only multiplies with the GC deviation it is not zero.

### Changed
- Introduce rough scoring for organelles identification.

## [1.1.3] - 2020-11-20

### Added
- Summary table

### Fixed
- Docker build fails due a wrong GeneMark file path.
- GC content analysis fails if not outliers in the GC content are existing.
- Dockerfile missed scripts directory.

### Changed
- Set default value for tRNAscan-SE from 10 to 70.

## [1.1.2] - 2020-11-19

### Added
- License and relevant publications will be displayed in the info box.
- What-To-Cite displays a list of relevant publications in the details.

### Fixed
- Vulnerability fix of v.1.1.1 leads MOSGA did not display SVG graphics.
- Get error message if user try to upload wrong file type files.

### Changed
- Simplified the structure of gui-rules.json (remove not used levels).

## [1.1.1] - 2020-11-17

### Added
- Accumulator argument to enable explicit bidirectional annotation.

### Fixed
- Vulnerability that could allow execution on uploaded files.
- The results file (genome.sqn and genome.tbl) were referenced even they did not exist.

## [1.1.0] - 2020-11-15

### Added
- An organelle scan.
- A mitochondria and plastid database.
- An organelle analysis that supports users to identify putative organelle scaffolds.
- A dynamic jquery file uploader that allows user to upload large files (limit at 20 GiB).
- Progress bar for uploads.
- g:Profiler g:OSt for functional profiling.
- GeneMark prediction.
- PHP Twig as the template engine for HTML rendering.
- A "Host" tab appears if gui/includes/templates/host.html exists.
- Allow user to exclude file-specified regions from annotation
- Prevent users to submit annotation jobs without the selection of any tools.
- Functional gene annotation in jBrowse view.
- jBrowse annotation for repeats.
- Protein evidence-based predictions for BRAKER.
- Duplicated file uploads will be detected and internally replaced by symlinks.

### Fixed
- Deletion scheduler wrongly deleted too new entries.
- PHP interprets the unique job id as a float number.
- Augustus genes at the edge of a scaffold were not correctly detected.
- Fix GlimmerHMM does sometimes reads invalid genes.
- Fix SNAP wrongly creates extremely large gene annotations.
- Display correctly separated tabs all over all pages.
- Fallback: mod rewrite entry for snakemake.php

### Changed
- Include snakemake.php into index.php.
- Normalize/rename FASTA header according convention instead of simple renaming.

### Removed
- The results file snakemake.php.
