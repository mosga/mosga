# Copyright © 2021 Roman Martin
__author__ = "Roman Martin"
__copyright__ = "Copyright (C) 2021 Roman Martin"
__license__ = "MIT"

import argparse

from lib.actions import MOSGAActions
from lib.database.database import Database
from lib.database.error import DataBaseError
from lib.errors import MOSGAError, UnknownAction


class MOSGA(object):
    """
    Initially, this class discriminates between the requested modes (reading or writing), but we will softly transform
    that into a centralized management class.
    """

    def __init__(self, arguments: argparse, action: MOSGAActions, store: bool = False):
        """
        Since the introduction of the MOSGAArgs classes this class decides what should be performed depending on the
        chosen action.
        :param arguments:   Arguments parser object.
        :param action:      Enumeration with the module that should be started.
        :param store:       Store intermediate results inside the objects (mostly relevant for the importer)
        """
        # Try to catch the right action
        try:
            self.__args = arguments
            self.__store: bool = store
            self.verbose: int = self.__args.verbose

            if self.verbose > 1:
                print("Coordinator: Enable in-depth verbose mode level %i" % self.verbose)

            actions = {
                MOSGAActions.IMPORT: self.__importer_run,
                MOSGAActions.COLLECT: self.__importer_run,
                MOSGAActions.EXPORT: self.__exporter_run,
                MOSGAActions.WRITE: self.__exporter_run,
                MOSGAActions.GET_PROTEINS_NUC_SEQ: self.__get_protein_nucleotide_sequence,
                MOSGAActions.CG_GENES_COMP: self.__cg_genes_comparison,
                MOSGAActions.INITIALIZE_GENOME: self.__initialize_genome,
                MOSGAActions.GOSTGPROFILER: self.__gprofiler,
                MOSGAActions.KEYPATHWAYMINER: self.__keypathwayminer,
                MOSGAActions.IID: self.__iid,
                MOSGAActions.STRINGDB: self.__string_db,
                MOSGAActions.VECSCREEN: self.__vecscreen,
                MOSGAActions.JBROWSE: self.__jbrowse,
                MOSGAActions.BENCHMARK: self.__benchmark,
                MOSGAActions.MISC: self.__misc
            }
            run = actions.get(action, self._error)
            self.__run = run()

        # Unknown internal action call
        except UnknownAction:
            raise UnknownAction("Unknown action. Please check what calls the MOSGA module.")
        # General error
        except MOSGAError:
            print("Unspecified error occurred")

    def _error(self):
        raise UnknownAction

    def __importer_run(self):

        try:
            self.__database = Database(self.__args)
            self.__database.initialize_database(True)
        except DataBaseError:
            exit(2)

        from lib.importer import Importer
        importer = Importer(
            self.__database,
            self.__args,
            self.__store
        )

        return importer

    def __exporter_run(self):
        self.__args.purge = False
        smt: int = 0
        try:
            smt: int = self.__args.threads
        except AttributeError:
            pass

        try:
            self.__database = Database(self.__args)
            self.__database.initialize_database(False)
        except DataBaseError:
            exit(2)

        if smt:
            from lib.export.exporter_mt import ExporterMT
            exporter_class = ExporterMT
        else:
            from lib.export.exporter_st import ExporterST
            exporter_class = ExporterST

        if self.verbose and smt:
            print("Select Multi-Threaded class")
        elif self.verbose:
            print("Select Single-Threaded class")

        exporter = exporter_class(
            self.__database,
            self.__args
        )
        exporter.export()

        return exporter

    def __get_protein_nucleotide_sequence(self):
        from lib.misc.protein_seq import ProteinSequences
        return ProteinSequences(self.__args, "nuc")

    def __gprofiler(self):
        from lib.analysis.external.gprofiler import gProfilerEnrichment
        gprofiler = gProfilerEnrichment(self.__args)
        return gprofiler.submit()

    def __string_db(self):
        from lib.analysis.external.stringdb import StringDB
        string_db = StringDB(self.__args)
        return string_db.submit()

    def __iid(self):
        from lib.analysis.external.iid import IID
        iid = IID(self.__args)
        return iid.submit()

    def __cg_genes_comparison(self):
        from lib.analysis.cg_genes_comparison import CGGenesComparisonError
        try:
            from lib.analysis.cg_genes_comparison import CGGenesComparison
            return CGGenesComparison(self.__args)
        except CGGenesComparisonError as e:
            print(e)
            exit(5)

    def __initialize_genome(self):
        try:
            self.__database = Database(self.__args)
            self.__database.initialize_database(True)
        except DataBaseError:
            exit(2)

        from lib.analysis.initialization import InitializeGenome
        return InitializeGenome(self.__database, self.__args)

    def __keypathwayminer(self):
        from lib.analysis.external.keypathwayminer import KeyPathwayMinerAPI
        return KeyPathwayMinerAPI(self.__args)

    def __vecscreen(self):
        try:
            self.__database = Database(self.__args)
            self.__database.initialize_database(True)
        except DataBaseError:
            exit(2)

        from lib.analysis.vecscreen import VecScreen
        return VecScreen(self.__args, self.__database)

    def __jbrowse(self):
        from lib.misc.jbrowse import JBrowse
        return JBrowse(self.__args)

    def __benchmark(self):
        from lib.analysis.benchmark import Benchmark
        benchmark = Benchmark(self.__args)
        benchmark.analyse()
        return benchmark

    def __misc(self):
        if self.__args.action is not None and self.__args.path is not None:
            if self.__args.action == "DiscreteSequencesFromPositions":
                from lib.misc.discrete_sequences_from_positions import DiscreteSequencesFromPositions
                DiscreteSequencesFromPositions(self.__args, self.verbose)
                pass
            elif self.__args.action == "ClassifySeqFromBlast":
                from lib.misc.classify_seq_from_blast import ClassifySeqFromBlast
                ClassifySeqFromBlast(self.__args, self.verbose)

    def get_run(self):
        """
        Return the initialized and executed module class
        :return: module class
        """
        return self.__run

    def get_db(self):
        """
        Return the initialized database class
        :return: the database class
        """
        return self.__database
