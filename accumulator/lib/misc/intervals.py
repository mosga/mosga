from lib.misc.file_io import FileIO
from lib.units.transcriptunit import TranscriptUnit
from lib.units.transcript import Transcript, TranscriptType
from lib.units.gene import Gene


class Intervals:

    @staticmethod
    def read_fasta_to_annotation(fast_file_path, with_sequence: bool = False, skip_pseudo_genes: bool = True):
        pseudo_annotation: dict = {}
        sequence = None

        for line in FileIO.get_file_reader(fast_file_path):
            if line[0] != ">":
                if not with_sequence:
                    continue
                else:
                    pseudo_annotation[sequence][-1].sequence = "%s%s" % (
                    pseudo_annotation[sequence][-1].sequence, line.strip())
            else:
                line = line.strip()[1:]
                v = line.split(" ")

                species = "human"

                if species == "alternative_homo_sapiens":
                    sequence = v[2][18:]
                    sequence = sequence[:sequence.find(":")]

                    if skip_pseudo_genes and ("pseudogene" in v[4] or v[-1].strip() == "pseuodgene" or v[-1].strip() == "pseudogene"):
                        continue

                    tmp = v[2][18:]
                    tmp = tmp[tmp.find(":") + 1:]
                    pos = tmp.split(":")
                else:
                    tmp = v[2].split(":")
                    sequence = tmp[2]
                    pos = tmp[3:]

                if sequence not in pseudo_annotation:
                    pseudo_annotation.update({sequence: []})

                # create pseudo gene with pseudo cds
                start_pos = int(pos[0])
                end_pos = int(pos[1])
                strand = False if pos[2] == "-1" else True

                if start_pos == 1:
                    start_pos = 0

                gene = Gene()
                gene.start = start_pos
                gene.end = end_pos
                gene.strand = strand
                gene.sequence = ""
                gene.name = v[0]

                transcript = Transcript()
                transcript.start = start_pos
                transcript.end = end_pos
                transcript.strand = strand

                unit = TranscriptUnit()
                unit.type = TranscriptType.CDS.value
                unit.start = start_pos
                unit.end = end_pos
                unit.strand = strand

                transcript.units = [unit]
                gene.transcripts = [transcript]
                pseudo_annotation[sequence].append(gene)

        return pseudo_annotation

    @staticmethod
    def sort_annotations_in_unit_types(units: dict):
        """
        Add wrapper with the unit type and sort all units into this dictionary
        :param units: the original dictionary.
        :return: units dictionary with unit types, scaffolds and lists of units.
        """
        type_dict: dict = {}
        if type(units) is list:
            for x in range(0, len(units)):
                type_dict.update({str(units[x][list(units[x])[0]][0]): units[x]})
        else:
            type_dict.update({str(units[list(units)[0]][0]): units})

        return type_dict

    @staticmethod
    def get_coding_intervals(units: list) -> list:
        interval: list = []
        for gene in units:
            for transcript in gene.transcripts:
                for unit in transcript.units:
                    if unit.type == TranscriptType.CDS.value:
                        if unit.start < unit.end:
                            start = unit.start
                            end = unit.end
                        else:
                            start = unit.end
                            end = unit.start

                        if start == 1:
                            start = 0
                        interval.append([start, end])

        return interval

    @staticmethod
    # source: https://learncodingfast.com/merge-intervals/#Graphical_Illustration
    def merge_intervals(intervals):
        if len(intervals) == 0 or len(intervals) == 1:
            return intervals
        intervals.sort(key=lambda x: x[0])
        result = [intervals[0]]
        for interval in intervals[1:]:
            if interval[0] <= result[-1][1]:
                result[-1][1] = max(result[-1][1], interval[1])
            else:
                result.append(interval)
        return result

    @staticmethod
    def check_overlap(ref: tuple, pred: tuple):
        return pred[0] < ref[0] < pred[1] or ref[0] < pred[0] < ref[1]
        # TODO: overlap definition
