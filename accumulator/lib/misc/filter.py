from lib.units.gene import Gene
from lib.units.rrna import rRNA, rRNAsType
from lib.units.transcript import TranscriptType


class Filter:
    """
    Contains multiple filter methods which will be applied after the initial reading of new units or final writing.
    """

    # General
    @staticmethod
    def minimal_length(length: int):
        return lambda x: x.length >= length

    @staticmethod
    def maximal_length(length: int):
        return lambda x: x.length <= length

    @staticmethod
    def minimal_score(score: float):
        return lambda x: x.score >= score

    @staticmethod
    def maximal_score(score: float):
        return lambda x: x.score <= score

    # rRNA
    @staticmethod
    def ignore_partial_rrnas():
        return lambda x: not x.partial

    # NCBI: DISC_SHORT_RRNA
    @staticmethod
    def check_ribosomal_rna_sizes(unit: rRNA) -> bool:
        if rRNAsType(unit.rrna_stype) is rRNAsType.srrna_28 and unit.length < 3299:
            return False
        return True

    @staticmethod
    def minimal_ribosomal_rna_size():
        return lambda x: Filter.check_ribosomal_rna_sizes(x)

    # tRNA
    @staticmethod
    def ignore_suppressor_trnas():
        return lambda x: x.trna_aa != "Sup"

    # Gene
    @staticmethod
    def minimal_transcripts(minimal: int):
        return lambda x: len(x.transcripts) >= minimal

    @staticmethod
    def maximal_transcripts(maximal: int):
        return lambda x: len(x.transcripts) <= maximal

    @staticmethod
    def check_intron_minimal_length(gene: Gene, minimal: int) -> bool:
        for transcript in gene.transcripts:
            cds: list = []
            has_introns = False
            for unit in transcript.units:
                if TranscriptType(unit.type) == TranscriptType.intron:
                    has_introns = True
                    if unit.length is not None and 0 < unit.length <= minimal:
                        return False
                elif not has_introns and TranscriptType(unit.type) == TranscriptType.CDS:
                    cds.append(unit)

            # Fallback mode if no exons are explicitly declared
            # Calculate intron length by the difference between the exons
            if len(cds) > 1:
                for i in range(0, len(cds)-1, 1):
                    if cds[0].end > cds[1].start:
                        distance = (cds[i + 1].end - cds[i].start)*-1
                    else:
                        distance = cds[i + 1].start - cds[0].end
                    if distance <= minimal:
                        return False
        return True

    @staticmethod
    def check_exon_minimal_length(gene: Gene, minimal: int) -> bool:
        for transcript in gene.transcripts:
            for unit in transcript.units:
                if (TranscriptType(unit.type) == TranscriptType.exon or
                    TranscriptType(unit.type) == TranscriptType.CDS) and \
                        unit.length is not None and unit.length <= minimal:
                    return False
        return True

    @staticmethod
    def minimal_intron_length(minimal: int):
        return lambda x: Filter.check_intron_minimal_length(x, minimal)

    @staticmethod
    def minimal_exon_length(minimal: int):
        return lambda x: Filter.check_exon_minimal_length(x, minimal)
