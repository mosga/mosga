import os
from typing import Tuple, List

from lib.misc.file_io import FileIO


class FASTA:
    """
    Contains multiple FASTA file relevant static methods.
    """

    @staticmethod
    def get_headers(path: str, remove_empty: bool = False, verbose: bool = False, only_headers: bool = False) -> Tuple[List, List]:
        """
        Reads a FASTA file and returns the FASTA sequence headers and sequence lengths.
        :param path: FASTA file path.
        :param remove_empty: removes empty sequence headers.
        :param verbose: show some information.
        :return:
        """
        headers: list = []
        seq_len: list = []
        nc = 0

        for line in FileIO.get_file_reader(path):
            if line[:1] == '>':
                if remove_empty and not nc:
                    try:
                        removed = headers.pop()
                        seq_len.pop()
                        if verbose:
                            print(removed + " removed")
                    except IndexError:
                        pass
                headers.append(line.rstrip()[1:])
                seq_len.append(nc)
                nc = 0
            else:
                if only_headers:
                    continue
                for nuc in line.rstrip():
                    nc += 1

        return headers, seq_len

    @staticmethod
    def split_fasta_file(fast_path: str, target_dir: str, subdir: str = "split", extension: str = ".fa") -> dict:
        from os import listdir, mkdir
        from os.path import isfile, join

        directory_name = "%s/%s/" % (target_dir, subdir)
        has_files: bool = False
        headers_map: dict = {}

        try:
            has_files = bool(len([f for f in listdir(directory_name) if isfile(join(directory_name, f))]))
        except FileNotFoundError:
            mkdir(directory_name)

        # Split files
        if not has_files:
            current_head: str = ""
            cnt = 1
            pointer = None
            for line in FileIO.get_file_reader(fast_path):
                if line[:1] == ">":
                    if pointer is not None:
                        pointer.close()

                    current_head = line[1:].rstrip()
                    headers_map.update({current_head: cnt})
                    pointer = open("%s%i%s" % (directory_name, cnt, extension), mode="w")
                    cnt += 1
                pointer.write(line + "\r\n")

            if pointer is not None:
                pointer.close()

        return headers_map
