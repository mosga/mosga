from lib.misc.intervals import Intervals


class DiscreteSequencesFromPositions:

    def __init__(self, args, verbose: int = 0):
        paths: list = args.path.split(",")

        if len(paths) == 2:
            input_path: str = paths[0]
            output_path: str = paths[1]
        else:
            exit("Not enough arguments!")

        units = Intervals.read_fasta_to_annotation(input_path, True)
        units = Intervals.sort_annotations_in_unit_types(units)

        discrete_intervals: dict = DiscreteSequencesFromPositions.get_discrete_intervals_map(units["Gene"], verbose)
        intervals_map: dict = DiscreteSequencesFromPositions.map_intervals_to_map(discrete_intervals)
        del discrete_intervals
        DiscreteSequencesFromPositions.map_intervals_with_genes(units["Gene"], intervals_map, output_path)

    @staticmethod
    def map_intervals_with_genes(units: dict, intervals_map: dict, target_file: str):
        new_fasta_file = open(target_file, "w")

        for sequence_id in units.keys():
            for gene in units[sequence_id]:
                if gene.start in intervals_map[sequence_id] and gene.end == intervals_map[sequence_id][gene.start]:
                    new_fasta_file.write(">%s %s:%d:%d:%d\n%s\n" % (
                        gene.name, sequence_id, gene.start, gene.end, -1 if gene.strand else 1, gene.sequence
                    ))

        new_fasta_file.close()


    @staticmethod
    def map_intervals_to_map(original_map: dict) -> dict:
        intervals_start_map: dict = {}

        # build dict with start as key and end as value
        for sequence_id in original_map.keys():
            intervals_start_map.update({sequence_id: {}})
            for entry in original_map[sequence_id]:
                intervals_start_map[sequence_id].update({entry[0]: entry[1]})

        return intervals_start_map

    @staticmethod
    def get_discrete_intervals_map(sorted_units: dict, verbose: int = 0) -> dict:
        remaining_intervals: dict = {}

        for sequence_id in sorted_units:
            cds = Intervals.get_coding_intervals(sorted_units[sequence_id])
            cds.sort(key=lambda x: x[0])
            cds = [tuple(x) for x in cds]

            if not len(cds):  # or sequence_id != "2"
                continue

            bucket: list = []
            overlap_bucket: list = []
            nxt = None
            cur = None

            while len(cds):
                if nxt is not None:
                    if len(overlap_bucket):
                        bucket.append(overlap_bucket)
                        overlap_bucket: list = []
                    else:
                        bucket.append([nxt])
                    cur = nxt
                else:
                    cur = cds.pop()

                if len(cds):
                    nxt = cds.pop()

                    if Intervals.check_overlap(cur, nxt):
                        if verbose > 4:
                            print(cur, nxt)
                        while Intervals.check_overlap(cur, nxt):
                            if verbose > 4:
                                print("loop %s %s" % (str(cur), str(nxt)))
                            overlap_bucket.append(cur)
                            cur = nxt
                            if len(cds):
                                nxt = cds.pop()
                            else:
                                break
                        if not len(cds):
                            overlap_bucket.append(nxt)

            if len(overlap_bucket):
                bucket.append(overlap_bucket)
            elif cur is not None:
                bucket.append(cur)

            bucket: list = DiscreteSequencesFromPositions.prune_bucket(bucket)
            remaining_intervals.update({sequence_id: bucket})

        return remaining_intervals

    @staticmethod
    def prune_bucket(bucket: list) -> list:
        new_bucket: list = []
        while len(bucket):
            item = bucket.pop()
            if len(item) > 1:
                new_bucket.extend(DiscreteSequencesFromPositions.find_discrete_intervals(item))
            else:
                new_bucket.extend(item)

        return new_bucket

    @staticmethod
    def find_discrete_intervals(intervals: list, verbose: int = 0) -> list:
        discrete_intervals: list = []
        last = None
        alternating = False  # False left, True right

        if verbose >= 4:
            print("Enter intervals %s" % str(intervals))

        if type(intervals) == tuple and len(intervals) == 2:
            intervals = [intervals]

        # check left
        while len(intervals) >= 2:
            if not Intervals.check_overlap(intervals[0], intervals[-1]):
                switch: int = 0 if alternating else -1
                discrete_intervals.append(intervals[switch])
                intervals.pop(switch)
                alternating = not alternating
            else:
                break

        if not len(discrete_intervals) and len(intervals):
            discrete_intervals.append(intervals.pop())

        if verbose >= 4:
            print("Finished intervals %s" % str(discrete_intervals))
        return discrete_intervals
