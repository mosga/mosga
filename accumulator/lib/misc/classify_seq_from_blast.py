from argparse import ArgumentParser
from typing.io import TextIO
from lib.misc.fasta import FASTA
from lib.misc.file_io import FileIO
from lib.readers.format.CSV import CSV


class SequenceWriter:

    def __init__(self, output_directory: str, verbose: int = 0):
        self.fp_coding: TextIO = open("%scoding.fasta" % output_directory, "w")
        self.fp_non_coding: TextIO = open("%snoncoding.fasta" % output_directory, "w")

    def write_result(self, match: dict) -> bool:
        coding: bool = True

        if "non-coding" in match["Human"]:
            coding = False

        entry = ">%s_%s\n%s\n" % (match["RNA"], match["Human"], match["sequence"])
        coding_ref = self.fp_coding if coding else self.fp_non_coding
        coding_ref.write(entry)

        return coding

    def close(self):
        self.fp_coding.close()
        self.fp_non_coding.close()


class ClassifySeqFromBlast:

    def __init__(self, args: ArgumentParser, verbose: int = 0):
        self.verbose = verbose

        if len(args.path.split(",")) != 4:
            print("You need three paths!")
            exit(9)

        paths: list = args.path.split(",")
        self._path_matches = paths[0]
        self._path_human_header = paths[1]
        self._path_rna_sequence = paths[2]
        self._path_output_files = paths[3]

        self.__process_files()

    def __process_files(self):
        headers: dict = self.__headers()
        matches: dict = self.__matches()
        self.__parse_sequenes(headers, matches)

    def __headers(self) -> dict:
        headers: dict = {}
        if self.verbose:
            print("# ClassifySeqFromBlast #1: Reading FASTA headers from %s" % self._path_human_header)

        raw_headers = FASTA.get_headers(self._path_human_header, only_headers=True)[0]
        if self.verbose >= 1:
            print("# ClassifySeqFromBlast #2: Found %d headers" % len(raw_headers))

        for header in raw_headers:
            headers.update({header.split(" ")[0]: header})

        return headers

    def __matches(self) -> dict:
        matches: dict = {}
        if self.verbose:
            print("# ClassifySeqFromBlast #1: Reading BLASTn results from %s" % self._path_matches)

        csv = CSV()
        results = csv.read(self._path_matches)

        head_blast = ['qseqid', 'sseqid', 'qstart', 'qend', 'sstart', 'send', 'nident', 'pident', 'evalue', 'bitscore',
                      'score', 'mismatch', 'gapopen', 'gaps', 'qcovs', 'qcovhsp', 'slen', 'qlen', 'lenght']

        counter = 0
        for result in results:
            counter += 1
            matches.update({result[0]: result[1][4:-1]})

            if self.verbose >= 1 and counter % 200000 == 0:
                print("\r# ClassifySeqFromBlast #2: Reading matches: %d" % counter, end="")

        if self.verbose >= 1:
            print("\r# ClassifySeqFromBlast #2: Reading matches: %d" % counter)
        elif self.verbose:
            print("# ClassifySeqFromBlast #1: Found %d matches" % len(matches))

        return matches

    def __parse_sequenes(self, headers: dict, matches: dict):
        if self.verbose:
            print("# ClassifySeqFromBlast #1: Map matches to reads from %s" % self._path_rna_sequence)

        skip = False
        match: dict = {"RNA": None, "Human": None, "sequence": None}
        n_coding: int = 0
        n_non_coding: int = 0
        counter: int = 0
        writer: SequenceWriter = SequenceWriter(self._path_output_files, self.verbose)
        for line in FileIO.get_file_reader(self._path_rna_sequence):
            counter += 1

            if self.verbose >= 1 and counter % 200000 == 0:
                print("\r# ClassifySeqFromBlast #2: Checking RNA sequences: %d" % counter, end="")

            if line[0] == ">":

                if match["RNA"] is not None:
                    if writer.write_result(match):
                        n_coding += 1
                    else:
                        n_non_coding += 1

                match: dict = {"RNA": None, "Human": None, "sequence": None}
                name = line[1:].split(" ")[0]
                if name in matches:
                    match["RNA"] = name
                    match["Human"] = headers[matches[name]]
                    skip = False
                else:
                    skip = True

            elif not skip:
                match["sequence"] = line
            elif skip:
                skip = False
                continue

        if self.verbose >= 1:
            print("\r# ClassifySeqFromBlast #2: Checking RNA sequences: %d" % counter)
        elif self.verbose:
            print("# ClassifySeqFromBlast #1: Wrote %d coding and %d non-coding sequences" % (n_coding, n_non_coding))

        writer.close()
        return True