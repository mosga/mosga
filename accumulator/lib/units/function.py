from lib.units.general import GeneralObject


class Function(GeneralObject):

    id = int()
    parent_id = int()
    name = str()
    short = str()
    source = bool()
    e_value = float()
    score = float()
    identity = float()

    def __init__(self):
        pass