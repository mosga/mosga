from lib.units.basic import BasicUnit


class Repeats(BasicUnit):

    def __init__(self, uid: int = None, start: int = None, end: int = None, source: str = None, strand: str = None):
        self.id = uid
        self.start: int = start
        self.end: int = end
        self.strand = strand
        self.source: str = source

        if start is not None and end is not None:
            self.length = end - start
