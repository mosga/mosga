from lib.units.general import GeneralObject


class BasicUnit(GeneralObject):
    id = int()
    name = str()
    length = int()
    sequence = str()
    source = str()
    strand = bool()
    start = int()
    end = int()
    parent_id = int()
    view = 1
    priority = 10
    note = str()
