import subprocess
from enum import Enum

from lib.misc.file_io import FileIO
from lib.readers.format.formatreader import FormatReader

# GeneBank Flat File
class GBFFSection(Enum):
    META = 0
    FEATURE = 1
    SEQUENCE = 2
    FINISHED = 3


class GBFF(FormatReader):
    """
    Parses a GenBank Flat File with annotation and contigs.
    """

    def __init__(self):
        """
        Initiates the class and manages some options.
        """
        self.store_sequence = False
        self.has_transcripts = True

    def read(self, path: str) -> tuple:
        """
        Reads in the specific file format and yields
        :param path: The path of the file to read in.
        :return: three sections as a tuple
        """
        state = GBFFSection.META
        meta: list = []
        features: list = []
        entry: list = []
        attribute: list = []
        sequence: str = ""

        # Check for potential GenBank file without transcripts
        sub = subprocess.run(["zgrep mRNA %s" % path], shell=True, check=False, stdout=subprocess.DEVNULL)
        if sub.returncode != 0:
            self.has_transcripts = False

        for line in FileIO.get_file_reader(path):
            # There are three sections/states:
            # Meta information, features, sequences, and finishing

            # Identify current stage
            new_state, skip = self.__check_state(line, state)
            if skip:
                state = new_state
                continue

            if new_state == GBFFSection.META:
                # meta state
                self.__get_meta(line, meta)
                sequence = ""
            elif new_state == GBFFSection.FEATURE:
                # feature state
                entry, attribute = self.__get_feature(line, features, entry, attribute)
                sequence = ""
            elif new_state == GBFFSection.SEQUENCE:
                # sequence state
                if len(entry):
                    entry.append(attribute)
                    features.append(entry)

                entry = []
                attribute = []

                if self.store_sequence:
                    sequence = self.__get_sequence(line, sequence)
            elif new_state == GBFFSection.FINISHED:
                # finished section
                new_meta, new_features, new_sequence = meta, features, sequence
                meta = []
                entry = []
                attribute = []
                features = []
                new_state = GBFFSection.META
                yield new_meta, new_features, new_sequence

            state = new_state

    @staticmethod
    def __check_state(line: str, last_state: GBFFSection) -> tuple:
        """
        Tries to define the current section. If it fails just return the last state.
        :param line: the file line.
        :param last_state: the last valid state
        :return: tuple with the current state as well a boolean if the line could be ignored.
        """
        state = last_state
        skip = False

        split_stripped_words = line.lstrip().split()
        if line[0:2] == "//":
            state = GBFFSection.FINISHED
        elif line[0:6] == "CONTIG" or line[0:6] == "ORIGIN":
            state = GBFFSection.SEQUENCE
            skip = True
        elif len(split_stripped_words) and split_stripped_words[0] == "LOCUS":
            state = GBFFSection.META
        elif len(split_stripped_words) and split_stripped_words[0] == "FEATURES":
            state = GBFFSection.FEATURE
            skip = True

        # new or current state, skip this line?
        return state, skip

    @staticmethod
    def __get_meta(line: str, meta: list):
        """
        Parses the meta information into the meta variable.
        :param line: the current file line.
        :param meta: the meta variable reference.
        """
        # No Header, no entry => empty row
        if len(line) <= 13:
            return None

        # Check for meta information, added if no header is present
        new_text = line[12:].lstrip().rstrip()

        if line[0:12].isspace():
            # only empty header, add to last entry
            meta[-1][1] += "\r\n" + new_text
        else:
            # a new entry
            meta.append([line.lstrip().split()[0], new_text])

    @staticmethod
    def __get_feature(line: str, features: list, entry: list, attribute: list) -> tuple:
        """
        Extracts the current feature from the current line.
        :param line: the current file line.
        :param features: the list of features.
        :param entry:  the current entry with the attributes.
        :param attribute: the current (unfinished) attribute list for a new entry.
        :return: the new entry, the new attributes
        """
        is_new_attribute = line.lstrip()[0] == "/"
        has_header = not line[:20].isspace()

        # Add a new entry
        if has_header:
            if len(attribute):
                entry.append(attribute)
                features.append(entry)

            entries = line.lstrip().split()
            entry = [entries[0]]
            attribute = [["position", entries[1]]]
        else:
            # add new attributes, or extend the last one
            if is_new_attribute:
                # add a new attribute
                shorted_line = line.lstrip()[1:]
                delimiter_position = shorted_line.find("=")
                attribute_name = shorted_line[:delimiter_position]
                attribute_value = shorted_line[delimiter_position + 1:]
                attribute.append([attribute_name, attribute_value])
            else:
                # continue last attribute
                attribute[-1][1] += " " + line.strip()

        return entry, attribute

    @staticmethod
    def __get_sequence(line: str, sequence: str) -> str:
        """
        Concatenate the current DNA sequence.
        :param line: the current file line.
        :param sequence: the "full" sequence.
        :return: the new extended sequence.
        """
        return sequence + "".join(line.lstrip().split()[1:])
