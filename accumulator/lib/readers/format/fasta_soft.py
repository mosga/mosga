from lib.readers.format.fasta_hard import FastaHardMasking


class FastaSoftMasking(FastaHardMasking):

    def __init__(self):
        super().__init__()

    @staticmethod
    def _check_start(c: str, start: bool):
        return c.islower() and not start

    @staticmethod
    def _check_continuation(c: str, start: bool):
        return c.islower() and start

    @staticmethod
    def _check_termination(c: str, start: bool):
        return not c.islower() and start
