from enum import Enum


class ReaderFormats(Enum):
    fasta = 1
    fasta_hard = 2
    fasta_soft = 3
    blast = 4
    GFF = 5
    CSV = 6
    RepeatMaskerOutput = 7
    # GenBank Flat File
    GBFF = 8
    # EMBL Flat File
    EFF = 9
    GFF3 = 10
    MOSGADB = 11
