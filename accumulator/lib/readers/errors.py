class ReaderError(Exception):
    pass


class ReaderEmptyDictionary(ReaderError):
    pass


class ReaderNotFound(ReaderError):
    pass


class ReaderFormatError(ReaderError):
    pass


class ReaderFormatInvalid(ReaderFormatError):
    pass
