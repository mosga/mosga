from lib.readers.format.GFF import GFF
from lib.readers.gene import ReaderGene
from lib.units.gene import Gene
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.units.transcript import Transcript, TranscriptType
from lib.units.transcriptunit import TranscriptUnit


class GeneGlimmerHMM(ReaderGene):

    _formats = (ReaderFormats.GFF,)
    _software = 'GlimmerHMM'

    # TODO: TranscriptUnit
    def read(self, path: str) -> Dict[str, List[Gene]]:
        self.__has_functions = True if hasattr(self, 'functions') and type(self.functions) == list else False
        self.__functionslist = list()
        if self.__has_functions:
            for function in self.functions:
                self.__functionslist.append(function.get())

        reader = GFF('\t', '=')
        entries = reader.read(path)
        sequences = dict()
        self.__gene_id = 0
        self.__transcript_id = 0
        self.__transcript_unit_id = 0
        gene = None

        for entry in entries:
            if entry['seqname'] not in sequences.keys():
                sequences.update({entry['seqname']: list()})

            if entry['feature'] == 'mRNA':

                # Store last gene
                if gene is not None:
                    sequences[entry['seqname']].append(gene)

                self.__gene_id += 1
                gene = self.__build_gene(entry)

            elif entry['feature'] == 'CDS':
                gene = self.__set_transcript_unit(gene, entry, 'CDS')

        # Store last gene
        if gene is not None:
            sequences[entry['seqname']].append(gene)

        return sequences

    def __build_gene(self, entry: dict) -> Gene:
        entry['feature'] = 'CDS'

        gene = Gene()
        gene.name = entry['attribute']['Name']
        gene.id = self.__gene_id
        gene.strand = True if entry['strand'] == '+' else False
        gene.start = int(entry['start'])
        gene.end = int(entry['end'])
        gene.length = int(entry['end']) - int(entry['start'])
        try:
            gene.score = float(entry['score'])
        except ValueError:
            gene.score = 0.0
        gene.transcripts = list()
        gene.source = self._software

        self.__transcript_id += 1

        transcript = Transcript()
        transcript.id = self.__transcript_id
        transcript.gene_id = self.__gene_id
        transcript.start = int(entry['start'])
        transcript.end = int(entry['end'])
        transcript.name = gene.name
        transcript.source = self._software
        transcript.length = int(entry['end']) - int(entry['start'])
        transcript.functions = None
        try:
            transcript.score = float(entry['score'])
        except ValueError:
            transcript.score = 0.0
        transcript.units = list()

        # Overwrite functions if matched with function source
        if self.__has_functions:
            search = self.__append_function(transcript, self.__functionslist)
            transcript.functions = search if len(search) else None

        gene.transcripts.append(transcript)

        return gene

    def __set_transcript_unit(self, gene: Gene, entry: dict, feature: str) -> Gene:
        transcript_unit = TranscriptUnit()
        self.__transcript_unit_id += 1
        transcript_unit.id = self.__transcript_unit_id
        transcript_unit.type = TranscriptType[feature].value
        transcript_unit.start = int(entry['start'])
        transcript_unit.end = int(entry['end'])
        transcript_unit.length = int(entry['end']) - int(entry['start'])
        transcript_unit.strand = True if entry['strand'] == '+' else False
        transcript_unit.source = self._software
        transcript_unit.name = gene.name

        try:
            transcript_unit.score = float(entry['score'])
        except ValueError:
            transcript_unit.score = 0.0

        gene.transcripts[0].units.append(transcript_unit)

        return gene

    def __append_function(self, unit, functions):
        result = list()
        for i in range(len(functions)):
            new_function = self._search_function(unit.name, functions[i], self.functions[i]._software)
            if new_function is not None:
                result.append(new_function)

        return result