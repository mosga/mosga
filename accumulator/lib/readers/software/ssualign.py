from typing import Dict, List

from lib.readers.format.CSV import CSV
from lib.readers.formats import ReaderFormats
from lib.readers.rrna import ReaderrRNA
from lib.units.rrna import rRNA, rRNAsType


class rRNAssualign(ReaderrRNA):

    _formats = (ReaderFormats.CSV,)
    _software = 'ssu-align'

    def read(self, path: str) -> Dict[str, List[rRNA]]:
        csv = CSV("   ")

        sequences = dict()

        for entry in csv.read(path):
            # Remove lines starting with '#'
            if not len(entry) or (len(entry[0]) and entry[0][0] == "#"):
                continue

            splitted_list = list(filter(None, entry))
            values = [x.strip() for x in splitted_list]

            rrna = rRNA()
            if int(values[2]) > int(values[3]):
                rrna.start = int(values[3])
                rrna.end = int(values[2])
                rrna.strand = False
            else:
                rrna.start = int(values[2])
                rrna.end = int(values[3])
                rrna.strand = True
            rrna.length = rrna.end - rrna.start
            rrna.score = float(values[6])
            rrna.rrna_stype = rRNAsType.srrna_18.value
            rrna.source = self._software
            rrna.partial = False

            seq_name = values[1]

            if seq_name not in sequences.keys():
                sequences.update({seq_name: list()})

            sequences[seq_name].append(rrna)

        return sequences
