from lib.annotation import AnnotationExport
from lib.database.database import Database
from lib.database.serializer import DbSerializer
from lib.readers.formats import ReaderFormats
from lib.readers.reader import Reader
from lib.readers.software.raw import RawReaderFunctions


class MOSGADB(Reader):

    _formats = (ReaderFormats.MOSGADB, )
    _software = "MOSGA Database"

    def __init__(self, extension: ReaderFormats):
        super().__init__(extension)
        self.database = None

    def read(self, path: str):
        database = Database(self.arguments)
        database.open_database(path)
        sequences = database.parse_full_sequences(database.get_full_sequences())

        annotations: dict = {}
        # Initialize class for conflict resolution
        annotation_export = AnnotationExport(self.arguments, sequences)
        DbSerializer(database, annotation_export.collect_all_annotation(database), annotations, 1)

        return RawReaderFunctions.split_by_unit_type(annotations)
