import re

from lib.readers.format.CSV import CSV
from lib.readers.function import FunctionReader
from lib.readers.formats import ReaderFormats


class FunctionEggNog(FunctionReader):
    """
    Functional reader for EggNog protein classification.
    """

    _formats = (ReaderFormats.CSV,)
    _software = "EggNog"

    def __init__(self, extension):
        """
        Initialize the reader class with defined thresholds for names.
        :param extension: the selected ReaderFormat enum.
        """
        super().__init__(extension)
        self.__forbidden_sym: list = ["\t", "\n"]
        self.__short_bad_names: list = ["and", "start", "end", "or", "and"]
        self.__min_short_len: int = 4
        self.__max_short_len: int = 9
        self.__max_short_names: int = 5

    def read(self) -> dict:
        """
        Reads the EggNog results.
        :return: a dictionary with each protein as key and the EggNog functions as values
        """
        reader = CSV("\t")
        entries = reader.read(self.path)
        functions: dict = {}

        # This one got overwritten
        short_name: str = ""

        # For each EggNog result entry
        for entry in entries:

            # Retrieve evaluated names
            e = self.__identify_names(entry)
            if e is False:
                continue

            identifier, e_value, score, short_name, full_name, GOs, ECs, KEGGs, KEGGPs = e

            # Formats the returning dictionary value.
            try:
                value = {
                    'e_value': e_value,
                    'score': score,
                    'short': short_name,
                    'full': full_name,
                    'identity': 0.0,
                    'coverage': 0.0
                }
                #print(value)
                functions.update({identifier: value})
            except ValueError:
                continue

        return functions

    @staticmethod
    def split_attribute(attribute: str = ""):
        return attribute.split(",") if len(attribute) > 3 else []

    def __identify_names(self, e: list) -> tuple or bool:
        """
        Analyse a EggNog result line and returns processed valid results.
        :param e: a single EggNog entry line.
        :return: a tuple with the formatted values, or False if something went wrong.
        """
        short_names: list = []
        short_counter: int = 0

        # Try to read the short and full names
        try:
            # Kick out too long descriptions...
            if len(e[3]) > 200:
                if len(e[3]) and e[3] != "-":
                    e[3] = ""
                else:
                    return False

            # Remove - as empty short names
            if e[4] == "-":
                short_name = ""

            # Concat short names
            else:
                short_tmp: list = e[4].split(",")
                for s in short_tmp:
                    short_counter += 1
                    if len(s) <= self.__max_short_len and short_counter <= self.__max_short_names:
                        short_names.append(s)
                short_name = ", ".join(short_names)

            # check for parenthesis
            if len(e[3]) and e[3].count(")") == e[3].count("(") and e[3].count(")") == 1:
                # an open pair of parentheses?
                brackets_content: str = e[3][e[3].find("(")+1:e[3].find(")")]
                e[3] = re.sub("\(.*?\)", "", e[3]).strip()

                # escape EC / TC numbers and words and too long content from parenthesis
                if (brackets_content[:3] != "TC " or brackets_content[:3] != "EC ") and \
                        not brackets_content.count(" ") and \
                        self.__max_short_len >= len(brackets_content) >= self.__min_short_len:
                    if len(short_name):
                        short_name = "%s, %s" % (short_name, brackets_content.strip())
                    else:
                        short_name = brackets_content

            # remove senseless name begin
            if e[3][:1] == "-" or e[3][:1] == ")":
                e[3] = e[3][1:]

            # Filter out specific words for short names
            if len(short_name):
                short_tmp: list = short_name.split(", ")
                short_names: list = []
                for s in short_tmp:
                    if not any(map(s.__contains__, self.__short_bad_names)):
                        short_names.append(s)
                short_name: str = ", ".join(short_names)

            # Only short names? Exchange short name with full name
            if not len(e[3]) and len(e[4]):
                full_name = short_name
                short_name = ""
            else:
                full_name = e[3]

            # replace dangerous symbols
            if full_name[:15] == "Belongs to the ":
                full_name = full_name.replace("Belongs to the ", "")

            # trim the "to Saccharomyces cerevisiae" sentences
            # TODO: Include the taxonomy species browser
            if full_name[:3] == "to ":
                full_name = " ".join(full_name.split(" ")[3:]).strip()

            # replace dangerous symbols
            for sym in self.__forbidden_sym:
                full_name = full_name.replace(sym, "")
                short_name = short_name.replace(sym, "")

            # remove doubled spaces
            full_name = full_name.replace("  ", " ")

            if not len(full_name) and not len(short_name):
                return False

        except IndexError:
            return False

        try:
            return e[0], float(e[1]), float(e[2]), short_name, full_name, FunctionEggNog.split_attribute(e[5]), \
                   FunctionEggNog.split_attribute(e[6]), FunctionEggNog.split_attribute(e[7]), \
                   FunctionEggNog.split_attribute(e[8])
        except (ValueError, IndexError):
            return False
