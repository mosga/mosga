from typing import List

from lib.readers.format.GFF3 import GFF3
from lib.readers.formats import ReaderFormats
from lib.readers.software.raw import RawReaderFunctions
from lib.readers.reader import Reader
from lib.units.cdsdatabasexreference import CDSDatabaseXReference
from lib.units.centromere import Centromere
from lib.units.databasexreference import DatabaseXReference
from lib.units.function import Function
from lib.units.gene import Gene
from lib.units.genedatabasexreference import GeneDatabaseXReference
from lib.units.originofreplication import OriginOfReplication
from lib.units.repeats import Repeats
from lib.units.telomere import Telomere
from lib.units.transcript import TranscriptType, Transcript
from lib.units.transcriptdatabasexreference import TranscriptDatabaseXReference
from lib.units.transcriptunit import TranscriptUnit
from lib.units.trna import tRNA


class RefSeq(Reader):

    """
    Supports the MOSGA abstraction based on features from the GFF3 format file.
    """

    _formats = (ReaderFormats.GFF3,)
    _software = 'RefSeq'

    def __init__(self, extension: ReaderFormats):
        super().__init__(extension)
        self.__scaffold_number: int = 1
        self.__has_transcripts: bool = True
        self.report: dict = {
            "recognized": {},
            "unknown": {}
        }
        self.__elements: dict = {}

    def read(self, path: str):
        self._initialize = self.arguments["initialize"] if "initialize" in self.arguments else False
        all_units = self.__read_all_units(path)
        if "verbose" in self.arguments and self.arguments["verbose"] >= 1:
            RawReaderFunctions.format_report(self.report)
        return RawReaderFunctions.split_by_unit_type(all_units)

    def __read_all_units(self, path):
        gff3_reader = GFF3()
        gff3_elements = gff3_reader.read(path)

        # put each element according to the sequence region and uniq identifier into a single dictionary entry
        for element in gff3_elements:
            if element["seqname"] not in self.__elements:
                self.__elements.update({element["seqname"]: {}})

            if "id" not in element["attribute"]:  # no real annotation
                continue

            element.update({"assigned": False})
            self.__elements[element["seqname"]].update({element["attribute"]["id"]: element})

            # has parents? # assign them as children to the parental entry
            if "parent" in element["attribute"]:
                parent_reference = self.__elements[element["seqname"]][element["attribute"]["parent"]]["attribute"]
                if "children" not in parent_reference:
                    parent_reference.update({"children": [element]})
                else:
                    parent_reference["children"].append(element)
                # mark entry as assigned to parent
                self.__elements[element["seqname"]][element["attribute"]["id"]]["assigned"] = True

        # Remove if an element is already nested as a chil from another element
        self.__elements_nested: dict = {}
        for seq in self.__elements:
            self.__elements_nested.update({seq: {}})
            for element in self.__elements[seq]:
                if not self.__elements[seq][element]["assigned"]:
                    self.__elements_nested[seq].update({element: self.__elements[seq][element]})

        self.__elements = self.__elements_nested
        units = self.__build_units(self.__elements)

        return units

    def __build_units(self, elements):

        self._tmp_units: dict = {}
        features_units: dict = {
            "gene":                     RefSeq.__build_gene,
            "tRNA":                     RefSeq.__build_tRNA,
            "telomere":                 RefSeq.__build_telomere,
            "centromere":               RefSeq.__build_centromere,
            "origin_of_replication":    RefSeq.__build_ori,
            "long_terminal_repeat":     RefSeq.__build_repeat,
            "repeat":                   RefSeq.__build_repeat
        }

        for seq in elements:
            self._tmp_units.update({seq: []})

            for element in elements[seq]:
                feature = elements[seq][element]["feature"]

                unit_function = features_units.get(feature, None)

                if unit_function is not None:
                    # some features like tRNA a splitted inside a gene parent
                    if feature == "gene":
                        unit = unit_function(elements[seq][element], self.report)
                    else:
                        unit = unit_function(elements[seq][element])

                    if unit is not None:
                        self._tmp_units[seq].append(unit)
                self.__add_to_report(self.report, unit_function is not None, feature)
        return self._tmp_units

    @staticmethod
    def __build_gene(element, report):
        unit = None

        # encapsulated tRNA/rRNA?
        if "children" in element["attribute"]:
            # contains multiple children (transcript variants)
            if len(element["attribute"]["children"]) > 1:
                pass
            else:
                child_feature = element["attribute"]["children"][0]["feature"]
                if child_feature == "mRNA":
                    unit = RefSeq.__build_recoursive_gene(element)
                elif child_feature == "tRNA":
                    unit = RefSeq.__build_tRNA(element["attribute"]["children"][0])
                    RefSeq.__add_to_report(report, True, child_feature)
                    RefSeq.__remove_from_report(report, True, "gene")
        return unit

    @staticmethod
    def __build_recoursive_gene(element: dict):
        gene = Gene()
        gene.start = int(element["start"])
        gene.end = int(element["end"])
        gene.strand = True if element["strand"] == "+" else False
        gene.length = gene.end - gene.start
        gene.source = "RefSeq"
        gene.name = element["attribute"]["id"]
        gene.transcripts = []
        for transcript in element["attribute"]["children"]:
            gene.transcripts.append(RefSeq.__build_transcript(transcript))
        if "dbxref" in element["attribute"]:
            gene.dbxref = RefSeq.__build_dbxref(element["attribute"]["dbxref"], GeneDatabaseXReference)
        else:
            gene.dbxref = None
        return gene

    @staticmethod
    def __build_transcript(element: dict):
        trans = Transcript()
        trans.name = element["attribute"]["id"]
        trans.start = int(element["start"])
        trans.end = int(element["end"])
        trans.strand = True if element["strand"] == "+" else False
        trans.length = trans.end - trans.start
        trans.source = "RefSeq"
        trans.functions = None
        units: list = []
        for TransUnit in element["attribute"]["children"]:
            if TransUnit["feature"] == "CDS":
                units.append(RefSeq.__build_transcript_unit(TransUnit))
        trans.units = units
        if "dbxref" in element["attribute"]:
            trans.dbxref = RefSeq.__build_dbxref(element["attribute"]["dbxref"], TranscriptDatabaseXReference)
        else:
            trans.dbxref = None

        if "product" in element["attribute"]:
            new_function = Function()
            new_function.source = "RefSeq"
            new_function.name = element["attribute"]["product"]
            trans.functions = [new_function]
        else:
            trans.functions = None

        return trans

    @staticmethod
    def __build_transcript_unit(element: dict):
        unit = TranscriptUnit()
        unit.name = element["attribute"]["id"]
        unit.start = int(element["start"])
        unit.end = int(element["end"])
        unit.strand = True if element["strand"] == "+" else False
        unit.length = unit.end - unit.start
        unit.source = "RefSeq"
        unit.type = TranscriptType.CDS.value

        if "dbxref" in element["attribute"]:
            unit.dbxref = RefSeq.__build_dbxref(element["attribute"]["dbxref"], CDSDatabaseXReference)
        else:
            unit.dbxref = None

        return unit

    @staticmethod
    def __build_dbxref(dbxref_str: str, reference_class: DatabaseXReference) -> List[DatabaseXReference]:
        dbxref_list = []
        if dbxref_str.find(":"):
            for dbxref in dbxref_str.split(","):
                db, xref = dbxref.split(":")
                ref = reference_class()
                ref.database = db
                ref.source = "RefSeq"
                ref.reference = xref
                dbxref_list.append(ref)
        else:
            ref = reference_class()
            ref.database = "Unknown"
            ref.source = "RefSeq"
            ref.reference = dbxref_str
            dbxref_list.append(ref)
        return dbxref_list

    @staticmethod
    def __build_tRNA(element: dict) -> tRNA:
        trna = tRNA()
        trna.name = element["attribute"]["id"]
        trna.start = int(element["start"])
        trna.end = int(element["end"])
        trna.strand = True if element["strand"] == "+" else False
        trna.length = trna.end - trna.start
        trna.source = "RefSeq"
        try:
            trna.trna_aa = element["attribute"]["product"][4:]
        except KeyError:
            pass
        return trna

    @staticmethod
    def __build_repeat(element: dict) -> Repeats:
        rep = Repeats()
        rep.name = element["attribute"]["id"]
        rep.start = int(element["start"])
        rep.end = int(element["end"])
        rep.strand = True if element["strand"] == "+" else False
        rep.length = rep.end - rep.start
        rep.source = "RefSeq"
        return rep

    @staticmethod
    def __build_telomere(element: dict) -> Telomere:
        telo = Telomere()
        telo.start = int(element["start"])
        telo.end = int(element["end"])
        telo.length = telo.end - telo.start
        telo.strand = True if element["strand"] == "+" else False
        telo.source = "RefSeq"
        telo.note = element["attribute"]["note"] if "note" in element["attribute"] and len(element["attribute"]["note"]) else None
        return telo

    @staticmethod
    def __build_centromere(element: dict) -> Centromere:
        cen = Centromere()
        cen.start = int(element["start"])
        cen.end = int(element["end"])
        cen.length = cen.end - cen.start
        cen.strand = True if element["strand"] == "+" else False
        cen.source = "RefSeq"
        cen.note = element["attribute"]["note"] if "note" in element["attribute"] and len(element["attribute"]["note"]) else None
        return cen

    @staticmethod
    def __build_ori(element: dict) -> OriginOfReplication:
        ori = OriginOfReplication()
        ori.start = int(element["start"])
        ori.end = int(element["end"])
        ori.length = ori.end - ori.start
        ori.strand = True if element["strand"] == "+" else False
        ori.source = "RefSeq"
        ori.note = element["attribute"]["note"] if "note" in element["attribute"] and len(element["attribute"]["note"]) else None
        return ori

    @staticmethod
    def __add_to_report(report: dict, recognized: bool, name: str):
        """
        Counts the feature to unit transformation.
        :param report:
        :param recognized:
        :param name:
        """
        dict_name = 'recognized' if recognized else 'unknown'

        try:
            report[dict_name][name] += 1
        except KeyError:
            report[dict_name].update({name: 1})

    @staticmethod
    def __remove_from_report(report: dict, recognized: bool, name: str):
        dict_name = 'recognized' if recognized else 'unknown'
        report[dict_name][name] -= 1