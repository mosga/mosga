import copy
import textwrap
from typing import Dict, List

from lib.readers.format.GBFF import GBFF
from lib.readers.formats import ReaderFormats
from lib.readers.reader import Reader
from lib.readers.software.raw import RawReaderFunctions
from lib.units.basic import BasicUnit
from lib.units.cdsdatabasexreference import CDSDatabaseXReference
from lib.units.databasexreference import DatabaseXReference
from lib.units.function import Function
from lib.units.gene import Gene
from lib.units.genedatabasexreference import GeneDatabaseXReference
from lib.units.repeats import Repeats
from lib.units.transcript import TranscriptType, Transcript
from lib.units.transcriptdatabasexreference import TranscriptDatabaseXReference
from lib.units.transcriptunit import TranscriptUnit
from lib.units.trna import tRNA

# TODO: Joined feature, currently work-around split into to units.


class GBFFnative(Reader):
    """
    Supports the MOSGA abstraction based on features from the GenBank flat format file.
    """

    _formats = (ReaderFormats.GBFF,)
    _software = 'GBFF'

    def __init__(self, extension: ReaderFormats):
        super().__init__(extension)
        self.__scaffold_number = 1
        self.__has_transcripts = True
        self.report = {
            "recognized": {},
            "unknown": {}
        }

    def __read_all_units(self, path: str) -> Dict[str, List[BasicUnit]]:
        """
        Collect the already abstracted objects and puts them into sequence-based dictionaries.
        :param path: the path to the GenBank flat format file.
        :return: A dictionary with all abstracted objects.
        """
        source_file = GBFF()
        source_file.store_sequence = self._initialize
        feature_sequence: dict = {}

        for entries in source_file.read(path):
            self.__has_transcripts = source_file.has_transcripts

            # encapsulate features and meta information
            meta, units = self.__prepare_units(entries[0], entries[1])

            # define sequence key
            if "VERSION" in meta:
                sequence_key = meta["VERSION"]
            elif "ACCESSION" in meta:
                sequence_key = meta["ACCESSION"]
            elif "LOCUS" in meta:
                sequence_key = meta["LOCUS"].split(" ")[0]
            else:
                sequence_key = "sequence_%i" % self.__scaffold_number
                self.__scaffold_number += 1

            # store sequence information
            if len(entries[2]):
                self.__store_sequence(entries[2], sequence_key)

            feature_sequence.update({sequence_key: units})

        if self._initialize:
            self.__genome_file.close()

        return feature_sequence

    def read(self, path: str) -> List[Dict[str, List[BasicUnit]]]:
        """
        Calls the comprehensively reading and splits the final dictionary into multiple dictionaries based on the
        unit type.
        :param path: Path to the GenBank flat format file.
        :return: A list of dictionaries that contains the sequence identifier as key and a list of the units.
        """
        self._initialize = self.arguments["initialize"] if "initialize" in self.arguments else False
        if self._initialize:
            new_file_path = self.arguments["dir"] + "/" + self.arguments["genome"] + "/sequence"
            self.__genome_file = open(new_file_path, "w")

        all_units = self.__read_all_units(path)
        if "verbose" in self.arguments and self.arguments["verbose"] >= 1:
            RawReaderFunctions.format_report(self.report)
        return RawReaderFunctions.split_by_unit_type(all_units)

    def __store_sequence(self, sequence: str, header: str):
        self.__genome_file.write(">" + header + "\n")
        self.__genome_file.write(textwrap.fill(sequence.upper(), 80) + "\n")

    def __prepare_units(self, meta: list, features: list) -> tuple:
        """
        Organizes the format changes of the meta information and the list of features.
        :param meta: the raw meta information.
        :param features: the raw list of features.
        :return: A tuple with the formatted meta information and the unit lists based from the features.
        """
        # Reformat meta information
        new_meta: dict = self.__reformat_meta(meta)

        # Build units from features
        units_list: list = self.__build_units(features)

        return new_meta, units_list

    @staticmethod
    def __reformat_meta(meta: list) -> dict:
        """
        Reformat meta list information into accessible a meta dictionary.
        :param meta: the meta list.
        :return: The reformatted meta information dictionary.
        """
        new_meta: dict = {}
        for i in range(len(meta)):
            m = meta.pop()
            if isinstance(m, list):
                if m[0] in new_meta.keys():
                    new_meta[m[0]] = [new_meta[m[0]], m[1]]
                else:
                    new_meta[m[0]] = m[1]

        return new_meta

    def __build_units(self, features: list) -> list:
        """
        Builds the units from the encapsulated features.
        :param features: The list of encapsulated features.
        :return: A list of unit objects.
        """
        units_list: list = []
        enc_features = GBFFnative.__encapsulate_features(features, self.__has_transcripts)
        features_units = {
            "repeat_region": GBFFnative.__build_repeating_region,
            "trna-coding-gene": GBFFnative.__build_trna,
            "protein-coding-gene": GBFFnative.__build_gene,
            "protein-coding-gene-wo-trans": GBFFnative.__build_gene_wo_trans
        }

        for unit_iterator in range(len(enc_features)):
            feature = enc_features.pop()

            multiplied_features: list = GBFFnative.__split_joined_features(feature)

            for feature in multiplied_features:
                unit_function = features_units.get(feature[0], None)

                if unit_function is not None:
                    new_feature = unit_function(feature)

                    if new_feature is not None:
                        units_list.append(unit_function(feature))

                GBFFnative.__add_to_report(self.report, unit_function is not None, feature[0])

        return units_list

    @staticmethod
    def __add_to_report(report: dict, recognized: bool, name: str):
        """
        Counts the feature to unit transformation.
        :param report:
        :param recognized:
        :param name:
        """
        dict_name = 'recognized' if recognized else 'unknown'

        try:
            report[dict_name][name] += 1
        except KeyError:
            report[dict_name].update({name: 1})

    @staticmethod
    def __add_or_append(feature: list, included_feature: list):
        """
        Encapsulates the second feature into the first one.
        :param feature: The feature that should inside contain a new feature.
        :param included_feature: The new features that should be integrated.
        :return: the encapsulated feature.
        """
        try:
            feature[2].append(included_feature)
        except IndexError:
            feature.append([included_feature])
        return feature

    @staticmethod
    def __encapsulate_features(features: list, has_transcripts: bool = True) -> list:
        """
        Organizes how features should be encapsulated and calls the process.
        :param features: the list of raw features
        :return: the list of encapsulated features.
        """
        features.reverse()
        temp_list: list = []

        for i in range(len(features)):
            # Reformat second entry to dictionary
            feature = GBFFnative.__make_feature_dict(features.pop())

            # Add into the new encapsulated list
            if i:
                last = temp_list[-1]
                if has_transcripts:
                    if feature[0] == "mRNA" and last[0] == "gene":
                        GBFFnative.__add_or_append(last, feature)
                        last[0] = "protein-coding-gene"
                        continue
                    elif feature[0] == "CDS" and last[0] == "protein-coding-gene" and last[2][0][0] == "mRNA":
                        GBFFnative.__add_or_append(last[2][0], feature)
                        continue
                    elif feature[0] == "tRNA" and last[0] == "gene":
                        last[0] = "trna-coding-gene"
                        GBFFnative.__add_or_append(last, feature)
                        continue
                elif not has_transcripts:
                    if feature[0] == "CDS" and last[0] == "gene":
                        last[0] = "protein-coding-gene-wo-trans"
                        GBFFnative.__add_or_append(last, feature)

            temp_list.append(feature)

        return temp_list

    @staticmethod
    def __make_feature_dict(feature: list) -> list:
        """
        Restructures the native feature list into a new feature list that contains lists for duplicated features
        information.
        :param feature: the original feature list.
        :return: the new feature list.
        """
        feature_values: dict = {}
        for i in range(len(feature[1])):
            value = feature[1].pop()

            if len(value) != 2:
                continue

            if value[0] in feature_values.keys():
                # make list instead of overwriting values
                if isinstance(feature_values[value[0]], list):
                    # is a list, append new value
                    feature_values[value[0]].append(value[1])
                else:
                    # make a new list
                    feature_values[value[0]] = [feature_values[value[0]], value[1]]
            else:
                # not existing, new update
                feature_values.update({value[0]: value[1]})

        feature[1] = feature_values
        return feature

    @staticmethod
    def __build_repeating_region(feature: list) -> Repeats:
        """
        Builds the Repeats object from a feature.
        :param feature: the repeat feature dictionary.
        :return: The Repeats object.
        """
        pos = GBFFnative.__get_position(feature[1]['position'])
        feature = Repeats(start=pos[0], end=pos[1], strand=not pos[2], source="Import")
        return feature

    @staticmethod
    def __initialize_gene(feature: dict) -> Gene:
        """
        Builds a Gene object from a feature.
        :param feature: the gene feature dictionary.
        :return: the Gene object.
        """
        pos = GBFFnative.__get_position(feature["position"])
        gene = Gene()
        gene.start = pos[0]
        gene.end = pos[1]
        gene.strand = not pos[2]
        gene.source = "Import"
        gene.dbxref = GBFFnative.__add_dbxref(feature, GeneDatabaseXReference) if "db_xref" in feature else None
        gene.name = feature["gene"][1:-1] if "gene" in feature else None

        return gene

    @staticmethod
    def __initialize_trans_from_gene(gene: Gene, feature: dict = {}) -> Transcript:
        """
        Builds a Transcript object from a Gene object (+ feature)
        :param gene: the required Gene object.
        :param feature: an optional feature dictionary.
        :return: the Transcript object.
        """
        trans = Transcript()
        trans.start = gene.start
        trans.end = gene.end
        trans.strand = gene.strand
        trans.source = "Import"
        if len(feature):
            trans.dbxref = GBFFnative.__add_dbxref(feature, TranscriptDatabaseXReference) if "db_xref" in feature else None
            trans.transcript_id = feature["transcript_id"][1:-1] if "transcript_id" in feature else None
            trans.gene = gene.name if gene.name is not None and len(gene.name) else None
        return trans

    @staticmethod
    def __initialize_unit_from_cds(feature: dict) -> TranscriptUnit:
        """
        Builds a CDS-TranscriptUnit from a feature.
        :param feature: the feature dictionary.
        :return: the TranscriptUnit object.
        """
        pos = GBFFnative.__get_position(feature["position"])
        cds = TranscriptUnit()
        cds.start = pos[0]
        cds.end = pos[1]
        cds.strand = not pos[2]
        cds.source = "Import"
        cds.type = TranscriptType.CDS.value
        cds.dbxref = GBFFnative.__add_dbxref(feature, CDSDatabaseXReference) if "db_xref" in feature else None
        cds.protein_id = feature["protein_id"][1:-1] if "protein_id" in feature else None
        cds.gene = feature["gene"][1:-1] if "gene" in feature else None
        return cds

    @staticmethod
    def __initialize_function_from_cds(feature: dict) -> Function:
        """
        Initialize Function object from a CDS-TranscriptUnit.
        :param feature: the CDS feature dictionary.
        :return: the Function object:
        """
        function = Function()
        function.name = feature["product"][1:-1]
        function.source = "Import"
        return function

    @staticmethod
    def __add_dbxref(feature: dict, class_type: object) -> List[DatabaseXReference]:
        """
        Builds a desired DatabaseXReference object from a feature dictionary, defined by class_type.
        :param feature: any feature dictionary.
        :param class_type: the object class, that should be returned.
        :return: the DatabaseXReference object, as a class of class_type
        """
        res = []
        dbxref = [feature["db_xref"]] if type(feature["db_xref"]) is not list else feature["db_xref"]
        for ref in dbxref:
            database, reference = ref[1:-1].split(":")
            xref = class_type()
            xref.database = database
            xref.reference = reference
            xref.source = "GBFF"
            res.append(xref)
        return res

    @staticmethod
    def __build_gene(feature: list) -> Gene or None:
        """
        Builds the Gene object from a feature.
        :param feature: The feature that contains all necessary information for protein-coding genes.
        :return: The Gene object.
        """
        gene: Gene = GBFFnative.__initialize_gene(feature[1])
        new_transcript = None
        transcript_units = []
        functions = []

        try:
            if len(feature[2]):
                for mrnas in feature[2]:
                    if mrnas[0] == "mRNA":
                        functions: list = []
                        transcript_units: list = []
                        new_transcript: Transcript or None = GBFFnative.__initialize_trans_from_gene(gene, mrnas[1])

                        if mrnas[2][0][0] == "CDS":
                            for cds in GBFFnative.__split_joined_features(mrnas[2][0]):
                                new_cds: TranscriptUnit = GBFFnative.__initialize_unit_from_cds(cds[1])

                                if "product" in cds[1]:
                                    function: Function = GBFFnative.__initialize_function_from_cds(cds[1])
                                    functions.append(function)

                            transcript_units.append(new_cds)

                if len(transcript_units):
                    new_transcript.units = transcript_units

                if len(functions):
                    functions = list(dict.fromkeys(functions))
                    new_transcript.functions = functions

        except IndexError:
            pass
            # TODO: ncRNA

        if new_transcript is not None:
            gene.transcripts = [new_transcript]

        if not len(gene.transcripts):
            return None

        return gene

    @staticmethod
    def __build_gene_wo_trans(feature: list) -> Gene or None:
        """
        Builds the Gene object from a feature without given transcripts/mRNA.
        :param feature: The feature that contains all necessary information for protein-coding genes.
        :return: The Gene object.
        """
        gene: Gene = GBFFnative.__initialize_gene(feature[1])
        new_transcript = None

        try:
            if len(feature[2]):
                functions: list = []
                transcript_units: list = []
                new_transcript: Transcript or None = GBFFnative.__initialize_trans_from_gene(gene, feature)

                for CDSS in feature[2]:
                    if CDSS[0] == "CDS":
                        for cds in GBFFnative.__split_joined_features(CDSS):
                            new_cds: TranscriptUnit = GBFFnative.__initialize_unit_from_cds(cds[1])

                            if "product" in cds[1]:
                                function: Function = GBFFnative.__initialize_function_from_cds(cds[1])
                                functions.append(function)

                            transcript_units.append(new_cds)

                if len(transcript_units):
                    new_transcript.units = transcript_units

                if len(functions):
                    functions = list(dict.fromkeys(functions))
                    new_transcript.functions = functions

        except IndexError:
            pass

        if new_transcript is not None:
            gene.transcripts = [new_transcript]

        if not len(gene.transcripts):
            return None

        return gene

    @staticmethod
    def __build_trna(feature: list) -> tRNA:
        """
        Builds tRNA objects from a feature.
        :param feature: The feature that contains all information.
        :return: The tRNA object.
        """
        trna = tRNA()
        pos = GBFFnative.__get_position(feature[1]["position"])

        if "product" in feature[2][0][1]:
            product = feature[2][0][1]["product"][1:-1]
            if "tRNA-" in product:
                product = product[5:]
            trna.trna_aa = product
        elif "pseudo" in feature[2][0][1]:
            trna.trna_aa = "Pseudo"

        trna.source = "Import"
        trna.start = pos[0]
        trna.end = pos[1]
        trna.strand = not pos[2]
        return trna

    @staticmethod
    def __split_joined_features(feature: list) -> list:
        """
        Copies joined features into multiple features.
        :param feature: The feature that includes the position.
        :return: The new position list.
        """
        features: list = []

        # check for joins
        positions = GBFFnative.__get_splitted_positions(feature[1]['position'])
        if len(positions):
            # create deep-copy and joins
            for p in positions:
                new_feature = copy.deepcopy(feature)
                new_feature[1]['position'] = p
                features.append(new_feature)
        else:
            features.append(feature)

        return features

    @staticmethod
    def __get_splitted_positions(pos: str) -> List[str]:
        """
        Gets the splitted positions into a list of positions instead of a string.
        :param pos: the joined positions string.
        :return: the list of positions.
        """
        new_positions: list = []
        has_complement = False

        if pos.find("join(") >= 0:
            position = pos

            if pos.find("complement(") >= 0:
                has_complement = True
                position = pos[pos.find("complement(")+len("complement("):-1]

            position = position[position.find("join(")+len("join("):-1]
            new_positions = position.split(",")

            if has_complement:
                new_positions = ["complement(" + np + ")" for np in new_positions]

        return new_positions

    @staticmethod
    def __get_position(position: str) -> tuple:
        """
        Get the position as integers as well the orientation and fuzziness of the start and end positions.
        :param position: the position string.
        :return: A tuple with the position information.
        """
        is_complement = False
        fuzzy_start = False
        fuzzy_end = False

        if position.find("complement") >= 0:
            position = position[position.find("complement")+len("complement("):-1]
            is_complement = True

        parts = position.split("..")
        if parts[0].isnumeric():
            start = int(parts[0])
        else:
            start = int(parts[0][1:])
            fuzzy_start = True

        if parts[1].isnumeric():
            end = int(parts[1])
        else:
            end = int(parts[1][1:])
            fuzzy_end = True

        return start, end, is_complement, fuzzy_start, fuzzy_end
