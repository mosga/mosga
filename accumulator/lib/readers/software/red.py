from lib.readers.format.CSV import CSV
from lib.units.repeats import Repeats
from lib.readers.formats import ReaderFormats
from typing import List, Dict

from lib.readers.repeats import ReaderRepeats


class RepeatsRed(ReaderRepeats):

    _formats = (ReaderFormats.CSV,)
    _software = 'Red'

    def read(self, path: str) -> Dict[str, List[Repeats]]:
        r = CSV()
        new_sequence: dict = {}
        new_id = 1

        for entry in r.read(path):
            # since red starts counting from 0 we have to shift them to 1
            new_rep = Repeats(new_id, int(entry[1]) + 1, int(entry[2]), self._software)
            new_id += 1
            seq_key = entry[0]

            try:
                new_sequence[seq_key].append(new_rep)
            except KeyError:
                new_sequence.update({seq_key: [new_rep]})

        return new_sequence
