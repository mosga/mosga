import re

from lib.readers.format.CSV import CSV
from lib.readers.function import FunctionReader
from lib.readers.formats import ReaderFormats


class FunctionIDTAXA(FunctionReader):

    _formats = (ReaderFormats.CSV,)
    _software = "IDTAXA"

    def __init__(self, extension):
        super().__init__(extension)
        self._min_level = 4
        self._max_short_name_len = 9

    def read(self):
        reader = CSV("\t")
        entries = reader.read(self.path)
        functions = dict()
        collection: dict = {}

        # Resorting entries according the genes
        for e in entries:
            try:
                collection[e[0][:-2]].append(e[1:])
            except KeyError:
                collection.update({e[0][:-2]: [e[1:]]})

        for c in collection.keys():
            if int(collection[c][-1][0]) >= self._min_level:
                short: str = ""
                full: str = ""
                ecs: list = ""
                ec_regex = re.compile('^[E]\d.\d.')

                func = collection[c][-1]
                func_split = func[2].split("  ")
                kegg = func_split[0]

                try:
                    name_attribute = func_split[1]
                except IndexError:
                    continue

                if name_attribute.count("[EC:") == 1:
                    temp_ec = name_attribute[name_attribute.find("[EC:")+4:name_attribute.rfind("]")]
                    ecs = temp_ec.split(" ")
                    name_attribute = name_attribute[:name_attribute.find("[EC:")].rstrip()

                name_splitted = name_attribute.split(",")
                name_splitted = list(map(lambda x: x.strip(), name_splitted))
                name_splitted = list([i for i in name_splitted if not ec_regex.match(i)])

                full = ", ".join(name_splitted)

                if len(name_splitted) == 2:
                    short = name_splitted[0]
                    full = name_splitted[1]

                    if ec_regex.match(name_splitted[0]):
                        short = ""
                else:
                    # lets walk, looking for the first longest name
                    match_pos = 0
                    for i in range(0, len(name_splitted)):
                        if len(name_splitted[i]) >= self._max_short_name_len:
                            match_pos = i
                            break

                    # build short and full names after the walk
                    try:
                        if match_pos > 0:
                            short = ", ".join(name_splitted[:match_pos])
                            # preserve white-spaces
                            full = ", ".join(name_splitted[match_pos:])
                    except IndexError as e:
                        pass

                # Confidence -> Score
                values = {
                    'e_value': 0.0,
                    'score': round(float(func[1]), 2),
                    'short': short,
                    'full': full,
                    'identity': 0.0,
                    'coverage': 0.0
                }
                functions.update({c: values})

        return functions
