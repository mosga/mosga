from lib.units.basic import BasicUnit

from abc import ABC, abstractmethod
from typing import List, Dict

from lib.readers.errors import ReaderFormatInvalid
from lib.readers.formats import ReaderFormats


class Reader(ABC):

    _formats = ()

    def __init__(self, extension: ReaderFormats):
        self._extension = extension
        if extension not in self._formats:
            err_msg = "Unsupported ReaderFormat: " + str(extension) + " in " + str(type(self))
            raise ReaderFormatInvalid(err_msg)

    @abstractmethod
    def read(self, path: str) -> Dict[str, List[BasicUnit]]:
        pass
