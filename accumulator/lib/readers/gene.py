from lib.units.gene import Gene
from lib.units.function import Function
from lib.readers.reader import Reader
from typing import List, Dict


class ReaderGene(Reader):

    def read(self, path: str) -> Dict[str, List[Gene]]:
        pass

    @staticmethod
    def _gene__analyzer(sequences):
        count = 0
        for key in list(sequences.keys()):
            count += 1
            seq = sequences[key]
            for gene in seq:
                print("Gene: " + str(gene.id) + ' ' + gene.name)
                for trans in gene.transcripts:
                    print(" Transcript: " + str(trans.id))
                    for tu in trans.units:
                        print("   Unit: " + str(tu.id) + ' ' + str(tu.type))

    @staticmethod
    def _search_function(name: str, function_dictionary: dict, source: str):
        try:
            entry = function_dictionary.pop(name)
            func = Function()
            func.id = 1
            func.name = entry['full']
            func.short = entry['short']
            func.score = entry['score']
            func.source = source
            func.e_value = entry['e_value']
            func.identity = entry['identity']
            return func
        except KeyError:
            return None
