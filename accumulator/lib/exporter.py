from argparse import ArgumentParser
from importlib import import_module
from lib.analysis.gc_content import GCcontent, GCcontentError
from lib.analysis.organelle import OrganelleScaffoldScan
from lib.annotation import AnnotationExport
from lib.database.database import Database
from lib.writers.errors import WriterNotFound, WriterImport
from lib.writers.writer import GenomeWriter
import timeit


class Exporter(object):

    def __init__(self, database: Database, args: ArgumentParser):
        """
        Initialize the database export
        :param args: arguments object
        :param database: database reference
        """
        self._args = args
        self._db = database
        self._scaffold_length = dict()
        self._sequences = self._db.get_sequences(False)
        self._time_track = False
        self._start_time = timeit.default_timer()

        self.verbose = self._args.verbose
        self.annotations: dict = {}

        # Get writer information
        self._writers, self._outputs, self._output_file_extensions = self.get_selected_writers(self._args.writer,
                                                                                                  self._args.output)
        self.split_params = {"dir": "split", "extension": ".fa"}

        # Get scaffold length from database as fallback
        if int(self._db.get_database_attribute("version")) and int(self._db.get_database_attribute("initialized")):
            scs = self._db.get_scaffold_length()
            self._scaffold_length = scs[0]
            if scs[1]:
                self._sequences = self._db.get_sequences(False)
        else:
            # TODO: Check scaffold lengths
            self._scaffold_length = self._get_scaffold_length_awk("%s%s" % (self._args.genome_file, ".sl.gz"))
        self._scaffold_smt_ids: dict = self._create_scaffold_smt_ids(self._sequences.keys())

        # Analyze GC content
        gc_data: dict = self._db.parse_full_sequences(self._db.get_full_sequences())
        self.gc = None
        try:
            self.gc = GCcontent(gc_data).get_results()
        except GCcontentError:
            pass

        # Organelle scan initialized
        if self._args.organelle_indication or self._args.odna:
            self.organelle_files = OrganelleScaffoldScan.scan_files(self._args.directory, self._args.genome, True)

        # Initialize class for conflict resolution
        self._annotation_export = AnnotationExport(args, self._sequences)
        self._stats = self._annotation_export.get_stats()

    @staticmethod
    def get_writer_class(args, scaffold_length, class_name: str, annotations: dict, interfix: str = None) -> GenomeWriter:
        """
        Select and initialize the genome writer class
        :param class_name: the selected genome writer class (Sequin/GFF)
        :return: return the selected initialized genome writer
        """
        try:
            path = 'lib.writers.' + class_name.lower() + '.' + class_name.lower()
            writer = getattr(import_module(path), class_name)
            output = writer(annotations, args, scaffold_length, interfix)
        except ModuleNotFoundError as e:
            err_msg = '%s: Writer class file was not found' % e
            raise WriterNotFound(err_msg)
        except AttributeError as e:
            err_msg = "Writer class '%s' not found under %s" % (class_name, e)
            raise WriterImport(err_msg)

        return output

    def get_units(self) -> dict:
        """
        Returns annotation dictionary
        :return: annotation dictionary
        """
        return self.annotations

    def get_selected_writers(self, writer: str, output: str) -> tuple:
        """
        Parses the writer and output arguments
        :param writer: passed writer argument.
        :param output: passed output argument.
        :return: a tuple with all writers, outputs and output file extensions.
        """
        extensions: list = []

        if writer.find(",") >= 0 and output.find(",") >= 0 \
                and len(writer.split(",")) == len(output.split(",")):
            writers = writer.split(",")
            outputs = output.split(",")
        else:
            writers = [writer]
            outputs = [output]

        for i in range(0, len(writers)):
            writer = writers[i]
            try:
                extensions.append(self.get_writer_class(self._args, self._scaffold_length, writer, {}).file_extension)
            except AttributeError:
                pass

        return writers, outputs, extensions

    def _report(self):
        """
        Simply prints the output from some collected numbers.
        """
        if self.verbose:
            for name in self._stats:
                print(name + ': ' + str(self._stats[name]))

    @staticmethod
    def chunks(lst, n):
        """Yield successive n-sized chunks from lst."""
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    @staticmethod
    def _create_scaffold_smt_ids(sequences) -> dict:
        """
        Create serialized ids for multithreaded write process.
        :param sequences: the original sequence names.
        :return: a dictionary with the original scaffold name as key and the serialized id as value.
        """
        scaffold_ids: dict = {}
        sc_prefix_len = len(str(len(sequences)))
        prefix = '{:0' + str(sc_prefix_len) + '}'
        identifier = 1
        for scaffold in sequences:
            scaffold_ids.update({scaffold: prefix.format(identifier)})
            identifier += 1
        return scaffold_ids

    @staticmethod
    def _clear_annotations(annotations: dict):
        """
        Checks for scaffolds which contains only Nones and removes them
        :param annotations: the annotations dictionary that should be cleared.
        """
        none_list: list = []
        for scaffold in annotations.keys():
            nones: int = 0
            for entry in annotations[scaffold]:
                if entry is None:
                    nones += 1
            if nones == len(annotations[scaffold]):
                none_list.append(scaffold)

        for n in none_list:
            annotations.pop(n, None)

    @staticmethod
    def _fasta_parser(file_path: str) -> list:
        """
        Collects the sequence identifier from FASTA files
        :param file_path: the fasta file path
        :return: a list of sequences identifier
        """
        sequences = list()

        file_pointer = open(file_path, 'r')
        for line in file_pointer:
            if line[0] == '>':
                sequences.append(line[1:].rstrip())
        file_pointer.close()

        return sequences

    @staticmethod
    def _get_scaffold_length_awk(awk_path: str) -> dict:
        """
        Reads the scaffold length from awk prepared file as a fallback.
        :param awk_path: file path to awk exported file with scaffold lenghts
        :return: dictionary with the scaffold names as key and sequence length as value
        """
        scaffolds: dict = {}
        from lib.misc.file_io import FileIO
        last_id = None
        try:
            for line in FileIO.get_file_reader(awk_path):
                if line[0] != ">":
                    if last_id is not None:
                        scaffolds.update({last_id: int(line)})
                else:
                    last_id = line[1:]
        except FileNotFoundError:
            return {}
        return scaffolds

    @staticmethod
    def output_files(directory: str, outputs: list, extensions: list, subdir: str = "writers"):
        from os import listdir
        from os.path import isfile, join

        writers_dir = "%s/%s" % (directory, subdir)
        output_buckets: list = []
        files = [f for f in listdir(writers_dir) if isfile(join(writers_dir, f))]
        files.sort(reverse=True)

        for out in outputs:
            output_buckets.append([out, []])

        # Check if amount of output files and file extensions are fitting
        if not (len(files) % len(outputs)) and not (len(files) % len(extensions)):
            for n in range(0, len(files)):
                file = files.pop()
                for i in range(0, len(extensions)):
                    if file[-(len(extensions[i])+1):] == "." + extensions[i]:
                        output_buckets[i][1].append("%s/%s" % (writers_dir, file))

        return output_buckets
