from argparse import ArgumentParser
from typing import TextIO, Dict, List

from lib.units.basic import BasicUnit
from lib.units.cpgisland import CpGIsland
from lib.units.gene import Gene
from lib.units.repeats import Repeats
from lib.units.rrna import rRNAsType, rRNA
from lib.units.transcript import TranscriptType
from lib.units.trna import tRNA
from lib.writers.writer import GenomeWriter


class GFF(GenomeWriter):
    """
    Writes GFF3 compliant files.
    """

    def __init__(self, units: Dict[str, List[BasicUnit]], args: ArgumentParser, sequences: dict, interfix: str = None):
        """
        Initialize the GFF writing process for a given dictionary with units.
        :param units: a dictionary with all units (sequences as keys).
        :param args: execution arguments.
        :param sequences: dictionary with sequence identifier as keys and sequence information
        :param interfix: a counting number that is placed between the names. It's necessary for multithreaded writing.
        """
        super().__init__(units, args, sequences, interfix)
        self.file_extension: str = "gff"
        self.only_ncbi_accepted_units = True

        # Enable short names
        self._short_ids = False
        try:
            self._short_ids = args.short_output_ids
        except AttributeError:
            pass

        # Associate units with writing functions
        self.translate_units = dict({
            'Repeats': self.__get_repeat,
            'Gene': self.__get_gene,
            'tRNA': self.__get_trna,
            'rRNA': self.__get_rrna,
            'CpGIsland': self.__get_cpgisland,
            'QCinternalNs': self.__get_assembly_gap,
        })

    def write(self, path: str) -> bool:
        """
        Coordinates the writing with the head file and the file pointer.
        :param path: the target writing path.
        :return:
        """
        with open(path, 'w') as fp:
            if not self._smt:
                fp.write("##gff-version 3\r\n")
                fp.write("##MOSGA GFF Output\r\n")
            for sequence_identifier in self._units:

                # Skip empty sequences
                if not len(sequence_identifier):
                    continue

                self.sequence_identifier = sequence_identifier
                # Loop through each unit in the dictionary.
                for element in self._units[sequence_identifier]:

                    # Skip empty entries
                    if element is None:
                        continue

                    # Call writing functions
                    self.__write_element(fp, element, sequence_identifier)

                    # Increase the counter
                    self._element_counter += 1
        return True

    def __write_element(self, fp: TextIO, element: BasicUnit, seq_id: str):
        """
        Triggers the line writing with the elements corresponding unit type writing function.
        :param fp: the file pointer.
        :param element: the current element.
        :param seq_id: the sequence id for this element.
        """
        # Call the corresponding writing function for that unit type
        try:
            write_type = self.translate_units[str(element)]
            lines = write_type(element, seq_id)
        except KeyError:
            print("Unknown GFF Feature: %s" % str(element))
            lines = None

        # Perform the file pointer writing if result is not empty
        if lines is not None:
            fp.write(lines)

            # Add the writing to the stats
            self._add_counter_stats(seq_id, str(element))

    def __get_gene(self, element: Gene, seq_id: str) -> str:
        """
        Formats the GFF3 gene lines
        :param element: the current Gene.
        :param seq_id: the current sequence id.
        :return:
        """

        # initialize important variables
        locus_number = self._get_locus_number()
        locus_gene_tag = self._get_locus_tag(locus_number)
        transcript_nr = 1
        transcript_bucket: list = []
        transcript_func_bucket: list = []
        cds_bucket: dict = {}

        head = seq_id + "\t" + element.source + "\t"

        # Gene ID format
        if self._short_ids:
            gene_id = "g.%s%d" % (self.interfix, locus_number)
        else:
            gene_id = locus_gene_tag

        # Preliminary gene line
        gene_line = (head + "gene\t%s\t%s\t%s\t%s\t.\tID=%s;Name=%s;" %
                     (element.start, element.end, element.score, self.fmt_strand(element.strand), gene_id, gene_id))

        for transcript in element.transcripts:
            trans_score = "."
            trans_functions: str = ""

            self._add_length_stats(self.sequence_identifier, "transcript", transcript.length)

            # Check for transcript functions
            try:
                trans_functions: list = transcript.functions
            except (AttributeError, TypeError) as e:
                pass

            # Transcript ID format
            if self._short_ids:
                transcript_id = "%st.%s" % (gene_id, transcript_nr)
            else:
                transcript_id = gene_id + '.' + str(transcript_nr)

            # Check for a valid transcript score
            try:
                trans_score = "." if transcript.score is None else str(transcript.score)
            except AttributeError:
                pass

            # Add preliminary transcript lines to bucket
            transcript_bucket.append("%smRNA\t%d\t%d\t%s\t%s\t0\tID=%s;Parent=%s;" % \
                                     (head, transcript.start, transcript.end, trans_score,
                                      self.fmt_strand(element.strand), transcript_id,
                                      gene_id))
            # Add transcript functions to a bucket
            transcript_func_bucket.append(trans_functions)

            # Reverse the units order for reverse transcripts
            if not transcript.strand:
                transcript.units.reverse()

            # Loop through all units in transcript
            unit_nr = 1
            for unit in transcript.units:
                unit.score = "."

                # Unit ID format
                if self._short_ids:
                    unit_id = "%sc.%s" % (transcript_id, unit_nr)
                else:
                    unit_id = self.protein_prefix + self.prefix_number.format(locus_number) + '.' + str(transcript_nr) + '.' + str(unit_nr)

                # Check for a valid unit score
                try:
                    unit_score = "." if unit.score is None else str(unit.score)
                except AttributeError:
                    pass

                # Only CDS right now
                if TranscriptType['CDS'].value == unit.type:
                    self._add_length_stats(self.sequence_identifier, "CDS", unit.length)
                    cds_line: str = "%sCDS\t%d\t%d\t%s\t%s\t1\tID=%s;Parent=%s;" %\
                                    (head, unit.start, unit.end, unit.score, self.fmt_strand(unit.strand),
                                              unit_id, transcript_id)
                    if transcript_nr not in cds_bucket:
                        cds_bucket.update({transcript_nr: [cds_line]})
                    else:
                        cds_bucket[transcript_nr].append(cds_line)
                    unit_nr += 1

            transcript_nr += 1

        # Assemble gene and transcripts with their functions
        out: str = gene_line

        out_inner: str = ""

        # Select functions to the gene line
        if len(transcript_bucket) > 1:
            functions = self.__format_functions(self.__unify_functions(transcript_func_bucket))
        elif len(transcript_bucket) > 0:
            functions = self.__format_functions(transcript_func_bucket[0])
        else:
            return "%s\r\n" % out

        # Add nested transcripts and CDS to container
        for t in range(0, len(transcript_bucket)):
            out_inner += "%s%s\r\n" % (transcript_bucket[t], self.__format_functions(transcript_func_bucket[t]))

            if len(cds_bucket):
                for cds in cds_bucket[t+1]:
                    out_inner += "%s\r\n" % cds

        # Assemble all pieces together
        out += "%s\r\n" % functions
        out += "%s" % out_inner

        return out

    def __get_repeat(self, element: Repeats, seq_id: str) -> str:
        locus_id = self._get_locus_number()
        locus_tag = self._get_locus_tag(locus_id)
        if self._short_ids:
            locus_tag = "r.%s%d" % (self.interfix, locus_id)

        try:
            result = "%s\t%s\trepeat\t%d\t%d\t.\t.\t.\tID=%s;\r\n" % \
                     (seq_id, element.source, element.start, element.end, locus_tag)
            return result
        except (KeyError, AttributeError):
            return ""

    def __get_cpgisland(self, element: CpGIsland, seq_id: str) -> str:
        locus_id = self._get_locus_number()
        locus_tag = self._get_locus_tag(locus_id)
        if self._short_ids:
            locus_tag = "cg.%s%d" % (self.interfix, locus_id)

        try:
            result = "%s\t%s\tCpG_island\t%d\t%d\t.\t.\t.\tID=%s;\r\n" % \
                     (seq_id, element.source, element.start, element.end, locus_tag)
            return result
        except (KeyError, AttributeError):
            return ""

    def __get_trna(self, element: tRNA, seq_id: str) -> str:
        locus_id = self._get_locus_number()

        if self._short_ids:
            locus_tag = "trna_%s%d" % (self.interfix, locus_id)
        else:
            locus_tag = self._get_locus_tag(locus_id)

        try:
            result = "%s\t%s\ttRNA\t%d\t%d\t%s\t%s\t.\tID=%s;Note=tRNA-%s;\r\n" % \
                     (seq_id, element.source, element.start, element.end, element.score, self.fmt_strand(element.strand)
                      , locus_tag, element.trna_aa)
            return result
        except (KeyError, AttributeError):
            return ""

    def __get_rrna(self, element: rRNA, seq_id: str) -> str:
        locus_id = self._get_locus_number()

        if self._short_ids:
            locus_tag = "rrna_%s%d" % (self.interfix, locus_id)
        else:
            locus_tag = self._get_locus_tag(locus_id)

        try:
            result = "%s\t%s\trRNA\t%d\t%d\t%s\t%s\t.\tID=%s;Note=%s;\r\n" % \
                     (seq_id, element.source, element.start, element.end, element.score, self.fmt_strand(element.strand)
                      , locus_tag, self.map_rrna(rRNAsType(element.rrna_stype).name))
            return result
        except (KeyError, AttributeError):
            return ""

    def __get_assembly_gap(self, element: object, seq_id: str) -> None:
        """
        Dummy functions doing nothing.
        :param element: an element.
        :param seq_id:  the sequence id.
        :return: none.
        """
        return None

    @staticmethod
    def map_rrna(entry: str) -> str:
        """
        Maps a sRNA type to a formatted string.
        :param entry: the sRNA type by the enum.
        :return: a formatted string.
        """
        map_rrna = {
            "srrna_0": "undefined rRNA",
            "srrna_5": "5S rRNA",
            "srrna_5_8": "5.8S rRNA",
            "srrna_18": "18S rRNA",
            "srrna_28": "28S rRNA",
            "srrna_40": "40S rRNA",
            "srrna_60": "60S rRNA",
            "srrna_80": "80S rRNA",
            "srrna_12": "12S rRNA",
            "srrna_16": "16S rRNA",
            "srrna_23": "23S rRNA"
        }
        try:
            return map_rrna[entry]
        except KeyError:
            return "undefined rRNA"

    @staticmethod
    def __unify_functions(functions: list) -> list:
        """
        Unifies a list of functions by remove redundant functions.
        :param functions: the complete list of functions.
        :return: a unified list of functions.
        """
        if not len(functions):
            return []

        flatten_functions: list = []
        new_functions: list = []
        names: list = []

        for function_list in functions:
            for function in function_list:
                flatten_functions.append(function)
                if function.name not in names:
                    names.append(function.name)

        for i in range(0, len(names)):
            for f in flatten_functions:
                if f.name == names[i]:
                    new_functions.append(f)
                    break

        return new_functions

    @staticmethod
    def __format_functions(functions: list) -> str:
        """
        Formats a GFF3 compliant string for the given functions.
        :param functions: the list of functions.
        :return: a formatted string.
        """
        function_string = ''
        forbidden_sym: list = [";", "\t", "=", "\n"]

        try:
            functions_list = []
            if functions is not None and len(functions):
                function_string = 'Note='

                for function in functions:
                    # replace dangerous symbols
                    for sym in forbidden_sym:
                        function.name = function.name.replace(sym, "")
                        function.short = function.short.replace(sym, "")

                    name = function.name
                    if len(function.short):
                        name += "(%s)" % function.short
                    name += "[%s]" % function.source
                    functions_list.append(name)
                function_string += ', '.join(functions_list) + ';'
        except (AttributeError, NameError, KeyError):
            pass

        return function_string
