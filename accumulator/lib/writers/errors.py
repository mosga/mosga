class WriterError(Exception):
    pass


class WriterNotFound(WriterError):
    pass


class WriterImport(WriterError):
    pass
