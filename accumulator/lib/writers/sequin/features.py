from lib.units.rrna import rRNAsType


class SequinFeature(object):

    def __init__(self):
        self.feature = str()
        self.start = int()
        self.end = int()
        self.note = list()

    def _format_note(self) -> str:
        notes = str()
        try:
            for note in self.note:
                notes += "\t\t\t" + "note" + "\t\t" + note + "\r\n"
        except AttributeError:
            pass
        return notes

    def output(self) -> str:
        out = ""
        out += str(self.start) + "\t" + str(self.end) + "\t" + self.feature + "\r\n"
        out += self._format_note()
        return out


class SequinDummy(SequinFeature):
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.feature = 'dummy'

    def output(self) -> str:
        return ""


class SequinRepeat(SequinFeature):

    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.feature = 'repeat_region'
        self.note = ['repeating region']


class SequinGene(SequinFeature):

    def __init__(self, start, end):
        self.feature = 'gene'
        self.start = start
        self.end = end
        self.reverse = False
        self.locus_tag = str()
        self.transcripts = list()

    def output(self) -> str:
        out = super().output()
        out += "\t\t\t" + 'locus_tag' + "\t" + self.locus_tag + "\r\n"
        out += "\t\t\t" + 'note' + "\t\t" + 'hypothetical protein' + "\r\n"

        for transcript in self.transcripts:
            out += self.__output_units(transcript, 'mRNA')
            out += self.__output_units(transcript, 'CDS')

        return out

    def __output_units(self, transcript: dict, feature: str) -> str:
        counter = 0
        out = ''
        for unit in transcript['units']:
            if self.reverse:
                out += str(unit[1]) + "\t" + str(unit[0])
            else:
                out += str(unit[0]) + "\t" + str(unit[1])
            out += "\r\n" if counter else "\t" + feature + "\r\n"
            counter += 1

        out += "\t\t\t" + 'protein_id' + "\t\t" + transcript['protein_id'] + "\r\n"
        out += "\t\t\t" + 'transcript_id' + "\t\t" + transcript['transcript_id'] + "\r\n"
        for i in range(0, len(transcript['functions']['name'])):
            out += "\t\t\t" + 'note' + "\t\t" + transcript['functions']['name'][i]
            if len(transcript['functions']['short'][i]):
                out += ' (' + transcript['functions']['short'][i] + ')'
            out += ' (' + transcript['functions']['source'][i] + ')'
            out += "\r\n"
        return out


class SequinTRNA(SequinFeature):

    def __init__(self, element):
        self.start = element.start
        self.end = element.end
        self.locus_tag = str()
        self.strand = element.strand
        self.feature = 'tRNA'
        self.product = 'tRNA-' + element.trna_aa

    def output(self) -> str:
        out = str()
        out += str(self.start) + "\t" + str(self.end) + "\tgene\r\n"
        out += "\t\t\t" + "locus_tag" + "\t\t" + self.locus_tag + "\r\n"
        out += str(self.start) + "\t" + str(self.end) + "\t" + self.feature + "\r\n"
        out += "\t\t\t" + "product" + "\t\t" + self.product + "\r\n"
        return out


class SequinRRNA(SequinFeature):

    def __init__(self, element):
        self.start = element.start
        self.end = element.end
        self.locus_tag = str()
        self.feature = "rRNA"
        self.note = [self.__map_rrna(rRNAsType(element.rrna_stype).name)]

        try:
            if element.partial:
                # self.note.append("partial rRNA")
                # leads to => SEQ_FEAT.InvalidQualifierValue
                pass
        except AttributeError:
            pass

    def __map_rrna(self, entry: str) -> str:
        map_rrna = {
            "srrna_0": "undefined rRNA",
            "srrna_5": "5S rRNA",
            "srrna_5_8": "5.8S rRNA",
            "srrna_18": "18S rRNA",
            "srrna_28": "28S rRNA",
            "srrna_40": "40S rRNA",
            "srrna_60": "60S rRNA",
            "srrna_80": "80S rRNA",
            "srrna_12": "12S rRNA",
            "srrna_16": "16S rRNA",
            "srrna_23": "23S rRNA"
        }
        try:
            return map_rrna[entry]
        except KeyError:
            return map_rrna["srrna_0"]

    def output(self) -> str:
        out = str()
        out += str(self.start) + "\t" + str(self.end) + "\tgene\r\n"
        out += "\t\t\t" + "locus_tag" + "\t\t" + self.locus_tag + "\r\n"
        out += str(self.start) + "\t" + str(self.end) + "\t" + self.feature + "\r\n"
        out += self._format_note()
        return out


class SequinAssemblyGap(SequinDummy):

    def __init__(self, start, end):
        super().__init__(start, end)
        self.feature = 'assembly_gap'


class SequinCpGIsland(SequinDummy):

    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.feature = 'CpG_island'