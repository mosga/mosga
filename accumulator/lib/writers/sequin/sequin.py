from argparse import ArgumentParser
from typing import TextIO, Dict, List

from lib.units.basic import BasicUnit
from lib.units.cpgisland import CpGIsland
from lib.units.gene import Gene
from lib.units.repeats import Repeats
from lib.units.rrna import rRNA
from lib.units.transcript import TranscriptType
from lib.units.trna import tRNA
from lib.writers.sequin.features import SequinRepeat, SequinGene, SequinTRNA, SequinRRNA, SequinAssemblyGap, \
    SequinCpGIsland, SequinDummy
from lib.writers.writer import GenomeWriter


class Sequin(GenomeWriter):

    def __init__(self, units: Dict[str, List[BasicUnit]], args: ArgumentParser, sequences: dict, interfix: str = None):
        super().__init__(units, args, sequences, interfix)
        self.file_extension: str = "tbl"
        self.translate_units = dict({
            'Repeats': self.__get_repeat,
            'Gene': self.__get_gene,
            'tRNA': self.__get_trna,
            'rRNA': self.__get_rrna,
            'CpGIsland': self.__get_cpgisland,
            'QCinternalNs': self.__get_assembly_gap,
        })

    def write(self, path: str) -> bool:
        with open(path, 'w') as fp:
            for sequence_identifier in self._units:

                if not len(sequence_identifier):
                    continue

                self.sequence_identifier = sequence_identifier
                fp.write('>Feature ' + sequence_identifier + "\r\n")
                try:
                    fp.write("1\t" + str(self._sequences[sequence_identifier]) + "\r\n")
                except KeyError:
                    if self._args.verbose:
                        print("Can not find contig length for " + sequence_identifier)

                for element in self._units[sequence_identifier]:

                    if element is None:
                        continue

                    self.__write_element(fp, element)
                    self._add_counter_stats(sequence_identifier, str(element))
                    self._element_counter += 1
        return True

    def __write_element(self, fp: TextIO, element: BasicUnit):
        try:
            sequin_feature_type = self.translate_units[str(element)]
            sequin_obj = sequin_feature_type(element)
            lines = sequin_obj.output()
            fp.write(lines)
        except KeyError:
            print("Unknown Sequin Feature: %s" % str(element))

    def __get_dummy(self, element) -> object:
        return SequinDummy(element.start, element.end)

    def __get_assembly_gap(self, element: BasicUnit) -> object:
        return SequinAssemblyGap(element.start, element.end)

    def __get_cpgisland(self, element: CpGIsland):
        return SequinCpGIsland(element.start, element.end)

    def __get_repeat(self, element: Repeats) -> object:
        start, end = self._get_start_end_pos(element.start, element.end, element.strand)
        repeat = SequinRepeat(start, end)
        return repeat

    def __get_trna(self, element: tRNA) -> object:
        trna = SequinTRNA(element)
        trna.locus_tag = self._get_locus_tag(self._get_locus_number())
        return trna

    def __get_rrna(self, element: rRNA) -> object:
        rrna = SequinRRNA(element)
        rrna.locus_tag = self._get_locus_tag(self._get_locus_number())
        return rrna

    def __get_gene(self, element: Gene) -> object:
        start, end = self._get_start_end_pos(element.start, element.end, element.strand)
        gene = SequinGene(start, end)
        transcripts = list()
        locus_number = self._get_locus_number()
        gene.locus_tag = self._get_locus_tag(locus_number)
        gene.reverse = not element.strand
        transcript_counter = 1

        for transcript in element.transcripts:
            self._add_length_stats(self.sequence_identifier, "transcript", transcript.length)
            new_transcript = dict({
                'units': list(),
                'unit_types': list(),
                'functions': {
                    'short': list(),
                    'name': list(),
                    'source': list()
                },
                'protein_id': self.protein_prefix + self.interfix + self.prefix_number.format(locus_number) + '.' + str(
                    transcript_counter),
                'transcript_id': self.transcript_prefix + self.interfix + self.prefix_number.format(locus_number) + '.'
                                 + str(transcript_counter)
            })

            for transcript_unit in transcript.units:
                if TranscriptType(transcript_unit.type) is not TranscriptType.CDS:
                    continue
                elif TranscriptType(transcript_unit.type) is TranscriptType.exon:
                    self._add_length_stats(self.sequence_identifier, "exon", transcript_unit.length)
                self._add_length_stats(self.sequence_identifier, "CDS", transcript_unit.length)
                new_transcript['units'].append([transcript_unit.start, transcript_unit.end])
                new_transcript['unit_types'].append(transcript_unit.type)

            # reverse order for + stranded units
            if not gene.reverse:
                new_transcript['units'].reverse()

            try:
                for function in transcript.functions:
                    new_transcript['functions']['short'].append(function.short)
                    new_transcript['functions']['name'].append(function.name)
                    new_transcript['functions']['source'].append(function.source)
            except TypeError:
                pass

            transcripts.append(new_transcript)
            transcript_counter += 1

        gene.transcripts = transcripts

        return gene
