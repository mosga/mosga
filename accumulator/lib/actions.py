from enum import Enum


class MOSGAActions(Enum):
    """
    Contains multiple enumeration that represents the single MOSGA modules.
    """
    NONE = 0
    IMPORT = 1
    COLLECT = 2
    EXPORT = 3
    WRITE = 4
    GET_PROTEINS_NUC_SEQ = 5
    CG_GENES_COMP = 6
    INITIALIZE_GENOME = 7
    KEYPATHWAYMINER = 8
    GOSTGPROFILER = 9
    IID = 10
    STRINGDB = 11
    VECSCREEN = 12
    JBROWSE = 13
    BENCHMARK = 14
    MISC = 15
