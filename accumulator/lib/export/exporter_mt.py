import os
import shutil
import multiprocessing
import timeit

from argparse import ArgumentParser
from multiprocessing import Semaphore
from lib.analysis.organelle import OrganelleScaffoldScan, OrganelleSummary, OrganelleScanError
from lib.analysis.summary import SummaryOutput, AssemblyStats, OverallReport
from lib.annotation import AnnotationExport
from lib.database.database import Database
from lib.database.serializer import DbSerializer
from lib.analysis.qc import QualityCheck
from lib.exporter import Exporter
from lib.writers.errors import WriterNotFound, WriterError, WriterImport


class ExporterMT(Exporter):

    def __init__(self, database: Database, args: ArgumentParser):
        """
        Initialize the database export
        :param args: arguments object
        :param database: database reference
        """
        super().__init__(database, args)
        self._smt: int = args.threads

    def export(self):
        # Initialize the writing process
        try:
            gene_summary, writer_summary_table = self._coordinate_smt_export()
        except WriterError as err:
            print("Could not write the output:")
            print(err)
            exit(2)

        # Finalize
        assembly_stats = AssemblyStats(self._db.get_full_sequences()).get_results()
        overall_report = OverallReport()
        overall_report.add_to_report(assembly_stats, "assembly")
        overall_report.add_to_report(gene_summary, "gene_length")
        overall_report.add_to_report(writer_summary_table, "writer_summary")

        try:
            overall_report.add_to_report(self.gc, "GC")
        except AttributeError:
            pass

        overall_report.parse_report()
        print("### Overall summary")
        print(overall_report.get_report())

        overall_summary_path = self._args.overall_summary
        if overall_summary_path is not None:
            summary_file = open(overall_summary_path, 'w+')
            summary_file.write(overall_report.get_report().get_html_string(attributes={"id": "summary", "class": "table table-striped table-sm"}))
            summary_file.close()

        # Add a bit more information
        if self.verbose:
            # Finishing time
            print("%.2f s: Finished" % (timeit.default_timer() - self._start_time))

    def _coordinate_smt_export(self):
        directory = self._outputs[0][:self._outputs[0].rfind("/")]

        # prepare smt, file splitting
        from lib.misc.fasta import FASTA
        self.headers_split_files = FASTA.split_fasta_file(self._args.genome_file, directory,
                                                     self.split_params["dir"], self.split_params["extension"])

        # prepare writing
        try:
            os.mkdir("%s/writers" % directory)
        except FileExistsError:
            pass

        return_dict = multiprocessing.Manager().dict()
        sema = Semaphore(self._smt)
        # workaround: avoiding too many open processes
        sublist = self.chunks(list(self._sequences.keys()), 256)
        for sub in sublist:
            jobs = []
            for i in sub:
                p = multiprocessing.Process(target=self.write_smt, args=(i, sema, return_dict))
                jobs.append(p)
                p.start()
            for proc in jobs:
                proc.join()
        writer_summary, organelles_summary, qc_summary, gene_summary, writer_summary_table = self._write_smt_summary(return_dict)

        # Cat files together
        try:
            from lib.misc.file_io import FileIO
            output_files = self.output_files(directory, self._outputs, self._output_file_extensions)
            output_list = self.chunks(output_files, 4)
            for outputs in output_list:
                jobs = []
                for out in outputs:
                    p = multiprocessing.Process(target=FileIO.cat_files, args=(out[1], out[0]))
                    jobs.append(p)
                    p.start()
                for proc in jobs:
                    proc.join()
        except FileNotFoundError:
            pass

        # Remove relicts
        shutil.rmtree("%s/writers" % directory)
        shutil.rmtree("%s/%s" % (directory, self.split_params["dir"]))

        return gene_summary, writer_summary_table

    def _write_smt_summary(self, summary):
        """
        Summarize the multithreaded writing stats.
        :param summary: the merged summaries.
        :return:
        """
        writer_summary: dict = {"counter": {}, "length": {}}
        organelles_summary: dict = {}
        qc_summary: dict = {}
        gene_summary: dict = {}
        writer_summary_table = None

        for name, entry in summary.items():

            # Organelles
            if self._args.organelle_indication:
                for org_type, org_nr in entry["Organelle"].items():
                    try:
                        organelles_summary[org_type].update(org_nr)
                    except KeyError:
                        organelles_summary.update({org_type: org_nr})

            # Writer
            writer_summary["counter"].update(entry["Writer"]["counter"])
            writer_summary["length"].update(entry["Writer"]["length"])

            # Quality-check
            if not self._args.skip_analysis and self._args.quality_check:
                qc_summary.update({name: entry["QC"]})

        if self._args.organelle_indication and not self._args.skip_analysis:
            organelles_summary.update(self.organelle_files)
            try:
                OrganelleSummary(organelles_summary, self.gc["scaffolds"], self._scaffold_length, self._args,
                                 self.gc["outliers"])
            except OrganelleScanError as e:
                print(e)
                print("Abort Organelle Scan summary table")

        # Show quality-check summary
        try:
            if self._args.quality_check and len(self._sequences):
                summary_out = SummaryOutput.qc_summary(qc_summary, list(self._sequences.keys()))
                print("### Quality Check Summary")
                print(summary_out)
        except IndexError as e:
            print(e)
            print("Something went wrong during the writing.\r\n"
                  "Please disable multi-threading and check writer class separately.")

        try:
            if self._args.summary is not None:
                writer_summary_table, gene_summary, gene_stats = SummaryOutput.summarize_writer(self._args.summary, writer_summary,
                                                                      list(self._sequences.keys()))

                if len(gene_summary):
                    print("### Average unit lengths")
                    print(gene_stats)

                print("### Annotation Summary")
                print(writer_summary_table)
        except AttributeError:
            pass

        return writer_summary, organelles_summary, qc_summary, gene_summary, writer_summary_table

    def write_smt(self, scaffold_name: str, sema,  result) -> bool:
        """
        Write the final output separately for each scaffold. Allows multithreading.
        :param scaffold_name:
        :param sema: semaphore, blocks request threads
        :param result: collector for resulting stats.
        :return:
        """
        sema.acquire()

        try:
            annotations: dict = {}
            DbSerializer(self._db, [self._annotation_export.collect_single_annotation(self._db, scaffold_name)],
                         annotations, self.verbose)
            invalid_genes: dict = {}
            stats: dict = {"Writer": {}}

            if self._args.organelle_indication:
                organelles = OrganelleScaffoldScan(self._args, annotations).get_summary()
            else:
                organelles = None

            if not self._args.skip_analysis:
                if self._args.quality_check and self._args.genome_file is not None:
                    genome_file_dir = self._args.genome_file[:self._args.genome_file.rfind("/")]

                    scaffold_file_name = self.headers_split_files[scaffold_name]
                    genome_file = "%s/%s/%i%s" % (genome_file_dir, self.split_params["dir"], scaffold_file_name,
                                                  self.split_params["extension"])
                    qc = QualityCheck(annotations, self._args, genome_file)
                    qc.perform()
                    invalid_genes = qc.get_invalid_genes()
                    stats.update({"QC": qc.get_stats()})
                    annotations = qc.get_qc_units()

                    # Run annotation output filters for NCBI-compliance
                    qc.ncbi_run_filter()

            # Discard quality-check failed genes
            AnnotationExport.remove_disabled_genes(annotations, invalid_genes)
            stats.update({'elimination_rounds': 0})

            while self._annotation_export.check_conflicts(annotations):
                stats["elimination_rounds"] += 1

            # Remove empty entries
            self._clear_annotations(annotations)
            sc_identifier = self._scaffold_smt_ids[scaffold_name]

            for i in range(0, len(self._writers)):
                writer = self._writers[i]
                output = self._outputs[i]
                try:
                    output_class = self.get_writer_class(self._args, self._scaffold_length, writer, annotations, sc_identifier)
                    writer_output_dir = output[:output.rfind("/")]
                    writer_output = "%s/writers/%s.%s" % (writer_output_dir, sc_identifier, output_class.file_extension)
                    output_class.write(writer_output)
                    stats["Writer"].update({"counter": output_class.get_stats()["counter"]})
                    stats["Writer"].update({"length": output_class.get_stats()["length"]})
                    stats.update({"Organelle": organelles})
                except WriterNotFound as err:
                    raise WriterError(err)
                except WriterImport as err:
                    raise WriterError(err)

            if self._time_track:
                print("%.2f s: Finished SMT Export for %s" % (timeit.default_timer() - self._start_time, scaffold_name))
            result.update({scaffold_name: stats})
        except FileNotFoundError as e:
            print("%s : %s ?" % (e, genome_file))
            pass
        except Exception as e:
            print(e)
            pass

        sema.release()
        return True
