from argparse import ArgumentParser

from lib.misc.file_io import FileIO
from lib.misc.filter import Filter
from lib.misc.protein_seq import ProteinSequences
from lib.units.gene import Gene
from lib.units.general import GeneralObject
from lib.units.qc import QCinternalNs


class QualityCheck:

    def __init__(self, annotations: dict, args: ArgumentParser, genome_file_path: str, start_codons: list = None,
                 stop_codons: list = None):
        """
        Performs quality checks, identifies invalid genes and generates reports.
        :param annotations: the entire annotation.
        :param args: arguments from the MOSGA call.
        :param genome_file_path: the path to the genome file.
        """
        self.__args = args
        self._get_start_codons(start_codons)
        self._get_stop_codons(stop_codons)
        self.__annotations = annotations
        self.__genome_file_path = genome_file_path
        self.__error_units = dict()
        self.__settings = {'iN': {'limit': 100}}
        self.__stats = {
            'Internal_Ns': 0,
            'NCBI_Filtered': 0,
            'qc_ncbi_filter': {}
        }
        self.__scaffold_length = dict()
        self.__invalid_genes: dict = {}

    def _get_start_codons(self, start_codons: list = None) -> list:
        """
        Try to get start codons from arguments.
        :return: list of start codons.
        """
        if start_codons is not None:
            self.__start_codons = start_codons
        else:
            try:
                sc = list(map(lambda x: x.upper(), self.__args.start_codon))
                self.__start_codons = list(filter(lambda x: len(x) == 3, sc))
            except AttributeError:
                self.__start_codons = ["ATG"]

    def _get_stop_codons(self, stop_codons: list = None) -> list:
        """
        Try to get stop codons from arguments.
        :return: list of stop codons,
        """
        if stop_codons is not None:
            self.__start_codons = stop_codons
        else:
            try:
                sc = list(map(lambda x: x.upper(), self.__args.stop_codon))
                self.__stop_codons = list(filter(lambda x: len(x) == 3, sc))
            except AttributeError:
                self.__stop_codons = ["TAA", "TGA", "TAG"]

    def perform(self) -> None:
        """
        Initializes all analysis methods.
        """
        try:
            self.__read_fasta()
            self.__quality_checks()
            self.__check_protein_sequences()
        except Exception as e:
            print("Quality Checks failed")
            print(e)
            raise QualityCheckError

    def __quality_checks(self) -> None:
        """
        Coordinates the quality check final analysis summary.
        :return:
        """
        for error_key in self.__error_units.keys():

            # Internal Ns
            for internalN in self.__error_units[error_key]['iN']:
                self.__find_Ns(error_key, internalN)

    def __find_Ns(self, seq: str, entry: tuple) -> None:
        """
        Creates QualityCheck Internal Gaps objects and place them into the annotation.
        :param seq: the sequence/scaffold id.
        :param entry: tuple containing the start and end position.
        """
        if entry[1]-entry[0] >= self.__settings['iN']['limit']:
            iN = QCinternalNs(entry[0], entry[1])
            iN.strand = None
            self.__place_unit_in_seq(seq, iN)

    def __place_unit_in_seq(self, seq: str, unit: object) -> None:
        """
        Inserts gap elements into the annotation.
        :param seq: the relevant sequence id.
        :param unit: the new unit.
        """
        counter = 0
        try:
            # TODO: Optimize speed for insertion
            for index in range(0, len(self.__annotations[seq])):
                if self.__annotations[seq][index].start > unit.start:
                    self.__stats['Internal_Ns'] += 1
                    insert_index = index if index > 0 else 0
                    self.__annotations[seq].insert(insert_index+1, unit)
                    break
        except KeyError:
            print("QC: Not loaded sequence id: " + seq)

    def __read_fasta(self) -> None:
        """
        Initializes the FASTA sequence analysis.
        """
        self.__fc = {
            'id': str(),
            'start': 0,
            'list': [],
            'iN': {
                'start': 0,
                'list': [],
            },
            'gc': {
                'buffer': 0,
                'scaffold': {}
            },
            'counter': 0
        }

        for line in FileIO.get_file_reader(self.__genome_file_path):
            if line[0] == '>':
                self.__fasta_new_block(line)
            else:
                self.__fasta_analyse_block(line)

        self.__fasta_store_dict()

    def __fasta_new_block(self, line: str) -> None:
        """
        Initialize a new FASTA block (sequence).
        :param line: the FASTA sequence header.
        """
        whole_sequence = line[1:len(line)]
        ids = whole_sequence.split()
        self.__fasta_store_dict()

        self.__fc['id'] = ids[0]
        self.__fc['start'] = 0
        self.__fc['counter'] = 0
        self.__fc['list'] = list()
        self.__fc['iN'] = {
            'start': 0,
            'list': list()
        }
        self.__fc['gc']['buffer'] = 0

    def __fasta_analyse_block(self, line: str) -> None:
        """
        Analyzes the FASTA sequence for gaps and gc content.
        :param line: the single FASTA sequence line.
        """
        # Check for every single entry
        for c in line:
            low = c.lower()
            # Start internalN
            if low == 'n' and self.__fc['iN']['start'] == 0:

                # Start a new internalN
                self.__fc['iN']['start'] = self.__fc['counter']
            elif low != 'n' and self.__fc['iN']['start'] != 0:

                # Close started internalN
                self.__fc['iN']['list'].append((self.__fc['iN']['start'], self.__fc['counter']))
                self.__fc['iN']['start'] = 0

            # Start GC counting
            if low == 'g' or low == 'c':
                self.__fc['gc']['buffer'] += 1

            self.__fc['counter'] += 1

    def __fasta_store_dict(self) -> None:
        """
        Store FASTA analysis block results.
        """
        try:
            self.__error_units[self.__fc['id']]
        except KeyError:
            if len(self.__fc['id']):
                self.__error_units[self.__fc['id']] = {
                    'iN': dict()
                }

        # Internal Ns
        if len(self.__fc['iN']['list']):
            self.__error_units[self.__fc['id']].update({'iN': self.__fc['iN']['list']})

        # GC Content
        if len(self.__fc['id']) and self.__fc['gc']['buffer']:
            self.__fc['gc']['scaffold'].update({self.__fc['id']: self.__fc['gc']['buffer']/(self.__fc['counter']+1)})

        if self.__fc['counter']:
            self.__scaffold_length.update({self.__fc['id']: self.__fc['counter'] + 1})

    def __check_protein_sequences(self) -> dict:
        """
        Creates a deep-copy of the annotation, isolates the genes, map the genes with nucleotide sequences,
        perform validation tests on the sequences and return a dictionary with lists of invalid gene uid.
        :return: the dictionary with the invalid gene uid.
        """
        from copy import deepcopy
        from lib.annotation import AnnotationExport

        self.__stats.update({
            'No_triplets': 0,
            'No_start_codon': 0,
            'No_stop_codon': 0,
            'Internal_stop_codon': 0
        })

        disable_genes: dict = {}

        # Copy annotation from self.__units
        annotations = deepcopy(self.__annotations)
        # Filter only gene units
        annotations = AnnotationExport.get_genes(annotations, False, self.__args)
        # Map genes with FASTA file
        genes = ProteinSequences.map_genes_with_FASTA(self.__genome_file_path, annotations)
        seq_keys = list(genes.keys())

        # Iterate over every gene and check it
        for seq in seq_keys:
            for gene in genes.pop(seq):

                if not self.__check_single_gene(gene):
                    try:
                        disable_genes[seq].update({gene.uid})
                    except KeyError:
                        disable_genes.update({seq: {gene.uid}})

        self.__invalid_genes = disable_genes
        del annotations

    def __check_single_gene(self, gene: Gene) -> bool:
        """
        Checks if all transcript variants passes the protein tests
        :param gene: the gene with all transcript variants.
        :return: True if all variants passed.
        """
        no_issues_transcript = 0
        for transcript in gene.transcripts:
            cds, includes_stop_codon = ProteinSequences.resolve_transcripts(transcript, gene)

            if not gene.strand:
                cds = ProteinSequences.reverse_complement(cds)

            check_transcript_sequence = self.__check_protein(cds, includes_stop_codon)
            no_issues_transcript += check_transcript_sequence

        if no_issues_transcript == len(gene.transcripts):
            return True

        return False

    def __check_protein(self, sequence: str, includes_stop_codon: bool = False) -> bool:
        """
        Performs several checks on the protein sequences like:
        1. is the sequence divisible by three (codons)
        2. does the sequence contains a valid start codon at the first place
        3. does the sequence stops with a valid stop codon
        :param sequence: the protein sequence
        :param includes_stop_codon: whatever contains the stop codon or not
        :return: True if all tests passed
        """

        # Not 3rd pairs -> no right triplets
        if len(sequence) % 3:
            self.__stats['No_triplets'] += 1
            return False

        # Split sequences into codons
        codons = [sequence[i:i + 3] for i in range(0, len(sequence), 3)]

        # Duplicated stop_codons?
        if includes_stop_codon and len(codons) > 1 and codons[-1] == codons[-2]:
            # Found duplicated last codon (=> BRAKER/Augustus workaround)
            codons.pop()

        try:
            # Check start codons
            if codons[0].upper() not in self.__start_codons:
                self.__stats['No_start_codon'] += 1
                return False

            if codons[-1].upper() not in self.__stop_codons:
                self.__stats['No_stop_codon'] += 1
                return False
        except IndexError:
            return False

        cnt = 1
        for codon in codons:
            if cnt != len(codons):
                # Internal Stop Codon
                if codon.upper() in self.__stop_codons:
                    self.__stats['Internal_stop_codon'] += 1
                    return False
            cnt += 1

        return True

    def ncbi_run_filter(self) -> None:
        """
        Iterates over the annotations and apply all NCBI filters.
        """
        for sequence in self.__annotations.keys():
            self.__annotations[sequence] = list(filter(self.__ncbi_unit_filter, self.__annotations[sequence]))

    def __ncbi_unit_filter(self, unit: GeneralObject) -> bool:
        """
        Performs unit type specific NCBI filters.
        :param unit: the unit object that will be checked.
        :return: True if all filters were passed.
        """
        filters = self.ncbi_filters(str(unit))
        if len(filters):
            for single_filter in filters:
                if not single_filter(unit):
                    self.__ncbi_filter_count(str(single_filter))
                    return False
        return True

    def __ncbi_filter_count(self, name: str) -> None:
        """
        Increases the count number for the stats
        :param name: the name of the relevant number.
        """
        try:
            self.__stats['qc_ncbi_filter'][name] += 1
        except KeyError:
            self.__stats['qc_ncbi_filter'].update({name: 1})
        self.__stats["NCBI_Filtered"] += 1

    def get_qc_units(self) -> dict:
        """
        Commits the current annotation.
        :return: the annotation.
        """
        return self.__annotations

    def get_stats(self) -> dict:
        """
        Commits a dictionary with some numbers.
        :return: the dictionary.
        """
        return self.__stats

    def get_scaffold_length(self):
        """
        Commits a dictionary with the scaffold lengths.
        :return: the dictionary.
        """
        return self.__scaffold_length

    def get_invalid_genes(self) -> dict:
        """
        Commits a dictionary with lists of the invalid genes uid
        :return: the dictionary.
        """
        return self.__invalid_genes

    @staticmethod
    def ncbi_filters(unit: str) -> list:
        """
        Maps lambdas filter to specific unit types
        :param unit: the name of the typ
        :return: returns a list with lambda expressions
        """
        switcher = {
            "rRNA": [Filter.ignore_partial_rrnas(), Filter.minimal_ribosomal_rna_size()],
            "tRNA": [Filter.ignore_suppressor_trnas()],
        }
        return switcher.get(unit, [])


class QualityCheckError(Exception):
    pass
