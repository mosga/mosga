__author__ = "Taher Jallouli, Roman Martin"

import math
from argparse import ArgumentParser
from typing import List

from lib.database.database import Database
from lib.importer import Importer
from lib.readers.errors import ReaderFormatError
from lib.readers.formats import ReaderFormats
from lib.units.basic import BasicUnit
from lib.units.transcriptunit import TranscriptUnit
from lib.units.transcript import Transcript, TranscriptType
from lib.units.gene import Gene
from lib.misc.intervals import Intervals


class Benchmark:

    def __init__(self, arguments: ArgumentParser):
        """
        Reads the prediction output and reference Genome Annotations using the importer.py module. The Two can be later
        compared via different statistical evaluation metrics.
        :param arguments: pass the argument object.
        """
        self._args = arguments
        self.verbose = self._args.verbose

        # Reading units
        self.units_ref = self.receive_annotation_units(self._args.reference_format, self._args.reference_unit,
                                                       self._args.reference_reader, self._args.reference_file)
        self.units_pred = self.receive_annotation_units(self._args.prediction_format, self._args.prediction_unit,
                                                        self._args.prediction_reader, self._args.prediction_file)
        self.comparable_unit_types = list(set(self.units_ref.keys()) & set(self.units_pred.keys()))
        self._scaffold_length = self.find_scaffold_lengths()

        if self.verbose >= 1:
            print("Benchmark: Reference unit types:  %s" % ", ".join(self.units_ref.keys()))
            print("Benchmark: Prediction unit types: %s" % ", ".join(self.units_pred.keys()))
            print("Benchmark: Comparable unit types: %s" % ", ".join(self.comparable_unit_types))

    def find_scaffold_lengths(self) -> dict:
        """
        Identify the corresponding nucleotide length for each scaffold
        :return: the scaffold length dictionary
        """
        scaffold_length: dict = Benchmark.estimate_scaffold_length_from_units(self.units_ref)
        scaffold_length: dict = Benchmark.estimate_scaffold_length_from_units(self.units_pred, scaffold_length)

        try:
            reference_format = Importer.get_format(self._args.reference_format)
        except (KeyError, ReaderFormatError):
            reference_format = self._args.reference_format

        try:
            prediction_format = Importer.get_format(self._args.prediction_format)
        except (KeyError, ReaderFormatError):
            prediction_format = self._args.prediction_format

        if reference_format == ReaderFormats.MOSGADB:
            scaffold_length: dict = self.get_scaffold_length_from_database(scaffold_length, self._args.reference_file)

        if prediction_format == ReaderFormats.MOSGADB:
            scaffold_length: dict = self.get_scaffold_length_from_database(scaffold_length, self._args.prediction_file)

        return scaffold_length

    def get_scaffold_length_from_database(self, scaffold_length: dict, database_file: str) -> dict:
        """
        Extract the scaffold lengths from an annotation database
        :param scaffold_length: already existing scaffold length dictionary
        :param database_file: database path
        :return: update scaffold dictionary
        """
        database = Database(self._args)
        database.open_database(database_file)
        db_scaffolds = database.get_scaffold_length()[0]

        for identifier in db_scaffolds:
            if identifier in scaffold_length and db_scaffolds[identifier] > scaffold_length[identifier]:
                scaffold_length[identifier] = db_scaffolds[identifier]
            elif identifier not in scaffold_length:
                scaffold_length.update({identifier: db_scaffolds[identifier]})

        return scaffold_length

    def analyse(self):
        """
        Performs the actual benchmark analysis
        """
        if not len(self.comparable_unit_types):
            print("Benchmark: No common unit type to compare")
            return None

        ocm: dict = {"nuc": {"tp": 0, "tn": 0, "fp": 0, "fn": 0}}  # overall confusion matrix
        for unit_type in self.comparable_unit_types:
            print("### %s" % unit_type)

            print("## Nucleotide Level")
            tp, tn, fp, fn = self.benchmark_nucleotide(self.units_ref, self.units_pred, self._scaffold_length,
                                                       unit_type)
            ocm["nuc"]["tp"] += tp
            ocm["nuc"]["tn"] += tn
            ocm["nuc"]["fp"] += fp
            ocm["nuc"]["fn"] += fn
            Benchmark.print_metric(tp, tn, fp, fn)

            if unit_type == "Gene":
                print("## Exon Level")
                tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true = self.benchmark_exon(
                    self.units_ref, self.units_pred, self._scaffold_length)
                Benchmark.print_exon_metric(tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true)

                print("## Gene Level")
                tg, ag, pg = self.benchmark_gene(self.units_ref, self.units_pred)
                Benchmark.print_gene_metric(tg, ag, pg)

        if len(self.comparable_unit_types) > 1:
            print("### Overall Nucleotide Level")
            self.print_metric(ocm["nuc"]["tp"], ocm["nuc"]["tn"], ocm["nuc"]["fp"], ocm["nuc"]["fn"])

    def receive_annotation_units(self, unit_format: str, unit_type: str, unit_reader: str, annotation_file_path: str):
        """
        Retrieves all units from a reader provided through the arguments.
        Units were sorted according to their unit type.
        :param unit_format: the unit format (GFF, GBFF, etc.)
        :param unit_type: the specific unit type for the Reader (Gene, Repeats etc.), or take an none ''
        :param unit_reader: the reader name
        :param annotation_file_path: the target annotation file path
        :return: all units in nexted dictionaries with the following hierarchy: unit_type, sequence_identifier, [units]
        """
        if self.verbose >= 2:
            print("Benchmark: Receive units from Reader %s, Format %s, Path %s" % (unit_reader, unit_format,
                                                                                   annotation_file_path))

        if unit_format == "FASTA" and unit_reader == "CDS":  # Special case
            units = Intervals.read_fasta_to_annotation(self._args.reference_file)
        else:
            reader_format = Importer.get_format(unit_format)
            reader = Importer.get_reader(unit_type, unit_reader, reader_format, {"verbose": self.verbose})
            units = reader.read(annotation_file_path)
        units_sorted = Intervals.sort_annotations_in_unit_types(units)
        return units_sorted

    def get_units(self) -> tuple:
        return self.units_ref, self.units_pred

    def get_scaffold_length(self) -> dict:
        return self._scaffold_length

    def benchmark_exon(self, units_ref: dict, units_pred: dict, scaff_length: dict):
        """
        At the exon structure level, we measure the accuracy of the predictions by comparing predicted and true exons
        along the benchmark gene sequence. An exon is considered correctly predicted (TP), when it is an exact match to
        the benchmark exon, i.e. when the 5′ and 3′ exon boundaries are identical. All others predicted exons are then
        considered FP. Sensitivity and specificity are then defined as before.
        Sensitivity_guigo = #correct_exons / #actual_exons (AE)
        Precision_guigo = #correct_exons / #predicted_exons (PE)
        the Missing Exons (ME): Proportion of true exons without overlap to predicted exons
        the Wrong Exons (WE): Proportion of predicted exons without overlap to actual exons
        MEScore = ME / Total number of true exons (Sensitivity Analog/Correction)
        WeScore = WE / Total number of predicted exons (Specificity Analog)
        5' = number of true 5' exon boundaries correctly predicted * 100 / number of correct predicted exons + number
        of wrong exons
        3' =  number of true 3' exon boundaries correctly predicted * 100 / number of correct predicted exons + number
        of wrong exons
        :param units_ref: reference units
        :param units_pred: predicted units
        :param scaff_length: scaffold lengths
        :return: tuple with confusion matrix elements
        """
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        me = 0
        we = 0
        ae = 0
        pe = 0

        five_prime_end_true = 0
        three_prime_end_true = 0

        min_nuc = 1

        for sequence_identifier in units_ref.get("Gene").keys():
            if sequence_identifier in units_pred.get("Gene"):
                scaff_pred: List[Gene] = units_pred.get("Gene")[sequence_identifier]
                scaff_ref: List[Gene] = units_ref.get("Gene")[sequence_identifier]
                max_nuc = scaff_length[sequence_identifier]
            else:
                # TODO: Better solution
                continue

            ref_exon_interval_list, ref_intron_interval_list = Benchmark.__get_exon_intron_interval_lists(scaff_ref)
            pred_exon_interval_list, pred_intron_interval_list = Benchmark.__get_exon_intron_interval_lists(scaff_pred)
            ref_non_exon_list = Benchmark.get_compliment_interval_list(ref_exon_interval_list, min_nuc, max_nuc)
            pred_non_exon_list = Benchmark.get_compliment_interval_list(pred_exon_interval_list, min_nuc, max_nuc)

            ae += len(ref_exon_interval_list)
            pe += len(pred_exon_interval_list)

            tp_this = Benchmark.overlap_count(
                set(ref_exon_interval_list), set(pred_exon_interval_list))
            tn_this = Benchmark.overlap_count(set(ref_non_exon_list), set(pred_non_exon_list))

            fn += len(ref_exon_interval_list) - tp_this
            fp += len(pred_exon_interval_list) - tp_this
            tp += tp_this
            tn += tn_this
            me_this = self.__partial_overlap_interval_lists_count(
                ref_exon_interval_list, pred_intron_interval_list)
            we_this = self.__partial_overlap_interval_lists_count(
                pred_exon_interval_list, ref_intron_interval_list)

            me += me_this
            we += we_this

            five_prime_end_true += self.__overlap_interval_five_prime(
                ref_exon_interval_list, pred_exon_interval_list)
            three_prime_end_true += self.__overlap_interval_three_prime(
                ref_exon_interval_list, pred_exon_interval_list)
        return tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true

    def __partial_overlap_interval_lists_count(self, interval_list1: List, interval_list2: List) -> int:
        """
        Gets the count of intervals that are partially or fully matching from two interval lists
        :param interval_list1: list of intervals
        :param interval_list2: list of intervals
        :return: count of partial overlaps
        """
        return sum([self.__is_inside(a, b) for a in interval_list1 for b in interval_list2])

    @staticmethod
    def benchmark_nucleotide(units_ref: dict, units_pred: dict, scaffold_lengths: dict, unit_type: str):
        """
        At the nucleotide level, we measure the accuracy of a gene prediction
        on a benchmark sequence by comparing the predicted state (exon or intron)
        with the true state for each nucleotide along the benchmark sequence.
        Nucleotides correctly predicted to be in either an exon or an intron are
        considered to be True Positives (TP) or True Negatives (TN) respectively.
        Conversely, nucleotides incorrectly predicted to be in exons or
        introns are considered to be False Positives (FP) or False Negatives (FN)
        respectively
        :param units_ref: reference units
        :param units_pred: predicted units
        :param scaffold_lengths: dictionary with scaffold lengths
        :param unit_type: the respective unit_type
        :return: tuple with confusion matrix elements
        """
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for sequence_identifier in units_ref.get(unit_type).keys():

            if sequence_identifier in units_pred.get(unit_type) and sequence_identifier in units_ref.get(unit_type):
                scaff_pred: list = units_pred.get(unit_type)[sequence_identifier]
                scaff_ref: list = units_ref.get(unit_type)[sequence_identifier]
            else:
                # TODO: Better solution
                continue

            ref_exon_set = Benchmark.get_exon_nuc_set(scaff_ref, unit_type)
            pred_exon_set = Benchmark.get_exon_nuc_set(scaff_pred, unit_type)
            max_nuc = scaffold_lengths[sequence_identifier]

            ref_non_coding_set = Benchmark.get_non_coding_nuc_set(ref_exon_set, max_nuc)
            pred_non_coding_set = Benchmark.get_non_coding_nuc_set(pred_exon_set, max_nuc)

            tp += Benchmark.overlap_count(ref_exon_set, pred_exon_set)
            tn += Benchmark.overlap_count(ref_non_coding_set, pred_non_coding_set)
            fn += Benchmark.overlap_count(pred_non_coding_set, ref_exon_set)
            fp += Benchmark.overlap_count(pred_exon_set, ref_non_coding_set)

        return tp, tn, fp, fn

    @staticmethod
    def benchmark_gene(units_ref: dict, units_pred: dict):
        """
        A gene is predicted correctly if the same exons (as in the annotation) are assigned
        to that gene and all of these exons are predicted correctly (as defined above).
        GENE_sens = tg / ag,
        GENE_Spec = tg / pg
        :param units_ref: reference units
        :param units_pred: predicted units
        :return: metric values
        """
        tg = 0
        ag = 0
        pg = 0
        for sequence_identifier in units_ref.get("Gene").keys():
            if sequence_identifier in units_pred.get("Gene"):
                scaff_pred: List[Gene] = units_pred.get("Gene")[sequence_identifier]
                scaff_ref: List[Gene] = units_ref.get("Gene")[sequence_identifier]
            else:
                continue

            gene_set_pred = Benchmark.get_gene_interval_set(scaff_pred)
            gene_set_ref = Benchmark.get_gene_interval_set(scaff_ref)
            tg += len(gene_set_pred.intersection(gene_set_ref))
            ag += len(gene_set_ref)
            pg += len(gene_set_pred)
        return tg, ag, pg

    @staticmethod
    def get_exon_nuc_set(scaffold: list, unit_type: str) -> set:
        """
        Gets a set of the indexes of all exon bps.
        :param scaffold: scaffold with units
        :param unit_type: units
        :return: indexes of exon bps
        """
        nuc_set = set()

        if unit_type == "Gene":
            gene: Gene
            for gene in scaffold:

                transcript: Transcript
                for transcript in gene.transcripts:
                    transcript_unit: TranscriptUnit
                    for transcript_unit in transcript.units:
                        if transcript_unit.type == TranscriptType.CDS.value:
                            nuc_set.update(
                                range(transcript_unit.start, transcript_unit.end + 1))
        else:
            unit: BasicUnit
            for unit in scaffold:
                nuc_set.update(range(unit.start, unit.end + 1))
        return nuc_set

    @staticmethod
    def get_non_coding_nuc_set(exon_set: set, maximal: int):
        """
        Get the compliment set of the exon set of bps
        :param exon_set: set of exon bps
        :param maximal: the length of the scaffold
        :return: set of non coding bps, indexes
        """
        return set(range(1, maximal + 1)).difference(exon_set)

    @staticmethod
    def __get_exon_intron_interval_lists(scaff: List[Gene]) -> tuple:
        """
        Converts list of genes to list of intervals. Each of which pertains to the start and end of an exon.
        The list of intervals is sorted and has no duplicates
        :param scaff: list of genes in a scaffold
        :return: sorted list of exon intervals without duplicates
        """
        exon_list = list()
        intron_list = list()
        gene: Gene
        for gene in scaff:
            if len(gene.transcripts) and len(gene.transcripts[0].units):
                this_exon_list = list()
                transcript: Transcript
                for transcript in gene.transcripts:
                    transcript_unit: TranscriptUnit
                    for transcript_unit in transcript.units:
                        if transcript_unit.type == TranscriptType.CDS.value:
                            this_exon_list.append((transcript_unit.start, transcript_unit.end))
            else:
                continue
            intron_list.extend(Benchmark.get_compliment_interval_list(this_exon_list, gene.start, gene.end))
            exon_list.extend(this_exon_list)
        exon_list = list(set(exon_list))
        intron_list = list(set(intron_list))
        exon_list.sort(key=lambda tup: tup[0])
        intron_list.sort(key=lambda tup: tup[0])

        return exon_list, intron_list

    @staticmethod
    def get_gene_interval_set(scaff: List[Gene]) -> List:
        """
        Converts list of genes to list of intervals. Each of which pertains to the start and end of an exon.
        The list of intervals is sorted and has no duplicates
        :param scaff: list of genes in a scaffold
        :return: sorted list of exon intervals without duplicates
        """
        gene_set = set()
        gene: Gene
        for gene in scaff:
            transcript: Transcript
            exon_set = set()
            for transcript in gene.transcripts:
                transcript_unit: TranscriptUnit
                for transcript_unit in transcript.units:
                    if transcript_unit.type == 3:
                        exon_set.add((transcript_unit.start, transcript_unit.end))
            gene_set.update(exon_set)
        return gene_set

    @staticmethod
    def __get_exon_interval_dict_with_strand(scaff: List[Gene]) -> dict:
        """
        Converts list of genes to dictionary of intervals. Each of which pertains to the start and end of an exon.
        Each key of the dictionary is an exon and each value is the strand
        :param scaff: list of genes in a scaffold
        :return: dictionary of exons and their strand
        """
        exon_dict = dict()
        gene: Gene
        for gene in scaff:
            transcript: Transcript
            for transcript in gene.transcripts:
                transcript_unit: TranscriptUnit
                for transcript_unit in transcript.units:
                    if transcript_unit.type == 3:
                        # print(transcript_unit.start,transcript_unit.end)
                        exon_dict.update(
                            {(transcript_unit.start, transcript_unit.end): transcript_unit.strand})

        return exon_dict

    @staticmethod
    def get_compliment_interval_list(exon_list: list, minimal: int, maximal: int) -> list:
        """
        Returns the interval compliment of an interval set
        :param exon_list: list of intervals with start and end values (eg: (1, 50))
        :param minimal: smallest bp in gene or scaff
        :param maximal: biggest bp in gene or scaff
        :return: the interval compliment of an interval set
        """
        intron_list = list()
        last_exon_end = minimal
        index_start = 0

        # Base case if the Scaff starts with an Exon
        if exon_list[0][0] == minimal:
            last_exon_end = exon_list[0][1] + 1
            index_start += 1

        for i in range(index_start, len(exon_list)):
            intron_list.append((last_exon_end, exon_list[i][0] - 1))
            last_exon_end = exon_list[i][1] + 1

        # Adding the last intron if the scaff doesn't end with an Exon
        if exon_list[len(exon_list) - 1][1] != maximal:
            intron_list.append((exon_list[len(exon_list) - 1][1] + 1, maximal))
        return intron_list

    @staticmethod
    def overlap_count(set1: set, set2: set) -> int:
        """
        The count of the overlap between two sets
        :param set1: set of  bp indexes
        :param set2: set of  bp indexes
        :return: the count of the overlap between two sets
        """
        return len(set1.intersection(set2))

    @staticmethod
    def __overlap_interval_five_prime(interval_list_ref: list, interval_list_pred: list):
        """
        Gets the count of intervals that have the same start value from the two interval lists
        :param interval_list_ref: list of intervals
        :param interval_list_pred: list of intervals
        :return: count of intervals that have the same start value
        """
        count = 0
        for interval_ref in interval_list_ref:
            for interval_pred in interval_list_pred:
                if interval_ref[0] == interval_pred[0]:
                    count += 1
        return min(count, len(interval_list_ref), len(interval_list_pred))

    @staticmethod
    def __overlap_interval_three_prime(interval_list_ref: list, interval_list_pred: list):
        """
        Gets the count of intervals that have the same end value from the two interval lists
        :param interval_list_ref: list of intervals
        :param interval_list_pred: list of intervals
        :return: count of intervals that have the same end value
        """
        count = 0
        for interval_ref in interval_list_ref:
            for interval_pred in interval_list_pred:
                if interval_ref[1] == interval_pred[1]:
                    count += 1
        return min(count, len(interval_list_ref), len(interval_list_pred))

    @staticmethod
    def __is_inside(a, b):
        if a[0] >= b[0] and a[1] <= b[1]:
            return 1
        else:
            return 0

    @staticmethod
    def precision(tp, fp):
        """
        Calculate precision
        :param tp:  true positives
        :param fp: false positives
        :return: precision
        """
        try:
            return round(tp / (tp + fp), 3)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def accuracy(tp, tn, fp, fn):
        """
        Calculate Accuracy
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :param tn: true negatives
        :return: accuracy
        """
        try:
            return (tp + tn) / (tp + tn + fn + fp)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def sensitivity(tp, fn):
        """
        According to Guigo et al., 1996
        :param tp:  true positives
        :param fn: false negatifs
        :return: sensitivity
        """
        try:
            return tp / (tp + fn)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def specificity(tn, fp):
        """
        According to Guigo et al., 1996
        :param tn: true negatives
        :param fp: false positives
        :return: specificity
        """
        try:
            return tn / (tn + fp)
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def f1_score(tp, fp, fn):
        """
        Calculate the F1 score
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :return: F1-score
        """
        try:
            sensi = Benchmark.sensitivity(tp, fn)
            speci = Benchmark.precision(tp, fp)

            return 2 * ((sensi * speci) / (sensi + speci))
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def mcc(tp, tn, fp, fn):
        """
        Calculate the Matthews correlation coefficient (MCC)
        :param tp: true positives
        :param fp: false positives
        :param fn: false negatives
        :param tn: true negatives
        :return: MCC
        """
        try:
            return (tp * tn - fp * fn) / math.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn))
        except ZeroDivisionError:
            return 0.0

    @staticmethod
    def estimate_scaffold_length_from_units(units: dict, scaffold_length: dict = None) -> dict:
        """
        Estimates the scaffold length based on all stored annotation units.
        :param units: the dictionary with all units
        :param scaffold_length: if provided, extend these lengths
        :return: dictionary with all scaffold lengths
        """
        if scaffold_length is None:
            scaffold_length: dict = {}

        if not len(units):
            return scaffold_length

        for unit_type in units:
            for sequence_identifier in units[unit_type].keys():
                for unit in units[unit_type][sequence_identifier]:
                    if not isinstance(unit, BasicUnit):
                        continue

                    if sequence_identifier in scaffold_length:
                        if unit.end > scaffold_length[sequence_identifier]:
                            scaffold_length[sequence_identifier] = unit.end
                    else:
                        scaffold_length.update({sequence_identifier: unit.end})

        return scaffold_length

    @staticmethod
    def print_metric(tp: int, tn: int, fp: int, fn: int):
        """
        Confusion matrix metrics computation.
        :param tp: true-positive
        :param tn: true-negative
        :param fp: false-positive
        :param fn: false-negative
        """
        accuracy = Benchmark.accuracy(tp, tn, fp, fn)
        specificity = Benchmark.specificity(tn, fp)
        sensitivity = Benchmark.sensitivity(tp, fn)
        precision = Benchmark.precision(tp, fp)
        f1_score = Benchmark.f1_score(tp, fp, fn)
        mcc = Benchmark.mcc(tp, tn, fp, fn)
        print("# TP=%i, TN=%i, FP=%i, FN=%i" % (tp, tn, fp, fn))
        print("# Accuracy=%.2f, Specificity=%.2f, Sensitivity=%.2f, Precision=%.2f, F1-Score=%.2f, MCC=%.2f" %
              (accuracy, specificity, sensitivity, precision, f1_score, mcc))

    @staticmethod
    def print_exon_metric(tp: int, tn: int, fp: int, fn: int, ae: int, pe: int, me: int, we: int,
                          five_prime_end_true: int, three_prime_end_true: int):
        """
        Metrics computation for exons.
        :param tp: true-positive
        :param tn: true-negative
        :param fp: false-positive
        :param fn: false-negative
        :param ae: actual excon
        :param pe: predicted exon
        :param me: missing exon
        :param we: wrong exon
        :param five_prime_end_true:
        :param three_prime_end_true:
        """
        Benchmark.print_metric(tp, tn, fp, fn)
        try:
            precision_guigo = tp / pe
            sensitivity_guigo = tp / ae
            me_score = me / ae
            we_score = we / pe
            five_prime_end_score = five_prime_end_true / (tp + fp)
            three_prime_end_score = three_prime_end_true / (tp + fp)
        except ZeroDivisionError:
            precision_guigo = 0.0
            sensitivity_guigo = 0.0
            me_score = 0.0
            we_score = 0.0
            five_prime_end_score = 0.0
            three_prime_end_score = 0.0

        print("# Precision(Guigó)=%.2f, Sensitivity(Guigó)=%.2f, MEscore=%.2f, WEscore=%.2f, 5'-UTR=%.2f, 3'-UTR=%.2f" %
              (precision_guigo, sensitivity_guigo, me_score, we_score, five_prime_end_score, three_prime_end_score))

    @staticmethod
    def print_gene_metric(tg: int, ag: int, pg: int):
        """
        Prints stats about the gene metrics
        :param tg: true gene
        :param ag: actual gene
        :param pg: predicted gene
        """
        try:
            sensitivity = tg / ag
            precision = tg / pg
        except ZeroDivisionError:
            sensitivity = 0.0
            precision = 0.0
        print("# Precision=%.2f, Sensitivity=%.2f, TG=%d, AG=%d, PG=%d" % (precision, sensitivity, tg, ag, pg))


class BenchmarkCDS(Benchmark):

    def __init__(self, args: ArgumentParser):
        self.nuc_metric: list = []
        self.loc_metric: list = []
        super().__init__(args)

    def get_metrics(self):
        return self.nuc_metric, self.loc_metric

    def analyse(self):
        sequences_ref = set(self.units_ref["Gene"].keys())
        sequences_pred = set(self.units_pred["Gene"].keys())
        sequences_keys = sequences_ref.intersection(sequences_pred)

        # clean memory
        for to_remove in sequences_ref - sequences_pred:
            del self.units_ref["Gene"][to_remove]
        for to_remove in sequences_pred - sequences_ref:
            del self.units_pred["Gene"][to_remove]

        total_nuc_metric = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        total_loc_metric = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        for sequence_identifier in sequences_keys:
            cds_intervals = self.check_scaffold(sequence_identifier, self.units_ref["Gene"], self.verbose)
            pred_intervals = BenchmarkCDS.check_scaffold(sequence_identifier, self.units_pred["Gene"], self.verbose)
            if self.verbose:
                print("BenchmarkCDS #1: Sequence %s" % sequence_identifier)
            nuc_metric, loc_metric = BenchmarkCDS.walker(cds_intervals, pred_intervals, self.verbose)
            total_nuc_metric = BenchmarkCDS.update_metric(total_nuc_metric, nuc_metric)
            total_loc_metric = BenchmarkCDS.update_metric(total_loc_metric, loc_metric)

        tp, tn, fp, fn, *rest = total_nuc_metric
        ltp, ltn, lfp, lfn, *rest = total_loc_metric
        print("Nucleotide level:")
        self.nuc_metric = BenchmarkCDS.get_metric(tp, tn, fp, fn, True)
        print("Locus level:")
        self.loc_metric = BenchmarkCDS.get_metric(ltp, ltn, lfp, lfn, True)

    @staticmethod
    def update_metric(ref: list, new: tuple):
        if len(ref) != len(new):
            return False

        for i in range(0, len(ref)):
            ref[i] += new[i]

        return ref

    @staticmethod
    def check_scaffold(sequence_id: str, units: dict, verbose: int = 0):
        interval_ref_cds: list = Intervals.get_coding_intervals(units[sequence_id])
        # interval_ref_cds.sort(key=lambda x: x[0])
        length_without_merging = len(interval_ref_cds)
        interval_ref_cds: list = Intervals.merge_intervals(interval_ref_cds)
        interval_ref_cds: list = [tuple(x) for x in interval_ref_cds]

        if verbose:
            print("BenchmarkCDS #1: Interval lengths before %d and after %d merging; Δ=%d " %
                  (length_without_merging, len(interval_ref_cds), length_without_merging - len(interval_ref_cds)))

        return interval_ref_cds

    @staticmethod
    def walker(reference_intervals: list, prediction_intervals: list, verbose: int = 0):
        reference_intervals.reverse()
        prediction_intervals.reverse()
        show_metrics: bool = False

        if verbose:
            show_metrics = True
            print("BenchmarkCDS #1: Starting Benchmark walker with:")
        if verbose > 1:
            print("BenchmarkCDS #2: Reference: %s" % str(reference_intervals))
            print("BenchmarkCDS #2: Prediction: %s" % str(prediction_intervals))

        ref: tuple = reference_intervals.pop()
        pred: tuple = prediction_intervals.pop()
        first_start, second_start = (ref[0], pred[0]) if ref[0] < pred[0] else (pred[0], ref[0])

        if first_start == 1:
            first_start -= 1

        # counters
        tp, tn, fp, fn, ltp, ltn, lfp, lfn = 0, first_start, 0, 0, 0, 0, 0, 0

        if first_start > 0:
            ltn += 1

        end: int = reference_intervals[0][1] if reference_intervals[0][1] < prediction_intervals[0][1] else \
            reference_intervals[0][1]

        count: int = 1
        count_limit: int = 1000000
        i: int = first_start
        list_ref_ended, list_pred_ended = False, False

        while i < end:

            if verbose >= 2:
                print(
                    "BenchmarkCDS #2: Round: %d, i: %d, TP: %d, TN: %d, FP: %d, FN: %d| ref: %s, pred: %s, overlap: %d" %
                    (count, i, tp, tn, fp, fn, str(ref), str(pred), int(Intervals.check_overlap(ref, pred))))

            inner: int = 1
            overlaps: dict = {"ref": [], "pred": []}
            while Intervals.check_overlap(ref, pred):

                if verbose >= 3:
                    print("BenchmarkCDS #3: Loop: %d, TP: %d, TN: %d, FP: %d, FN: %d| ref: %s, pred: %s" %
                          (inner, tp, tn, fp, fn, str(ref), str(pred)))

                first_start, second_start, last_end, second_end = BenchmarkCDS.get_positions(ref, pred)

                if not len(overlaps["ref"]) or (len(overlaps["ref"]) and ref is not overlaps["ref"][-1]):
                    overlaps["ref"].append(ref)

                if not len(overlaps["pred"]) or (len(overlaps["pred"]) and pred is not overlaps["pred"][-1]):
                    overlaps["pred"].append(pred)

                if not list_ref_ended:
                    try:
                        if first_start == ref[0] or list_pred_ended:
                            i = ref[1]
                            ref: tuple = reference_intervals.pop()
                    except IndexError:
                        list_ref_ended = True

                if not list_pred_ended:
                    try:
                        if first_start == pred[0] or list_ref_ended:
                            i = pred[1]
                            pred: tuple = prediction_intervals.pop()
                    except IndexError:
                        list_pred_ended = True

                inner += 1

                if list_ref_ended and list_pred_ended:
                    i = first_start
                    break

                if inner > 1000:
                    print(
                        "### Warning 1000 overlaps in a row?? Skipping! ref: %s, pred: %s" % (str(ref), str(pred)))
                    break

            if inner > 1:
                i, ntp, ntn, nfp, nfn, nltp, nltn, nlfp, nlfn = BenchmarkCDS.resolve_overlaps(overlaps["ref"],
                                                                                              overlaps["pred"],
                                                                                              verbose)
                tp += ntp
                tn += ntn
                fp += nfp
                fn += nfn
                ltp += nltp
                ltn += nltn
                lfp += nlfp
                lfn += nlfn

            first_start, second_start, last_end, second_end = BenchmarkCDS.get_positions(ref, pred)

            if (second_start - second_end) > 0:
                tn, ltn = BenchmarkCDS.count_for_loci(second_start - second_end, tn, ltn)

            i = last_end

            if verbose > 3:
                print(
                    "BenchmarkCDS #4: References ended %d, References length %d, Prediction ended %d, Prediction length %d" %
                    (list_ref_ended, len(reference_intervals), list_pred_ended, len(prediction_intervals)))
            if list_ref_ended and list_pred_ended:
                break
            elif not list_ref_ended and not list_pred_ended:  # get new element
                if reference_intervals[-1][0] <= prediction_intervals[-1][0] and \
                        (i >= second_end == ref[1] or i >= last_end == ref[1]):
                    ref: tuple = reference_intervals.pop()
                    list_ref_ended = not bool(len(reference_intervals))
                elif prediction_intervals[-1][0] <= reference_intervals[-1][0] and \
                        (i >= second_end == pred[1] or i >= last_end == pred[1]):
                    pred: tuple = prediction_intervals.pop()
                    list_pred_ended = not bool(len(prediction_intervals))
            elif list_ref_ended and not list_pred_ended:
                if verbose > 3:
                    print("BenchmarkCDS #4: Only prediction continues")
                i, fp, lfp, tn, ltn = BenchmarkCDS.resolve_last_chain(reference_intervals, i, fp, lfp, tn, ltn)
            elif list_pred_ended and not list_ref_ended:
                if verbose > 3:
                    print("BenchmarkCDS #4: Only reference continues")
                i, fn, lfn, tn, ltn = BenchmarkCDS.resolve_last_chain(reference_intervals, i, fn, lfn, tn, ltn)

            count += 1
            if count >= count_limit:
                print("!!!! Ups, too many rounds %d from %d allowed. Terminated!" % (count, count_limit))
                break

        if verbose:
            print("Nucleotide level:")
        nuc_metric = Benchmark.get_metric(tp, tn, fp, fn, show_metrics)

        if verbose:
            print("Loci level:")
        loc_metric = Benchmark.get_metric(ltp, ltn, lfp, lfn, show_metrics)
        return nuc_metric, loc_metric

    @staticmethod
    def resolve_last_chain(intervals: list, break_point: int, nuc_match: int, loc_match: int, nuc_tn: int,
                           loc_tn: int):
        run: bool = True

        while run:
            if len(intervals) > 0:
                interval = intervals.pop()
                nuc_tn, loc_tn = BenchmarkCDS.count_for_loci(interval[0] - break_point, nuc_tn, loc_tn)
                nuc_match, loc_match = BenchmarkCDS.count_for_loci(interval[1] - interval[0], nuc_match, loc_match)
                break_point = interval[1]
            else:
                run = False

        return break_point, nuc_match, loc_match, nuc_tn, loc_tn

    @staticmethod
    def resolve_overlaps(reference: list, prediction: list, verbose: int = 0) -> tuple:
        break_point, tp, tn, fp, fn, ltp, ltn, lfp, lfn = 0, 0, 0, 0, 0, 0, 0, 0, 0

        if verbose > 1:
            print("BenchmarkCDS #1: Solving overlaps with references %s and predictions %s" %
                  (str(reference), str(prediction)))

        if len(reference) == 1 and len(reference) == len(prediction):  # No chain!
            ref: tuple = reference.pop()
            pred: tuple = prediction.pop()

            if ref[0] < pred[0]:  # reference first
                if verbose > 2:
                    print("BenchmarkCDS #2: Only two elements: Reference first")
                if pred[0] > ref[0] and pred[1] < ref[1]:  # it is completely inside reference
                    if verbose > 2:
                        print("BenchmarkCDS #2: prediction is inside the reference")
                    fn, lfn = BenchmarkCDS.count_for_loci(pred[0] - ref[0], fn, lfn)
                    tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - pred[0], tp, ltp)
                    fn, lfn = BenchmarkCDS.count_for_loci(ref[1] - pred[1], fn, lfn)
                else:
                    fn, lfn = BenchmarkCDS.count_for_loci(pred[0] - ref[0], fn, lfn)
                    fp, lfp = BenchmarkCDS.count_for_loci(pred[1] - ref[1], fp, lfp)
                    tp, ltp = BenchmarkCDS.count_for_loci(ref[1] - pred[0], tp, ltp)
            elif pred[0] < ref[0]:  # prediction first
                if verbose > 2:
                    print("BenchmarkCDS #2: Only two elements: Prediction first")
                if pred[0] > ref[0] and pred[1] < ref[1]:  # it is completely inside reference
                    if verbose > 2:
                        print("BenchmarkCDS #2: reference is inside the prediction")
                    fp, lpn = BenchmarkCDS.count_for_loci(ref[0] - pred[1], fp, lfp)
                    tp, ltp = BenchmarkCDS.count_for_loci(ref[1] - ref[0], tp, ltp)
                    fp, lfp = BenchmarkCDS.count_for_loci(pred[1] - ref[1], fp, lfp)
                else:
                    fn, lfn = BenchmarkCDS.count_for_loci(ref[1] - pred[1], fn, lfn)
                    fp, lfp = BenchmarkCDS.count_for_loci(ref[0] - pred[0], fp, lfp)
                    tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - ref[0], tp, ltp)
            elif ref[0] == pred[0] and ref[1] == pred[1]:
                tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - pred[0], tp, ltp)

            break_point = ref[1] if ref[1] > pred[1] else pred[1]
        else:  # chain
            if verbose > 2:
                print("BenchmarkCDS #2: Solving the overlap chain")

            reference.reverse()
            prediction.reverse()

            ref: tuple = reference.pop()
            pred: tuple = prediction.pop()
            o: int = 0

            # begin of chain
            if ref[0] < pred[0]:  # reference first
                fn, lfn = BenchmarkCDS.count_for_loci(pred[0] - ref[0], fn, lfn)
                tp, ltp = BenchmarkCDS.count_for_loci(ref[1] - pred[0], tp, ltp)
                break_point = ref[1]
            else:
                fp, lfp = BenchmarkCDS.count_for_loci(ref[0] - pred[0], fp, lfp)
                tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - ref[0], tp, ltp)
                break_point = pred[1]

            # start overlapping region
            while Intervals.check_overlap(ref, pred):
                if verbose > 2:
                    print("### Overlap %d, breakpoint: %d, TP: %d, TN: %d, FP: %d, FN: %d| ref: %s, pred: %s" %
                          (o, break_point, tp, tn, fp, fn, str(ref), str(pred)))

                # check for next element
                if pred[1] <= break_point and len(prediction):  # need a new prediction
                    pred = prediction.pop()
                    first_start, second_start, last_end, second_end = BenchmarkCDS.get_positions(ref, pred)

                    if verbose > 3:
                        print("#### New prediction %s, last break %d" % (str(pred), break_point))

                    if pred[0] > ref[0] and pred[1] < ref[1]:
                        if verbose > 3:
                            print("#### Prediction %s is completely inside reference %s, last break point %d" %
                                  (str(pred), str(ref), break_point))
                        fn, lfn = BenchmarkCDS.count_for_loci(pred[0] - break_point, fn, lfn)
                        tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - pred[0], tp, ltp)
                        break_point = pred[1]
                    else:
                        fn, lfn = BenchmarkCDS.count_for_loci(pred[0] - break_point, fn, lfn)
                        tp, ltp = BenchmarkCDS.count_for_loci(ref[1] - pred[0], tp, ltp)  # TODO: ReCheck got -
                        if (ref[1] - pred[0]) < 0:
                            exit(5)
                        break_point = ref[1]
                    continue

                elif ref[1] <= break_point and len(reference):  # need a new reference
                    ref = reference.pop()
                    first_start, second_start, last_end, second_end = BenchmarkCDS.get_positions(ref, pred)

                    if verbose > 3:
                        print("#### New reference %s, last break %d" % (str(ref), break_point))

                    if ref[0] > pred[0] and ref[1] < pred[1]:
                        if verbose > 3:
                            print("#### Reference %s is completely inside prediction %s, new break point %d" %
                                  (str(ref), str(pred), break_point))
                        fp, lfp = BenchmarkCDS.count_for_loci(ref[0] - break_point, fp, lfp)
                        tp, ltp = BenchmarkCDS.count_for_loci(pred[1] - ref[0], tp, ltp)
                        break_point = ref[1]
                    else:
                        fp, lfp = BenchmarkCDS.count_for_loci(ref[0] - break_point, fp, lfp)
                        # tp, ltp = BenchmarkCDS.count_for_loci(second_end - second_start, tp, ltp)  # TODO: ReCheck got -
                        break_point = pred[1]
                    continue

                else:  # one list is finished at least
                    if verbose > 2:
                        print("### loop finished with ref %s, pred %s, references %s, predictions %s" %
                              (str(ref), str(pred), str(reference), str(prediction)))
                    break

            # tail
            if pred[1] > ref[1]:  # prediction is longer
                fp, lfp = BenchmarkCDS.count_for_loci(pred[1] - ref[1], fp, lfp)
                break_point = pred[1]
            else:
                fn, lfn = BenchmarkCDS.count_for_loci(ref[1] - pred[1], fn, lfn)
                break_point = ref[1]

        return break_point, tp, tn, fp, fn, ltp, ltn, lfp, lfn

    @staticmethod
    def count_for_loci(delta: int, nuc_ref: int, loci_ref: int) -> tuple:
        nuc_ref += delta
        if delta:
            loci_ref += 1
        return nuc_ref, loci_ref

    @staticmethod
    def get_positions(ref: tuple, pred: tuple):
        first_start, second_start = (ref[0], pred[0]) if ref[0] < pred[0] else (pred[0], ref[0])
        last_end, second_end = (pred[1], ref[1]) if ref[1] < pred[1] else (ref[1], pred[1])
        return first_start, second_start, last_end, second_end