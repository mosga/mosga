import matplotlib.pyplot as plt
import pandas
import numpy
import seaborn

from typing import Tuple, List
from prettytable import PrettyTable
from argparse import ArgumentParser
from collections import Counter
from lib.analysis.charts import Charts
from lib.misc.fasta import FASTA
from lib.misc.file_io import FileIO


class CGGenesComparison:
    """
    Compares multiple set of genes from genomes against each other.
    """

    def __init__(self, args: ArgumentParser):
        """
        Initiates the comparison.
        Depending on the arguments a total heatmap will be generated or a binned radar/spider chart. The resulting files
        will be stored in the directory under the argument chart.
        :param args: the arguments.
        """
        self.__args = args
        self._imported = self.__args.from_import
        headers, genes_length = FASTA.get_headers(self.__args.proteins, True, self.__args.verbose)

        if self._imported:
            headers = self.filter_names(headers)

        genome_sizes = self.count_genome_frequency(headers)

        if self.__args.tool == "blastn":
            results = self.__read_blastn_result(self.__args.result, self._imported)
        else:
            raise CGUnknownToolSelected("Unknown tool selected: " + self.__args.tool)
            exit(5)

        # Reads the database search file
        bins = ((70, 80), (80, 90), (90, 101))
        bins_label = ["70-80 %", "80-90 %", "90-100 %"]
        matrix, genomes_bin, genome_matrix = self.__read_results(results, headers, genome_sizes, bins,
                                                                 self.__args.threshold)

        # Generates table and heatmap with all genome gene matches
        self.__compare_genes_identity(genome_matrix, genome_sizes, self.__args.chart, self.__args.heatmap,
                                      self.__args.labels, True)

        # Generates a radar/spider plot with all
        if self.__args.radar:
            self.__create_radar_plot(genomes_bin, bins, genome_sizes, self.__args.chart, self.__args.labels, True,
                                     bins_label)

        # Generates a heatmap with all single gene matches
        if self.__args.gene_heatmap:
            self.__create_heatmap(self.__args.chart, matrix)

    @staticmethod
    def __read_results(results, headers: list, genomes_sizes: dict, bins: list,
                       percent_identity: int = 90) -> tuple:
        """
        Reads the results generator and creates two matrices. On matrix plots every gene against every gene while the
        other contained genome binned genes
        :param results: the results file yield generator that returns the single comparison (blast/diamond) results.
        :param headers: list of all genes / FASTA file sequence ids.
        :param genomes_sizes: dictionary with the genomes and the number of containing genes.
        :param bins: list of tuple (bins) for the radar plot binning.
        :param percent_identity: the threshold of the percentage of nucleotide identity.
        :return:
        """
        # Headers dictionary
        if percent_identity > 100 or percent_identity <= 0:
            print("Overwrite threshold to 90 due to invalid input")
            percent_identity = 90

        headers_len = len(headers)
        headers_dict: dict = {}
        for h in range(0, headers_len):
            headers_dict.update({headers[h]: h})
        genome_index: dict = {}
        g = 0
        for k in genomes_sizes.keys():
            genome_index.update({k: g})
            g += 1

        # Matrix
        gene_matrix = numpy.zeros(shape=(headers_len, headers_len), dtype='uint8')
        genome_matrix = numpy.zeros(shape=(len(genomes_sizes), len(genomes_sizes)), dtype='uint16')

        # Binning
        genomes_values: dict = {k: [] for k in genomes_sizes.keys()}
        genomes_dict: dict = {k: None for k in genomes_sizes.keys()}

        c = 0
        for res in results:
            try:
                # fill matrix
                query = headers_dict[res[0]]
                seq = headers_dict[res[1]]

                qg = genome_index[res[0].split('_')[0]]
                sg = genome_index[res[1].split('_')[0]]

                try:
                    gene_matrix[query][seq] = int(res[2])

                    if res[2] >= percent_identity and res[4]:
                        genome_matrix[qg][sg] += 1

                except IndexError:
                    pass

                # fill bins
                single_bin = CGGenesComparison.find_bin(res[2], bins)
                genomes_values[res[1].split('_')[0]].append(single_bin)
            except KeyError:
                continue

        # count bins
        for g in genomes_values.keys():
            genomes_dict.update({g: Counter(genomes_values[g])})
        del genomes_values

        return gene_matrix, genomes_dict, (genome_matrix, genome_index)

    @staticmethod
    def __compare_genes_identity(genome_matrix: tuple, genome_sizes: dict, chart_directory: str, heatmap: bool = False,
                                 labels: dict = {}, normalize: bool = False, annotation: bool = False):
        """
        Compares multiple genomes against each other from a genome matrix.
        :param genome_matrix: matrix with the genomes and the percentage of nucleotide identity as value.
        :param genome_sizes: dictionary with genome names as key and number of genes as value.
        :param chart_directory: path to the directory where the results will be written.
        :param heatmap: write a heatmap from the genome comparison matrix.
        :param labels: dictionary with hash code as keys and the wished genome name as value.
        :param normalize: perform normalization before printing final results.
        :rtype: object
        """
        matrix, index = genome_matrix

        if normalize:
            normalized_matrix = numpy.zeros(shape=(len(genome_sizes), len(genome_sizes)), dtype='float32')
            for r in range(0, len(genome_sizes)):
                index_keys = list(index.keys())
                genome = index_keys[r]
                size = genome_sizes[genome]
                for c in range(0, len(genome_sizes)):
                    if normalized_matrix[r][c] <= 0:
                        normalized_matrix[r][c] = round((matrix[r][c] / size) * 100, 2)
            matrix = normalized_matrix

        map_names = CGGenesComparison.map_names(index.keys(), labels)

        if heatmap:
            seaborn.set(font_scale=1.4)
            df = pandas.DataFrame(matrix, index=map_names, columns=map_names)
            cmp = seaborn.color_palette("crest", as_cmap=True)
            fg = seaborn.clustermap(df, cmap=cmp, annot=annotation, fmt="g")
            fg.savefig(chart_directory + "/cg_genome_similarity.svg", dpi=250)

        # Create table
        table = PrettyTable()
        fields: list = ["Genomes"]
        fields.extend(map_names)
        table.field_names = fields
        c = 0
        for row in matrix:
            cols: list = [map_names[c]]
            cols.extend(row)
            table.add_row(cols)
            c += 1

        org = open(chart_directory + "/cg_gene_matches.html", "w+")
        org.write(table.get_html_string(attributes={"id": "cg_gene_matches", "class": "table table-striped table-sm"}))
        org.close()

        org_raw = open(chart_directory + "/cg_gene_matches.txt", "w+")
        org_raw.write(str(table))
        org_raw.close()

    @staticmethod
    def count_genome_frequency(headers: list) -> dict:
        """
        Separates the list of genes into the corresponding genomes and counts the number of containing genes.
        :param headers: list of all gene names.
        :rtype: object
        """
        genomes_temp = [g.split('_')[0] for g in headers]
        genomes_temp.sort()
        genomes: list = genomes_temp
        genes_counter: dict = dict().fromkeys(genomes, 0)

        for gene in headers:
            genome = gene.split('_')[0]
            genes_counter.update({genome: genes_counter[genome] + 1})

        return genes_counter

    @staticmethod
    def __read_blastn_result(filepath: str, apply_filter: bool = False):
        """
        Reads the result of the blastn database search
        # blastn -num_threads 6 -db blast/CafeteriaGene -query cafeteria_pure.fna -outfmt "6 qseqid sseqid nident pident evalue bitscore score length mismatch gapopen gaps qcovs qcovhsp" | gzip -c > cafs_gene.csv.gz
        # zcat cafs_gene.csv.gz | sort --version-sort -k1,1 -k4,4 -k5,5r | sort --version-sort -u -k1,1 -k2,2 | gzip -c > cafs_gene_filtered.csv.gz
        :param filepath: the path to the BLASTN result file.
        :return: tuple of single comparison results.
        """
        for result in FileIO.get_file_reader(filepath):
            r = result.split("\t")
            if apply_filter:
                r[0] = CGGenesComparison.filter_names(r[0])
                r[1] = CGGenesComparison.filter_names(r[1])

            # query id, sequence id, percent identity, evalue, nuc ident
            yield r[0], r[1], float(r[3]), float(r[4]), float(r[2])

    @staticmethod
    def __create_heatmap(chart_directory: str, gene_matrix: numpy):
        """
        Creates a gene-wide heatmap.
        :param chart_directory: the directory to write the heatmap.
        :param gene_matrix: numpy matrix that contains all genes and the percentage of nucleotide identity as values.
        """
        plt.imshow(gene_matrix, cmap='YlGnBu', interpolation='nearest')
        plt.savefig(chart_directory + "/cg_gene_matches_heatmap.png", dpi=100)

    @staticmethod
    def create_bins(lower_bound: int, width: int, quantity: int) -> List[Tuple]:
        """
        Creates tuple of bins depending on the given arguments.
        :param lower_bound: the lower bound.
        :param width: width of teach bin.
        :param quantity: number of bins to generate.
        :return: list of tuples (bins).
        """
        bins = []
        for low in range(lower_bound, lower_bound + quantity * width + 1, width):
            bins.append((low, low + width))
        return bins

    @staticmethod
    def find_bin(value: float, bins: list):
        """

        :param value: value that should be binned.
        :param bins: list of tuples with the bins.
        :return:
        """
        for i in range(0, len(bins)):
            if bins[i][0] <= value < bins[i][1]:
                return i
        return -1

    @staticmethod
    def __create_radar_plot(genomes_bins: dict, bins: list, genomes_sizes: dict, chart_directory: str,
                            labels: dict = {}, normalize: bool = True, legend_labels: list = None):
        """
        Creates a radar/spider plot from the binned genes.
        :param genomes_bins: dictionary with the genome name as key and a list of binned genes number as value.
        :param bins: the defined bins.
        :param genomes_sizes: dictionary with the genome name as key and genes number as value.
        :param chart_directory: path to write the plot.
        :param labels: dictionary with the hash code as key and wished genome name as value.
        :param normalize: performs normalization.
        :param legend_labels: overwrites the bins labels with a given list.
        """
        values: list = [[] for h in range(0, len(bins))]
        bin_range: list = [h for h in range(0, len(bins))]
        max_nr = 0

        for g in genomes_bins.keys():
            c = 0
            for t in bin_range:
                if normalize:
                    v = (genomes_bins[g][t] / len(genomes_bins))
                    #v = int(genomes_bin[g][t] / genomes_sizes[g] )
                    #v = round((genomes_bin[g][t] / len(genomes_bin)) / genomes_sizes[g], 2)
                else:
                    v = genomes_bins[g][t]

                max_nr = v if v > max_nr else max_nr
                values[c].append(v)
                c += 1

        # Radar plot
        lb = CGGenesComparison.map_names(list(genomes_bins.keys()), labels)

        data = [lb, ('Coding Genes Identity', values)]
        theta = Charts.radar_factory(len(data[0]), frame='polygon')

        spoke_labels = data.pop(0)
        title, case_data = data[0]

        fig, ax = plt.subplots(figsize=(6, 6), subplot_kw=dict(projection='radar'))
        fig.subplots_adjust(top=0.90, bottom=0.05)

        ax.set_title(title, position=(0.5, 1.1), ha='center')

        for d in case_data:
            line = ax.plot(theta, d)
            ax.fill(theta, d, alpha=0.33)
        ax.set_varlabels(spoke_labels)

        ax.set_rgrids([int(max_nr * (1/4)), int(max_nr * (2/4)), int(max_nr * (3/4))])
        legend_labels: list = [str(h) for h in bins] if legend_labels is None else legend_labels
        ax.legend(legend_labels, loc=(0.9, .95), labelspacing=0.1, fontsize='small')
        plt.savefig(chart_directory + "/cg_coding_genes_radar.png", dpi=200)

    @staticmethod
    def map_names(original_list: list, labels: dict) -> list:
        """
        Maps a list to a dictionary's values.
        :param original_list: list with the original names.
        :param labels: dictionary with the key that maps the original names and the desired new name as value.
        :return:
        """
        names: list = []
        for i in original_list:
            try:
                names.append(labels[i])
            except KeyError:
                names.append(i)
        return names

    @staticmethod
    def filter_names(names):
        """
        Removes prefix scaffold names for the comparative genomics workflow.
        :param names: a string or list of strings
        :return: the trimmed name or list of trimmed names
        """
        pattern = "/DB_"

        if type(names) is list:
            for i in range(0, len(names)):
                n = names[i]
                pos = n.find(pattern)
                if pos != -1:
                    n = n[pos+4:]
                names[i] = n
            return names

        if type(names) is str:
            pos = names.find(pattern)
            if pos != -1:
                return names[pos + 4:]


class CGGenesComparisonError(Exception):
    pass


class CGUnknownToolSelected(CGGenesComparisonError):
    pass
