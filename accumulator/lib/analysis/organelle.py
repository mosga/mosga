import pandas as pd
from argparse import ArgumentParser
from prettytable import PrettyTable
from lib.readers.format.CSV import CSV


class OrganelleScaffoldScan:
    """
    Counts the presence of specific units and collects the number of mitochondrial and plastid gene matches.
    :var self.__summary: Collects as dictionary all values that could be counted.
    """

    def __init__(self, args: ArgumentParser, annotations: dict = None):
        """
        Starts the scan and collection.
        :param annotations: The dictionary with all scaffolds including the units.
        :param args: argument parser
        """
        self.extend_values: bool = args.extend_organelle_indication or args.odna
        self.__summary = {}

        try:
            self.__scan(annotations)
        except Exception as e:
            print(e)
            raise OrganelleScanError("Abort organelle scan analysis")

    def __scan(self, annotations: dict) -> bool:
        """
        Scans each unit and push them into the corresponding functions. This will start the counting processes
        By this technique we can avoid software crashes.
        :return: True if it could be walked through the whole annotation.
        """
        if not len(annotations):
            return False

        # check for each sequence identifier
        for seq_id in annotations:

            # check for each element
            for unit in annotations[seq_id]:

                # basically a switch-case statement
                if self.extend_values and str(unit) == "Repeats":
                    self.__add_to_summary(seq_id, "Repeats")
                elif self.extend_values and str(unit) == "CpGIsland":
                    self.__add_to_summary(seq_id, "CpGIsland")
                if str(unit) == "tRNA":
                    self.__add_to_summary(seq_id, "tRNA")
                elif str(unit) == "rRNA":
                    self.__add_to_summary(seq_id, "rRNA")
                    try:
                        if unit.partial:
                            self.__add_to_summary(seq_id, 'rRNA(p)')
                    except (AttributeError, NameError):
                        pass

        return True

    def __add_to_summary(self, seq_id: str, name: str) -> bool:
        """
        Increase the counter for the appearance of a unit in a sequences
        :param seq_id: the related sequence identifier.
        :param name: defines which counter should be increased.
        :return: True if not failing.
        """

        try:
            self.__summary[name]
        except KeyError:
            self.__summary[name] = {}

        try:
            self.__summary[name][seq_id] += 1
        except KeyError:
            self.__summary[name].update({seq_id: 1})

        return True

    def get_summary(self):
        """
        Returns a condensed summary
        :return: A dictionary with the type depending on numbers
        """
        summary = self.__summary
        return summary

    @staticmethod
    def scan_files(directory: str, genome: str, extended: bool = False) -> dict:
        """
        Scans for mitos.txt and plastids.txt files and count the scaffold matches.
        """
        file_scan: dict = {}
        # Defines paths and name for diamond organelle search files.
        paths: list = [("%s/%s/genes/mitos.txt.gz" % (directory, genome), "Mito"),
                       ("%s/%s/genes/mitos.txt" % (directory, genome), "Mito"),
                       ("%s/%s/genes/plastids.txt.gz" % (directory, genome), "Plastid"),
                       ("%s/%s/genes/plastids.txt.txt.gz" % (directory, genome), "Plastid")
                       ]

        extend_paths: list = []

        # extend path for extend analysis
        if extended:
            paths.extend(extend_paths)

        scan_reader = {
            "Mito": DiamondResultScanner,
            "Plastid": DiamondResultScanner
        }

        bucket: dict = {}

        # Reads all organelle search files.
        for org_file in paths:
            try:
                for org in scan_reader[org_file[1]](org_file[0]).read():

                    # just basic counter
                    try:
                        file_scan[org_file[1]]
                    except KeyError:
                        file_scan.update({org_file[1]: {}})
                    try:
                        file_scan[org_file[1]][org[0]] += 1
                    except KeyError:
                        file_scan[org_file[1]].update({org[0]: 1})

                    # add additional operations
                    if len(org) > 1:
                        # pairs of values: name, value
                        for values in org[1:]:

                            # add program
                            if org_file[1] not in bucket:
                                bucket.update({org_file[1]: {}})

                            # add task
                            if values[0] not in bucket[org_file[1]]:
                                bucket[org_file[1]].update({values[0]: {}})

                            # add scaffold
                            if org[0] not in bucket[org_file[1]][values[0]]:
                                bucket[org_file[1]][values[0]].update({org[0]: []})

                            bucket[org_file[1]][values[0]][org[0]].append(values[1])
            except IOError as e:
                print(e)
                continue

        # process bucket
        for src in bucket:
            for task in bucket[src]:
                if task == "kAvg":
                    value_name = "%s%s" % (src, task)
                    if value_name not in file_scan:
                        file_scan.update({value_name: {}})
                    for seq in bucket[src][task]:
                        avg = int(round((sum(bucket[src][task][seq]) / len(bucket[src][task][seq])) / 1000, 0))
                        file_scan[value_name].update({seq: avg})
                else:
                    print("Task %s unknown for scan_file bucket entry." % task)

        del bucket
        return file_scan


class OrganelleSummary:

    def __init__(self, scan_summary: dict, gc_content: dict, scaffolds_length: dict, args: ArgumentParser,
                 outliers=None):
        """
        Builds the summary table by combing several inputs like the GC content or mitochondrial genes matches.
        :param scan_summary: The scan summary from the OrganelleScaffoldScan class
        :param gc_content: The dictionary contains all GC content values for all scaffolds
        :param scaffolds_length: The dictionary contains all scaffolds lengths.
        :param args: The argument parser
        """
        self.__scan = scan_summary
        self.__gc = gc_content
        self.__sl = scaffolds_length
        self.__outliers = outliers
        self.__args = args
        self.__extend_values: bool = self.__args.extend_organelle_indication or self.__args.odna
        self.__table = self.__build_table()
        self.__write_table()

    def __build_table(self) -> PrettyTable:
        """
        Builds up the PrettyTable object by parsing all results from the scan summary, GC content and contig length.
        :return: PrettyTable The finished and sorted table object.
        """
        temporary_rows = []
        field_names = ["Scaffold"]
        scan_summary_keys = []
        table = PrettyTable()

        # Fill rows by scan_summary
        try:
            for key in self.__scan.keys():
                field_names.append(key)
                scan_summary_keys.append(key)
                for seq in self.__scan[key]:
                    temporary_rows.append(seq)
        except (AttributeError, KeyError):
            pass

        # Fill rows by gc_content
        if len(self.__gc):
            field_names.append('GC dev')
            field_names.append('GC')
            temporary_rows.extend(self.__gc.keys())

        field_names.append('Len [kbp]')
        if 'Mito' in self.__scan:
            field_names.append('MD')

        if 'Plastid' in self.__scan:
            field_names.append('PD')

        # prepare fraction
        total_length: int = sum(self.__sl.values())

        # Create Density
        if self.__extend_values:
            if "Repeats" in self.__scan:
                field_names.append("RD")
            if "tRNA" in self.__scan:
                field_names.append("TD")
            if "rRNA" in self.__scan:
                field_names.append("RRD")
            if "CpGisland" in self.__scan:
                field_names.append("CpGD")
            field_names.append("Fraction")

        if len(scan_summary_keys):
            field_names.append("Score")

        if not len(temporary_rows):
            return table

        rows = list(set(temporary_rows))
        rows.sort()
        table.field_names = field_names

        for seq_id in rows:
            columns = [seq_id]
            sum_columns = []
            score = 0

            # Parses the scan summary
            if len(scan_summary_keys):
                for scan_key in scan_summary_keys:
                    try:
                        value = self.__scan[scan_key][seq_id]
                        if scan_key != "CpGislandAvg" and scan_key != "Repeats" and scan_key != "CpGisland":
                            sum_columns.append(value)
                        columns.append(value)
                    except KeyError:
                        columns.append(0)

                score = sum(sum_columns)

            if len(self.__gc):
                try:
                    gc_dev = self.__gc[seq_id][1]
                    gc = self.__gc[seq_id][0]

                    try:
                        if self.__outliers is not None and seq_id in self.__outliers and gc_dev > 0:
                            score *= gc_dev
                            gc_dev = str(gc_dev) + "*"

                    except KeyError:
                        score *= gc_dev

                    columns.append(gc_dev)
                    columns.append(gc)
                except KeyError:
                    columns.append(0.0)
                    columns.append(0.0)

            # Add scaffold length kbp
            columns.append(round(self.__sl[seq_id] / 1000, 2))

            # Mitos / Plastids
            if 'Mito' in self.__scan:
                MD = self.__calculate_density(columns, seq_id, 'Mito')
                if MD > 0:
                    score *= MD
            if 'Plastid' in self.__scan:
                PD = self.__calculate_density(columns, seq_id, 'Plastid')
                if PD > 0:
                    score *= PD

            if self.__extend_values:
                if "Repeats" in self.__scan:
                    self.__calculate_density(columns, seq_id, "Repeats")
                if "tRNA" in self.__scan:
                    self.__calculate_density(columns, seq_id, "tRNA", 1000000)
                if "rRNA" in self.__scan:
                    self.__calculate_density(columns, seq_id, "rRNA", 1000000)
                if "CpGisland" in self.__scan:
                    self.__calculate_density(columns, seq_id, "CpGisland", 1000000)

                # Fraction
                columns.append(round((self.__sl[seq_id] / total_length)*1000, 2))

            if len(scan_summary_keys):
                columns.append(int(score))

            # Add the whole column to the row (only if score not 0)
            if int(score) > 0 or self.__extend_values:
                table.add_row(columns)
            else:
                continue

        # Apply ODNA prediction
        if self.__args.odna:
            results, scaffolds = self.__apply_odna(table)
            if len(results):
                table = self.__copy_odna_table(table, results, not self.__args.extend_organelle_indication)

        # Try to resort the table
        try:
            if "Score" in table.field_names:
                table.sortby = "Score"
                table.reversesort = True
            else:
                table.sorty = "Scaffold"
        # table will raise an Exception which is not specified
        except:
            print("Sorting and reversing failed.")
            pass

        print("### Organllar DNA Scanner")
        print(table)

        return table

    @staticmethod
    def __copy_odna_table(table: PrettyTable, results: list, trimming: bool = True) -> PrettyTable:
        index_to_ignore: list = []
        old_field_names = table.field_names
        to_trim: list = ["Repeats", "RD", "TD", "RRD", "CpGD", "Fraction"]

        if not trimming:
            field_names = old_field_names
        else:
            trimmed_field_names: list = []

            for trim in to_trim:
                if trim in old_field_names:
                    index_to_ignore.append(old_field_names.index(trim))

            for i in range(0, len(old_field_names)):
                if i in index_to_ignore:
                    continue
                else:
                    trimmed_field_names.append(old_field_names[i])
            field_names = trimmed_field_names

        field_names.append("ODNA")
        new_table = PrettyTable()
        new_table.field_names = field_names

        if len(table._rows) == len(results):
            for x in range(0, len(table._rows)):
                old_row = table._rows[x]
                continue_line: bool = False

                if not trimming:
                    new_row = old_row
                else:
                    new_row: list = []

                    # Skip line?
                    if "Score" in old_field_names and int(old_row[old_field_names.index("Score")]) == 0 and \
                            results[x] == 0:
                        continue_line = True

                    # Skip extended columns
                    for i in range(0, len(old_row)):
                        if i in index_to_ignore:
                            continue
                        else:
                            new_row.append(old_row[i])

                if continue_line:
                    continue

                new_row.append(results[x])
                new_table.add_row(new_row)
        else:
            return table

        return new_table

    def __apply_odna(self, table: PrettyTable) -> tuple:
        from sklearn.pipeline import Pipeline
        from sklearn.preprocessing import StandardScaler
        from sklearn.ensemble import ExtraTreesClassifier
        from sklearn.ensemble import RandomForestClassifier

        def warn(*args, **kwargs):
            pass
        import warnings
        warnings.warn = warn

        from pickle import load

        df: pd.DataFrame = self.__convert_PrettyTable_to_DataFrame(table)
        if df is None:
            print("Failed to build pandas table, ignore ODNA.")
            return [], []

        names = df["Scaffold"].values
        df = df.drop(columns=["Scaffold"])

        # Decide between the two models
        if len(df) <= 500:
            scaler: StandardScaler = load(open("/opt/mosga/data/odna_scaler_7_500.pkl", "rb"))
            model: ExtraTreesClassifier = load(open("/opt/mosga/data/odna_xt_7_500.pkl", "rb"))
        else:
            scaler: StandardScaler = load(open("/opt/mosga/data/odna_scaler_7_50000.pkl", "rb"))
            model: RandomForestClassifier = load(open("/opt/mosga/data/odna_rnd_7_50000.pkl", "rb"))
        pipeline = Pipeline([('std_scaler', scaler)])

        prepared_values = pipeline.transform(df)
        predict_values = model.predict(prepared_values)

        found_scaffolds = []
        for x in range(0, len(predict_values)):
            if predict_values[x] == 1:
                found_scaffolds.append(x)

        return predict_values, found_scaffolds

    def __convert_PrettyTable_to_DataFrame(self, table: PrettyTable):
        name_map: dict = {
            "Scaffold": "Scaffold",
            "Repeats": "Repeats",
            "tRNA": "tRNA",
            "rRNA": "rRNA",
            "rRNA(p)": "rRNAp",
            "Mito": "Mito",
            "Plastid": "Plastid",
            "CpGisland": "CpGisland",
            "GC": "GC",
            "GC dev": "GC-dev",
            "Len [kbp]": "Len",
            "MD": "MD",
            "PD": "PD",
            "RD": "RD",
            "TD": "TD",
            "RRD": "RRD",
            "CpGD": "CpGD",
            "Fraction": "Fraction",
            "Score": "Score"
        }

        values: dict = {
            "Scaffold": [],
            "Repeats": [],
            "tRNA": [],
            "rRNA": [],
            "rRNAp": [],
            "Mito": [],
            "Plastid": [],
            "CpGisland": [],
            "GC": [],
            "GC-dev": [],
            "GC-out": [],
            "Len": [],
            "MD": [],
            "PD": [],
            "RD": [],
            "TD": [],
            "RRD": [],
            "CpGD": [],
            "Fraction": [],
        }

        # check for required values
        for row in table:
            row.border = False
            row.header = False
            gc_out = 0

            for col in name_map.keys():
                try:
                    table_value = row.get_string(fields=[col])
                except Exception:  # PrettyTable error...
                    table_value = 0

                try:
                    target_value = values[name_map[col]]
                except (KeyError, AttributeError):
                    continue

                if col == "GC dev":
                    if table_value.strip().find("*") >= 0:
                        table_value = table_value.strip()[:-1]
                        gc_out = 1

                new_value = str(table_value).strip()
                # check for string or int
                try:
                    a = int(new_value)
                    b = float(new_value)
                    if a == b:
                        new_value = a
                    else:
                        new_value = b
                except (TypeError, ValueError):
                    pass

                target_value.append(new_value)

            values["GC-out"].append(gc_out)

        try:
            df = pd.DataFrame.from_dict(values).astype({"GC": float, "GC-dev": float, "GC-out": int, "Len": float,
                                                        "MD": float, "PD": float, "RD": float, "TD": float,
                                                        "RRD": float, "CpGD": float, "Fraction": float})
        except Exception:
            df = None

        return df

    def __calculate_density(self, column: list, seq_id: str, target: str, divisor: int = 10000) -> float:
        """
        Adds a target attribute devided by the division of the scaffold length into the given column list.
        :param column: The column will be extended.
        :param seq_id: The sequence/scaffold identifier
        :param target: The target attribute inside self.__scan
        :param divisor: The divisor for the scaffold length ( bp -> 10 kpb ).
        """
        try:
            entry = self.__scan[target][seq_id]
        except KeyError:
            entry = 0.0
        result = round(entry / (self.__sl[seq_id] / divisor), 2)
        column.append(result)
        return result

    def __write_table(self):
        """
        Writes the final table as HTML and text file.
        """
        try:
            if self.__args.organelle_file is not None:
                org = open(self.__args.organelle_file, 'w+')
                org.write(self.__table.get_html_string(
                    attributes={"id": "organelles", "class": "table table-striped table-sm"}))
                org.close()
            org_raw = open(self.__args.directory + '/' + self.__args.genome + '/organelles.txt', 'w+')
            org_raw.write(str(self.__table))
            org_raw.close()
        except PermissionError as e:
            print(e)
            print("Can not write organelle HTML file")
        except Exception as e:
            raise OrganelleScanError(e)

    def get_table(self) -> PrettyTable:
        """
        Returns the table.
        :return: Returns the final table.
        """
        return self.__table


class DiamondResultScanner:
    """"
    Scans for the Diamond results and yields the sequence/scaffold identifier per entry.
    """

    def __init__(self, path: str):
        self.path = path

    def read(self):
        reader = CSV("\t")
        try:
            entries = reader.read(self.path)

            for entry in entries:
                yield entry[0],
        except FileNotFoundError:
            return []


class OrganelleScanError(Exception):
    pass
