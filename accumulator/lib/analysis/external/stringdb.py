import json
from argparse import ArgumentParser
from time import sleep
from prettytable import PrettyTable
from stripe.http_client import requests
from lib.analysis.protein_names import ProteinNames


class StringDBError(Exception):
    pass


class StringDBEmptyFunctions(StringDBError):
    pass


class StringDBNoMapping(StringDBError):
    pass


class StringDB(ProteinNames):

    def __init__(self, args: ArgumentParser):
        """
        Initialize the class and collect the functions name list from the database.
        :param args: the arguments.
        """
        super().__init__(args)
        self.string_dir = args.directory + args.genome + "/genes/string/"
        self.url = "https://string-db.org/api"
        self.headers = {'User-Agent': 'MOSGA STRING API 1.0'}
        #self.get_organism_list()

    def __get_string_ids(self) -> list:
        if not len(self._functions):
            raise StringDBError

        # limit 2000: TODO: split into chunks and send requests
        if len(self._functions) > 2000:
            self._functions = self._functions[:2000]

        ids: list = []
        params = {
            "identifiers": "\r".join(self._functions),
            "limit": 1,
            "echo_query": 1,
            "caller_identity": self.headers['User-Agent']
        }

        if self.args.specie != 0:
            params.update({"species": self.args.specie})
        elif len(self._functions) > 10:
            self._functions = self._functions[:10]
            print("Sending IDs without a specified taxonomy ID. Truncating protein names to the first 10 protein names")
            print(self._functions)

        request_url = "/".join([self.url, "tsv-no-header", "get_string_ids"])
        results = requests.post(request_url, data=params, headers=self.headers)

        for line in results.text.strip().split("\n"):
            ls = line.split("\t")
            ids.append((ls[0], ls[2], ls[5], ls[6]))

        if not len(ids):
            raise StringDBNoMapping("Got empty list back")

        return ids

    def __create_table(self, ids: list) -> bool:
        tab = PrettyTable()
        tab.field_names = ["MOSGA", "STRING ID", "Protein ID", "Description"]
        for row in ids:
            tab.add_row(row)

        output = open(self.args.output, 'w+')
        output.write(tab.get_html_string(attributes={"id": "stringdb", "class": "table table-striped table-sm"}))
        output.close()

        return True

    def __create_images(self, string_ids: list):
        request_url = "/".join([self.url, "image", "network"])

        if len(string_ids) <= 10:
            self.__whole_network_image(request_url, string_ids)
        else:
            print("Downloading " + str(len(string_ids)) + " network images.")
            for gene in string_ids:
                params = {
                    "identifiers": gene[1],
                    "add_white_nodes": 15,
                    "network_flavor": "confidence",
                    "caller_identity": self.headers['User-Agent']
                }
                if self.args.specie != 0:
                    params.update({"species": self.args.specie})
                response = requests.post(request_url, data=params, headers=self.headers)
                file_name = self.string_dir + "%s" % gene[1] + "_network.png"
                with open(file_name, 'wb') as fh:
                    fh.write(response.content)
                sleep(0.75)

    def __whole_network_image(self, request_url: str, string_ids: list):
        params = {
            "identifiers": "\r".join([x[1] for x in string_ids]),
            "add_white_nodes": 15,
            "network_flavor": "confidence",
            "caller_identity": self.headers['User-Agent']
        }
        if self.args.specie != 0:
            params.update({"species": self.args.specie})
        response = requests.post(request_url, data=params, headers=self.headers)
        file_name = self.string_dir + "network.png"
        with open(file_name, 'wb') as fh:
            fh.write(response.content)

    def __prepare_submission(self):
        try:
            string_ids = self.__get_string_ids()
            self.__create_table(string_ids)
            #self.__create_images(string_ids)
        except (StringDBNoMapping, StringDBEmptyFunctions) as e:
            print(e)
            raise StringDBError

    def submit(self):
        try:
            self.__prepare_submission()
        except StringDBError as e:
            print(e)
            print("STRING API could not successfully submit your request")

    def get_organism_list(self):
        organism_url = "https://stringdb-static.org/organism_overview.html"
        results = requests.get(organism_url, headers=self.headers)

        organism_rows: list = []
        table_started: bool = False
        for line in results.text.strip().split("\n"):
            line = line.rstrip()

            if line == "<table>":
                table_started = True

            if line == '</table>':
                table_started = False

            if table_started and line != "<table>":
                name = line[49:line.find("</span>")]
                id_start = line.find("/overview.")+10
                ncbi_id = line[id_start:line.find(".", id_start)]
                organism_rows.append((name, ncbi_id))

        organism_rows.sort(key=lambda x: x[0])
        json_list = json.dumps(organism_rows)

        with open("/opt/mosga/gui/includes/modules/stringdb_species.json", "w") as fp:
            fp.write(json_list)

        return True
