#!/usr/bin/env python3

__author__ = "Roman Martin"
__copyright__ = "Copyright (C) 2021 Roman Martin"
__license__ = "MIT"
__version__ = "1.0.0"

import base64
import json
import secrets
import requests

from argparse import ArgumentParser
from enum import Enum
from requests import Response


class KeyPathwayMinerError(Exception):
    pass


class KeyPathwayMinerAPIError(KeyPathwayMinerError):
    pass


class KeyPathwayMinerUnknownNetwork(KeyPathwayMinerError):
    pass


class KeyPathwayMinerActions(Enum):

    getNetworks = 0
    submitDatasets = 1


class KeyPathwayMinerAPI:

    def __init__(self, args: ArgumentParser):
        """
        Initialize the KeyPathwayMiner API and invokes the API execution.
        :param args: the arguments.
        """
        try:
            self.__args = args

            self.verbose = self.__args.verbose
            self.__url = self.__args.url

            if self.__url[-1] != '/':
                self.__url += '/'

            self.__task = KeyPathwayMinerActions[self.__args.task]

            self.__datasets = self.__args.dataset.split(",")
            self.attachedId = secrets.token_hex(nbytes=16).upper()
            self.headers = {
                'User-Agent': 'MOSGA KeyPathwayMiner API 1.0'
            }
            self.verify = not self.__args.ignore_ssl

            try:
                self.network = int(self.__args.network)
            except ValueError:
                self.network = self._get_network_id(self.__args.network)
            except TypeError:
                self.network = None

            self._select()
        except KeyPathwayMinerError as e:
            print("KeyPathwayMinerAPI failed" + str(e))

    def _select(self):
        """
        Performs the decided task.
        """
        if self.__task is KeyPathwayMinerActions.getNetworks:
            try:
                print(self.networks)
            except AttributeError:
                networks = self._get_available_networks()
                print(networks)
        elif self.__task is KeyPathwayMinerActions.submitDatasets:
            submission = self._submit_datasets()
            if not submission:
                exit(1)

    def _submit_datasets(self) -> bool:
        datasets = self._prepare_dataset()
        settings = self._prepare_settings()
        target = self.__url + "requests/submitAsync"
        payload = {"kpmSettings": json.dumps(settings, separators=(',', ':')), "datasets": json.dumps(datasets, separators=(',', ':'))}
        if self.verbose:
            print("Start submission to " + target)
        resp = requests.post(target, payload, files={"e": "e"}, headers=self.headers, verify=self.verify)
        if resp.status_code != 200:
            raise KeyPathwayMinerAPIError('Failed ' + target + " : " + format(resp.status_code))

        if self.verbose:
            print("Submission successfully closed")

        response = resp.json()

        try:
            if response["success"]:
                if self.verbose:
                    print("Status: " + self.__url + "requests/runStatus?questID=" + str(response['questID']))
                print(str(response['questID']) + "\t" + response["resultUrl"])
                return True
        except KeyError:
            pass

        print(response)
        return False

    def _prepare_settings(self) -> dict:
        run_id = secrets.token_hex(nbytes=3).upper()

        k_values: dict = {
            "val":          str(self.__args.Kmin),
            "val_step":     str(self.__args.Kstep),
            "val_max":      str(self.__args.Kmax),
            "use_range":    self.boolean_to_string(self.__args.range),
            "isPercentage": "false"
        }

        l_values: dict = {
            "val":          str(self.__args.Lmin),
            "val_step":     str(self.__args.Lstep),
            "val_max":      str(self.__args.Lmax),
            "use_range":    self.boolean_to_string(self.__args.range),
            "isPercentage": "false",
            "datasetName":  "dataset1"
        }

        perturbation: list = [{
            "technique":        "Node-swap",
            "startPercent":     "5",
            "stepPercent":      "1",
            "maxPercent":       "15",
            "graphsPerStep":    "1"
        }]

        parameters: dict = {
            "name":                 "MOSGA API " + run_id,
            "algorithm":            "Greedy",
            "strategy":             self.__args.strategy,
            "removeBENs":           self.boolean_to_string(self.__args.removeBENs),
            "unmapped_nodes":       "Add to negative list",
            "computed_pathways":    20,
            "graphID":              self.network,
            "l_samePercentage":     self.boolean_to_string(self.__args.l_same_percentage),
            "samePercentage_val":   self.__args.same_percentage,
            "k_values":             k_values,
            "l_values":             l_values
        }

        settings: dict = {
            "parameters":           parameters,
            "withPerturbation":     self.boolean_to_string(self.__args.withPerturbation),
            "perturbation":         perturbation,
            "linkType":             self.__args.linkType,
            "attachedToID":         self.attachedId,
            "positiveNodes":        "",
            "negativeNodes":        ""
        }

        return settings

    def _prepare_dataset(self) -> list:
        """
        Prepare dataset for submission.
        :return: list of json
        """
        data_list: list = []

        c = 0
        for dataset in self.__datasets:
            c += 1
            data_raw = self._base64_file(dataset)
            data_asci = data_raw.decode('ascii')
            data_list.append({
                "name": "dataset" + str(c),
                "attachedToID": self.attachedId,
                "contentBase64": data_asci
            })
        if self.verbose:
            print("Prepared " + str(len(data_list)) + " dataset(s)")
        return data_list

    @staticmethod
    def _base64_file(path: str) -> bytes:
        """
        Encode file to base64
        :param path:
        :return:
        """
        with open(path, "r") as f:
            return base64.b64encode((f.read().rstrip()).encode())

    def _get_network_id(self, name: str) -> int:
        """
        Returns the network id by the selected name.
        :param name: the name to search for.
        :return: the matched id.
        """
        try:
            networks = self.networks
        except AttributeError:
            networks = self._get_available_networks()

        for net in networks:
            if net[1] == name:
                if self.verbose:
                    print('Found network "' + name + '" matched to graphID ' + str(net[0]))
                return net[0]

        raise KeyPathwayMinerUnknownNetwork('Could not found network "' + name + '"')

    def _get_available_networks(self):
        """
        Searches for available networks on the host.
        :return: id and network name.
        """
        network_url = self.__url + "rest/availableNetworks/"
        results = self.__get_response(network_url).json()
        networks: list = []
        for result in results:
            if result is None:
                continue
            networks.append((int(result['graphID']), result['name']))

        # Store list as tuple
        self.networks = networks

        return networks

    def __get_response(self, url: str) -> Response:
        """
        Get response from a defined url.
        :param url: the url have to be opened.
        :return: the raw response.
        """
        if self.verbose:
            print("Start request to " + url)
        resp = requests.get(url, headers=self.headers, verify=self.verify)
        if resp.status_code != 200:
            raise KeyPathwayMinerAPIError('Failed ' + url + " : " + format(resp.status_code))

        if self.verbose:
            print("Request successfully closed")

        return resp

    @staticmethod
    def boolean_to_string(v: bool) -> str:
        if v:
            return "true"
        return "false"
