import os
import sqlite3
import tempfile
import subprocess
from argparse import ArgumentParser

from lib.database.error import DataBaseNotExist


class DatabaseCreation(object):

    def __init__(self, args: ArgumentParser, importer: bool):
        self.__args = args

        # Generate the database file path
        database_file_path = None
        try:
            if self.__args.dry_run:
                database_file_path = tempfile.NamedTemporaryFile(prefix=self.__args.genome).name
        except AttributeError:
            pass

        if database_file_path is None:
            database_file_path = self.__args.directory + '/' + self.__args.genome + "/annotation" + ".db"

        database_file_exists = os.path.isfile(database_file_path)

        # Check for database file and mode
        if not importer and not database_file_exists:
            print("Database not exist or could not be loaded: " + database_file_path)
            raise DataBaseNotExist(database_file_path)

        # Create basic table structure
        try:
            purge = True if importer and self.__args.purge else False
        except AttributeError:
            purge = False

        create_tables = True if not database_file_exists or purge else False
        # Recreate database
        if purge and database_file_exists:
            if self.__args.verbose >= 2:
                print("Database #2: Purging is enabled, removing existing database file.")
            os.remove(database_file_path)

        try:
            os.makedirs(self.__args.directory, exist_ok=True)
        except FileExistsError as e:
            print("Can not create or find directory '" + self.__args.directory + "'")
            raise

        try:
            os.makedirs(self.__args.directory + '/' + self.__args.genome, exist_ok=True)
        except FileExistsError as e:
            print("Can not create or find directory '" + self.__args.directory + '/' + self.__args.genome + "'")
            raise

        try:
            if importer:
                self.__db = sqlite3.connect(database_file_path)
            else:
                self.__db = sqlite3.connect(database_file_path, check_same_thread=False)
        except OSError:
            print("Can not open or create database")
            raise
        else:
            self.__db.execute('PRAGMA foreign_keys = OFF;')
            self.__db.commit()

        if create_tables:
            try:
                self.__create_tables(database_file_path)
            except sqlite3.OperationalError:
                pass

    def __create_tables(self, path: str = "") -> bool:
        """
        Create basic tables.
        :return: true if tables could be inserted
        """
        try:
            if self.__args.verbose:
                print("Database #1: Create new database in %s" % path)
        except AttributeError:
            pass
        c = self.__db.cursor()

        c.execute('''
        CREATE TABLE
            `sources` (
        `source_id`	    INTEGER PRIMARY KEY AUTOINCREMENT,
        `source_name`	TEXT
        );
        ''')

        c.execute('''
        CREATE TABLE
            `sequences` (
        `sequence_id`	INTEGER PRIMARY KEY AUTOINCREMENT,
        `sequence_name`	TEXT,
        `sequence_order` INTEGER,
        `sequence_length` INTEGER,
        `sequence_gc_content` REAL,
        `sequence_start` INTEGER,
        `sequence_end` INTEGER,
        `sequence_original_name` TEXT
        );
        ''')

        c.execute('''
        CREATE TABLE `transcript_type` (
              `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
              `name` INTEGER
            );
        ''')

        c.execute('''
        INSERT INTO `transcript_type`
              (`name`)
            VALUES
              ( "transcript" ),
              ( "start_codon" ),
              ( "CDS" ),
              ( "exon" ),
              ( "intron" ),
              ( "stop_codon" );
        ''')

        c.execute('''
        CREATE TABLE `database` (
            `name`	TEXT NOT NULL UNIQUE,
            `value`	TEXT
            );
        ''')

        git_commit = self.__git_commit()
        c.execute('''
        INSERT INTO `database` 
            (`name`,`value`)
            VALUES 
            ("lock","unlocked"),
            ("version","1"),
            ("initialized","0"),
            ("git_commit","''' + git_commit + '''");
        ''')

        c.execute('''
        CREATE TABLE `units` (
              `unit_id` INTEGER PRIMARY KEY AUTOINCREMENT,
              `table_name` TEXT,
              `unit_type` TEXT,
              `parent_unit_id` INTEGER DEFAULT NULL,
              `parent_unit_table_name` TEXT DEFAULT NULL,
              `view` INTEGER DEFAULT 1);''')

        self.__db.commit()
        c.close()

        return True

    def __git_commit(self):
        try:
            label = subprocess.check_output(['git', 'log', '-p', '-1']).strip().decode('utf-8')
            c_label = label[7:label.find('\n')]
        except:
            c_label = '0'
        return c_label

    def get_db(self) -> sqlite3:
        """
        Returns the current database session object.
        :return: sqlite3 database
        """
        return self.__db
