import functools
import sqlite3

from typing import Dict, List, Tuple

from lib.database.error import DeserializerError, EmptyScaffold
from lib.units.basic import BasicUnit


class DbDeserializer(object):

    def __init__(self, db, values: Dict[str, List[BasicUnit]], verbose: int = 0):
        """
        Inserts recursively given values objects into the SQLite database.
        :param db: reference for the sqlite3 database object
        :param values: dictionary with the sequence name as key and a list of new objects for insertion
        """
        self.__db = db
        self.__db_conn = self.__db.get_db()
        self._values = values
        self._parent_units_table_id = 0
        self._parent_name = ''
        self._parent_types = list()
        self._parent_id_column_name = ''
        self._top_object = True
        self._created_tables = list()
        self._recursion_list = list()

        self.type_name: str = ""
        self.verbose = verbose
        self.inserted = 0

    def insert(self):
        """
        Starts the insertion process.
        """
        try:
            self.__read_element()
        except EmptyScaffold:
            raise DeserializerError("Empty nested values: %s (%d %s) : %s" % (self._parent_name,
                                                                              self._parent_units_table_id,
                                                                              str(self._parent_types),
                                                                              str(self._values)))
        self.__prepare_tables()
        try:
            self.__insert()
        # Catch missing table, add them
        except sqlite3.OperationalError as e:
            # Try to complete the missing columns
            errmsg = str(e).split()
            table = errmsg[1]
            column = errmsg[6]
            if self.__db.add_missing_unit_column(table, column):
                self.__insert()
        except DeserializerError as err:
            if self.verbose:
                print(err)
            pass

    def __read_element(self):
        # Object description #

        try:
            self.__first_element = self._values[list(self._values.keys())[0]][0]
        except IndexError as e:
            raise EmptyScaffold(e)

        self.__data_type = str(self.__first_element)
        self.type_name = self.__data_type
        self.__table_name = "unit_" + str(self.__first_element)
        self.__source_id = self.__db.get_sources(False, self.__first_element.source)[self.__first_element.source]
        self.__sequences = self.__db.get_sequences(False)
        self.__attributes = self.get_types(self.__first_element)

    def __prepare_tables(self):
        """
        Prepares the database tables by inserting entries to the global units table as well the creation of
        object specific tables.
        """

        # Rewrite data_type for recursive reconstruction
        self._parent_types.append(self.__data_type)
        self.__data_type = "_".join(self._parent_types)

        data_type = self.__data_type
        table_name = self.__table_name

        # TODO: Re-implement recursive deletion -> constraints

        self.__create_unit_table(table_name)
        unit_table_id = self.__check_unit_table(table_name, data_type)

        if unit_table_id == 0:
            self.__add_unit_table_entry(table_name, data_type)

    def __check_unit_table(self, table_name: str, data_type: str) -> int:
        """
        Lookup for the unit id in the table units. Non finding will result lead to the return value 0.
        :param table_name: table name as criteria
        :param data_type: data type name (Repeat, tRNA, Gene) as criteria
        :return: the units id or 0 if not existing
        """
        cur = self.__db_conn.cursor()
        wheres = (table_name, data_type)
        stat = '''SELECT 
                unit_id as u
            FROM
                units
            WHERE 
                table_name=? AND
                unit_type=?
            '''
        cur.execute(stat, wheres)
        result = cur.fetchone()
        cur.close()

        if result is not None:
            self.__unit_table_id = result[0]
            self.__unit_table_name = table_name
            return result[0]
        else:
            return 0

    def __add_unit_table_entry(self, table_name: str, data_type: str):
        """
        Add a new entry into the units table.
        :param table_name: table name for insertion
        :param data_type: data type name for insertion
        """

        if self.verbose >= 2:
            print("DbDeserializer #2: Creating table for %s (%s)" % (table_name, data_type))

        # Save created unit table to general unit table list
        if self._parent_units_table_id == 0:
            stat = '''
                INSERT INTO `units`
                    (`table_name`,`unit_type`,`parent_unit_id`,`parent_unit_table_name`, `view`)
                VALUES
                    ( "''' + table_name + '","' + data_type + '", NULL , NULL,"' + str(int(self._top_object)) + '");'
        else:
            parent_id = self.__db.get_unit_id(self._parent_types[0], 'unit_type')
            if parent_id > 0:
                self._parent_units_table_id = parent_id
            stat = '''
                INSERT INTO `units`
                    (`table_name`,`unit_type`,`parent_unit_id`,`parent_unit_table_name`, `view`)
                VALUES
                    ( "''' + table_name + '","' + data_type + '","' + str(self._parent_units_table_id) + '","' + \
                   self._parent_id_column_name + '","' + str(int(self._top_object)) + '" );'
        ex = self.__db_conn.execute(stat)
        self._parent_units_table_id = ex.lastrowid
        self.__db_conn.commit()

    def __create_unit_table(self, table_name: str):
        """
        Create unit table in the database
        :param table_name: table name as defined in the units table (derived from data type)
        """
        if self.verbose >= 2:
            print("DbDeserializer #2: Creating table %s" % table_name)

        if table_name in self._created_tables:
            if self.verbose >= 3:
                print("DbDeserializer #2: Table exists already. Skipping.")
            return None

        # Overrides attributes type mapping by overridden source (allows foreign key)
        attributes = self.__override_attributes(self.__attributes, 'source', int)
        types = self.__map_attributes(attributes[1])

        if self.verbose >= 3:
            print("DbDeserializer #3: Identified attributes %s" % str(attributes))
            print("DbDeserializer #3: Mapped types %s" % str(types))

        # Create table for the corresponding unit
        stat = """
        CREATE TABLE IF NOT EXISTS
            `""" + table_name + """` (
                `uid` INTEGER PRIMARY KEY,
                `sequence_id` INTEGER"""
        for i in range(len(attributes[0])):
            stat += """,
                `""" + attributes[0][i] + "` " + types[i] + " DEFAULT NULL"
        stat += """,
        FOREIGN KEY (sequence_id) REFERENCES sequences(sequence_id)
        """
        if self._parent_units_table_id > 0:
            stat += """,FOREIGN KEY (parent_id) REFERENCES """ + self._parent_name + "(id)"
        stat += ');'
        self._created_tables.append(table_name)
        if self.verbose >= 3:
            print("DbDeserializer #3: Creation statement %s" % stat)
        self.__db_conn.execute(stat)
        self.__db_conn.commit()

    def __insert(self) -> None:
        """
        Saves containing values recursively into database
        sequence_id will be appended and source overridden
        """
        list_recursive_attributes = list()
        values = self._values

        for i in range(len(self.__attributes[1])):
            if self.__attributes[1][i] == list:
                list_recursive_attributes.append(self.__attributes[0][i])

        single_value = not len(list_recursive_attributes) == 0
        names = self.__attributes[0][:]
        names.append('sequence_id')

        for sequence_name in values.keys():
            new_entry_list = list()

            # Iterate over all data to store
            for i in range(len(values[sequence_name])):

                # Override object.source attribute to normalize tables
                # TODO: Bug SwissProt Genes
                values[sequence_name] = list(map(functools.partial(
                    self.__override_unit_attribute_source),
                    values[sequence_name]))

                # Pick up last element
                new_entry = values[sequence_name].pop()
                recursive_children = dict()

                # Iterate over nested values
                for recursive_attribute in list_recursive_attributes:
                    recursive_values = getattr(new_entry, recursive_attribute)

                    if type(recursive_values) == list and len(recursive_values) == 0:
                        recursive_values = None

                    setattr(new_entry, recursive_attribute, None)
                    recursive_children.update({recursive_attribute: recursive_values})

                # Insert structure into the database
                new_entry_values = self.__map_values(self.__attributes[0], new_entry)
                new_entry_values.append(str(self.__get_sequence_key(sequence_name)))
                new_entry_list.append(new_entry_values)

                if single_value:
                    query = self.__db.insert_query_builder(self.__table_name, names, [new_entry_values])
                    db_cursor = self.__db_conn.cursor()
                    db_cursor.execute(query)
                    last_row_id = db_cursor.lastrowid
                    self.inserted += 1

                # Check for recursive values
                for child in recursive_children:
                    if recursive_children[child] is None:
                        continue
                    new_recursive_values = list(map(functools.partial(
                        self.__override_unit_attribute_value, attribute='parent_id', value=last_row_id),
                        recursive_children[child]))

                    recur = DbDeserializer(self.__db, {sequence_name: new_recursive_values}, self.verbose)
                    recur._parent_units_table_id = self._parent_units_table_id
                    recur._parent_name = self.__table_name
                    recur._parent_id_column_name = child
                    recur._parent_types.append(self.__data_type)
                    recur._top_object = False
                    recur._created_tables = self._created_tables
                    recur.insert()

            # Insert multiple non-recursive object values into database at once (speed-up)
            if not single_value:
                query = self.__db.insert_query_builder(self.__table_name, names, new_entry_list)
                self.inserted += len(new_entry_list)
                db_cursor = self.__db_conn.cursor()
                db_cursor.execute(query)

    def __get_sequence_key(self, key: str) -> int:
        """
        Returns corresponding sequence key id from database
        Caches sequences ids into a internal dictionary
        :param key: sequence_key from fasta genome file
        :return sequence id
        :rtype: int sql foreign key from sequences
        """
        try:
            return self.__sequences[key]
        except KeyError:
            self.__db.add_sequence(key)
            self.__sequences = self.__db.get_sequences(False)
            return self.__sequences[key]

    @staticmethod
    def __map_values(attributes: List[str], value) -> List[str]:
        values_list = list()
        for attribute in attributes:
            values_list.append(getattr(value, attribute))
        return values_list

    @staticmethod
    def __override_unit_attribute_value(unit: object, attribute: str, value) -> object:
        setattr(unit, attribute, value)
        return unit

    def __override_unit_attribute_source(self, unit: object) -> object:
        source = getattr(unit, 'source')
        if type(source) == str:
            source_id = self.__db.get_sources(False, source)[source]
        else:
            source_id = source
        setattr(unit, 'source', source_id)
        return unit

    @staticmethod
    def __override_attributes(attributes: tuple, name: str, data_type):
        new_type_list = attributes[1][:]
        new_type_list[attributes[0].index(name)] = data_type
        return attributes[0], new_type_list

    @staticmethod
    def __map_attributes(attributes: list) -> list:
        sql_types = list()
        for x in attributes:
            sql_type = 'INTEGER'
            if x == float:
                sql_type = 'REAL'
            elif x == str:
                sql_type = 'TEXT'
            sql_types.append(sql_type)
        return sql_types

    @staticmethod
    def get_types(element: BasicUnit) -> Tuple[List[str], list]:
        type_list = list()
        name_list = list()
        for attribute in dir(element):
            if not callable(getattr(element, attribute)) and not attribute.startswith("__"):
                name_list.append(attribute)
                type_list.append(type(getattr(element, attribute)))
        return name_list, type_list