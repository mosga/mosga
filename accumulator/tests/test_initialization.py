import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TestGenomeInitialization(TestCase):

    def setUp(self) -> None:
        # Initialize yeast genome
        initial_genome = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        initial_genome = MOSGAArgs.arg_genome_initialization(initial_genome)
        self.initial_yeast_genome = initial_genome.parse_args([
            'Yeast',  # Genome
            '/opt/mosga/accumulator/tests/testdata/Yeast/yeast_xii_n.fna.gz',  # Genome FASTA file
            '-orig', '/opt/mosga/accumulator/tests/testdata/Yeast/yeast_xii.fna.gz',  # Genome FASTA file
            '-d',    # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
        ])

    def test_initialization(self):
        coordinator = MOSGA(self.initial_yeast_genome, MOSGAActions.INITIALIZE_GENOME)
        read = coordinator.get_run()
        scaffolds = read.get_scaffolds()[0]
        db_scaffolds = coordinator.get_db().get_full_sequences()[0]

        self.assertEqual("NC_001144.5", scaffolds[0], "FASTA Header does not match with file")
        self.assertEqual(scaffolds[0], db_scaffolds[1], "Scaffolds DB does not match with read file")
        self.assertEqual(1078178, scaffolds[1], "Scaffold length does not match with file")
        self.assertEqual(38, int(scaffolds[2]), "GC Content does not match with file")
        self.assertNotEqual(db_scaffolds[1], db_scaffolds[7], "Could not differentiate between original and new header")
