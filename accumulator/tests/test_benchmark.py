import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.analysis.benchmark import Benchmark
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TestBenchmark(TestCase):

    def setUp(self) -> None:
        # GBFF + tRNAscan
        benchmark_gbff_trnascan_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        benchmark_gbff_trnascan_parser = MOSGAArgs._arg_benchmark(benchmark_gbff_trnascan_parser_class)
        self.benchmark_gbff_trnascan_parser = benchmark_gbff_trnascan_parser.parse_args([
            "/opt/mosga/accumulator/tests/testdata/Yeast/yeast.gbff.gz", "GBFF", "Raw",
            "/opt/mosga/accumulator/tests/testdata/Yeast/trnascan_gbff_bench.txt.gz", "CSV", "tRNAscan",
            "--prediction_unit", "tRNA"
            ])

        # GBFF + Augustus
        benchmark_gbff_augustus_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        benchmark_gbff_augustus_parser = MOSGAArgs._arg_benchmark(benchmark_gbff_augustus_parser_class)
        self.benchmark_gbff_augustus_parser = benchmark_gbff_augustus_parser.parse_args([
            "/opt/mosga/accumulator/tests/testdata/Yeast/yeast.gbff.gz", "GBFF", "Raw",
            "/opt/mosga/accumulator/tests/testdata/Yeast/augustus.gbff_bench.gtf.gz", "GFF", "Augustus",
            "--prediction_unit", "Gene"
            ])

        # GBFF + MOSGADB
        benchmark_gbff_mosgadb_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        benchmark_gbff_mosgadb_parser = MOSGAArgs._arg_benchmark(benchmark_gbff_mosgadb_parser_class)
        self.benchmark_gbff_mosgadb_parser = benchmark_gbff_mosgadb_parser.parse_args([
            "/opt/mosga/accumulator/tests/testdata/Yeast/yeast.gbff.gz", "GBFF", "Raw",
            "/opt/mosga/accumulator/tests/testdata/Yeast/annotation.db", "MOSGADB", "MOSGADB"
            ])

    def test_benchmark_gbff_trnascan(self):
        benchmark = MOSGA(self.benchmark_gbff_trnascan_parser, MOSGAActions.BENCHMARK, False).get_run()
        units_ref, units_pred = benchmark.get_units()
        scaffold_length = benchmark.get_scaffold_length()
        tp, tn, fp, fn = Benchmark.benchmark_nucleotide(units_ref, units_pred, scaffold_length, 'tRNA')
        self.assertEqual(1365, tp, "true-positive count is wrong")
        self.assertEqual(1036977, tn, "true-negative count is wrong")
        self.assertEqual(0, fp, "false-positive count is wrong")
        self.assertEqual(0, fn, "false-negative count is wrong")

    def test_benchmark_gbff_augustus(self):
        benchmark = MOSGA(self.benchmark_gbff_augustus_parser, MOSGAActions.BENCHMARK, False).get_run()
        units_ref, units_pred = benchmark.get_units()
        scaffold_length = benchmark.get_scaffold_length()
        # Nucleotide Level
        tp, tn, fp, fn = Benchmark.benchmark_nucleotide(units_ref, units_pred, scaffold_length, 'Gene')
        self.assertEqual(569003, tp, "true-positive count is wrong")
        self.assertEqual(297229, tn, "true-negative count is wrong")
        self.assertEqual(11113, fp, "false-positive count is wrong")
        self.assertEqual(163451, fn, "false-negative count is wrong")
        self.assertAlmostEqual(0.83, Benchmark.accuracy(tp, tn, fp, fn), 2, "wrong accuracy")
        self.assertAlmostEqual(0.78, Benchmark.sensitivity(tp, fn), 2, "wrong sensitivity")
        self.assertAlmostEqual(0.96, Benchmark.specificity(tn, fp), 2, "wrong specificity")
        self.assertAlmostEqual(0.98, Benchmark.precision(tp, fp), 2, "wrong precision")
        self.assertAlmostEqual(0.68, Benchmark.mcc(tp, tn, fp, fn), 2, "wrong MCC")

        # Exon level
        tp, tn, fp, fn, ae, pe, me, we, five_prime_end_true, three_prime_end_true = \
            benchmark.benchmark_exon(units_ref, units_pred, scaffold_length)
        self.assertEqual(237, tp, "wrong true-positive count")
        self.assertEqual(209, tn, "wrong true-negative count")
        self.assertEqual(258, fp, "wrong false-positive count")
        self.assertEqual(272, fn, "wrong false-negative count")
        self.assertEqual(509, ae, "wrong actual exon count")
        self.assertEqual(495, pe, "wrong predicted exon count")
        self.assertEqual(4, me, "wrong missing exon count")
        self.assertEqual(20, we, "wrong 'wrong' exon count")
        self.assertEqual(325, five_prime_end_true, "wrong true 5'UTR exon start")
        self.assertEqual(321, three_prime_end_true, "wrong true 3'UTR exon end")
        # Gene level
        tg, ag, pg = benchmark.benchmark_gene(units_ref, units_pred)
        self.assertEqual(237, tg, "wrong true gene count")
        self.assertEqual(509, ag, "wrong actual gene count")
        self.assertEqual(495, pg, "wrong predicted exon count")

    def test_benchmark_gbff_MOSGADB(self):
        benchmark = MOSGA(self.benchmark_gbff_mosgadb_parser, MOSGAActions.BENCHMARK, False).get_run()
        units_ref, units_pred = benchmark.get_units()
        scaffold_length = benchmark.get_scaffold_length()
        tp, tn, fp, fn = Benchmark.benchmark_nucleotide(units_ref, units_pred, scaffold_length, 'Gene')
        self.assertEqual(544663, tp, "true-positive count is wrong")
        self.assertEqual(301155, tn, "true-negative count is wrong")
        self.assertEqual(9794, fp, "false-positive count is wrong")
        self.assertEqual(187791, fn, "false-negative count is wrong")
