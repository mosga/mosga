import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TesttRNAReader(TestCase):

    def setUp(self) -> None:
        # Barrnap
        barrnap_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        barrnap = MOSGAArgs.arg_import(barrnap_parser_class)
        self.barrnap_parser = barrnap.parse_args([
            'Yeast',  # Genome
            'rrna',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--rrna', '/opt/mosga/accumulator/tests/testdata/Yeast/barrnap.gff.gz',
            '--rrna-format', 'GFF',
            '--rrna-min-score', '0',
            '--reader-rrna', 'Barrnap'
        ])

        # SILVA LSU filtered
        silva_lsu_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        silva_lsu_parser = MOSGAArgs.arg_import(silva_lsu_parser_class)
        self.silva_lsu_parser = silva_lsu_parser.parse_args([
            'Yeast',  # Genome
            'rrna',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--rrna', '/opt/mosga/accumulator/tests/testdata/Yeast/silvalsu.txt.gz',
            '--rrna-format', 'blast',
            '--rrna-min-score', '100',
            '--rrna-stype', '4',
            '--reader-rrna', 'SILVA'
        ])

    def test_barrnap(self):
        units = MOSGA(self.barrnap_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(13, len(units["rRNA"]["NC_001144.5"]), "Could not find all RNAs")

    def test_silva_lsu(self):
        units = MOSGA(self.silva_lsu_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(1, len(units["rRNA"]["NC_001133.9"]), "Could not find rRNAs")
        self.assertEqual(1, len(units["rRNA"]["NC_001134.8"]), "Could not find rRNAs")