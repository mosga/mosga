import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TestCpGIslandsReader(TestCase):

    def setUp(self) -> None:
        # CpGIScan Reader
        cpgiscan_pre_parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        cpgiscan_parser = MOSGAArgs.arg_import(cpgiscan_pre_parser)
        self.cpgiscan_parser = cpgiscan_parser.parse_args([
            'Yeast',  # Genome
            'cpgisland',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--cpgisland', '/opt/mosga/accumulator/tests/testdata/Yeast/cpgiscan.gff.gz',
            '--cpgisland-format', 'GFF',
            '--cpgisland-min-length', '100',
            '--reader-cpgisland', 'CpGIScan'
        ])

        # newcpgreport Reader
        newcpgreport_pre_parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        newcpgreport_parser = MOSGAArgs.arg_import(newcpgreport_pre_parser)
        self.newcpgreport_parser = newcpgreport_parser.parse_args([
            'Yeast',  # Genome
            'cpgisland',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--cpgisland', '/opt/mosga/accumulator/tests/testdata/Yeast/newcpgreport.eff.gz',
            '--cpgisland-format', 'EFF',
            '--cpgisland-min-length', '100',
            '--reader-cpgisland', 'newcpgreport'
        ])

    def test_cpgiscan(self):
        units = MOSGA(self.cpgiscan_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(9, len(units['CpGIsland']['CM001574.1']), "Could not find all CpGIScan CpG islands.")

    def test_newcpgreport(self):
        units = MOSGA(self.newcpgreport_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(80, len(units['CpGIsland']['CM001574.1']), "Could not find all newcpgreport CpG islands.")