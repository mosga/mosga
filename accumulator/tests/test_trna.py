import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TesttRNAReader(TestCase):

    def setUp(self) -> None:
        # tRNAScan SE2
        trnascan_parser_class = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        trnascan_parser = MOSGAArgs.arg_import(trnascan_parser_class)
        self.trnascan_parser = trnascan_parser.parse_args([
            'Yeast',  # Genome
            'trna',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--trna', '/opt/mosga/accumulator/tests/testdata/Yeast/trnascan_se2.txt.gz',
            '--trna-format', 'CSV',
            '--trna-min-score', '70',
            '--reader-trna', 'tRNAscan'
        ])

        # tRNAScan SE2 unfiltered
        trnascan_parser_class_un = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        trnascan_parser_un = MOSGAArgs.arg_import(trnascan_parser_class_un)
        self.trnascan_parser_un = trnascan_parser_un.parse_args([
            'Yeast',  # Genome
            'trna',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--trna', '/opt/mosga/accumulator/tests/testdata/Yeast/trnascan_se2.txt.gz',
            '--trna-format', 'CSV',
            '--trna-min-score', '0',
            '--reader-trna', 'tRNAscan'
        ])

    def test_trnascan(self):
        units = MOSGA(self.trnascan_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(7, len(units["tRNA"]["NC_001144.5"]), "Could not find all filtered tRNAs")

    def test_trnascan_unfiltered(self):
        units = MOSGA(self.trnascan_parser_un, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(21, len(units["tRNA"]["NC_001144.5"]), "Could not find all unfiltered tRNAs")
