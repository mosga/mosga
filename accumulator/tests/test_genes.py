import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from lib.units.transcript import TranscriptType
from mosga import MOSGAArgs


class TestGenesReader(TestCase):

    def setUp(self) -> None:
        # Augustus Yeast chromosome
        augustus_parser_yeast = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        augustus_parser_yeast = MOSGAArgs.arg_import(augustus_parser_yeast)
        self.augustus_parser_yeast = augustus_parser_yeast.parse_args([
            'Yeast',  # Genome
            'gene',   # Type
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
            '--gene', '/opt/mosga/accumulator/tests/testdata/Yeast/augustus.gff.gz',  # Gene File
            '--gene-format', 'GFF',  # Gene Format
            '--reader-gene', 'Augustus',  # Gene Reader
            '-v'
        ])

        # Augustus Yeast chromosome + EggNog + SwissProt
        augustus_parser_yeast_func = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        augustus_parser_yeast_func = MOSGAArgs.arg_import(augustus_parser_yeast_func)
        self.augustus_parser_yeast_func = augustus_parser_yeast_func.parse_args([
            'Yeast',  # Genome
            'gene',   # Type
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
            '--gene', '/opt/mosga/accumulator/tests/testdata/Yeast/augustus.gff.gz',  # Gene File
            '--gene-format', 'GFF',  # Gene Format
            '--reader-gene', 'Augustus',  # Gene Reader
            '--gene-function', '/opt/mosga/accumulator/tests/testdata/Yeast/swissprot.txt.gz,/opt/mosga/accumulator/tests/testdata/Yeast/eggnog.txt.gz',
            '--gene-function-format', 'CSV,CSV',
            '--reader-gene-function', 'Swissprot_Diamond,EggNog',
            '--gene-function-score', '0.0,0.0',
            '--gene-function-identity', '30,0.0',
            '--gene-function-evalue', '10e-3,10e-3',
            '--gene-function-coverage', '50,0.0',
            '-vvv'
        ])

        # SNAP Yeast
        snap_parser_yeast = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        snap_parser_yeast = MOSGAArgs.arg_import(snap_parser_yeast)
        self.snap_parser_yeast = snap_parser_yeast.parse_args([
            'Yeast',  # Genome
            'gene',   # Type
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
            '--gene', '/opt/mosga/accumulator/tests/testdata/Yeast/snap.gff.gz',  # Gene File
            '--gene-format', 'GFF',  # Gene Format
            '--reader-gene', 'SNAP'  # Gene Reader
        ])

        # SNAP Yeast + Functions
        snap_parser_yeast_func = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        snap_parser_yeast_func = MOSGAArgs.arg_import(snap_parser_yeast_func)
        self.snap_parser_yeast_func = snap_parser_yeast_func.parse_args([
            'Yeast',  # Genome
            'gene',   # Type
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
            '--gene', '/opt/mosga/accumulator/tests/testdata/Yeast/snap_full.gff.gz',  # Gene File
            '--gene-format', 'GFF',  # Gene Format
            '--reader-gene', 'SNAP',  # Gene Reader
            '--gene-function', '/opt/mosga/accumulator/tests/testdata/Yeast/snap_swissprot.txt.gz,/opt/mosga/accumulator/tests/testdata/Yeast/snap_eggnog.txt.gz,/opt/mosga/accumulator/tests/testdata/Yeast/snap_idtaxa.tsv.gz',
            '--gene-function-format', 'CSV,CSV,CSV',
            '--reader-gene-function', 'Swissprot_Diamond,EggNog,IDTAXA',
            '--gene-function-score', '0.0,0.0,0.0',
            '--gene-function-identity', '30,0.0,0.0',
            '--gene-function-evalue', '10e-3,10e-3,0.0',
            '--gene-function-coverage', '50,0.0,0.0',
        ])

    def test_augustus_yeast(self):
        augustus_units = MOSGA(self.augustus_parser_yeast, MOSGAActions.IMPORT, True).get_run().get_units()
        counters = self.genes_counter(augustus_units['Gene'])
        self.augustus_test(counters)

    def test_augustus_yeast_func(self):
        augustus_units_func = MOSGA(self.augustus_parser_yeast_func, MOSGAActions.IMPORT, True).get_run().get_units()
        counters = self.genes_counter(augustus_units_func['Gene'])
        self.augustus_test(counters)
        self.assertEqual(822, counters[10], 'Could not import all functional annotation for Augustus genes')

    def augustus_test(self, counters: tuple):
        self.assertEqual(419, counters[0], 'Could not import all Augustus genes')
        self.assertEqual(419, counters[1], 'Could not import all Augustus transcripts')
        self.assertEqual(439, counters[2], 'Could not import all Augustus CDS')
        self.assertEqual(235195436, counters[3], 'Summary of all start positions from Augustus CDS does not match')
        self.assertEqual(235924105, counters[4], 'Summary of all end positions from Augustus CDS does not match')
        self.assertEqual(0, counters[5], 'Could not import all Augustus introns')
        self.assertEqual(0, counters[6], 'Could not import all Augustus 5-UTR')
        self.assertEqual(0, counters[7], 'Could not import all Augustus 3-UTR')
        self.assertEqual(419, counters[8], 'Could not import all Augustus start codons')
        self.assertEqual(419, counters[9], 'Could not import all Augustus stop codons')

    def test_snap_yeast(self):
        snap_units_func = MOSGA(self.snap_parser_yeast, MOSGAActions.IMPORT, True).get_run().get_units()
        counters = self.genes_counter(snap_units_func['Gene'])
        self.assertEqual(516, counters[0], "Could not import all SNAP genes")
        self.assertEqual(516, counters[1], "Could not import all SNAP transcripts")
        self.assertEqual(748, counters[2], "Could not import all SNAP CDS")
        self.assertEqual(397046530, counters[3], 'Summary of all start positions from SNAP CDS does not match')
        self.assertEqual(397813110, counters[4], 'Summary of all end positions from SNAP CDS does not match')

    def test_snap_yeast_func(self):
        snap_units_func = MOSGA(self.snap_parser_yeast_func, MOSGAActions.IMPORT, True).get_run().get_units()
        counters = self.genes_counter(snap_units_func['Gene'])
        self.assertEqual(78, counters[0], "Could not import all SNAP genes")
        self.assertEqual(78, counters[1], "Could not import all SNAP transcripts")
        self.assertEqual(295, counters[2], "Could not import all SNAP CDS")
        self.assertEqual(120126172, counters[3], 'Summary of all start positions from SNAP CDS does not match')
        self.assertEqual(120187338, counters[4], 'Summary of all end positions from SNAP CDS does not match')

    @staticmethod
    def genes_counter(genes) -> tuple:
        genes_count = 0
        transcript_count = 0
        functions_count = 0
        cds_count = 0
        cds_start_sum = 0
        cds_end_sum = 0
        intron_count = 0
        utr5_count = 0
        utr3_count = 0
        start_count = 0
        stop_count = 0

        for seq_id in genes.keys():
            for gene in genes[seq_id]:
                genes_count += 1

                if gene.transcripts is not None and len(gene.transcripts):
                    for transcript in gene.transcripts:
                        transcript_count += 1

                        if transcript.units is not None and len(transcript.units):
                            for unit in transcript.units:
                                unit_type = TranscriptType(unit.type)
                                if unit_type == TranscriptType.CDS:
                                    cds_start_sum += unit.start
                                    cds_end_sum += unit.end
                                    cds_count += 1
                                elif unit_type == TranscriptType.intron:
                                    intron_count += 1
                                elif unit_type == TranscriptType.utr_5:
                                    utr5_count += 1
                                elif unit_type == TranscriptType.utr_3:
                                    utr3_count += 1
                                elif unit_type == TranscriptType.start_codon:
                                    start_count += 1
                                elif unit_type == TranscriptType.stop_codon:
                                    stop_count += 1

                            if transcript.functions is not None and len(transcript.functions):
                                functions_count += len(transcript.functions)

        return (genes_count, transcript_count, cds_count, cds_start_sum, cds_end_sum, intron_count, utr5_count,
                utr3_count, start_count, stop_count, functions_count)
