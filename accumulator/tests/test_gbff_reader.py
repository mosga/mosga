import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TestGBFFReader(TestCase):

    def setUp(self) -> None:
        # GenBank Flat Format Reader
        parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        parser_yeast = MOSGAArgs.arg_import(parser)
        self.parser_yeast = parser_yeast.parse_args([
            'Yeast',  # Genome
            'raw',   # Type
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',  # Directory
            '--file', '/opt/mosga/accumulator/tests/testdata/Yeast/yeast.gbff.gz'
        ])

    def test_units_in_gbff(self):
        units = MOSGA(self.parser_yeast, MOSGAActions.IMPORT, True).get_run().get_units()

        # Count Genes
        genes_count = 0
        repeats_count = 0
        trnas_count = 0
        for unit_type in units:
            for sequence_key in units[unit_type]:
                number = len(units[unit_type][sequence_key])
                if unit_type == "Gene":
                    genes_count += number
                elif unit_type == 'Repeats':
                    repeats_count += number
                elif unit_type == 'tRNA':
                    trnas_count += number

        # Simple check or the amount of units
        self.assertEqual(510, genes_count, "Could not identify all genes.")
        self.assertEqual(31, repeats_count, "Could not identify all repeats.")
        self.assertEqual(17, trnas_count, "Could not identify all tRNAs.")

        # Call a nested debug() function
        print(units['Gene']['NC_001133.9'][0].debug(True))

