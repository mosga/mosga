import argparse
from unittest import TestCase

from lib.actions import MOSGAActions
from lib.coordinator import MOSGA
from mosga import MOSGAArgs


class TestRepeatsReaders(TestCase):

    def setUp(self) -> None:
        # WindowsMasker FASTA Hard Masking Filtered
        winmasker_pre_parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        winmasker_parser = MOSGAArgs.arg_import(winmasker_pre_parser)
        self.winmasker_parser_hard = winmasker_parser.parse_args([
            'Yeast',  # Genome
            'repeat',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--repeat', '/opt/mosga/accumulator/tests/testdata/Yeast/windowmasker.hard.gz',
            '--repeat-format', 'fasta_hard',
            '--repeat-min-length', '100',
            '--reader-repeat', 'WindowMasker'
        ])

        # WindowsMasker FASTA Hard Masking Unfiltered
        winmasker_pre_parser_un = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        winmasker_parser_un = MOSGAArgs.arg_import(winmasker_pre_parser_un)
        self.winmasker_parser_un = winmasker_parser_un.parse_args([
            'Yeast',  # Genome
            'repeat',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--repeat', '/opt/mosga/accumulator/tests/testdata/Yeast/windowmasker.hard.gz',
            '--repeat-format', 'fasta_hard',
            '--repeat-min-length', '0',
            '--reader-repeat', 'WindowMasker'
        ])

        # RepeatMasker Filtered
        repeatmasker_pre_parser = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        repeatmasker_parser = MOSGAArgs.arg_import(repeatmasker_pre_parser)
        self.repeatmasker_parser = repeatmasker_parser.parse_args([
            'Yeast',  # Genome
            'repeat',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--repeat', '/opt/mosga/accumulator/tests/testdata/Yeast/repeatmasker.out.gz',
            '--repeat-format', 'RepeatMaskerOutput',
            '--repeat-min-length', '100',
            '--reader-repeat', 'RepeatMasker'
        ])

        # RepeatMasker Unfiltered
        repeatmasker_pre_parser_un = MOSGAArgs.arg_basic_arguments(argparse.ArgumentParser(add_help=False))
        repeatmasker_parser_un = MOSGAArgs.arg_import(repeatmasker_pre_parser_un)
        self.repeatmasker_parser_unfiltered = repeatmasker_parser_un.parse_args([
            'Yeast',  # Genome
            'repeat',
            '-d',     # Dry-run
            '-dir', '/opt/mosga/accumulator/tests/testdata',
            '--repeat', '/opt/mosga/accumulator/tests/testdata/Yeast/repeatmasker.out.gz',
            '--repeat-format', 'RepeatMaskerOutput',
            '--repeat-min-length', '0',
            '--reader-repeat', 'RepeatMasker'
        ])

    def test_windowmasker_hard_filtered(self):
        units = MOSGA(self.winmasker_parser_hard, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(2, len(units['Repeats']['NC_001144.5']), "Could not find all filtered WindowsMasker Repeats")

    def test_windowmasker_hard_unfiltered(self):
        units = MOSGA(self.winmasker_parser_un, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(5232, len(units['Repeats']['NC_001144.5']), "Could not find all unfiltered WindowsMasker Repeats")

    def test_repeatmasker_filtered(self):
        units = MOSGA(self.repeatmasker_parser, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(9, len(units['Repeats']['NC_001144.5']), "Could not find all filtered RepeatMasker Repeats")

    def test_repeatmasker_unfiltered(self):
        units = MOSGA(self.repeatmasker_parser_unfiltered, MOSGAActions.IMPORT, True).get_run().get_units()
        self.assertEqual(250, len(units['Repeats']['NC_001144.5']), "Could not find all unfiltered RepeatMasker Repeats")
