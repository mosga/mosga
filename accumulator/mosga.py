#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = "Roman Martin"
__copyright__ = "Copyright © 2022 Roman Martin"
__license__ = "MIT"
__version__ = "2.1.6"

import argparse
import sys

from argparse import ArgumentParser
from lib.actions import MOSGAActions
from lib.errors import UnknownAction
from lib.coordinator import MOSGA


class StoreDictKeyPair(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        my_dict = {}
        for kv in values.split(","):
            k, v = kv.split("=")
            my_dict[k] = v
        setattr(namespace, self.dest, my_dict)


class MOSGAArgs:
    """
    MOSGA wrapper that organizes the arguments for all different modules.
    """

    def __init__(self):
        self.action = None

    def main(self):
        """
        Merges all argument parsers together.
        """
        parser = argparse.ArgumentParser(
            prog="MOSGA",
            description=
            "MOSGA: Modular Open-Source Genome Annotator " + __version__ + " \r\n"
            + __copyright__ + "\r\n\r\n"
            "Roman Martin, Thomas Hackl, Georges Hattab, Matthias G Fischer, Dominik Heider\r\n"
            "MOSGA: Modular Open-Source Genome Annotator (2021)\r\n"
            "Bioinformatics. 36(22-23). 5514–5515\r\n"
            "doi: 10.1093/bioinformatics/btaa1003\r\n\r\n"
            "Roman Martin, Hagen Dreßler, Georges Hattab, Thomas Hackl, Matthias G Fischer, Dominik Heider\r\n"
            "MOSGA 2: Comparative genomics and validation tools (2021)\r\n"
            "Computational and Structural Biotechnology Journal. 19. 5504-5509\r\n"
            "doi: 10.1016/j.csbj.2021.09.024\r\n",
            formatter_class=argparse.RawTextHelpFormatter,
            add_help=False)

        # Preferred Argument Parsing Order: parameter, type, action, default, metavar, .. , help
        action_choice = [e.name.lower() for e in MOSGAActions]
        parser.add_argument("action", type=str, choices=action_choice, help="Please define what you would like to do.")

        # No arguments given
        if len(sys.argv) == 1:
            parser.print_help()
            exit(0)

        args_tmp: tuple = ()
        try:
            args_tmp = parser.parse_known_args()
        except (argparse.ArgumentError, SystemExit):
            parser.print_help()
            exit(0)

        self.action = MOSGAActions[args_tmp[0].action.upper()]

        # Extended by additional arguments
        arguments = {
            MOSGAActions.NONE: self._none,
            MOSGAActions.IMPORT: self.arg_import,
            MOSGAActions.COLLECT: self.arg_import,
            MOSGAActions.EXPORT: self.arg_export,
            MOSGAActions.WRITE: self.arg_export,
            MOSGAActions.GET_PROTEINS_NUC_SEQ: self.arg_get_protein_nuc_seq,
            MOSGAActions.CG_GENES_COMP: self.arg_cg_genes_comparison,
            MOSGAActions.INITIALIZE_GENOME: self.arg_genome_initialization,
            MOSGAActions.GOSTGPROFILER: self.arg_gprofiler,
            MOSGAActions.KEYPATHWAYMINER: self.arg_keypathwayminer,
            MOSGAActions.IID: self.arg_iid,
            MOSGAActions.STRINGDB: self.arg_stringdb,
            MOSGAActions.VECSCREEN: self.arg_vecscreen,
            MOSGAActions.JBROWSE: self.arg_jbrowse,
            MOSGAActions.BENCHMARK: self._arg_benchmark,
            MOSGAActions.MISC: self._arg_misc
        }

        arg_extension = arguments.get(self.action, self.__error_unknown_action)
        parser = MOSGAArgs.arg_basic_arguments(parser)
        parser = arg_extension(parser)
        args = parser.parse_args()
        MOSGA(args, self.action)

    @staticmethod
    def arg_basic_arguments(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("-h", "--help", action="help", help="show help message")
        parser.add_argument('-v', '--verbose', action='count', default=0,
                            help="enable verbose mode, repeat v for a higher verbose mode level")
        return parser

    @staticmethod
    def arg_get_protein_nuc_seq(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("file", type=str, help="file path to genome FASTA")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-f", "--format", type=str, default="FASTA", help="output format")
        parser.add_argument("-i", "--ignore_conflicts", action="store_true", default=False,
                            help="ignores conflicts")
        parser.add_argument("-n", "--normalize", action="store_false", default=True, help="override gene name")
        parser.add_argument("--prefix_genome", action="store_true", default=False,
                            help="add genome hash as prefix for the sequence names")
        parser.add_argument("--no_transcripts", action="store_true", default=False)
        parser.add_argument("-b", "--bidirectional", action="store_true", default=False,
                            help="allow bidirectional annotation like conflicts")
        parser.add_argument("-p", "--position", action="store_true", default=False,
                            help="add start and stop position into the sequence name")
        return parser

    @staticmethod
    def arg_cg_genes_comparison(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("proteins", type=str, help="file path to nucleotide FASTA with protein coding genes")
        parser.add_argument("result", type=str, help="file path database search result")
        parser.add_argument("-t", "--tool", type=str, default="blastn", help="database search tool name")
        parser.add_argument("-c", "--chart", type=str, default="./",
                            help="file path where charts and tables were stored")
        parser.add_argument("-r", "--radar", action="store_true", default=False,
                            help="generates a radar plot with all gene matches through all genomes")
        parser.add_argument("-ghm", "--gene_heatmap", action="store_true", default=False,
                            help="generates a heatmap with all genes(not recommended, extreme high memory consumption)")
        parser.add_argument("-hm", "--heatmap", action="store_true", default=False,
                            help="generates a heatmap with all genomes")
        parser.add_argument("-th", "--threshold", type=int, default=90)
        parser.add_argument("-l", "--labels", action=StoreDictKeyPair, metavar="KEY1=VAL1,KEY2=VAL2...", default={},
                            help="key value pairs of genome hash code and the printed genome name")
        parser.add_argument("-i", "--from_import", action="store_true", default=False, help="apply header filter")
        return parser

    @staticmethod
    def arg_genome_initialization(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("file", type=str, help="file path to genome FASTA")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-orig", "--original-genome", type=str, default=None,
                            help="file path to the original not normalized genome FASTA")
        parser.add_argument("-d", "--dry-run", action="store_true", default=False,
                            help="Does actually not save the operations")
        parser.add_argument("-re", "--reinitialize", action="store_true", default=False,
                            help="reinitialization of a known genome database")
        return parser

    @staticmethod
    def arg_import(parser: ArgumentParser) -> ArgumentParser:
        # Repeats
        parser.add_argument("--repeat", help="path to repeat prediction file", type=str, default=None)
        parser.add_argument("--repeat-format", help="repeat file format", type=str, default="fasta_hard")
        parser.add_argument("--repeat-min-length", help="filter: minimal repeat length", type=int,
                            default=100)
        # tRNAs
        parser.add_argument("--trna", help="path to tRNA prediction file", type=str, default=None)
        parser.add_argument("--trna-format", help="tRNA file format", type=str, default="CSV")
        parser.add_argument("--trna-min-score", help="filter: minimal tRNA score", type=int, default=0)
        parser.add_argument("--trna-min-iso-score", type=int, default=0, help="filter: minimal isoform score")

        # rRNAs
        parser.add_argument("--rrna", help="path to rRNA prediction file", type=str, default=None)
        parser.add_argument("--rrna-format", help="rRNA file format", type=str, default="blast")
        parser.add_argument("--rrna-stype", help="sRNAS: 5S = 1, 5.8S = 2, 18S = 3, 28S = 4, 40S = 5, 60S = 6, 80S = 7",
                            type=int, default=0)
        parser.add_argument("--rrna-min-score", help="filter: minimal rRNA score", type=float, default=0.0)

        # Cpg islands
        parser.add_argument("--cpgisland", help="path to CpG island file", type=str, default=None)
        parser.add_argument("--cpgisland-format", help="CpG file format", type=str, default="GFF")
        parser.add_argument("--cpgisland-min-length", help="filter: minimal CpG island length", type=int,
                            default=100)
        # Genes
        parser.add_argument("--gene", help="path to gene prediction file", type=str, default=None)
        parser.add_argument("--gene-format", help="gene reader format", type=str, default="GFF")
        parser.add_argument("--gene-min-length", help="filter: minimal gene length", type=int, default=0)
        parser.add_argument("--gene-min-score", help="filter: minimal gene score", type=float, default=0.0)
        parser.add_argument("--gene-min-intron-length", type=int, default=10, help="minimal intron length")
        parser.add_argument("--gene-min-exon-length", type=int, default=15, help="minimal exon length")
        parser.add_argument("--gene-max-transcripts", help="filter: maximal amount of gene transcripts",
                            type=int, default=99)
        parser.add_argument("--gene-function", help="path to prediction gene functions file", type=str,
                            default=None)
        parser.add_argument("--gene-function-format", help="gene function reader format", type=str,
                            default="CSV,CSV")
        parser.add_argument("--gene-function-score", help="minimal gene function match score (float)",
                            type=str, default="0.0,0.0")
        parser.add_argument("--gene-function-identity", help="minimum gene function match identity (float)",
                            type=str, default="0.0,0.0")
        parser.add_argument("--gene-function-evalue", help="maximum gene function match evalue (float)",
                            type=str, default="0.1,0.1")
        parser.add_argument("--gene-function-coverage", help="minimum gene function match coverage (float)",
                            type=str, default="30.0,0.0")

        # General
        parser.add_argument("--file", type=str, default=None, help="read in file with undefined type")
        parser.add_argument("--file-format", type=str, default="GBFF", help="read in file format of a undefined type")

        # GenBank Flat Format (GBFF)
        parser.add_argument("--initialize-from-file", action="store_true", default=False,
                            help="try to initialize genome sequence from the import file (only GBFF currently")

        # Implementations
        parser.add_argument("--reader-repeat", type=str, default="WindowMasker", help="repeat reader",)
        parser.add_argument("--reader-trna", type=str, default="tRNAscan", help="tRNA reader")
        parser.add_argument("--reader-rrna", type=str, default="SILVA", help="rRNA reader")
        parser.add_argument("--reader-gene", type=str, default="Augustus", help="gene reader")
        parser.add_argument("--reader-gene-function", type=str, default="Eggnog_Augustus,Blast_Augustus",
                            help="gene functions reader")
        parser.add_argument("--reader-cpgisland", type=str, default="newcpgreport")
        parser.add_argument("--reader-raw", type=str, default="GBFFnative", help="raw reader")

        # Execution parameter
        parser.add_argument("-d", "--dry-run", help="Does actually not save the operations", action="store_true",
                            default=False)
        parser.add_argument("-dir", "--directory", help="specify directory for annotation results",
                            type=str, default="../gui/uploads")
        parser.add_argument("-p", "--purge", help="Purge already imported genetic units if they get re-readed",
                            action="store_true", default=False)

        # What should be imported #
        parser.add_argument("genome", type=str, help="name of the genome")
        parser.add_argument("type", type=str, help="specify a input source type (repeat, trna, rrna, gene)")

        return parser

    @staticmethod
    def arg_export(parser: ArgumentParser) -> ArgumentParser:
        # Execution parameter
        parser.add_argument("-b", "--bidirectional", action="store_true", default=False,
                            help="allow bidirectional annotation")
        parser.add_argument("-d", "--dry-run", action="store_true", default=False,
                            help="Does actually not save the operations")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify directory for annotation results")
        parser.add_argument("-pre", "--prefix", type=str, default="gnl|MOSGA|",
                            help="defines sqn feature table prefix for entries")
        parser.add_argument("-loc-pre", "--locus-prefix", type=str, default="MOSGA", help="locus tag prefix")
        parser.add_argument("-qc", "--quality-check", action="store_true", default=False,
                            help="performs quality checks")
        parser.add_argument("-pri", "--priority", type=str, help="priority settings")
        parser.add_argument("-gf", "--genome_file", type=str, default=None, help="path to genome FASTA file")
        parser.add_argument("-sa", "--skip_analysis", action="store_true", default=False, help="skip results analysis")
        parser.add_argument("-oi", "--organelle_indication", action="store_true", default=False,
                            help="performs scans for organelles")
        parser.add_argument("-xoi", "--extend_organelle_indication", action="store_true", default=False)
        parser.add_argument("-og", "--organelle_all_gc", action="store_true", default=True,
                            help="return all GC deviations in the organelle scan output report.")
        parser.add_argument("-of", "--organelle_file", type=str, default=None, help="outputs the organelle scan")
        parser.add_argument("-w", "--writer", type=str, default="Sequin", help="writer class")
        parser.add_argument("-o", "--output", type=str, default="genome.tbl", help="output file")
        parser.add_argument("-sum", "--summary", type=str, default=None, help="write a summary HTML table")
        parser.add_argument("--start_codon", nargs="+", default=["ATG"], help="list of valid start codons")
        parser.add_argument("--stop_codon", nargs="+", default=["TAA", "TGA", "TAG"], help="list of valid start codons")
        parser.add_argument("-soi", "--short-output-ids", action="store_true", help="shorting the output ids")
        parser.add_argument("-odna", action="store_true", help="enable machine learned based organellar DNA scanner")
        parser.add_argument("-os", "--overall_summary", type=str, default=None, help="path to HTML overall summary")
        parser.add_argument("-t", "--threads", type=int, default=0, help="number of used threads")
        parser.add_argument("genome", type=str, help="hash of the genome")
        return parser

    @staticmethod
    def arg_gprofiler(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("output", type=str, default=None, help="output html file from g:Profiler submission")
        parser.add_argument("organism", type=str, default="hsapiens", help="organism name for g:Profiler submission")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-i", "--ignore_conflicts", action="store_true", default=False,
                            help="ignores conflicts")
        parser.add_argument("-b", "--bidirectional", action="store_true", default=False,
                            help="allow bidirectional annotation like conflicts")
        return parser

    @staticmethod
    def arg_vecscreen(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("input", type=str, help="BLASTn searches results")
        parser.add_argument("output", type=str, help="output path to result report HTML table")
        parser.add_argument("-r", "--report", type=str, default=None, help="output path to result report text table. ")
        parser.add_argument("-m", "--matches", type=str, default=None, help="output path for a matches text table.")
        parser.add_argument("-mh", "--matches_html", type=str, default=None,
                            help="output path for a detail HTML table.")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-cs", "--chopping_size", type=int, default=50000, help="chopping size of the genome file")
        parser.add_argument("-si", "--strong_internal", type=int, default=30)
        parser.add_argument("-mi", "--moderate_internal", type=int, default=25)
        parser.add_argument("-wi", "--weak_internal", type=int, default=23)
        parser.add_argument("-st", "--strong_terminal", type=int, default=24)
        parser.add_argument("-mt", "--moderate_terminal", type=int, default=19)
        parser.add_argument("-wt", "--weak_terminal", type=int, default=16)
        parser.add_argument("-gff", "--gff", type=str, default=None)
        return parser

    @staticmethod
    def arg_iid(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("output", type=str, default=None, help="output csv file from IID")
        parser.add_argument("specie", type=str, default="human", help="organism name for IID submission")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-i", "--ignore_conflicts", action="store_true", default=False,
                            help="ignores conflicts")
        parser.add_argument("-b", "--bidirectional", action="store_true", default=False,
                            help="allow bidirectional annotation like conflicts")
        return parser

    @staticmethod
    def arg_stringdb(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("genome", type=str, help="genome hash code")
        parser.add_argument("output", type=str, default=None, help="output file from STRING")
        parser.add_argument("-s", "--specie", type=int, default=0, help="NCBI taxonomy id for STRING submission")
        parser.add_argument("-dir", "--directory", type=str, default="../gui/uploads",
                            help="specify top directory for all jobs")
        parser.add_argument("-i", "--ignore_conflicts", action="store_true", default=False,
                            help="ignores conflicts")
        parser.add_argument("-b", "--bidirectional", action="store_true", default=False,
                            help="allow bidirectional annotation like conflicts")
        return parser

    @staticmethod
    def arg_keypathwayminer(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("url", type=str, default=None, help="Server URL")
        parser.add_argument("task", type=str, default=None, help="What this module should do?")
        parser.add_argument("-net", "--network", type=str, default=None, help="available network id or name")
        parser.add_argument("-data", "--dataset", type=str, default=None,
                            help="one or mulitple comma separated file paths")
        parser.add_argument("--ignore_ssl", action="store_true", default=False, help="ignore SSL certificate")
        # KeyPathMiner Parameters
        parser.add_argument("--Lmin", type=int, default=0)
        parser.add_argument("--Lmax", type=int, default=0)
        parser.add_argument("--Lstep", type=int, default=1)
        parser.add_argument("--Kmin", type=int, default=0)
        parser.add_argument("--Kmax", type=int, default=0)
        parser.add_argument("--Kstep", type=int, default=1)
        parser.add_argument("--strategy", type=str, default="GLONE", help="GLONE/INES")
        parser.add_argument("--removeBENs", default=False, action="store_true")
        parser.add_argument("--range", default=False, action="store_true")
        parser.add_argument("--linkType", type=str, default="OR")
        parser.add_argument("--l_same_percentage", default=False, action="store_true")
        parser.add_argument("--same_percentage", type=int, default=0)
        parser.add_argument("--withPerturbation", default=False, action="store_true")
        return parser

    @staticmethod
    def arg_jbrowse(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("reader", type=str, help="determine the readers name")
        parser.add_argument("unit", type=str, help="determine the readers type")
        parser.add_argument("input", type=str, help="file path to the input file")
        parser.add_argument("output", type=str, default=None, help="the output file path")
        parser.add_argument("-f", "--format", type=str, default="CSV", help="input type format (ReaderFormat")
        parser.add_argument("-w", "--writer", type=str, default="GFF", help="default output writer")
        return parser

    @staticmethod
    def _arg_benchmark(parser: ArgumentParser) -> ArgumentParser:
        # Reference
        parser.add_argument("reference_file", type=str, default=None)
        parser.add_argument("reference_format", type=str, default="GFF")
        parser.add_argument("reference_reader", type=str, default=None)
        parser.add_argument("--reference_unit", type=str, default="")
        # Prediction
        parser.add_argument("prediction_file", type=str, default=None)
        parser.add_argument("prediction_format", type=str, default="GFF")
        parser.add_argument("prediction_reader", type=str, default=None)
        parser.add_argument("--prediction_unit", type=str, default="")

        parser.add_argument("--compare_unit", type=str, default=None)
        return parser

    @staticmethod
    def _arg_misc(parser: ArgumentParser) -> ArgumentParser:
        parser.add_argument("action", type=str, default=None)
        parser.add_argument("path", type=str, default=None)
        return parser

    @staticmethod
    def _arg_empty(parser: ArgumentParser) -> ArgumentParser:
        return parser

    @staticmethod
    def _none(parser: ArgumentParser):
        parser.print_help()
        print("Nothing to do ;-)")
        exit(0)

    def __error_unknown_action(self, parser) -> Exception:
        """
        Throws exception if command is not known.
        :return: exception
        """
        raise UnknownAction("Unknown parameter " + self.action)


if __name__ == "__main__":
    MOSGAArgs().main()
