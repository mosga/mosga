
g_files = [config["files"]["genomes"][x]["path"] for x in range(len(config["files"]["genomes"]))] # string: genome files
g_names = [config["files"]["genomes"][x]["name"] for x in range(len(config["files"]["genomes"]))] # string: genome names

# Add phylogenetic outgroup genome
outgroups = []
try:
    outgroups = [g_files[g_names.index(x)][:10] for x in config["outgroups"][0]]
    if len(outgroups) > 0:
        outgroup_exists = True
    else:
        outgroup_exists = False
except KeyError or ValueError:
    outgroup_exists = False
    pass

# Check if gbff files are present
gbff_names = list(filter(lambda f: f[-4:] == "gbff" or f[-7:] == "gbff.gz", g_names))
gbff_files = [g_files[i] for i in [g_names.index(x) for x in gbff_names]]

include: rules_dir + "comparative/gbff_import.smk"

comparative_outputs = [] # collection of inputs & outputs for each rule

# Phylogenetics & Genome completeness
try:
    completeness = config["completeness"]["0"]
    include: rules_dir + "comparative/genome_completeness.smk"
except KeyError:
    # rule is not active
    completeness = None

# Average nucleotide identity
try:
    ani = config["ani"]["0"]
    include: rules_dir + "comparative/ani.smk"
except KeyError:
    # rule is not active
    ani = None

# Gene comparison
try:
    comparison = config["comparison"]["0"]
    include: rules_dir + "comparative/gene_comparison.smk"
except KeyError:
    # rule is not active
    comparison = None
