class Config(object):

    def __init__(self, config: dict, default: dict):
        self._config = self._dict(config)
        self._properties = default
        self._properties.update(self._config)

    def _dict(self, config : dict):
        for k, v in config.items():
            if isinstance(v, dict):
                config[k] = self._dict(v)
            if isinstance(v, list):
                for i in range(len(v)):
                    if isinstance(v[i], dict):
                        v[i] = self._dict(v[i])
        return dict(config)

    def __len__(self):
        return len(self._properties)

    def __getitem__(self, index):
        try:
            v = self._properties[index]
            return v
        except KeyError:
            raise KeyError(f"The property '{index}' could not be found in the config!")

    def __setitem__(self, index, value):
        self._properties[index] = value
