annotation_types = []

include: rules_dir + "annotation/normalize_fasta_header.smk"
include: rules_dir + "annotation/initialize_genome.smk"
include: rules_dir + "annotation/chop_genome.smk"
include: rules_dir + "annotation/organelles.smk"

# Load repeats/softmasking prediction
try:
    repeats = len(config["repeats"]["0"]) > 0
    annotation_types.append(repeats)
    include: rules_dir + "annotation/repeats.smk"
except KeyError:
    repeats = None

# Load gene prediction
try:
    genes = config["genes"]["gene"]["0"]
    annotation_types.append(genes)
    include: rules_dir +"annotation/genes_gene.smk"
except KeyError as e:
    # KeyError in genes_gene, skip gene modules.
    genes = None

# Load tRNAs prediction
try:
    trnas = len(config["trnas"]["0"]) > 0
    annotation_types.append(trnas)
    include: rules_dir + "annotation/trnas.smk"
except KeyError:
    trnas = None

# Load rRNAs prediction
try:
    rrnas = len(config["rrnas"]["0"]) > 0
    annotation_types.append(rrnas)
    include: rules_dir + "annotation/rrnas.smk"
except KeyError:
    rrnas = None

if genes != None and len(gene_tools):
    gene_collection = gene_tools.values()

try:
    gene_function = config["genes"]["function"]
except KeyError:
    gene_function = None

# Run Writer & tbl2asn
feature_table = target_dir + "/" + os.path.basename( final_genome_file ) + ".tbl",
gff_output = target_dir + "/genome.gff"
writer_log = log_dir + "/writer.txt"
organelles_result = target_dir + "/organelles.html"
report_file = target_dir + "/logs/tbl2asn-discrepancy.txt"
sqn_file = target_dir + "/genome.sqn"
tbl2asn_log = log_dir + "/tbl2asn.txt"
include: rules_dir + "annotation/tbl2asn.smk"

# CpG Islands
try:
    cpgisland_tools: list = []
    include: rules_dir + "annotation/cpgisland/cpgiscan.smk"
    include: rules_dir + "annotation/cpgisland/newcpgreport.smk"
except KeyError:
    cpgisland_tools = None

# Validation tools
try:
    validation_tools: list = []
    if len(config["validation"]) > 0:
        include: rules_dir + "annotation/validation/validation.smk"
except KeyError:
    validation_tools = None

# MOSGA
include: rules_dir + "annotation/mosga.smk"

# Load modules
include: rules_dir + "annotation/spliceator.smk"
include: rules_dir + "annotation/api/gprofiler.smk"
include: rules_dir + "annotation/api/iid.smk"
include: rules_dir + "annotation/api/stringdb.smk"

# Load jBrowse
include: rules_dir + "annotation/jbrowse/jbrowse.smk"

# File Index
include: rules_dir + "annotation/file_index.smk"
