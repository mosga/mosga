class WhatToCite:

    def __init__(self, path: str):
        self._publications = []
        self._path = path

    def add(self, citation):
        if type(citation) is list:
            for cit in citation:
                self._publications.append(cit)
        else:
            self._publications.append(citation)

    def write(self):
        pubs = list(set(self._publications))
        fp = open(self._path, 'w',  encoding='utf-8')
        for pub in pubs:
            fp.write(pub + '\r\n')
        fp.close()

publications = {
    "mosga": "Martin R, Hackl T, Hattab G, Fischer MG, Heider D (2020). MOSGA: Modular Open-Source Genome Annotator. Bioinformatics. 36(22-23):5514-5515. doi: 10.1093/bioinformatics/btaa1003",
    "mosga2": "Martin R, Dreßler H, Hattab G, Hackl T, Fischer MG, Heider D (2021). MOSGA 2: Comparative genomics and validation tools. Computational and Structural Biotechnology Journal. 19. 5504-5509. doi: 10.1016/j.csbj.2021.09.024",
    "diamond": "Buchfink B, Xie C, Huson DH (2015). Fast and sensitive protein alignment using DIAMOND. Nat Methods. 12(1):59‐60.",
    "rna-pred": "Hoff, K. J. and Stanke, M. (2019). Predicting Genes in Single Genomes with AUGUSTUS. Current Protocols in Bioinformatics, 65(1).",
    "samtools": "Li H, Handsaker B, Wysoker A, Fennell T, Ruan J, Homer N, Marth G, Abecasis G, Durbin R (2009). 1000 Genome Project Data Processing Subgroup. The Sequence Alignment/Map format and SAMtools. Bioinformatics. A 15;25(16):2078-9. doi: 10.1093/bioinformatics/btp352",
    "hisat": "Kim D, Langmead B, Salzberg SL (2015). HISAT: a fast spliced aligner with low memory requirements. Nat Methods. 12(4):357-60. doi: 10.1038/nmeth.3317",
    "tophat": "Kim D, Pertea G, Trapnell C, Pimentel H, Kelley R, Salzberg SL (2013). TopHat2: accurate alignment of transcriptomes in the presence of insertions, deletions and gene fusions. Genome Biol. 25;14(4):R36. doi: 10.1186/gb-2013-14-4-r36",
    "gth": "Gremme G, Brendel V, Sparks ME, and Kurtz S (2005). Engineering a software tool for gene structure prediction in higher organisms. Information and Software Technology, 47(15):965-978",
    "bowtie": "Langmead B, Salzberg SL (2012). Fast gapped-read alignment with Bowtie 2. Nat Methods. 4;9(4):357-9. doi: 10.1038/nmeth.1923",
    "jbrowse": "Buels R, Yao E, Diesh CM, Hayes RD, Munoz-Torres M, Helt G, Goodstein DM, Elsik CG, Lewis SE, Stein L, Holmes IH (2016). JBrowse: a dynamic web platform for genome visualization and analysis. Genome Biol. 12;17:66. doi: 10.1186/s13059-016-0924-1",
    "gprofiler": "Raudvere U, Kolberg L, Kuzmin I, Arak T, Adler P, Peterson H, Vilo J (2019). g:Profiler: a web server for functional enrichment analysis and conversions of gene lists. Nucleic Acids Res. doi:10.1093/nar/gkz369",
    "iid": "Kotlyar M, Pastrello C, Malik Z, Jurisica I (2019). IID 2018 update: context-specific physical protein-protein interactions in human, model organisms and domesticated species. Nucleic Acids Res. 47(D1):D581-D589. doi:10.1093/nar/gky1037",
    "stringdb": "Szklarczyk D, Gable AL, Lyon D, Junge A, Wyder S, Huerta-Cepas J, Simonovic M, Doncheva NT, Morris JH, Bork P, Jensen LJ, Mering CV. STRING v11: protein-protein association networks with increased coverage, supporting functional discovery in genome-wide experimental datasets. Nucleic Acids Res. 2019 8;47(D1):D607-D613. doi: 10.1093/nar/gky1131.",
    "spaln": "Gotoh O. A space-efficient and accurate method for mapping and aligning cDNA sequences onto genomic sequence (2008). Nucleic Acids Res. 36(8):2630-8. doi: 10.1093/nar/gkn105.",
    "ggtree": "Yu G, Smith D, Zhu H, Guan Y, Lam T T. ggtree: an R package for visualization and annotation of phylogenetic trees with their covariates and other associated data (2017). Methods in Ecology and Evolution, 8(1):28-36. doi:10.1111/2041-210X.12628",
    "pplacer": "Matsen FA, Kodner RB, Armbrust EV. pplacer: linear time maximum-likelihood and Bayesian phylogenetic placement of sequences onto a fixed reference tree (2010). BMC Bioinformatics. 11:538. doi: 10.1186/1471-2105-11-538",
    "infernal": "Nawrocki EP, Kolbe DL, Eddy SR. Infernal 1.0: inference of RNA alignments (2009). Bioinformatics 25(10):1335-7. Erratum in: Bioinformatics. 2009 Jul 1;25(13):1713. doi: 10.1093/bioinformatics/btp157",
    "altair": "VanderPlas J, Granger B, Heer J, Moritz D, Wongsuphasawat K, Satyanarayan A, Sievert S. Altair: Interactive statistical visualizations for python (2018). Journal of Open Source Software, 3(32), 1057. doi: 10.21105/joss.01057",
    "spliceator": "Scalzitti N, Kress A, Orhand R, Weber T, Moulinier L, Jeannin-Girardon A, Collet P, Poch O, Thompson JD (2021). Spliceator: multi-species splice site prediction using convolutional neural networks. BMC Bioinformatics. 22(1):561. doi: 10.1186/s12859-021-04471-3",
    "cpgiscan": "Fan Z, Yue B, Zhang X, Du L, Jian, Z. (2017). CpGIScan: An Ultrafast Tool for CpG Islands Identification from Genome Sequence. Go, 12(2), 181–184. doi: 10.2174/1574893611666160907111325"
}
