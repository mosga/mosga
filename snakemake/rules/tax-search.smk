import operator


class TaxonomySearch:

    def __init__(self, tree_file: str, path_file: str, config: dict):
        self._results: dict = {}
        try:
            if config["ncbi"]["taxsearch"][0] == "ncbi_taxsearch":
                self.doSearch = True
                self.ref = config["ncbi"]["tax"]["id"][0]
                self._tree = TaxonomySearch._open_json(tree_file)
                self._paths = TaxonomySearch._open_json(path_file)
            else:
                self.doSearch = False
        except KeyError:
            self.doSearch = False

    def find(self, rule: dict, name: str = "..."):
        if not self.doSearch:
            raise Exception("Search was not set in the config.")
        names, uids = TaxonomySearch._read_rule(rule)
        distances = []
        # Index 0: All, Index 1: Closest
        self._results.update({name: [[]]})
        for i in range(len(uids)):
            try:
                if int(self.ref) == int(uids[i]):
                    dist = 0
                else:
                    dist = self._dist_fast(int(self.ref), int(uids[i]))
            except KeyError:
                dist = sys.maxsize
            distances.append(dist)
            # Index 0: Entry, Index 1: NCBI-ID, Index: 2 Distance)
            self._results[name][0].append([names[i], uids[i], dist])
        index = distances.index(min(distances))
        self._results[name].append([names[index], uids[index], distances[index]])
        return names[index]

    def _dist_fast(self, uid, target):
        try:
            path = self._paths[target]
        except KeyError:
            return self._dist(uid, target)
        lst = uid
        nxt = self._tree[lst][1]
        dist = 0
        while True:
            dist += self._tree[nxt][0]
            if nxt == target:
                break
            if nxt in path[1]:
                idx = path[1].index(nxt)
                for i in range(idx - 1, -1, -1):
                    dist += path[0][i]
                dist += self._tree[target][0]
                break
            lst = nxt
            nxt = self._tree[lst][1]
        return dist

    def _dist(self, uid, target):
        lst = uid
        nxt = self._tree[lst][1]
        search = True
        dist = 0
        while search:
            dist += self._tree[nxt][0]
            if nxt == target:
                break
            for child in self._tree[nxt][2]:
                if child == lst:  # dont check origin id again
                    continue
                d, f = self._dist_childs(child, target, self._tree[child][0])
                if f:
                    search = False
                    dist += d
                    break
            lst = nxt
            nxt = self._tree[lst][1]
        return dist

    def _dist_childs(self, child, target, dist=1):
        if child == target:
            return dist, True
        if len(self._tree[child][2]) > 0:
            c = []
            for child in self._tree[child][2]:
                d, f = self._dist_childs(child, target, dist + self._tree[child][0])
                if f:
                    return d, f
                else:
                    c.append(d)
            return min(c), False
        else:
            return sys.maxsize, False

    def write(self, out_path: str, log_path: str):
        from prettytable import PrettyTable

        # Summary file
        short = PrettyTable()
        full = PrettyTable()
        short.field_names = ["Tool", "Hit", "NCBI-ID", "Distance"]
        full.field_names = ["Tool", "Option", "NCBI-ID", "Distance", "Selected"]

        for s in self._results:
            h = self._results[s][1]
            short.add_row([s, h[0], h[1], h[2]])

            for f in self._results[s][0]:
                full.add_row([s, f[0], f[1], f[2], f[0] == h[0]])

        fsp = open(out_path, "w",encoding="utf-8")
        fsp.write(short.get_html_string(
            attributes={"id": "tax-summary", "class": "table table-striped table-sm"}))
        fsp.close()

        fp = open(log_path, "w")
        fp.write(full.get_string(sort_key=operator.itemgetter(1, 0), sortby="Distance"))
        fp.close()

    @staticmethod
    def _open_json(json_path):
        with open(json_path) as json_file:
            return json.load(json_file, object_hook=lambda d: {int(k): v for k, v in d.items()})

    @staticmethod
    def _read_rule(rule):
        names, uids = [], []
        for key, value in rule.items():
            try:
                uids.append(value["tax"])
                names.append(key)
            except KeyError:
                continue
        return names, uids


class GeneticCode:

    # https://www.ncbi.nlm.nih.gov/Taxonomy/taxonomyhome.html/index.cgi?chapter=cgencodes
    nuclear: dict = {
        "root": {
            "tax": 2759 # Eukaryota
        },
        "ciliata": {
            "tax": 210569
        },
        "dasycladaceae": {
            "tax": 3135
        },
        "batophora": {
            "tax": 3139
        },
        "diplomonadida": {
            "tax": 5738
        },
        "euplotidae": {
            "tax": 100127
        }
    }

    mito: dict = {
        "root": {
            "tax": 2759
        },
        "vertebrata": {
            "tax": 7742
        },
        "saccharomyceta": {
            "tax": 716545
        },
        "alveolata": {
            "tax": 33630
        },
        "discoba": {
            "tax": 2611352
        },
        "leotiomyceta": {
            "tax": 716546
        },
        "gigartinales": {
            "tax": 28017
        },
        "cnidarians": {
            "tax": 6073
        },
        "ctenophores": {
            "tax": 10197
        },
        "nematoda": {
            "tax": 6231
        },
        "mollusca": {
            "tax": 6447
        },
        "crustacea": {
            "tax": 6657
        },
        "insecta": {
            "tax": 50557
        },
        "asterozoa": {
            "tax": 7587
        },
        "echinozoa": {
            "tax": 7624
        },
        "rhabditophora": {
            "tax": 147100
        },
        "chlorophyceae": {
            "tax":  3166
        },
        "trematoda": {
            "tax":  3166
        }
    }

    plastid: dict = {
        "plantplastid": {
            "tax": 2759
        }
    }

    codes: dict = {
        "root": 1,
        "vertebrata": 2,
        "saccharomyceta": 3,
        "alveolata": 4,
        "discoba": 4,
        "leotiomyceta": 4,
        "gigartinales": 4,
        "cnidarians": 4,
        "ctenophores": 4,
        "nematoda": 5,
        "mollusca": 5,
        "crustacea": 5,
        "insecta": 5,
        "ciliata": 6,
        "dasycladaceae": 6,
        "batophora": 6,
        "diplomonadida": 6,
        "asterozoa": 9,
        "echinozoa": 9,
        "rhabditophora": 9,
        "euplotidae": 10,
        "plantplastid": 11,
        "chlorophyceae": 16,
        "trematoda": 21
    }
