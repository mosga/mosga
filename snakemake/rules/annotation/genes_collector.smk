rule MOSGA_import_genes:
    input:
        genes = gene_collector[0],
        additionals = gene_collector[3]["list"] if len(gene_collector[3]["list"]) else [],
        igf = igf
    output:
        gene_collector[1]
    threads:
        cores
    params:
        uid = config["dir"],
        genes_format = gene_collector[2]["format"],
        genes_reader = gene_collector[2]["reader"],
        functions = gene_collector[3]["params"] if gene_collector[3]["params"] is not None else "",
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.uid} gene -v -dir {params.up} --gene {input.genes} --gene-format {params.genes_format} --reader-gene {params.genes_reader} {params.functions} > {output}"
