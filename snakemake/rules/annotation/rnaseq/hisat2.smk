rule hisat2_build_index:
    input:
        hisat2_build_input
    output:
        hisat2_build_output
    log:
        hisat2_log
    threads:
        cores_s
    shell:
        "hisat2-build --large-index {hisat2_build_input} {hisat2_build_param} >{log} 2>&1"

rule hisat2_align:
    input:
        hisat2_build_output
    output:
        hisat2_align_output
    log:
        hisat2_log
    threads:
        cores
    shell:
        "hisat2 -p {threads} -x {hisat2_align_database} -q {hisat2_align_rna} --summary-file {hisat2_align_summary_file} -S {output} >{log} 2>&1"
