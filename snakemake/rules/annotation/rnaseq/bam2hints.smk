rule bam2hints:
    input:
        bam_sorted_file
    output:
        hints_file
    log:
        bam2hints_log
    shell:
        "/opt/mosga/tools/augustus/bin/bam2hints --intronsonly --in={input} --out={output} >{log} 2>&1"
