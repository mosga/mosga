# declare & update locations
eukcc_data = "/opt/mosga/tools/EukCC/eukccdb"
eukcc_path = target_dir + '/validation/eukcc/'
eukcc_out = eukcc_path + "eukcc.tsv"
eukcc_out_vis = eukcc_path + "eukcc.svg"
eukcc_log = log_dir + "/eukcc.txt"
validation_tools.append(eukcc_out_vis)

if "license_file" not in locals():
    try:
        license_file =  target_dir + "/" + config["files"]["validation"]["completeness"]["eukcc"]["license"][0]["path"]
    except KeyError:
        license_file = 'gm_key'
        print("Missing GeneMark License!")

rule eukcc:
    input:
        genome_file
    output:
        eukcc_out
    log:
        eukcc_log
    params:
        out_dir= eukcc_path,
        db_dir = eukcc_data
    threads:
        cores
    shell:
        """
        cp {license_file} ~/.gm_key 2> /dev/null
        PATH=$PATH:/usr/local/bin eukcc --db {params.db_dir} --ncores {threads} --ncorespplacer 1 --outdir {params.out_dir} --force {input} >{log} 2>&1
        rm -f ~/.gm_key 2> /dev/null
        """

rule eukcc_single_imager:
    input:
        eukcc_out
    output:
        eukcc_out_vis
    log:
        eukcc_log
    threads:
        cores_s
    shell:
        "python3 /opt/mosga/scripts/eukcc_single_imager.py -i {input} -o {output} >{log} 2>&1"
