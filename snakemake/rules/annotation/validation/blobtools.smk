# Catch BlobTools parameters
try:
    blobtools_taxonomy = config["validation"]["contamination"]["blobtools"]["radio"]["taxonomy-rank"][0] != "no"
    blobtools_ranking = config["validation"]["contamination"]["blobtools"]["radio"]["taxonomy-rank"][0]
except KeyError:
    blobtools_taxonomy = False
    blobtools_ranking = "superkingdom"

blobtools_path = target_dir + "/validation/blobtools/"
blobtools_tax_raw_search = blobtools_path + "swissprot.txt.gz"
blobtools_tax_search = blobtools_path + "hints.out"
blobtools_blobdb = blobtools_path + "blobDB.json"
blobtools_pseudo = blobtools_path + "empty.txt"
blobtools_cov = blobtools_path + "read_coverage.svg"
blobtools_plot = blobtools_path + "plot.svg"
blobtools_log = log_dir + "/blobtools.txt"

if blobtools_taxonomy:
    include: rules_dir + "annotation/validation/blobtools_taxonomy.smk"

rule blobtools_create:
    input:
        genome = genome_file,
        bam = bam_sorted_file,
        bai = samtools_index_output,
        hints = blobtools_tax_search if blobtools_taxonomy else []
    output:
        blobtools_blobdb
    log:
        blobtools_log
    params:
        hints = "-t "+ blobtools_tax_search if blobtools_taxonomy else "",
        dir = blobtools_path
    threads:
        cores_4
    shell:
        """
        cd {params.dir}
        /opt/mosga/tools/blobtools/blobtools create -i {input.genome} -b {input.bam} {params.hints} --db /opt/mosga/tools/blobtools/data/nodesDB.txt --title MOSGA -o {params.dir} >{log} 2>&1
        """

rule blobtools_view:
    input:
        blobtools_blobdb
    output:
        blobtools_pseudo
    log:
        blobtools_log
    params:
        rank = "-r " + blobtools_ranking if blobtools_taxonomy else "",
        dir = blobtools_path
    threads:
        cores_4
    shell:
        """
        cd {params.dir}
        /opt/mosga/tools/blobtools/blobtools view {params.rank} -i {input} && touch {output}  >>{log} 2>&1
        """

rule blobtools_plot:
    input:
        blobdb = blobtools_blobdb,
        empty = blobtools_pseudo
    output:
        blobtools_plot
    log:
        blobtools_log
    params:
        rank = "-r " + blobtools_ranking if blobtools_taxonomy else "",
        dir = blobtools_path,
        cov = blobtools_cov,
        plot = blobtools_plot
    threads:
        cores_4
    shell:
        """
        cd {params.dir}
        /opt/mosga/tools/blobtools/blobtools plot {params.rank} --notitle --format svg -i {input.blobdb} >>{log} 2>&1
        mv {params.dir}MOSGA.*.read_cov*.svg {params.cov}
        mv {params.dir}MOSGA.*.svg {params.plot}
        """

validation_tools.append(blobtools_plot)
