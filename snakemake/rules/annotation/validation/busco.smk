if tax.doSearch:
    busco_lineage = tax.find(*gui["annotation"]["validation"]["content"]["completeness"]["tools"]["busco"]["settings"]["lineage"]["values"],
        name="BUSCO")
else:
    busco_lineage = config["validation"]["completeness"]["busco"]["radio"]["lineage"][0]

busco_path = target_dir + "/validation/"
busco_evalue = config["validation"]["completeness"]["busco"]["filter"]["evalue"][0]
busco_lin = "run_" + busco_lineage + "/full_table.tsv"
busco_tsv = "/" + busco_lin
busco_temp_out = busco_path + "busco/" + busco_tsv
busco_out = busco_path + "busco/summary.txt"
busco_graph = busco_path + "busco/busco.svg"
busco_log = log_dir + "/busco.txt"

rule busco:
    input:
        genome_file
    output:
        busco_out
    log:
        busco_log
    params:
        name = "busco",
        out_dir = busco_path,
        lineage = busco_lineage,
        config = "/opt/mosga/scripts/busco_config.ini",
        evalue = busco_evalue,
        result = "run_" + busco_lineage + "/full_table.tsv"
    threads:
        cores
    shell:
        """
        cd {params.out_dir}
        python3 /opt/mosga/tools/BUSCO/bin/busco -i {input} -o {params.name} --out_path {params.out_dir} -l {params.lineage} --cpu {threads} --force --config {params.config} >{log} 2>&1
        mv busco/{params.result} busco/busco.tsv
        sed '1,/Results:/d' busco/short_summary.specific.{params.lineage}.busco.txt | sed -e 's/^[[:space:]]*//' | sed 1d > busco/summary.txt
        """

rule busco_graph:
    input:
        busco_out
    output:
        busco_graph
    log:
        busco_log
    params:
        lin = busco_lineage
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/scripts/busco_single_imager.py {input} {output} --title {params.lin} >>{log} 2>&1"

validation_tools.append(busco_graph)
