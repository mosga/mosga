vecscreen_blast_output = target_dir + "/validation/vecscreen/matches.raw.txt.gz"
vecscreen_remapped_results = target_dir + "/validation/vecscreen/matches.remapped.txt.gz"
vecscreen_output = target_dir + "/validation/vecscreen/report.html"
vecscreen_output_txt = target_dir + "/validation/vecscreen/report.txt"
vecscreen_output_matches = target_dir + "/validation/vecscreen/matches.html"
vecscreen_gff = target_dir + "/validation/vecscreen/vecscreen.gff"
vecscreen_database = "/opt/mosga/tools/vecscreen/UniVec"
vescreen_log = log_dir + "/vescreen_search.txt"
vecscreen_analyse_log = log_dir + "/vecscreen.txt"

# Prepare parameters
try:
    vecscreen_si = int(config["validation"]["contamination"]["vecscreen"]["filter"]["si"][0])
    vecscreen_mi = int(config["validation"]["contamination"]["vecscreen"]["filter"]["mi"][0])
    vecscreen_wi = int(config["validation"]["contamination"]["vecscreen"]["filter"]["wi"][0])
    vecscreen_st = int(config["validation"]["contamination"]["vecscreen"]["filter"]["st"][0])
    vecscreen_mt = int(config["validation"]["contamination"]["vecscreen"]["filter"]["mt"][0])
    vecscreen_wt = int(config["validation"]["contamination"]["vecscreen"]["filter"]["wt"][0])
except (KeyError|TypeError):
    vecscreen_si = 30
    vecscreen_mi = 25
    vecscreen_wi = 23
    vecscreen_st = 24
    vecscreen_mt = 19
    vecscreen_wt = 16

# Perform the search against the UniVec database
rule search_vecscreen:
    input:
        chopped_genome
    output:
        vecscreen_blast_output
    log:
        vescreen_log
    params:
        db = vecscreen_database
    threads:
        cores_2
    shell:
        "gunzip -c {input} | blastn -task blastn  -db {params.db} -num_threads {threads} -reward 1 -penalty -5 -gapopen 3 -gapextend 3 -dust yes -soft_masking true -evalue 700 -searchsp 1750000000000 -outfmt \"6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue score\" 2>{log} | gzip -c > {output}"

# Interpret the VecScreen outcome
rule MOSGA_vecscreen_analyse:
    input:
        vec = vecscreen_blast_output,
        igf = igf
    output:
        out = vecscreen_output,
        gff = vecscreen_gff
    log:
        vecscreen_analyse_log
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        si = vecscreen_si,
        mi = vecscreen_mi,
        wi = vecscreen_wi,
        st = vecscreen_st,
        mt = vecscreen_mt,
        wt = vecscreen_wt,
        out_txt = vecscreen_output_txt,
        out_matches = vecscreen_output_matches,
        cs = config["genome_chopping_size"]
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py vecscreen {params.genome} {input.vec} {output.out} -si {params.si} -mi {params.mi} -wi {params.wi} -st {params.st} -mt {params.mt} -wt {params.wt} -mh {params.out_matches} -r {params.out_txt} -cs {params.cs} -dir {params.up} -gff {output.gff} >{log} 2>&1"

validation_tools.append(vecscreen_output)
