# Search the whole genome against the SwissProt database
rule blobtools_taxonomy_search:
    input:
        chopped_genome
    output:
        blobtools_tax_raw_search
    threads:
        cores_4
    shell:
        "/opt/mosga/tools/diamond/diamond blastx --db /opt/mosga/tools/diamond/swissprot.dmnd --threads {threads} --query {chopped_genome} --outfmt 6 qseqid bitscore score stitle | gzip -c > {output}"

rule blobtools_taxonomy_prepare:
    input:
        blobtools_tax_raw_search
    output:
        blobtools_tax_search
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/scripts/blobtools_diamond.py {input} {output}"
