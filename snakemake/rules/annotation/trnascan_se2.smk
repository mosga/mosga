rule trnascan_se2:
    input:
        trnascan_input
    output:
        out = trnascan_output,
        bed = trnascan_output_bed
    threads:
        cores_3
    params:
        stats = trnascan_directory + "trna.stats",
        fa = trnascan_directory + "trna.faa",
        bed = trnascan_directory + "trna.bed"
    shell:
        "/opt/mosga/tools/tRNAscan/tRNAscan-SE -Q -q -E {input} -o {output.out} -m {params.stats} -a {params.fa} -b {params.bed} --detail --thread {threads}"

rule MOSGA_import_trnascan2:
    input:
        trna = trnascan_output,
        igf = igf
    output:
        trnascan_collector
    threads:
        cores
    params:
        genome = config["dir"],
        filter = trnascan_filter_score,
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} trna -v -dir {params.up} --trna {input.trna} --trna-format CSV --trna-min-score {params.filter} --reader-trna tRNAscan > {output}"
