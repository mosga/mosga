# Template file
try:
    tbl2asn_tmpl_file = config["files"]["template"]
    tbl2asn_tmpl_file = tbl2asn_tmpl_file[0]["path"]
except KeyError:
    tbl2asn_tmpl_file = "/opt/mosga/gui/template.sbt"

# -j Source Identifier
if len(config["genbank"]["specie"]) or len(config["genbank"]["strain"]):
    tbl2asn_source_identifier = "-j "

    if len(config["genbank"]["specie"]) and len(config["genbank"]["strain"]):
        tbl2asn_source_identifier += '"[organism='+ config['genbank']['specie'] + '][strain='+ config['genbank']['strain'] +']"'
    elif len(config["genbank"]["specie"]):
        tbl2asn_source_identifier += '"[organism='+ config['genbank']['specie'] + ']"'
else:
    tbl2asn_source_identifier = ""

# Assembly Gap
if config["genbank"]["linkage"] != "none":
    tbl2asn_linkage = "-l " + config["genbank"]["linkage"]
    tbl2asn_assembly = "-a " + config["genbank"]["polyN"]
else:
    tbl2asn_linkage = ""
    tbl2asn_assembly = ""

# Writer prefix
if len(config["genbank"]["prefix"]):
    writer_prefix = '-pre "' + config['genbank']['prefix'] + '"'
else:
    writer_prefix = ""

if len(config["genbank"]["loc_prefix"]):
    writer_loc_prefix = '-loc-pre "' + config['genbank']['loc_prefix'] + '"'
else:
    writer_loc_prefix = ""

# Run tbl2asn
rule tbl2asn:
    input:
        feature = feature_table,
        genome = final_genome_file
    output:
        report = report_file,
        sqn = sqn_file
    log:
        tbl2asn_log
    params:
        dir = config["dir"],
        tmpl = tbl2asn_tmpl_file,
        source_id = tbl2asn_source_identifier,
        link = tbl2asn_linkage,
        ass = tbl2asn_assembly,
        name = os.path.basename( final_genome_file ),
        genome = os.path.basename( final_genome_file )+".sqn",
        val = os.path.basename( final_genome_file )+".val",
        tbl = os.path.basename( final_genome_file )+".tbl",
        up = uploads_dir_relative,
        target = target_dir
    threads:
        cores_s
    shell:
        """
        cd {params.target}/
        /opt/mosga/tools/tbl2asn -t {params.tmpl} -i {params.name} -V v {params.source_id} -M n -Z {output.report} {params.link} {params.ass} >{log} 2>&1
        ln -s -f {params.target}/{params.tbl} {params.target}/genome-table.txt
        mv {params.target}/{params.genome} {params.target}/genome.sqn
        mv {params.target}/{params.val} {params.target}/logs/tbl2asn-validation.txt
        mv {params.target}/errorsummary.val {params.target}/logs/tbl2asn-error-summary.txt
        """
