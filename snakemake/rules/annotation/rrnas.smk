rrnas_tools = list()
rrnas_output = list()

for rrna in config["rrnas"]:
    try:
        rrnas_tools.extend( config["rrnas"][str(int(rrna))] )
    except ValueError:
        pass

blast_db_path = "/opt/mosga/tools/silva"
blast_rrna_threads = 8
silva_blastn_outfmt = "6 qseqid sseqid qstart qend sstart send length nident pident evalue bitscore score mismatch gapopen gaps qcovs qcovhsp stitle"

if "rrnas_silvassu" in rrnas_tools:
    ssu_database = blast_db_path + "/ssu"
    ssu_remapped_output = target_dir + "/rrnas/silva/ssu.remapped.txt.gz"
    ssu_perc = config["rrnas"]["silvassu"]["filter"]["min"]["ident"][0]
    ssu_evalue = config["rrnas"]["silvassu"]["filter"]["min"]["evalue"][0]
    ssu_bs = config["rrnas"]["silvassu"]["filter"]["min"]["bs"][0]
    ssu_unfiltered_output = target_dir + "/rrnas/silvassu/ssu.raw.txt.gz"
    ssu_output = target_dir + "/rrnas/silvassu/ssu.txt.gz"
    ssu_collect = target_dir + "/rrnas/silvassu/collect.txt"
    ssu_log = log_dir + "/silva_ssu.txt"
    rrnas_output.append(ssu_collect)

    # Cite
    try:
        cite.add(gui["annotation"]["rrnas"]["content"]["rrnas"]["tools"]["silvassu"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/rrna/silvassu.smk"

if "rrnas_silvalsu" in rrnas_tools:
    lsu_database = blast_db_path + "/lsu"
    lsu_remapped_output = target_dir + "/rrnas/silva/lsu.remapped.txt.gz"
    lsu_perc = config["rrnas"]["silvalsu"]["filter"]["min"]["ident"][0]
    lsu_evalue = config["rrnas"]["silvalsu"]["filter"]["min"]["evalue"][0]
    lsu_bs = config["rrnas"]["silvalsu"]["filter"]["min"]["bs"][0]
    lsu_unfiltered_output = target_dir + "/rrnas/silva/lsu.raw.txt.gz"
    lsu_output = target_dir + "/rrnas/silva/lsu.txt.gz"
    lsu_collect = target_dir + "/rrnas/silva/collect.txt"
    silva_lsu_gff = target_dir + "/rrnas/silva/lsu.gff"
    lsu_log = log_dir + "/silva_lsu.txt"
    rrnas_output.append(lsu_collect)

    # Cite
    try:
        cite.add(gui["annotation"]["rrnas"]["content"]["rrnas"]["tools"]["silvalsu"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/rrna/silvalsu.smk"

if "rrnas_barrnap" in rrnas_tools:
    barrnap_path = "/opt/mosga/tools/barrnap/bin/barrnap"
    barrnap_kingdom = config["rrnas"]["barrnap"]["radio"]["kingdom"][0]
    barrnap_lencutoff = int(config["rrnas"]["barrnap"]["filter"]["lencutoff"][0])/100
    barrnap_reject = int(config["rrnas"]["barrnap"]["filter"]["reject"][0])/100
    barrnap_evalue = config["rrnas"]["barrnap"]["filter"]["min"]["evalue"][0]
    barrnap_output = target_dir + "/rrnas/barrnap/barrnap.gff"
    barrnap_collect = target_dir + "/rrnas/barrnap/collect.txt"
    barrnap_log = log_dir + "/barrnap.txt"
    rrnas_output.append(barrnap_collect)

    # Cite
    try:
        cite.add(gui["annotation"]["rrnas"]["content"]["rrnas"]["tools"]["barrnap"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/rrna/barrnap.smk"

if "rrnas_ssualign" in rrnas_tools:
    ssualign_dir = target_dir + "/rrnas/ssu-align/"
    ssualign_collect = ssualign_dir + "collect.txt"
    ssualign_output = ssualign_dir + "ssu-align.tab"
    ssualign_log = log_dir + "/ssu-align.txt"
    ssualign_kingdom = config["rrnas"]["ssualign"]["radio"]["kingdom"][0]
    ssualign_mosga_minscore = int(config["rrnas"]["ssualign"]["filter"]["minscore"][0])
    rrnas_output.append(ssualign_collect)

    # Cite
    try:
        cite.add(gui["annotation"]["rrnas"]["content"]["rrnas"]["tools"]["ssu-align"]["publication"])
    except KeyError:
        pass

    include: rules_dir + "annotation/rrna/ssu-align.smk"
