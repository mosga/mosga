license_file = check_genemark_license(target_dir, config)
cite.add(publications["spaln"])

rule prothint:
    input:
        genome_file
    output:
        hints_file
    params:
        db = orthodb_db,
        dir = prothint_dir
    threads:
        cores
    run:
        shell("cp {license_file} ~/.gm_key 2> /dev/null")
        shell("python3 /opt/mosga/scripts/prothint_orthodb.py {input} {params.db} {params.dir} -c {threads}")
        shell("rm -f ~/.gm_key 2> /dev/null")
