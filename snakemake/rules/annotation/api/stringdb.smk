stringdb_file = target_dir + "/genes/stringdb.html"

try:
    stringdb = config["stringdb"][0]
    stringdb_organism = config["stringdb-organism"][0]
except KeyError:
    stringdb = None
    stringdb_organism = None

if stringdb != None and genes != None and "gene_collector" in locals():
    modules.append(stringdb_file)
    cite.add(publications["stringdb"])

rule stringdb:
    input:
        annotation_tools
    output:
        stringdb_file
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        organism = stringdb_organism
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/accumulator/mosga.py stringdb {params.genome} {output} -s {params.organism} -dir {params.up} && touch {output}"
