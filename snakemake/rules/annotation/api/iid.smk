iid_file = target_dir + "/genes/iid.csv"

try:
    iid = config["iid"][0]
    iid_organism = config["iid-organism"][0]
except KeyError:
    iid = None
    iid_organism = None

if iid != None and genes != None and "gene_collector" in locals():
    modules.append(iid_file)
    cite.add(publications["iid"])

rule iid:
    input:
        annotation_tools
    output:
        iid_file
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        organism = iid_organism
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/accumulator/mosga.py iid {params.genome} {output} {params.organism} -dir {params.up}"
