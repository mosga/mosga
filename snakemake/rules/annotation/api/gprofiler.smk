gprofiler_file = target_dir + "/genes/gprofiler.html"

try:
    gprofiler = config["gprofiler"][0]
    gprofiler_organism = config["gprofiler-organism"][0]
except KeyError:
    gprofiler = None
    gprofiler_organism = None

if gprofiler != None and genes != None and "gene_collector" in locals():
    modules.append(gprofiler_file)
    cite.add(publications["gprofiler"])

rule gprofiler:
    input:
        annotation_tools
    output:
        gprofiler_file
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        organism = gprofiler_organism
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/accumulator/mosga.py gostgprofiler {params.genome} {output} {params.organism} -dir {params.up} && touch {output}"
