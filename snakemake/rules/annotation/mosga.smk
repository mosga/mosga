try:
    priority_list = "--priority " + config["priority_list"]
    if len(config["priority_list"]) == 0:
        priority_list = ""
except KeyError:
    priority_list = ""

annotation_tools = target_dir + "/control/annotation_tools.txt"
independent_tools = target_dir + "/control/independent_tools.txt"
if not organelles and not validation_tools:
    independent_tools = []

rule MOSGA_annotation_tools:
    input:
        s = samtools_index_output if genes != None and genes_mode == "genes_mode_evidence-based" and evidence_format == "raw" else [],
        go = genes_output if genes != None else [],
        gfo = gene_function_output if genes != None and gene_function != None else [],
        gc = gene_collection if genes != None else [],
        ro = repeat_output if repeats != None else [],
        to = trnas_output if trnas != None else [],
        rro = rrnas_output if rrnas != None else [],
        cpgs = cpgisland_tools if cpgisland_tools != None else []
    output:
        annotation_tools
    shell:
        "touch {output}"

rule MOSGA_independent_tools:
    input:
        val = validation_tools if validation_tools else []
    output:
        independent_tools
    shell:
        "touch {output}"

rule MOSGA_export_FeatureTable_GFF:
    input:
        toolsA = annotation_tools,
        genome = genome_file,
        orgs = organelles_list if organelles else [],
    output:
        feature = feature_table,
        gff = gff_output
    log:
        writer_log
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        prefix = writer_prefix,
        loc_prefix = writer_loc_prefix,
        pri = priority_list,
        orgs = "-oi" if organelles else "",
        orgs_file = "--organelle_file %s" % organelles_result if organelles else "",
        xorgs = "-xoi" if organelles and organelles_extended else "",
        summary = target_dir + "/summary.html",
        overall_summary = target_dir + "/overall_summary.html"
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py export {params.genome} -v -qc -soi {params.xorgs} -dir {params.up} {params.orgs} {params.orgs_file} {params.prefix} {params.loc_prefix} {params.pri} -o {output.feature},{output.gff} -w Sequin,GFF -gf {input.genome} -sum {params.summary} -os {params.overall_summary} -t {threads} > {log}"
