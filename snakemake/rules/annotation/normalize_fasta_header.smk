gfile = target_dir + "/" + config["files"]["genome"][0]["path"]
genome_file = gfile + "_n"
file_scan = log_dir + "/file_scan.txt"

tmp_fname = config["files"]["genome"][0]["name"].lower()
annotation_import = tmp_fname[-4:] == "gbff" or tmp_fname[-7:] == "genbank" or tmp_fname[-3:] == "gbk" or tmp_fname[-7:] == "gbff.gz" or tmp_fname[-6:] == "gbk.gz"

if annotation_import:
    include: rules_dir + "annotation/annotation_import.smk"
else:
    rule normalize_fasta_header:
        input:
            gfile
        output:
            genome_file
        log:
            file_scan
        params:
            name = "scaffold",
            dir = target_dir
        shell:
            """
            bash /opt/mosga/scripts/scan_uploaded_files.sh {params.dir} >{log}
            norm=`bash /opt/mosga/scripts/fasta_check_whitespaces.sh {input}`; if [ "$norm" == "0" ]; then ln -s {input} {output}; else python3 /opt/mosga/scripts/normalize_fasta.py {input} {output}; fi
            """

chopped_genome = target_dir + "/genes/chopped_genome.fna.gz"
chopped_raw_genome = target_dir + "/genes/chopped_genome.fna"

try:
    ignore_regions = target_dir + "/" + config["files"]["ignoreregion"][0]["path"]
    genome_ir_file = gfile + "_ir"
    final_genome_file = genome_file
    include: rules_dir + "annotation/ignore_regions.smk"
    genome_file = genome_ir_file
except KeyError:
    final_genome_file = genome_file
