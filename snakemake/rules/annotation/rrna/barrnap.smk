rule barrnap:
    input:
        genome_file
    threads:
        cores_3
    params:
        kingdom = barrnap_kingdom,
        len = barrnap_lencutoff,
        rej = barrnap_reject,
        eval = barrnap_evalue,
        barrnap_bin = barrnap_path
    output:
        barrnap_output
    log:
        barrnap_log
    shell:
        "{params.barrnap_bin} {input} --kingdom {params.kingdom} --lencutoff {params.len} --reject {params.rej} --evalue {params.eval} --threads {threads} --quiet > {output} 2>{log}"

rule MOSGA_import_barrnap:
    input:
        rnap = barrnap_output,
        igf = igf
    output:
        barrnap_collect
    threads:
        cores
    params:
        genome = config["dir"],
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} rrna -v -dir {params.up} --rrna {input.rnap} --rrna-format GFF --reader-rrna Barrnap > {output}"
