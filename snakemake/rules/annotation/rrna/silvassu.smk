rule blastn_silva_ssu:
    input:
        chopped_genome
    output:
        ssu_unfiltered_output
    log:
        ssu_log
    params:
        perc_identity = ssu_perc,
        best_overhang = "0.25",
        outfmt = silva_blastn_outfmt,
        evalue = ssu_evalue,
        database = ssu_database
    threads:
        cores_2
    shell:
        "gunzip {input} -c | blastn -db {params.database} -best_hit_overhang {params.best_overhang} -evalue {params.evalue} -perc_identity {params.perc_identity} -num_threads {threads} -outfmt \"{params.outfmt}\" 2>{log} | gzip -c >{output}"

rule remap_silvassu_results:
    input:
        ssu_unfiltered_output
    output:
        ssu_remapped_output
    params:
        cs = config["genome_chopping_size"]
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/scripts/rename_chopped_results.py {input} {output} -cs {params.cs}"

rule filter_silvassu:
    input:
        ssu_remapped_output
    output:
        ssu_output
    shell:
        "gunzip {input} -c | sort --version-sort -k1,1 -k10,10 -k11,11r | sort --version-sort -u -k1,1 | gzip -c >{output}"

rule MOSGA_import_silvassu:
    input:
        su = ssu_output,
        igf = igf
    output:
        ssu_collect
    params:
        genome = config["dir"],
        filter = ssu_bs,
        up = uploads_dir_absolute
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} rrna -v -dir {params.up} --rrna {input.su} --rrna-format blast --rrna-stype 3 --rrna-min-score {params.filter} --reader-rrna SILVA > {output}"
