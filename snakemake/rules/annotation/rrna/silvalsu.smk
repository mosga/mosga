rule blastn_silva_lsu:
    input:
        chopped_genome
    output:
        lsu_unfiltered_output
    log:
        lsu_log
    params:
        perc_identity = lsu_perc,
        best_overhang = "0.25",
        outfmt = silva_blastn_outfmt,
        evalue = lsu_evalue,
        database = lsu_database
    threads:
        cores_3
    shell:
        "gunzip {input} -c | blastn -db {params.database} -best_hit_overhang {params.best_overhang} -evalue {params.evalue} -perc_identity {params.perc_identity} -num_threads {threads} -outfmt \"{params.outfmt}\" 2>{log} | gzip -c >{output}"

rule remap_silvalsu_results:
    input:
        lsu_unfiltered_output
    output:
        lsu_remapped_output
    params:
        cs = config["genome_chopping_size"]
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/scripts/rename_chopped_results.py {input} {output} -cs {params.cs}"

rule filter_silvalsu:
    input:
        lsu_remapped_output
    output:
        lsu_output
    threads:
        cores_s
    shell:
        "gunzip {input} -c | sort --version-sort -k1,1 -k10,10 -k11,11r | sort --version-sort -u -k1,1 | gzip -c >{output}"

#rule silvalsu_to_gff3:
#    input:
#        lsu_output
#    output:
#        silva_lsu_gff
#    threads:
#        cores_s
#    shell:
#        "python3 /opt/mosga/scripts/silva_to_gff.py {input} {output}"

rule MOSGA_import_silvalsu:
    input:
        su = lsu_output,
        igf = igf
    output:
        lsu_collect
    params:
        genome = config["dir"],
        filter = lsu_bs,
        up = uploads_dir_absolute
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} rrna -v -dir {params.up} --rrna {input.su} --rrna-format blast --rrna-stype 4 --rrna-min-score {params.filter} --reader-rrna SILVA > {output}"
