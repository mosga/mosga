rule ssu_align:
    input:
        genome_file
    output:
        ssualign_output
    log:
        ssualign_log
    threads:
        1
    params:
        dir = ssualign_dir,
        kingdom = ssualign_kingdom
    shell:
        "ssu-align -f -m /usr/local/share/ssu-align-0.1.1/{params.kingdom}-0p1.cm {input} {params.dir} > {log} 2>&1"

rule MOSGA_import_ssualign:
    input:
        ssu = ssualign_output,
        igf = igf
    output:
        ssualign_collect
    log:
        ssualign_log
    threads:
        1
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        score = ssualign_mosga_minscore
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} rrna -v -dir {params.up} --rrna {input.ssu} --rrna-format CSV --reader-rrna ssualign --rrna-min-score {params.score} > {output} 2>> {log}"
