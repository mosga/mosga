cpgiscan_output = target_dir + "/cpgisland/cpgiscan/out.gff"
cpgiscan_collect = target_dir + "/cpgisland/cpgiscan/collect.txt"
cpgiscan_log = log_dir + "/cpgiscan.log"

try:
    cpgiscan = config["cpgiscan"][0]
except:
    cpgiscan = None

if cpgiscan != None:
    cpgisland_tools.append(cpgiscan_collect)
    modules.append(cpgiscan_output)
    cite.add(publications["cpgiscan"])

rule cpgiscan:
    input:
        genome_file
    output:
        cpgiscan_output
    log:
        cpgiscan_log
    threads:
        cores_s
    shell:
        "/opt/mosga/tools/cpgiscan -G {output} {input} >{log} 2>&1"

rule MOSGA_import_CpGIScan:
    input:
        cpg = cpgiscan_output,
        igf = igf
    output:
        cpgiscan_collect
    params:
        genome = config["dir"],
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} cpgisland -v -dir {params.up} --cpgisland {input.cpg} --cpgisland-min-length 100 --cpgisland-format GFF --reader-cpgisland CpGIScan > {output}"
