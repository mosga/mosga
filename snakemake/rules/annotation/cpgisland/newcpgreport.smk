newcpgreport_output = target_dir + "/cpgisland/newcpgreport/out.gff"
newcpgreport_collect = target_dir + "/cpgisland/newcpgreport/collect.txt"
newcpgreport_log = log_dir + "/newcpgreport.log"

try:
    newcpgreport = config["newcpgreport"][0]
except:
    newcpgreport = None

if newcpgreport != None:
    cpgisland_tools.append(newcpgreport_collect)
    modules.append(newcpgreport_output)
    #cite.add(publications["newcpgreport"])

rule newcpgreport:
    input:
        genome_file
    output:
        newcpgreport_output
    log:
        newcpgreport_log
    threads:
        cores_s
    shell:
        "newcpgreport -sequence {input} -window 100 -shift 1 -minlen 200 -minoe 0.6 -minpc 50. -outfile {output} >{log} 2>&1"

rule MOSGA_import_newcpgreport:
    input:
        cpg = newcpgreport_output,
        igf = igf
    output:
        newcpgreport_collect
    params:
        genome = config["dir"],
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} cpgisland -v -dir {params.up} --cpgisland {input.cpg} --cpgisland-min-length 100 --cpgisland-format EFF --reader-cpgisland newcpgreport > {output}"
