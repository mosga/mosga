# Mito
if "mitos_output" in locals():
    jbrowse_mito_output = target_dir + "/genes/mitos.jbrowse.txt"
    jbrowse_list.append(jbrowse_mito_output)

    jbrowse_mito_gz = target_dir + "/genes/mitos.gff.gz"
    jbrowse_mito_tabix = jbrowse_mito_gz + ".tbi"

    rule mito_tabix:
        input:
            mitos_output
        output:
            g = jbrowse_mito_gz,
            t = jbrowse_mito_tabix
        threads:
            cores_s
        shell:
            """
            python3 /opt/mosga/scripts/match_to_gff3.py {input} -s Mito -t Match | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
            tabix {output.g}
            """

    rule jbrowse2_mito:
        input:
            gff = jbrowse_mito_gz,
            cnf = jbrowse_output
        output:
            jbrowse_mito_output
        log:
            jbrowse_log
        params:
            real = jbrowse_real_dir,
            load = jbrowse_load_parameter,
            color = jbrowse_colors["organellar"],
            category = "Organellar",
            name = "Mitochondrial Genes", # allow spaces
            display = "mito"
        threads:
            cores
        shell:
            """
            cd {params.real}
            /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
            touch {output}
            """

if "plastid_output" in locals():
    jbrowse_plastid_output = target_dir + "/genes/plastids.jbrowse.txt"
    jbrowse_list.append(jbrowse_plastid_output)

    jbrowse_plastid_gz = target_dir + "/genes/plastids.gff.gz"
    jbrowse_plastid_tabix = jbrowse_plastid_gz + ".tbi"

    rule plastid_tabix:
        input:
            plastid_output
        output:
            g = jbrowse_plastid_gz,
            t = jbrowse_plastid_tabix
        threads:
            cores_s
        shell:
            """
            python3 /opt/mosga/scripts/match_to_gff3.py {input} -s Mito -t Match | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
            tabix {output.g}
            """

    rule jbrowse2_plastid:
        input:
            gff = jbrowse_plastid_gz,
            cnf = jbrowse_output
        output:
            jbrowse_plastid_output
        log:
            jbrowse_log
        params:
            real = jbrowse_real_dir,
            load = jbrowse_load_parameter,
            color = jbrowse_colors["organellar"],
            category = "Organellar",
            name = "Plastid Genes", # allow spaces
            display = "plastid"
        threads:
            cores
        shell:
            """
            cd {params.real}
            /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
            touch {output}
            """
