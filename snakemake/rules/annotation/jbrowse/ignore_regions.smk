jbrowse_ignore_regions = target_dir + "jbrowse_ir.txt"
jbrowse_list.append(jbrowse_ignore_regions)

rule jbrowse_ignore_regions:
    input:
        bed = ignore_regions,
        j = jbrowse_output
    output:
        jbrowse_ignore_regions
    params:
        genome=config["dir"],
        real=jbrowse_real_dir
    threads:
        cores
    shell:
        "/opt/mosga/gui/jbrowse/bin/flatfile-to-json.pl --compress -key 'Ignored regions' --bed {input.bed} --out {params.real} --trackLabel IgnoreRegions --trackType CanvasFeatures && touch {output}"
