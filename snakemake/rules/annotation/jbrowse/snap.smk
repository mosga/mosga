jbrowse_snap_output = target_dir + "/genes/snap/jbrowse.txt"
jbrowse_list.append(jbrowse_snap_output)

jbrowse_snap_gff = snap_gff + "_jbrowse.gff"
jbrowse_snap_gffgz = jbrowse_snap_gff + ".gz"
jbrowse_snap_tabix = jbrowse_snap_gff + ".gz.tbi"

rule snap_tabix:
    input:
        snap_gff
    output:
        gff = jbrowse_snap_gff,
        g = jbrowse_snap_gffgz,
        t = jbrowse_snap_tabix
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse -f GFF SNAP Gene {input} {output.gff} && gt gff3 -sortlines -tidy -retainids {output.gff} 2> /dev/null | bgzip -c > {output.g}
        tabix {output.g}
        """

rule jbrowse2_snap:
    input:
        gff = jbrowse_snap_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_snap_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["gene"],
        category = "Genes",
        name = "SNAP", # allow spaces
        display = "snap"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
