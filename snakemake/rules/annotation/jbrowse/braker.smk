jbrowse_braker_output = target_dir + "/genes/braker/jbrowse.txt"
jbrowse_list.append(jbrowse_braker_output)

jbrowse_braker_output_gffgz = target_dir + "/genes/braker/braker.gff.gz"
jbrowse_braker_output_tabix = target_dir + "/genes/braker/braker.gff.gz.tbi"

rule braker_tabix:
    input:
        braker_output
    output:
        g = jbrowse_braker_output_gffgz,
        t = jbrowse_braker_output_tabix
    threads:
        cores_s
    shell:
        """
        cat {input} | sed '/^#/d' | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_braker:
    input:
        gff = jbrowse_braker_output_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_braker_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["gene"],
        category = "Genes",
        name = "BRAKER", # allow spaces
        display = "braker"

    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
