jbrowse_trnascan_output = trnascan_directory + "jbrowse.txt"
jbrowse_list.append(jbrowse_trnascan_output)

jbrowse_trnascan_tabix = trnascan_output_bed + ".gz.tbi"
jbrowse_trnascan_gffgz = trnascan_output_bed + ".gz"

rule trnascan_se2_tabix:
    input:
        trnascan_output_bed
    output:
        t = jbrowse_trnascan_tabix,
        g = jbrowse_trnascan_gffgz
    threads:
        cores_s
    shell:
        """
        bgzip {input} -c > {output.g}
        tabix {output.g}
        """

rule jbrowse2_trnascan_se2:
    input:
        gff = jbrowse_trnascan_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_trnascan_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["tRNA"],
        category = "tRNAs",
        name = "tRNAScan-SE", # allow spaces
        display = "trnascanse"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
