jbrowse_repeatmasker_output = target_dir + "/repeats/repeatmasker/jbrowse.txt"
jbrowse_list.append(jbrowse_repeatmasker_output)

repeatmasker_gff_output = target_dir + "/repeats/repeatmasker/repeatmasker.gff"
repeatmasker_gz_output = repeatmasker_gff_output + ".gz"

rule MOSGA_jbrowse_RepeatMasker:
    input:
        repeatmasker_output
    output:
        gff = repeatmasker_gff_output,
        gz = repeatmasker_gz_output
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse -f RepeatMaskerOutput RepeatMasker Repeats {input} {output.gff} && gt gff3 -sortlines -tidy -retainids {output.gff} 2> /dev/null | bgzip > {output.gz}
        tabix {output.gz}
        """

rule jbrowse2_repeatmasker:
    input:
        gff = repeatmasker_gz_output,
        cnf = jbrowse_output
    output:
        jbrowse_repeatmasker_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["repeat"],
        category = "Repeats",
        name = "RepeatMasker", # allow spaces
        display = "repeatmasker"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
