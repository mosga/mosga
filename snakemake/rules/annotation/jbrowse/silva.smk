jbrowse_silvalsu_output = target_dir + "/rrnas/silva/jbrowse_lsu.txt"
jbrowse_list.append(jbrowse_silvalsu_output)

jbrowse_silvalsu_gz = silva_lsu_gff + ".gz"
jbrowse_silvalsu_tabix = silva_lsu_gff + ".gz.tbi"

rule silvalsu_tabix:
    input:
        lsu_output
    output:
        g = jbrowse_silvalsu_gz,
        t = jbrowse_silvalsu_tabix
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/scripts/match_to_gff3.py {input} -s SILVA -t rRNA -st LSU | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_silvalsu:
    input:
        gff = jbrowse_silvalsu_gz,
        cnf = jbrowse_output
    output:
        jbrowse_silvalsu_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["rRNA"],
        category = "rRNAs",
        name = "SILVA LSU", # allow spaces
        display = "silvalsu"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
