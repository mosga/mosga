jbrowse_ssualign_output = target_dir + "/rrnas/ssu-align/jbrowse.txt"
jbrowse_list.append(jbrowse_ssualign_output)

jbrowse_ssualign_gff =  target_dir + "/rrnas/ssu-align/ssu-align.gff"
jbrowse_ssualign_output_gz = jbrowse_ssualign_gff + ".gz"
jbrowse_ssualign_output_tbi = jbrowse_ssualign_gff + ".gz.tbi"

rule ssualign_tabix:
    input:
        ssualign_output
    output:
        gff = jbrowse_ssualign_gff,
        g = jbrowse_ssualign_output_gz,
        t = jbrowse_ssualign_output_tbi
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse ssualign rRNA {input} {output.gff} && gt gff3 -sortlines -tidy -retainids {output.gff} 2> /dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_ssualign:
    input:
        gff = jbrowse_ssualign_output_gz,
        cnf = jbrowse_output
    output:
        jbrowse_ssualign_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["rRNA"],
        category = "rRNAs",
        name = "SSU-ALIGN", # allow spaces
        display = "ssualign"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
