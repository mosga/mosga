jbrowse_gff_output = target_dir + "/control/jbrowse2_mosga.txt"
jbrowse2_out = target_dir + "/control/jbrowse2.txt"
jbrowse_mosga_log = log_dir + "/jbrowse2_mosga.log"
jbrowse_mosga_output_gz = gff_output + ".gz"
jbrowse_mosga_output_tabix = gff_output + ".gz.tbi"

rule mosga_gff_tabix:
    input:
        gff_output
    output:
        g = jbrowse_mosga_output_gz,
        t = jbrowse_mosga_output_tabix
    log:
        jbrowse_mosga_log
    threads:
        cores_s
    shell:
        """
        cat {input} | gt gff3 -sortlines -tidy -retainids 2>{log} | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_mosga_gff:
    input:
        gff = jbrowse_mosga_output_gz,
        cnf = jbrowse_output
    output:
        jbrowse_gff_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["mosga"],
        category = "Genes",
        name = "MOSGA", # allow spaces
        display = "mosga"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """

rule jbrowse2_all:
    input:
        jbrowse_list,
        jbrowse_gff_output if len(annotation_types) > 0 else []
    output:
        jbrowse2_out
    shell:
        "touch {output}"
