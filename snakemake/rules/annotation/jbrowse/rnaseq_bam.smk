jbrowse_rnaseq_bam_output = target_dir + "/rna-seq/jbrowse_bam.txt"
jbrowse_list.append(jbrowse_rnaseq_bam_output)

rule jbrowse2_rnaseq:
    input:
        bam = bam_sorted_file,
        bam_index = samtools_index_output,
        j = jbrowse_output
    output:
        jbrowse_rnaseq_bam_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.bam} --load {params.load} --category=RNAseq --subDir=RNAseq --force -n RNA-Seq >>{log} 2>&1
        touch {output}
        """
