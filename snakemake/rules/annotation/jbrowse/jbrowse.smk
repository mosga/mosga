# Run jBrowse
jbrowse_list = list()
jbrowse_link = "/opt/mosga/gui/jbrowse2/mosga/" + config["dir"]
jbrowse_real_dir = target_dir + "/jbrowse"
jbrowse_config = jbrowse_real_dir + "/config.json"
jbrowse_output = jbrowse_real_dir + "/setview.txt"
jbrowse_load_parameter = "copy"
jbrowse_log = log_dir + "/jbrowse2.txt"

cite.add(publications["jbrowse"])
jbrowse_colors: dict = {
    "mosga":      color_palette[10],
    "gene":       color_palette[2],
    "tRNA":       color_palette[0],
    "rRNA":       color_palette[8],
    "repeat":     color_palette[13],
    "organellar": color_palette[11],
    "validation": color_palette[4],
    "supporting": color_palette[16]
}

# Genome as Reference
include: rules_dir + "annotation/jbrowse/refseq.smk"

# Red
if repeats != None and "red_repeats" in locals():
    include: rules_dir + "annotation/jbrowse/red.smk"

# WindowMasker
if repeats != None and "windowmasker_hard_output" in locals():
    include: rules_dir + "annotation/jbrowse/windowmasker.smk"

# RepeatMasker
if repeats != None and "repeatmasker_output" in locals():
    include: rules_dir + "annotation/jbrowse/repeatmasker.smk"

# tRNAscan-SE
if trnas != None and "trnascan_output" in locals():
    include: rules_dir + "annotation/jbrowse/trnascan_se2.smk"

# Imported annotation
if annotation_import and "annotation_import_gff" in locals():
    include: rules_dir + "annotation/jbrowse/imported_annotation.smk"

# barrnap
if rrnas != None and "barrnap_output" in locals():
    include: rules_dir + "annotation/jbrowse/barrnap.smk"

# ssu-align
if rrnas != None and "ssualign_output" in locals():
    include: rules_dir + "annotation/jbrowse/ssu-align.smk"

# SILVA
if rrnas != None and "lsu_output" in locals():
    include: rules_dir + "annotation/jbrowse/silva.smk"

# VecScreen
if validation_tools != None and "vecscreen_gff" in locals():
    include: rules_dir + "annotation/jbrowse/vecscreen.smk"

"""
# Ignore regions
if "ignore_regions" in locals():
    include: rules_dir + "annotation/jbrowse/ignore_regions.smk"
"""

# Spliceator
if spliceator != None and "spliceator_output" in locals():
    include: rules_dir + "annotation/jbrowse/spliceator.smk"

# CpGIScan
if cpgiscan != None and "cpgiscan_output" in locals():
    include: rules_dir + "annotation/jbrowse/cpgiscan.smk"

# newcpgreport
if newcpgreport != None and "newcpgreport_output" in locals():
    include: rules_dir + "annotation/jbrowse/newcpgreport.smk"

# Organelles: Mito / Plastid
if organelles == True:
    include: rules_dir + "annotation/jbrowse/organelles.smk"

# Genes
if genes != None:

    # RNA-seq
    if genes_mode == "genes_mode_evidence-based" \
    and "bam_sorted_file" in locals() and "evidence_format" in locals() \
    and evidence_format == "raw":
        include: rules_dir + "annotation/jbrowse/rnaseq_bam.smk"

    # Augustus
    if "augustus_output" in locals():
        include: rules_dir + "annotation/jbrowse/augustus.smk"

    # BRAKER
    if "braker_output" in locals():
        include: rules_dir + "annotation/jbrowse/braker.smk"

    # GeneMark
    #if "genemark_output" in locals():
    #    include: rules_dir + "annotation/jbrowse/genemark.smk"

    # GlimmerHMM
    #if "glimmerhmm_gff" in locals():
    #    include: rules_dir + "annotation/jbrowse/glimmerhmm.smk"

    # SNAP
    if "snap_gff" in locals():
        include: rules_dir + "annotation/jbrowse/snap.smk"

# JBrowse overview
include: rules_dir + "annotation/jbrowse/mosga.smk"
