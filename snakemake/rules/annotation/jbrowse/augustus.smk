jbrowse_augustus_output = target_dir + "/genes/augustus/jbrowse.txt"
augustus_output_gff3 = target_dir + "/genes/augustus/output.gff"

jbrowse_list.append(jbrowse_augustus_output)
jbrowse_augustus_output_gz = augustus_output_gff3 + ".gz"
jbrowse_augustus_output_tabix = augustus_output_gff3 + ".gz.tbi"

rule augustus_tabix:
    input:
        augustus_output
    output:
        g = jbrowse_augustus_output_gz,
        t = jbrowse_augustus_output_tabix
    threads:
        cores_s
    shell:
        """
        grep -v '^#' {input} | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_augustus:
    input:
        gff = jbrowse_augustus_output_gz,
        cnf = jbrowse_output
    output:
        jbrowse_augustus_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["gene"],
        category = "Genes",
        name = "Augustus", # allow spaces
        display = "augustus"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
