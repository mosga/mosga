jbrowse_newcpgreport_output = target_dir + "/cpgisland/newcpgreport/jbrowse.txt"
jbrowse_list.append(jbrowse_newcpgreport_output)

jbrowse_newcpgreport_output_gff = target_dir + "/cpgisland/newcpgreport/newcpgreport.gff"
jbrowse_newcpgreport_output_gffgz = jbrowse_newcpgreport_output_gff + ".gz"
jbrowse_newcpgreport_output_tabix = jbrowse_newcpgreport_output_gff + ".gz.tbi"

rule newcpgreport_tabix:
    input:
        newcpgreport_output
    output:
        gff = jbrowse_newcpgreport_output_gff,
        g = jbrowse_newcpgreport_output_gffgz,
        t = jbrowse_newcpgreport_output_tabix
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse newcpgreport CpGIsland {input} {output.gff} -f EFF && gt gff3 -sortlines -tidy -retainids {output.gff} 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_newcpgreport:
    input:
        gff = jbrowse_newcpgreport_output_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_newcpgreport_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["supporting"],
        category = "Supporting",
        name = "newcpgreport", # allow spaces
        display = "newcpgreport"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
