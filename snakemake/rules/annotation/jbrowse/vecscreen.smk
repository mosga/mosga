jbrowse_vecscreen_output = target_dir + "/validation/vecscreen/jbrowse.txt"
jbrowse_list.append(jbrowse_vecscreen_output)


jbrowse_vecscreen_gz = vecscreen_gff + ".gz"
jbrowse_vecscreen_tabix = vecscreen_gff + ".gz.tbi"

rule vecscreen_tabix:
    input:
        vecscreen_gff
    output:
        g = jbrowse_vecscreen_gz,
        t = jbrowse_vecscreen_tabix
    threads:
        cores_s
    shell:
        """
        cat {input} | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_vecscreen:
    input:
        gff = jbrowse_vecscreen_gz,
        cnf = jbrowse_output
    output:
        jbrowse_vecscreen_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["validation"],
        category = "Validation",
        name = "VecScreen", # allow spaces
        display = "vecscreen"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
