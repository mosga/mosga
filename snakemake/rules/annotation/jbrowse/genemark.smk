jbrowse_genemark_output = target_dir + "/genes/genemark/jbrowse.txt"
jbrowse_list.append(jbrowse_genemark_output)

rule jbrowse_genemark:
    input:
        gff = genemark_output,
        j = jbrowse_output
    output:
        jbrowse_genemark_output
    params:
        genome=config["dir"],
        real=jbrowse_real_dir
    threads:
        cores
    shell:
        "/opt/mosga/gui/jbrowse/bin/flatfile-to-json.pl --compress -key 'GeneMark' --gff {input.gff} --out {params.real} --trackLabel genemark --trackType CanvasFeatures && touch {output}"
