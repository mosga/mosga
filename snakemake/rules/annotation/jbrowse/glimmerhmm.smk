jbrowse_glimmerhmm_output =  target_dir + "/genes/glimmerhmm/jbrowse.txt"
jbrowse_list.append(jbrowse_glimmerhmm_output)

rule jbrowse_glimmerhmm:
    input:
        gff=glimmerhmm_gff,
        j=jbrowse_output
    output:
        jbrowse_glimmerhmm_output
    params:
        genome=config["dir"],
        real=jbrowse_real_dir
    threads:
        cores
    shell:
        "/opt/mosga/gui/jbrowse/bin/flatfile-to-json.pl --compress -key 'GlimmerHMM' --gff {input.gff} --out {params.real} --trackLabel GlimmerHMM --trackType CanvasFeatures && touch {output}"
