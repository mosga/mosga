jbrowse_windowmasker_output = target_dir + "/repeats/windowmasker/jbrowse.txt"
jbrowse_list.append(jbrowse_windowmasker_output)

windowmasker_gff_output = target_dir + "/repeats/windowmasker/windowmasker.gff"
windowmasker_gz_output = windowmasker_gff_output + ".gz"

rule windowmasker_to_gff:
    input:
        windowmasker_masking
    output:
        gff = windowmasker_gff_output,
        gz = windowmasker_gz_output
    threads:
        cores_s
    params:
        format = windowmasker_format
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse -f {params.format} WindowMasker Repeats {input} {output.gff} && gt gff3 -sortlines -tidy -retainids {output.gff} 2>/dev/null | bgzip > {output.gz}
        tabix {output.gz}
        """

rule jbrowse2_windowmasker:
    input:
        gz = windowmasker_gz_output,
        cnf = jbrowse_output
    output:
        jbrowse_windowmasker_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["repeat"],
        category = "Repeats",
        name = "WindowMasker", # allow spaces
        display = "windowmasker"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gz} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
