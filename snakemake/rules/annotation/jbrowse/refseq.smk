jbrowse_list.append(jbrowse_output)

jbrowse2_assembly_index = genome_file + ".fai"

rule jbrowse_assembly_index:
    input:
        genome_file
    output:
        jbrowse2_assembly_index
    params:
        link = jbrowse_link,
        real = jbrowse_real_dir
    threads:
        cores_s
    shell:
        """
        /opt/mosga/tools/samtools/samtools faidx {input}
        ln -s -f {params.real} {params.link} 2>/dev/null
        """

rule jbrowse2_add_assembly:
    input:
        g = genome_file,
        a = jbrowse2_assembly_index
    output:
        jbrowse_config
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-assembly {input.g} --out {params.real} -l {params.load} -t indexedFasta -n Genome >{log} 2>&1
        """

rule jbrowse2_set_default_view:
    input:
        jbrowse_config
    output:
        jbrowse_output
    log:
        jbrowse_log
    params:
        name = "MOSGA " + config["dir"]
    shell:
        """
        /usr/bin/jbrowse set-default-session --target {input} --view LinearGenomeView -n \"{params.name}\" >>{log} 2>&1
        touch {output}
        """
