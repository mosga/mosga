jbrowse_cpgiscan_output = target_dir + "/cpgisland/cpgiscan/jbrowse.txt"
jbrowse_list.append(jbrowse_cpgiscan_output)

jbrowse_cpgiscan_output_gff = target_dir + "/cpgisland/cpgiscan/cpgiscan.gff"
jbrowse_cpgiscan_output_gffgz = jbrowse_cpgiscan_output_gff + ".gz"
jbrowse_cpgiscan_output_tabix = jbrowse_cpgiscan_output_gff + ".gz.tbi"

rule cpgiscan_tabix:
    input:
        cpgiscan_output
    output:
        gff = jbrowse_cpgiscan_output_gff,
        g = jbrowse_cpgiscan_output_gffgz,
        t = jbrowse_cpgiscan_output_tabix
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse CpGIScan CpGIsland {input} {output.gff} -f GFF && gt gff3 -sortlines -tidy -retainids {output.gff} 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_cpgiscan:
    input:
        gff = jbrowse_cpgiscan_output_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_cpgiscan_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["supporting"],
        category = "Supporting",
        name = "CpGIScan", # allow spaces
        display = "cpgiscan"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
