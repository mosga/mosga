jbrowse_barrnap_output = target_dir + "/rrnas/barrnap/jbrowse.txt"
jbrowse_list.append(jbrowse_barrnap_output)

jbrowse_barrnap_output_gff = barrnap_output + "_jbrowse.gff"
jbrowse_barrnap_output_gz = jbrowse_barrnap_output_gff + ".gz"
jbrowse_barrnap_output_tbi = jbrowse_barrnap_output_gff + ".gz.tbi"

rule barrnap_tabix:
    input:
        barrnap_output
    output:
        gff = jbrowse_barrnap_output_gff,
        g = jbrowse_barrnap_output_gz,
        t = jbrowse_barrnap_output_tbi
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py jbrowse Barrnap rRNA {input} {output.gff} -f GFF && gt gff3 -sortlines -tidy -retainids {output.gff} 2> /dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_barrnap:
    input:
        gff = jbrowse_barrnap_output_gz,
        cnf = jbrowse_output
    output:
        jbrowse_barrnap_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["rRNA"],
        category = "rRNAs",
        name = "Barrnap", # allow spaces
        display = "barrnap"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
