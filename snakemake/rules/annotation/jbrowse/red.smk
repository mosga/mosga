jbrowse_red_output = target_dir + "/repeats/red/jbrowse.txt"
jbrowse_list.append(jbrowse_red_output)

jbrowse_red_tabix = red_repeats + ".tbi"

rule red_tabix:
    input:
        red_repeats
    output:
        jbrowse_red_tabix
    threads:
        cores_s
    shell:
        """
        tabix {input}
        """

rule jbrowse2_red:
    input:
        tb = jbrowse_red_tabix,
        cnf = jbrowse_output
    output:
        jbrowse_red_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        gff = red_repeats,
        color = jbrowse_colors["repeat"],
        category = "Repeats",
        name = "Red", # allow spaces
        display = "red"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {params.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
