jbrowse_spliceator_output = target_dir + "/genes/spliceator/jbrowse.txt"
jbrowse_list.append(jbrowse_spliceator_output)

jbrowse_spliceator_output_gffgz = target_dir + "/genes/spliceator/spliceator.gff.gz"
jbrowse_spliceator_output_tabix = target_dir + "/genes/spliceator/spliceator.gff.gz.tbi"

rule spliceator_tabix:
    input:
        spliceator_output
    output:
        g = jbrowse_spliceator_output_gffgz,
        t = jbrowse_spliceator_output_tabix
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/scripts/spliceator_to_gff3.py {input} | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_spliceator:
    input:
        gff = jbrowse_spliceator_output_gffgz,
        cnf = jbrowse_output
    output:
        jbrowse_spliceator_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = jbrowse_colors["supporting"],
        category = "Supporting",
        name = "Spliceator", # allow spaces
        display = "spliceator"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
