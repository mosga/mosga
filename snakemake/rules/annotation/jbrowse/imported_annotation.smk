jbrowse_import_gff_output = target_dir + "/annotation/jbrowse.txt"
jbrowse_list.append(jbrowse_import_gff_output)


jbrowse_imported_output_gz = annotation_import_gff + ".gz"
jbrowse_imported_output_tabix = annotation_import_gff + ".gz.tbi"

rule imported_annotation_tabix:
    input:
        annotation_import_gff
    output:
        g = jbrowse_imported_output_gz,
        t = jbrowse_imported_output_tabix
    threads:
        cores_s
    shell:
        """
        cat {input} | gt gff3 -sortlines -tidy -retainids 2>/dev/null | bgzip > {output.g}
        tabix {output.g}
        """

rule jbrowse2_imported_annotation:
    input:
        gff = jbrowse_imported_output_gz,
        cnf = jbrowse_output
    output:
        jbrowse_import_gff_output
    log:
        jbrowse_log
    params:
        real = jbrowse_real_dir,
        load = jbrowse_load_parameter,
        color = color_palette[18],
        category = "Genes",
        name = "Imported Annotation", # allow spaces
        display = "impanno"
    threads:
        cores
    shell:
        """
        cd {params.real}
        /usr/bin/jbrowse add-track {input.gff} --out {params.real} --load {params.load} --category={params.category} --subDir={params.category} --force -n "{params.name}" --config '{{"displays":[{{"type":"LinearBasicDisplay","displayId":"Disp{params.display}Id","renderer":{{"type":"SvgFeatureRenderer","color1":"{params.color}"}}}}]}}' >>{log} 2>&1
        touch {output}
        """
