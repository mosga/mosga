# Workaround for BRAKER
if "braker" in gene_tools:
    functional_annotations["idtaxa"]["output_aa"] = genes_output

rule idtaxa_prepare:
    input:
        genes_output
    output:
        functional_annotations["idtaxa"]["output_aa"]
    threads:
        cores_s
    log:
        functional_annotations["idtaxa"]["translation_log"]
    shell:
        "transeq --sequence {input} --outseq {output} -trim -clean >{log} 2>&1"

rule idtaxa:
    input:
        functional_annotations["idtaxa"]["output_aa"]
    output:
        functional_annotations["idtaxa"]["output"]
    log:
        functional_annotations["idtaxa"]["log"]
    params:
        db = functional_annotations["idtaxa"]["database"],
        fl = round(functional_annotations["idtaxa"]["length"]/100,2),
        th = functional_annotations["idtaxa"]["params"]["score"]
    threads:
        cores_2
    shell:
        "Rscript /opt/mosga/scripts/idtaxa_annotation.R {params.db} {input} {params.fl} {params.th} {threads} {output} >{log} 2>&1"
