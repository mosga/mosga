# Workaround for BRAKER
if "braker" in gene_tools:
    genes_output = braker_output_aa
    functional_annotations["eggnog"]["translate"] =  "" if braker_eggnog_translate == False else functional_annotations["eggnog"]["translate"]

rule eggnog:
    input:
        genes_output
    output:
        functional_annotations["eggnog"]["output_search_full"]
    log:
        functional_annotations["eggnog"]["log"]
    params:
        output = functional_annotations["eggnog"]["output_search"],
        translate = functional_annotations["eggnog"]["translate"]
    threads:
        cores
    shell:
        """
        cd /opt/mosga/tools/eggnog-mapper
        python3 emapper.py --override -i {input} --output {params.output} -m diamond --cpu {threads} {params.translate} >{log} 2>&1
        """

rule filter_eggnog:
    input:
        functional_annotations["eggnog"]["output_search_full"]
    output:
        functional_annotations["eggnog"]["output"]
    threads:
        cores_s
    shell:
        "grep -v \"^#\" {input} | awk -F\"\t\" '{{print $1\"\t\"$3\"\t\"$4\"\t\"$8\"\t\"$9\"\t\"$10\"\t\"$11\"\t\"$12\"\t\"$13}}' | sort --version-sort -k1,1 | gzip -c > {output}"
