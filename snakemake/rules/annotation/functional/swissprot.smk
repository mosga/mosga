# Workaround for BRAKER
if "braker" in gene_tools:
    functional_annotations["swissprot"]["command"] = "blastp"

rule diamond_swissprot:
    input:
        genes_output
    output:
        functional_annotations["swissprot"]["search_output"]
    log:
        functional_annotations["swissprot"]["log"]
    params:
        database = functional_annotations["swissprot"]["database"],
        evalue = functional_annotations["swissprot"]["params"]["evalue"],
        ident = functional_annotations["swissprot"]["params"]["ident"],
        qc = functional_annotations["swissprot"]["params"]["query_cov"],
        cmd = functional_annotations["swissprot"]["command"]
    threads:
        cores_3
    shell:
        "/opt/mosga/tools/diamond/diamond {params.cmd} -d {params.database} -q {input} -o {output} --threads {threads} --evalue {params.evalue} --id {params.ident} --query-cover {params.qc} -f 6 qseqid sseqid qstart qend sstart send length nident pident evalue bitscore score mismatch gapopen gaps qcovhsp stitle >{log} 2>&1"

rule diamond_filter_result:
    input:
        functional_annotations["swissprot"]["search_output"]
    output:
        functional_annotations["swissprot"]["output"]
    shell:
        "sort {input} --version-sort -k1,1 -k12,12r -u | sort -u -k1,1 --version-sort > {output}"
