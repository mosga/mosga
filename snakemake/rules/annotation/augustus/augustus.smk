rule augustus:
    input:
        augustus_genome
    output:
        augustus_output
    log:
        augustus_log
    params:
        species = augustus_species,
        softmask = "--masked" if softmasking else "",
        hints = hintsfile_para,
        extrinsic = extrinsic_para
    threads:
        cores
    run:
        shell("/opt/mosga/scripts/augustus_parallel.py {input} {threads} {params.species} {output} {params.hints} {params.extrinsic} {params.softmask}")

rule augustus_codingseq:
    input:
        augustus_output
    output:
        augustus_coding_output
    run:
        shell("/opt/mosga/tools/augustus/scripts/getAnnoFasta.pl {input}")
