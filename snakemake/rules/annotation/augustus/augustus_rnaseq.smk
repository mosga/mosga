rule augustus_rnaseq:
    input:
        genome=augustus_genome,
        hints=hints_file
    output:
        augustus_output
    log:
        augustus_log
    params:
        species=augustus_species,
        softmask="--softmasking=1" if softmasking else "",
        hints=hintsfile
    threads:
        cores_s
    run:
        shell("/opt/mosga/tools/augustus/bin/augustus {input.genome} --species={params.species} {params.hints} {params.softmask} --genemodel=complete --protein=on --codingseq=1 --gff3=off >{output} 2>{log}")

rule augustus_codingseq:
    input:
        augustus_output
    output:
        augustus_coding_output
    run:
        shell("/opt/mosga/tools/augustus/scripts/getAnnoFasta.pl {input}")
