def find_fun_annotation(search, entries):
    for entry in entries:
        if entries[entry]["name"] == search:
            return entry
    return False

def add_annotation_tool(conf, tool, functions, gene_function_output, gene_tools):
    # include rule
    try:
        include:    conf[tool]["include"]
        gene_function_output.append(conf[tool]["output"])
        functions.update({tool: (conf[tool]["output"], conf[tool]["params"])})
    except KeyError as e:
        pass


    # add citations
    try:
        for c in conf[tool]["cite"]:
            cite.add(c)
    except KeyError:
        pass

gene_function_tools: list = []
gene_function_output: list = []
functions: dict = {}

for function in config["genes"]["function"]:
    try:
        gene_function_tools.extend( config["genes"]["function"][str(int(function))] )
    except ValueError:
        pass

functional_annotations = {
    "swissprot": {
        "name": "genes_function_swissprot",
        "output": target_dir + "/genes/swissprot/result.txt",
        "log": log_dir + "/swiss-prot.txt",
        "database": "/opt/mosga/tools/diamond/swissprot",
        "search_output": target_dir + "/genes/swissprot/result.raw",
        "command": "blastx",
        "params": {
            "evalue": config["genes"]["function"]["swissprot"]["filter"]["min"]["evalue"][0],
            "ident": config["genes"]["function"]["swissprot"]["filter"]["min"]["ident"][0],
            "score": config["genes"]["function"]["swissprot"]["filter"]["min"]["bs"][0],
            "query_cov": config["genes"]["function"]["swissprot"]["filter"]["min"]["qc"][0]
        },
        "cite": [gui["annotation"]["genes"]["content"]["functional"]["tools"]["swissprot"]["publication"],
        publications["diamond"]],
        "include": rules_dir + "annotation/functional/swissprot.smk"
    },
    "eggnog": {
        "name": "genes_function_eggnog",
        "output": target_dir + "/genes/eggnog/result.txt.gz",
        "log": log_dir + "/eggnog.txt",
        "output_search": target_dir + "/genes/eggnog/result",
        "output_search_full": target_dir + "/genes/eggnog/result.emapper.annotations",
        "translate": " --translate --itype CDS",
        "params": {
            "score": config["genes"]["function"]["swissprot"]["filter"]["min"]["bs"][0],
            "evalue": config["genes"]["function"]["swissprot"]["filter"]["min"]["evalue"][0]
        },
        "cite": [gui["annotation"]["genes"]["content"]["functional"]["tools"]["eggnog"]["publication"]],
        "include": rules_dir + "annotation/functional/eggnog.smk"
    }
}

# Since v.2.0.8 introduced (backwards-compatibility)
try:
    functional_annotations.update({
        "idtaxa": {
            "name": "genes_function_idtaxa",
            "output": target_dir + "/genes/idtaxa/result.tsv.gz",
            "output_aa": target_dir + "/genes/idtaxa/genes.faa",
            "log": log_dir + "/idtaxa.txt",
            "translation_log": log_dir + "/idtaxa_translation.txt",
            "database": "/opt/mosga/tools/idtaxa/KEGG_AllEukaryotes_r95.RData",
            "length": int(config["genes"]["function"]["idtaxa"]["filter"]["length"][0]),
            "params": {
                "score": float(config["genes"]["function"]["idtaxa"]["filter"]["threshold"][0])
            },
            "cite": [gui["annotation"]["genes"]["content"]["functional"]["tools"]["idtaxa"]["publication"]],
            "include": rules_dir + "annotation/functional/idtaxa.smk"
        }
    })
except KeyError as e:
    # No idtaxa configuration found, enable backwards-compatibility.
    pass

collector_output = target_dir + "/genes/collect.txt"
func_anno_workflow: list = []
for annotation_tool in gene_function_tools:
    anno_key = find_fun_annotation(annotation_tool, functional_annotations)
    add_annotation_tool(functional_annotations, anno_key, functions, gene_function_output, gene_tools)

# Overwrite workflow to force functional annotation
if "snap" in gene_tools:
    func_anno_workflow = [snap_gff, []]
if "glimmerhmm" in gene_tools:
    func_anno_workflow = [glimmerhmm_gff, []]
if "augustus" in gene_tools:
    func_anno_workflow = [augustus_output, [augustus_coding_output]]
if "braker" in gene_tools:
    func_anno_workflow = [braker_output, []]
if "genemark" in gene_tools:
    func_anno_workflow = [genemark_output, [genemark_output_coding]]

for anno in gene_tools:
    gene_tools.update({anno: collector_output})
    gene_collector = genes_collector(anno, func_anno_workflow[0], collector_output, functions, func_anno_workflow[1])
