rule braker_evidence_hints:
    input:
        genome = braker_genome,
        hints = hints_file if evidence_format != "hints" else []
    output:
        braker_output,
        braker_output_aa
    params:
        softmask = "--softmasking" if softmasking else "",
        hints = hints_file
    log:
        braker_log
    threads:
        cores
    run:
        shell("cp {license_file} ~/.gm_key 2> /dev/null")
        shell("/opt/mosga/tools/BRAKER/scripts/braker.pl --CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta --DIAMOND_PATH=/opt/mosga/tools/diamond/ --AUGUSTUS_SCRIPTS_PATH=/opt/mosga/tools/BRAKER/scripts --AUGUSTUS_BIN_PATH=/opt/mosga/tools/augustus/bin/ --AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config/ --GENEMARK_PATH=/opt/mosga/tools/GeneMark/ --CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta --genome={input.genome} --hints={params.hints} --workingdir={braker_directory} {params.softmask} --core {threads} >{log} 2>&1")
        shell("rm -f ~/.gm_key 2> /dev/null")
