rule braker_evidence_protein_orthodb:
    input:
        genome = braker_genome,
        orthodb = orthodb_file
    output:
        braker_output,
        braker_output_aa
    params:
        softmask="--softmasking" if softmasking else ""
    log:
        braker_log
    threads:
        cores
    run:
        shell("cp {license_file} ~/.gm_key 2> /dev/null")
        shell("/opt/mosga/tools/BRAKER/scripts/braker.pl --CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta/ --DIAMOND_PATH=/opt/mosga/tools/diamond/ --PROTHINT_PATH=/opt/mosga/tools/ProtHint/bin/ --AUGUSTUS_BIN_PATH=/opt/mosga/tools/augustus/bin/ --AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config/ --AUGUSTUS_SCRIPTS_PATH=/opt/mosga/tools/BRAKER/scripts --GENEMARK_PATH=/opt/mosga/tools/GeneMark/ --genome={input.genome} --prot_seq={input.orthodb} --workingdir={braker_directory} {params.softmask} --core {threads} >{log} 2>&1")
        shell("rm -f ~/.gm_key 2> /dev/null")
