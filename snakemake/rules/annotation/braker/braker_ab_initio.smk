rule braker_ab_initio:
    input:
        genome=braker_genome
    output:
        braker_output,
        braker_output_aa
    log:
        braker_log
    params:
        softmask="--softmasking" if softmasking else ""
    threads:
        cores
    run:
        shell("cp {license_file} ~/.gm_key 2> /dev/null")
        shell("/opt/mosga/tools/BRAKER/scripts/braker.pl --AUGUSTUS_ab_initio --DIAMOND_PATH=/opt/mosga/tools/diamond/ --CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta --AUGUSTUS_BIN_PATH=/opt/mosga/tools/augustus/bin/ --AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config/ --GENEMARK_PATH=/opt/mosga/tools/GeneMark/ --genome={input.genome} --esmode --workingdir={braker_directory} {params.softmask} --core {threads} >{log} 2>&1")
        shell("rm -f ~/.gm_key 2> /dev/null")
