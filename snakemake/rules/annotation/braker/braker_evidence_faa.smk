rule rename_protein_header:
    input:
        faa_evidence_file
    output:
        faa_normalized_file
    threads:
        cores
    shell:
        "awk '/^>/{{print \">protein_\" ++i; next}}{{print}}' < {input} > {output}"

rule braker_evidence_protein:
    input:
        genome = braker_genome,
        faa = faa_normalized_file
    output:
        braker_output,
        braker_output_aa
    log:
        braker_log
    params:
        softmask='--softmasking' if softmasking else ''
    threads:
        cores
    shell:
        "/opt/mosga/tools/BRAKER/scripts/braker.pl --ALIGNMENT_TOOL_PATH=/opt/mosga/tools/gth/bin/ --DIAMOND_PATH=/opt/mosga/tools/diamond/ --CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta --AUGUSTUS_SCRIPTS_PATH=/opt/mosga/tools/augustus/scripts --AUGUSTUS_BIN_PATH=/opt/mosga/tools/augustus/bin/ --AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config/ --genome={input.genome} --prot_seq={input.faa} --workingdir={braker_directory} --prg=gth --trainFromGth {params.softmask} --core {threads} >{log} 2>&1"
