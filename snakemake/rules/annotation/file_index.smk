file_index = target_dir + "/index.txt"

rule file_index:
    input:
        independent_tools,
        sqn_file if len(annotation_types) > 0 else [],
        gff_output if len(annotation_types) > 0 else [],
        modules
    output:
        file_index
    threads:
        cores
    params:
        dir = target_dir + "/"
    shell:
        "cd {params.dir} && find . -type f | cut -c3- | grep -v jbrowse | grep -v '^$' > {output}"
