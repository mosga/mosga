def genes_collector( tool: str, input_file: str, output_file: str, functions: dict = {}, additional_input_file: list = [] ):
    """
    Generate the parameters and dependencies for the genes collection.
    :param: tool: Shortname of the gene prediction tool.
    :param: input_file: Path of the input file.
    :param: output_file: Path of the output file.
    :param: functions: Dictionary with the shortname of functional gene
    predictor as key. The values containing the path for the functional gene
    prediction result and a dictonary with the corresponding parameters.
    :param: additional_input_file: List with additional files which should be
    added as input.
    :return: Tuple with in- and ouput file path, general parameters and
    funtional annotation parameters.
    """
    function_output = {
        "list": [],
        "params": None
    }

    tools = {
        "snap": {
            "format": "GFF",
            "reader": "SNAP"
        },
        "glimmerhmm": {
            "format": "GFF",
            "reader": "GlimmerHMM"
        },
        "augustus": {
            "format": "GFF3",
            "reader": "Augustus"
        },
        "braker": {
            "format": "GFF",
            "reader": "BRAKER"
        },
        "genemark": {
            "format": "GFF",
            "reader":  "GeneMark"
        }
    }

    function_tools = {
        "swissprot": {
            "format": "CSV",
            "reader": "Swissprot_Diamond",
            "score": 0.0,
            "identity": 0.0,
            "evalue": 0.0,
            "coverage": 0.0
        },
        "eggnog": {
            "format": "CSV",
            "reader": "EggNog",
            "score": 0.0,
            "identity": 0.0,
            "evalue": 0.0,
            "coverage": 0.0
        },
        "idtaxa": {
            "format": "CSV",
            "reader": "IDTAXA",
            "score": 0.0,
            "identity": 0.0,
            "evalue": 0.0,
            "coverage": 0.0
        }
    }
    if len(functions):
        par = {}

        for fun in functions.keys():
            function_output["list"].append(functions[fun][0])
            fp = functions[fun][1]

            function_tools[fun]["evalue"]   = fp["evalue"] if "evalue" in fp else function_tools[fun]["evalue"]
            function_tools[fun]["score"]    = fp["score"] if "bitscore" in fp else function_tools[fun]["score"]
            function_tools[fun]["identity"] = fp["ident"] if "ident" in fp else function_tools[fun]["identity"]
            function_tools[fun]["coverage"] = fp["query_cov"] if "query_cov" in fp else function_tools[fun]["coverage"]

        par.update({ "input":    "--gene-function " + ",".join(functions[f][0] for f in functions.keys())})
        par.update({ "format":   "--gene-function-format " + ",".join(function_tools[k]["format"] for k in functions.keys()) })
        par.update({ "reader":   "--reader-gene-function " + ",".join(function_tools[k]["reader"] for k in functions.keys()) })
        par.update({ "score":    "--gene-function-score " + ",".join(str(function_tools[k]["score"]) for k in functions.keys()) })
        par.update({ "identity": "--gene-function-identity " + ",".join(str(function_tools[k]["identity"]) for k in functions.keys()) })
        par.update({ "evalue":   "--gene-function-evalue " + ",".join(str(function_tools[k]["evalue"]) for k in functions.keys()) })
        par.update({ "coverage": "--gene-function-coverage " + ",".join(str(function_tools[k]["coverage"]) for k in functions.keys()) })
        function_output["params"] = " ". join(par[x] for x in par.keys())

    parameters = tools[tool]

    if len(additional_input_file):
        function_output["list"].extend(additional_input_file)

    return (input_file, output_file, parameters, function_output)
