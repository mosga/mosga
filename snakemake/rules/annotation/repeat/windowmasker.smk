rule windowmasker_count:
    input:
        genome_file
    output:
        windowmasker_count_output
    log:
        windowmasker_log
    shell:
        "windowmasker -mk_counts -in {input} -infmt fasta -out {output} -sformat ascii -checkdup true >{log} 2>&1"

rule windowmasker_softmasking:
    input:
        genome = genome_file if genome_file != final_genome_file else final_genome_file,
        counter = windowmasker_count_output
    output:
        windowmasker_output
    log:
        windowmasker_log
    shell:
        "windowmasker -in {input.genome} -infmt fasta -ustat {input.counter} -out {output} -outfmt fasta >>{log} 2>&1"

rule windowmasker_hardmasking:
    input:
        windowmasker_output
    output:
        windowmasker_hard_output
    shell:
        "perl -pe '/^[^>]/ and $_=~ s/[a-z]/N/g' {input} > {output}"

rule MOSGA_import_windowmasker:
    input:
        masked = windowmasker_masking,
        igf = igf
    output:
        windowmasker_collector
    params:
        genome = config["dir"],
        filter = windowmasker_collector_filter,
        format_reader = windowmasker_format,
        up = uploads_dir_absolute
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} repeat -v -dir {params.up} --repeat {input.masked} --repeat-format {params.format_reader} --repeat-min-length {params.filter} --reader-repeat WindowMasker > {output}"
