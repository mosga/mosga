rule red:
    input:
        genome_file
    output:
        genome = red_genome_file,
        repeats = red_repeats,
        masked = red_masked,
        tmp = red_repeats_tmp
    log:
        red_log
    params:
        gdir = red_genome_dir,
        mdir = red_masked_dir,
        rdir = red_repeats_dir,
        r = red_repeats_tmp
    threads:
        cores_s
    shell:
        """
        ln -s -f {input} {output.genome}
        /opt/mosga/tools/Red/Red -gnm {params.gdir} -msk {params.mdir} -rpt {params.rdir} -frm 2 >{log} 2>&1
        sed -i 's/>//' {params.r}
        awk '{{$2=$2+1; $3=$3+1; print $1"\tRed\trepeat\t"$2"\t"$3}}' {params.r} | bgzip -c > {output.repeats}
        """

rule MOSGA_import_red:
    input:
        rep = red_repeats_tmp,
        igf = igf
    output:
        red_collector
    params:
        genome = config["dir"],
        filter = red_collector_filter,
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} repeat -v -dir {params.up} --repeat {input.rep} --repeat-min-length {params.filter} --repeat-format CSV --reader-repeat Red > {output}"
