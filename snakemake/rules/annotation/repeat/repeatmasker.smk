try:
    repeatmasker_species = config["repeats"]["repeatmasker"]["radio"]["species"][0]
    repeatmasker_manual_species = config["repeats"]["repeatmasker"]["filter"]["speciesmanual"][0]
    repeatmasker_species = repeatmasker_manual_species if len(repeatmasker_manual_species) else repeatmasker_species
except KeyError:
    repeatmasker_species = "default"

if repeatmasker_species == "root":
    repeatmasker_species = "default"

rule repeatmasker:
    input:
        genome_file
    output:
        out = repeatmasker_output,
        masked = repeatmasker_masked
    log:
        repeatmasker_log_wrapper
    params:
        dir = repeatmasker_dir,
        tmp = repeatmasker_tmp,
        new = repeatmasker_new,
        species = repeatmasker_species,
        tax = repeatmasker_taxonomy,
        log = repeatmasker_log
    threads:
        cores
    shell:
        """
        bash /opt/mosga/scripts/repeatmasker.sh {input} {threads} {params.dir} "{params.species}" {params.log} {params.tax} >{log} 2>&1
        mv {params.tmp}.out {output.out}
        mv {params.tmp}.tbl {params.new}.tbl
        mv {params.tmp}.masked {output.masked}
        """

rule MOSGA_import_repeatmasker:
    input:
        rep = repeatmasker_output,
        igf = igf
    output:
        repeatmasker_collector
    params:
        genome = config["dir"],
        filter = repeatmasker_collector_filter,
        up = uploads_dir_absolute
    shell:
        "python3 /opt/mosga/accumulator/mosga.py import {params.genome} repeat -v -dir {params.up} --repeat {input.rep} --repeat-format RepeatMaskerOutput --repeat-min-length {params.filter} --reader-repeat RepeatMasker > {output}"
