rule glimmerhmm_batch:
    input: genome_file
    output: glimmerhmm_gff
    threads: 1
    params:
        split = glimmerhmm_split_dir,
        specie = glimmerhmm_specie
    shell:
        "/opt/mosga/scripts/glimmerhmm_bath.sh {params.split} {input} /opt/mosga/tools/GlimmerHMM/bin/glimmerhmm_linux_x86_64 /opt/mosga/tools/GlimmerHMM/trained_dir/{params.specie}/ {output}"
