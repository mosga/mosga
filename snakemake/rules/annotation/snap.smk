rule snap:
    input:
        genome_file
    output:
        gff = snap_gff,
        faa = snap_faa,
        fna = snap_fna
    log:
        snap_log
    params:
        model=snap_model
    threads:
        cores_4
    shell:
        "/opt/mosga/tools/SNAP/snap /opt/mosga/tools/SNAP/HMM/{params.model} {input} -gff -quiet -lcmask -tx {output.fna} -aa {output.faa} > {output.gff} 2>{log}"
