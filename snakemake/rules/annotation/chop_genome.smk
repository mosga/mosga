rule chop_genome:
    input:
        genome_file
    output:
        chopped_genome
    params:
        cs = config["genome_chopping_size"]
    threads:
        cores_s
    shell:
        "/opt/mosga/tools/seq-scripts/bin/seq-chop -l {params.cs} -p {input} 2>/dev/null | gzip -c > {output}"
