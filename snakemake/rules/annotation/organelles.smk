organelles = False
organelles_extended = False

try:
    if config["organelle"][0] == "scan":
        organelles = True
    if len(config["organelle"]) > 1 and config["organelle"][1] == "xoi":
            organelles_extended = True
except KeyError:
    pass

# Perform rules
if organelles == True:
    mitos_chopped_output = target_dir + "/genes/mitos.chopped.txt.gz"
    mitos_output = target_dir + "/genes/mitos.txt.gz"
    mitos_log = log_dir + "/mitos.txt"
    plastid_chopped_output = target_dir + "/genes/plastids.chopped.txt.gz"
    plastid_output = target_dir + "/genes/plastids.txt.gz"
    plastid_log = log_dir + "/plastids.txt"

    organelles_list = [mitos_output,plastid_output]

    rule mitos:
        input:
            chopped_genome
        output:
            mitos_chopped_output
        log:
            mitos_log
        threads:
            cores_3
        shell:
            "/opt/mosga/tools/diamond/diamond blastx -d /opt/mosga/tools/diamond/mito -q {input} -o {output} -b4 --compress 1 --threads {threads} --evalue 0.001 -f 6 qseqid sseqid qstart qend sstart send length nident pident evalue bitscore score mismatch gapopen gaps qcovhsp stitle >{log} 2>&1"

    rule plastids:
        input:
            chopped_genome
        output:
            plastid_chopped_output
        log:
            plastid_log
        threads:
            cores_3
        shell:
            "/opt/mosga/tools/diamond/diamond blastx -d /opt/mosga/tools/diamond/plastid -q {input} -o {output} -b4 --compress 1 --threads {threads} --evalue 0.001 -f 6 qseqid sseqid qstart qend sstart send length nident pident evalue bitscore score mismatch gapopen gaps qcovhsp stitle >{log} 2>&1"

    rule plastids_finalize:
        input:
            plastid_chopped_output
        output:
            plastid_output
        threads:
            cores_4
        shell:
            "python3 /opt/mosga/scripts/rename_chopped_results.py {input} {output}"

    rule mitos_finalize:
        input:
            mitos_chopped_output
        output:
            mitos_output
        threads:
            cores_4
        shell:
            "python3 /opt/mosga/scripts/rename_chopped_results.py {input} {output}"
