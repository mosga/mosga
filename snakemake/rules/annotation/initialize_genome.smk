# initialize genome file (igf)
igf = target_dir + "/control/igf.txt"

rule MOSGA_initialize_genome:
    input:
        normalized = final_genome_file,
        original = gfile
    output:
        igf
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        original = "-orig " + gfile if not annotation_import else "",
        re = "-re" if annotation_import else ""
    threads:
        cores
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py initialize_genome {params.genome} {input.normalized} {params.original} -dir {params.up} {params.re}
        touch {output}
        """
