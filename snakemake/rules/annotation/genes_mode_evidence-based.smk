# RNA / Hints based
if genes_mode == "genes_mode_evidence-based":
    input_raw_rna = target_dir + "/" + config["files"]["genes"]["mode"]["evidence-based"]["rnaseq"][0]["path"]
    evidence_format = config["genes"]["mode"]["evidence-based"]["radio"]["format"][0]
    aligner = config["genes"]["mode"]["evidence-based"]["radio"]["aligner"][0]

    sam_file = target_dir + "/rna-seq/rna.sam"
    bam_sorted_file = target_dir + "/rna-seq/rna.sort.bam"
    hints_file = target_dir + "/rna-seq/rna.hints"
    bam2hints_log = log_dir + "/bam2hints.txt"
    samtools_log = log_dir + "/samtools.txt"

    if evidence_format == "raw":
        # Cite
        cite.add(publications["rna-pred"])
        cite.add(publications["samtools"])
        cite.add(publications["bowtie"])

        # Run aligner software
        if aligner == "tophat":
            include: rules_dir + "annotation/genes_mode_evidence-based_tophat.smk"
            cite.add(publications["tophat"])
            bam_file = tophat2_align_output
        elif aligner == "hisat":
            include: rules_dir + "annotation/genes_mode_evidence-based_hisat.smk"
            cite.add(publications["hisat"])
            bam_file = target_dir + "/rna-seq/rna.bam"
            include: rules_dir + "annotation/rnaseq/samtools_view_bam.smk"

        samtools_index_output = bam_sorted_file + ".bai"

        include: rules_dir + "annotation/rnaseq/samtools_sort_bam.smk"
        include: rules_dir + "annotation/rnaseq/samtools_index_bam.smk"
        include: rules_dir + "annotation/rnaseq/bam2hints.smk"

    elif evidence_format == "hints":
        hints_file = input_raw_rna

# Protein based
elif genes_mode == "genes_mode_evidence-based-prot":
    evidence_format = config["genes"]["mode"]["evidence-based-prot"]["radio"]["format"][0]
    faa_normalized_file = target_dir + "/genes/protein/normalized.faa"
    cite.add(publications["gth"])

    if evidence_format == "fna":
        # translate to protein
        fna2faa_fna = target_dir + "/" + config["files"]["genes"]["mode"]["evidence-based-prot"]["proteins"][0]["path"]
        faa_evidence_file = fna2faa_faa = target_dir + "/genes/protein/transeq.faa"
        fna2faa_clean = "Y"
        fna2faa_trim = "Y"
        include: rules_dir + "annotation/transeq.smk"

    elif evidence_format == "faa":
        faa_evidence_file = target_dir + "/" + config["files"]["genes"]["mode"]["evidence-based-prot"]["proteins"][0]["path"]

# Ortholy OrthoDB based
elif genes_mode == "genes_mode_orthodbbased":
    evidence_format = "orthodb"
    orthodb_db = config["genes"]["mode"]["orthodbbased"]["radio"]["database"][0].lower()
    orthodb_file = "/opt/mosga/data/orthodb_" + orthodb_db.lower() + ".faa"
    orthodb_dir = "/opt/mosga/data/"
    include: rules_dir + "annotation/orthodb.smk"
