genome_file = target_dir + "/sequence"
annotation_import_summary = target_dir + "/annotation/summary.html"
annotation_import_gff = target_dir + "/annotation/import.gff"
annotation_import_log = log_dir + "/import_annotation.txt"
annotation_import_writer = log_dir + "/export_gff_after_import.txt"
temporary_genome_file = genome_file + ".tmp"
temporary_genome_length_file = genome_file + ".tmp.sl.gz"
igf = target_dir + "/control/igf.txt"

rule MOSGA_import_annotation:
    input:
        gfile
    output:
        genome = temporary_genome_file,
        genome_length = temporary_genome_length_file
    log:
        annotation_import_log
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        seq = genome_file,
        tmp_genome = temporary_genome_file
    threads:
        cores
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py import {params.genome} raw --file {input} -dir {params.up} -v --initialize-from-file -p >{log} 2>&1
        mv {params.seq} {output.genome}
        awk '/^>/{{if (l!="") print l; print; l=0; next}}{{l+=length($0)}}END{{print l}}' {output.genome} | gzip -c > {output.genome_length}
        """

rule MOSGA_export_imported_annotation:
    input:
        temporary_genome_file
    output:
        g = genome_file,
        log = annotation_import_writer,
        feature = annotation_import_gff
    params:
        genome = config["dir"],
        up = uploads_dir_absolute,
        g = genome_file,
        summary = annotation_import_summary,
        seq = genome_file,
        seq2 = genome_file + ".tmp"
    threads:
        cores
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py export {params.genome} -v -dir {params.up} -gf {input} -o {output.feature} -w GFF -soi -sum {params.summary} >{output.log} 2>&1
        mv {params.seq2} {params.seq}
        """
