rule ignore_regions:
    input:
        genome = genome_file,
        regions = ignore_regions
    output:
        genome_ir_file
    shell:
        "bedtools maskfasta -fi {input.genome} -bed {input.regions} -fo {output}"
