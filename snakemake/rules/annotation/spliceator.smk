spliceator_output = target_dir + "/genes/spliceator/output.csv"
protein_gene_sequences = target_dir + "/genes/protein_gene_sequences.fasta"

protein_gene_sequence_log = log_dir + "/mosga_extract_genes.log"
spliceator_log = log_dir + "/spliceator.log"

try:
    spliceator = config["spliceator"][0]
    spliceator_md = int(config["spliceator-md"][0])
    spliceator_ma = int(config["spliceator-ma"][0])
except:
    spliceator = None
    spliceator_md = 200
    spliceator_ma = 140

if spliceator != None and genes != None and "gene_collector" in locals():
    modules.append(spliceator_output)
    cite.add(publications["spliceator"])

rule extract_gene_sequences:
    input:
        gf = genome_file,
        gc = gene_collector[1] if "gene_collector" in locals() else []
    output:
        protein_gene_sequences
    log:
        protein_gene_sequence_log
    params:
        uid = config["dir"],
        up = uploads_dir_absolute
    threads:
        cores
    shell:
        "python3 /opt/mosga/accumulator/mosga.py get_proteins_nuc_seq {params.uid} {input.gf} -dir {params.up} --no_transcripts --position > {output} 2>{log}"

rule spliceator:
    input:
        protein_gene_sequences
    output:
        spliceator_output
    log:
        spliceator_log
    params:
        md = spliceator_md,
        ma = spliceator_ma
    threads:
        cores
    conda:
        rules_dir + "../envs/spliceactor.yml"
    shell:
        "cd /opt/mosga/tools/spliceator && ./Spliceator -f {input} -o {output} -md {params.md} -ma {params.ma} >{log} 2>&1"
