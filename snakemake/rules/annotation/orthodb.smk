rule orthodb:
    input:
        genome_file
    output:
        orthodb_file
    params:
        db = orthodb_db,
        dir = orthodb_dir
    run:
        shell("cp {license_file} ~/.gm_key 2> /dev/null")
        shell("bash /opt/mosga/scripts/orthodb_downloader.sh {params.db} {params.dir}")
        shell("rm -f ~/.gm_key 2> /dev/null")
