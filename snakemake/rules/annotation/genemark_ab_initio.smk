rule genemark_ab_initio:
    input:
        genemark_genome
    output:
        genemark_output
    log:
        genemark_log
    params:
        up = uploads_dir_absolute,
        softmask = "--soft_mask auto" if softmasking else "",
        fungus = "--fungus" if genemark_fungus else "",
        genome = config["dir"],
    threads:
        cores
    shell:
        """
        cp {license_file} ~/.gm_key 2> /dev/null
        cd {params.up}/{params.genome}/genes/genemark/
        /opt/mosga/tools/GeneMark/gmes_petap.pl --ES {params.softmask} {params.fungus} --sequence {input} --cores {threads} >{log} 2>&1
        cd ../../../../../snakemake/
        rm -f ~/.gm_key 2> /dev/null
        """

rule genemark_codingseq:
    input:
        genome = genemark_genome,
        gtf = genemark_output
    output:
        genemark_output_coding,
        genemark_output_aa
    params:
        dir = genemark_output_cds_names
    threads:
        cores_4
    shell:
        "/opt/mosga/tools/augustus/scripts/getAnnoFastaFromJoingenes.py -g {input.genome} --gtf {input.gtf} -s True -o {params.dir}"
