rule phylogenetic_tree:
    input:
        phytree_input
    output:
        target_dir + "/phylogenetic/phy_tree.svg"
    log:
        phytree_log
    params:
        newick = target_dir + "/phylogenetic/phy_tree.nwk",
        scale = phytree_scale,
        highlight = phytree_highlight,
        labels = labels,
        outgroup = phytree_outgroup
    threads:
        cores_s
    shell:
        """
        cp {input} {params.newick}
        Rscript /opt/mosga/scripts/PhyTree.R {input} {output} {params.scale} {params.highlight} \"{params.labels}\" {params.outgroup} >{log} 2>&1
        """
