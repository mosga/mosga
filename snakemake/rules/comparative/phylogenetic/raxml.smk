rule raxml_maximum_likelihood:
    input:
        out_trimming
    output:
        raxml_likelyhood_path + "RAxML_bestTree." + raxml_likelyhood_name
    log:
        raxml_likelyhood_log
    params:
        m = raxml_m,
        name = raxml_likelyhood_name,
        out_dir = raxml_likelyhood_path,
        seed = raxml_seed_parsimony,
        iter = raxml_iter_parsimony,
        outgroup = raxml_outgroup
    threads:
        cores_2
    shell:
        "/opt/mosga/tools/RAxML/raxmlHPC-PTHREADS-AVX -T {threads} -m {params.m} -s {input} {params.outgroup} -n {params.name} -w {params.out_dir} -# {params.iter} -p {params.seed} >{log} 2>&1"

rule raxml_bootstrapping:
    input:
        out_trimming
    output:
        raxml_bootstrapping_path + "RAxML_bootstrap." + raxml_bootstrapping_name
    log:
        raxml_bootstrapping_log
    params:
        m = raxml_m,
        name = raxml_bootstrapping_name,
        out_dir = raxml_bootstrapping_path,
        seed_p = raxml_seed_parsimony,
        seed_b = raxml_seed_bootstrapping,
        iter = raxml_iter_bootstrapping,
        outgroup = raxml_outgroup
    threads:
        cores_2
    shell:
        "/opt/mosga/tools/RAxML/raxmlHPC-PTHREADS-AVX -f d -T {threads} -m {params.m} -s {input} {params.outgroup} -n {params.name} -w {params.out_dir} -# {params.iter} -p {params.seed_p} -b {params.seed_b} >{log} 2>&1"

rule raxml:
    input:
        maximum_likelihood = raxml_likelyhood_path + "RAxML_bestTree." + raxml_likelyhood_name,
        bootstrapping = raxml_bootstrapping_path + "RAxML_bootstrap." + raxml_bootstrapping_name
    output:
        raxml_out
    log:
        raxml_log
    params:
        name = raxml_name,
        out_dir = raxml_path,
        outgroup = raxml_outgroup
    threads:
        cores
    shell:
        "/opt/mosga/tools/RAxML/raxmlHPC-PTHREADS-AVX -f b -T {threads} -m GTRCAT -z {input.bootstrapping} -t {input.maximum_likelihood} -n {params.name} {params.outgroup} -w {params.out_dir} >{log} 2>&1"
