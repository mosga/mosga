rule fastme:
    input:
        out_trimming
    output:
        fastme_out
    log:
        fastme_log
    params:
        output = fastme_path + fastme_bootstrapping_name,
        replicates = fastme_replicates,
        seed = fastme_seed
    threads:
        cores_s
    shell:
        "/usr/local/bin/fastme -i {input} -o {output} -p -b {params.replicates} -z {params.seed} -B {params.output} >{log} 2>&1"
