# FastANI
if "ani_fastani" in ani:
    # cite
    try:
        cite.add(gui["comparative"]["ani"]["content"]["fastani"]["tools"]["fastani"]["publication"])
    except KeyError:
        pass

    # declare parameters
    fastani_kmer = config["ani"]["fastani"]["filter"]["kmer"][0]
    fastani_fragmentlen = config["ani"]["fastani"]["filter"]["fragmentlen"][0]
    fastani_minfraction = config["ani"]["fastani"]["filter"]["minfraction"][0]
    fastani_text = config["ani"]["fastani"]["radio"]["text"][0]
    fastani_text_overlay = "--text" if fastani_text == "yes" else ""

    fastani_log = log_dir + "/fastani.txt"

    # declare & update locations
    ani_path = target_dir + "/ani/"
    comparative_outputs.append(ani_path + "heatmap.html")
    include: rules_dir + "comparative/ani/fastani.smk"
