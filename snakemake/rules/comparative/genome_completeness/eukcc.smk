try:
    license_file =  target_dir + "/" + config["files"]["completeness_eukcc_license"]["path"]
except KeyError:
    license_file = "gm_key"
    print("Missing GeneMark License!")

rule eukcc:
    input:
        _get_input_normal
    output:
        eukcc_path + "{file}" + eukcc_tsv
    log:
        log_dir + "/eukcc_" + "{file}" + ".txt"
    params:
        out_dir = eukcc_path + "{file}",
        db_dir = eukcc_data
    threads:
        cores
    shell:
        """
        cp {license_file} ~/.gm_key 2> /dev/null
        PATH=$PATH:/usr/local/bin eukcc --db {params.db_dir} --ncores {threads} --ncorespplacer 1 --outdir {params.out_dir} --force {input} >{log} 2>&1
        cd {params.out_dir}
        tar cpf - eukcc.tsv workfiles/ | pbzip2 -c > eukcc.tar.bz2
        rm -rf  workfiles/gmes/ workfiles/hmmer/ workfiles/pplacer/p* workfiles/pplacer/horizontalAlignment.fasta 2>/dev/null
        rm -f ~/.gm_key 2> /dev/null
        """

rule eukcc_visual:
    input:
        expand(eukcc_path + "{file}" + eukcc_tsv, file=g_files)
    output:
        eukcc_path + "scatter_plot.html"
    params:
        name = expand("\"{name}\"", name=g_names),
        in_dir = expand(eukcc_path + "{file}/", file=g_files),
        out_dir = eukcc_path,
    threads:
        cores_s
    shell:
        "python3 /opt/mosga/scripts/genome_completeness.py visualize eukcc -i {params.in_dir} -o {params.out_dir} -n {params.name}"

rule eukcc_comparator:
    input:
        expand(eukcc_path + "{file}" + eukcc_tsv, file=g_files),
        eukcc_path + "scatter_plot.html"
    output:
        eukcc_path + "genes.fa"
    params:
        name = expand("\"{name}\"", name=g_files),
        out_dir = eukcc_path,
        in_dir = expand(eukcc_path + "{file}/",file=g_files)
    threads:
        cores_s
    shell:
        "python3 /opt/mosga/scripts/eukcc_comparator.py -i {params.in_dir} -o {params.out_dir} -n {params.name}"
