rule busco:
    input:
        _get_input_normal
    output:
        busco_path + "{file}" + busco_tsv
    log:
        log_dir + "/busco_" + "{file}" + ".txt"
    params:
        name="{file}",
        out_dir = busco_path,
        lineage = busco_lineage,
        config = "/opt/mosga/scripts/busco_config.ini"
    threads:
        cores
    shell:
        """
        cd {params.out_dir}
        python3 /opt/mosga/tools/BUSCO/bin/busco -i {input} -o {params.name} --out_path {params.out_dir} -l {params.lineage} --cpu {threads} --force --config {params.config} >{log} 2>&1
        cd {params.out_dir}/{params.name}
        tar cpf - logs/ run_{params.lineage}/ short_summary.* | pbzip2 -c > busco_{params.name}.tar.bz2  2>/dev/null
        rm -rf logs/ run_eukaryota_odb10/augustus_output/ run_eukaryota_odb10/blast_output/ run_eukaryota_odb10/hmmer_output/ run_eukaryota_odb10/busco_sequences/fragmented_busco_sequences/ 2>/dev/null
        """

rule busco_visual:
    input:
        expand(busco_path + "{file}" + busco_tsv, file=g_files)
    output:
        busco_path + "bar_chart.html",
        busco_path + "strip_chart.html"
    params:
        name = expand("\"{name}\"", name=g_names),
        lin = busco_lineage,
        in_dir = expand(busco_path + "{file}" + busco_lin, file=g_files),
        out_dir = busco_path
    threads:
        cores_s
    shell:
        "python3 /opt/mosga/scripts/genome_completeness.py visualize busco -i {params.in_dir} -o {params.out_dir} -n {params.name} --title {params.lin}"

rule busco_comparator:
    input:
        expand(busco_path + "{file}" + busco_tsv, file=g_files),
        busco_path + "bar_chart.html",
        busco_path + "strip_chart.html"
    output:
        busco_path + "genes.fa"
    log:
        busco_comparator_log
    params:
        name = expand("\"{name}\"", name=g_files),
        out_dir = busco_path,
        in_lin = expand(busco_path + "{file}" + busco_lin, file=g_files),
        min_score = busco_min_score,
        min_length = busco_min_length,
        min_duplicates = busco_min_duplicates
    threads:
        cores_s
    shell:
        "python3 /opt/mosga/scripts/genome_completeness.py compare busco -i {params.in_lin} -o {params.out_dir} -n {params.name} -ms {params.min_score} -ml {params.min_length} -md {params.min_duplicates} >{log} 2>&1"
