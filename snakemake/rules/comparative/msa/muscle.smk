rule muscle:
    input:
        target_dir + "/phylogenetic/genes.fa"
    output:
        out_msa
    threads:
        cores_s
    shell:
        "muscle -seqtype protein -in {input} -phyiout {output} -quiet"
