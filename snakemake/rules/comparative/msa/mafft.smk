rule mafft:
    input:
        target_dir + "/phylogenetic/genes.fa"
    output:
        out_msa
    params:
        gap_op = mafft_gap_opening_penalty,
        gap_ep = mafft_gap_extension_penalty
    threads:
        cores
    shell:
        "mafft --thread {threads} --amino --phylipout --op {params.gap_op} --ep {params.gap_ep} --quiet {input} > {output}"
