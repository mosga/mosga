rule clustalw:
    input:
        target_dir + "/phylogenetic/genes.fa"
    output:
        out_msa
    params:
        gap_op = clustalw_gap_opening_penalty,
        gap_ep = clustalw_gap_extension_penalty,
        matrix = clustalw_matrix
    threads:
        cores_s
    shell:
        "clustalw -infile={input} -outfile={output} -output=PHYLIP -gapopen={params.gap_op} -gapext={params.gap_ep} -matrix={params.matrix} -type=PROTEIN -quiet"