rule prepare_msa:
    input:
        out_completeness
    output:
        target_dir + "/phylogenetic/genes.fa"
    params:
        tools = msa_input,
        in_busco = target_dir + "/busco/genes.fa",
        in_eukcc = target_dir + "/eukcc/genes.fa"
    threads:
        cores_s
    run:
        if len(input) == 1 or params.tools == "busco" or params.tools == "eukcc":
            if len(input) == 1:         # there is only one output that can be taken
                shell("cp {input} {output}")
            elif params.tools == "busco":       # EukCC output is ignored
                shell("cp {params.in_busco} {output}")
            else:                               # BUSCO output is ignored
                shell("cp {params.in_eukcc} {output}")
        else:       # BUSCO & EukCC output is taken for MSA
            data = dict()
            for path in input:
                with open(path, "r") as f:
                    lines = f.read().splitlines()
                    name = ""
                    for line in lines:
                        if line[:1] == ">":
                            name = line[1:]
                        else:
                            if name in data:
                                data[name] += line
                            else:
                                data[name] = line
            with open(str(output), "w") as f:
                for name, seq in data.items():
                    f.write('>' + name + "\n")
                    for i in range(0,len(seq), 60):
                        f.write(seq[i:i + 60] + "\n")
