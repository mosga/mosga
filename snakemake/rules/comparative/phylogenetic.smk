# prepare labels for charts
labels = ""
for i in range(0,len(g_files)):
    labels += f"{g_files[i]}={g_names[i]}" if i == len(g_files) - 1 else f"{g_files[i]}={g_names[i]},"

phytree_log = log_dir + "/phylogenetic_tree.txt"

# RAxML
if phylogenetics == "phylogenetics_raxml":
    if len(g_files) >= 4:  # RAxML needs at least 4 taxa
        # cite
        try:
            cite.add(gui["comparative"]["pcas"]["content"]["pca"]["tools"]["raxml"]["publication"])
            cite.add(publications['ggtree'])
        except KeyError:
            pass

        # declare parameters
        raxml_m = "PROT" + config["phylogenetics"]["raxml"]["radio"]["model"][0] + config["phylogenetics"]["raxml"]["radio"]["matrix"][0]
        if config["phylogenetics"]["raxml"]["radio"]["frequency"][0] != "equal":
            raxml_m += config["phylogenetics"]["raxml"]["radio"]["frequency"][0]
        raxml_iter_parsimony = config["phylogenetics"]["raxml"]["filter"]["iterations"]["parsimony"][0]
        raxml_iter_bootstrapping = config["phylogenetics"]["raxml"]["filter"]["iterations"]["bootstrapping"][0]
        raxml_seed_parsimony = config["phylogenetics"]["raxml"]["filter"]["seed"]["parsimony"][0]
        raxml_seed_bootstrapping = config["phylogenetics"]["raxml"]["filter"]["seed"]["bootstrapping"][0]
        phytree_scale = config["phylogenetics"]["raxml"]["radio"]["scaling"][0]
        phytree_highlight = config["phylogenetics"]["raxml"]["radio"]["highlighting"][0]
        phytree_highlight = "none" if phytree_highlight == "off" else phytree_highlight
        raxml_outgroup = ""
        phytree_outgroup = ""
        if outgroup_exists:
            raxml_outgroup = "-o " + ",".join(outgroups)

        # declare & update locations
        raxml_name = "tree"
        raxml_likelyhood_name = raxml_name + "-ml"
        raxml_bootstrapping_name = raxml_name + "-bo"
        raxml_path = target_dir + "/phylogenetic/raxml/"
        raxml_likelyhood_path = raxml_path + "maximum_likelihood/"
        raxml_likelyhood_log = log_dir + "/raxml_likelyhood.txt"
        raxml_bootstrapping_path = raxml_path + "bootstrapping/"
        raxml_bootstrapping_log = log_dir + "/raxml_bootstrapping.txt"
        raxml_log = log_dir + "/raxml.txt"
        phytree_input = raxml_out = raxml_path + "RAxML_bipartitions." + raxml_name
        comparative_outputs.append(target_dir + "/phylogenetic/phy_tree.svg")
        include: rules_dir + "comparative/phylogenetic/raxml.smk"
        include: rules_dir + "comparative/phylogenetic/phytree.smk"

# FastME
if phylogenetics == "phylogenetics_fastme":
    if len(g_files) >= 4:  # FastME needs at least 4 taxa
        # cite
        try:
            cite.add(gui["comparative"]["pcas"]["content"]["pca"]["tools"]["fastme"]["publication"])
            cite.add(publications["ggtree"])
        except KeyError:
            pass

        # declare parameters
        fastme_replicates = config["phylogenetics"]["fastme"]["filter"]["replicates"][0]
        fastme_seed = config["phylogenetics"]["fastme"]["filter"]["seed"][0]
        fastme_log = log_dir + "/fastme.txt"
        phytree_scale = config["phylogenetics"]["fastme"]["radio"]["scaling"][0]
        phytree_highlight = config["phylogenetics"]["fastme"]["radio"]["highlighting"][0]
        phytree_highlight = "none" if phytree_highlight == "off" else phytree_highlight
        phytree_outgroup = ""
        if outgroup_exists:
            phytree_outgroup = "\"" + ",".join(outgroups) + "\""

        # declare & update locations
        fastme_name = "tree.nwk"
        fastme_bootstrapping_name = fastme_name + "-bo"
        fastme_path = target_dir + "/phylogenetic/fastme/"
        phytree_input = fastme_out = fastme_path + fastme_name      # TODO: plot bootstrap tree, not only the inferred tree
        comparative_outputs.append(target_dir + "/phylogenetic/phy_tree.svg")
        include: rules_dir + "comparative/phylogenetic/fastme.smk"
        include: rules_dir + "comparative/phylogenetic/phytree.smk"
