rule fastani:
    input:
        _get_input_normal(g_files)
    output:
        ani_path + "ani.out"
    log:
        fastani_log
    params:
        names = expand("\"{name}\"", name=g_names),
        out_dir = ani_path,
        kmer = fastani_kmer,
        flen = fastani_fragmentlen,
        min = fastani_minfraction
    threads:
        cores
    run:
        shell.executable("/bin/bash")
        shell("""
        N={threads}
        i=0
        t=0
        FILES=({input})
        NAMES=({params.names})
        (
        for (( query=0 ; query<${{#FILES[@]}} ; query++ )) ; do
            for (( ref=0 ; ref<${{#FILES[@]}} ; ref++ )) ; do
                if [[ "$query" == "$ref" ]]; then continue; fi
                QUERY_FILE=${{FILES[query]}}
                REF_FILE=${{FILES[ref]}}
                NAME_QUERY=${{NAMES[query]}}
                NAME_REF=${{NAMES[ref]}}
                i=$(($i%$N)); ((i++==0)) && wait
                ((t=t+1))
                bash /opt/mosga/scripts/fastani_parallel.sh $QUERY_FILE $REF_FILE {params.out_dir} {params.kmer} {params.flen} {params.min} {log} "$NAME_QUERY" "$NAME_REF" {output}res$t &
            done
        done
        wait
        )
        cat {output}res*.visual > {output}.visual
        rm {output}res*.visual
        cat {output}res* > {output}
        rm {output}res*
        """)

rule fastani_visual:
    input:
        ani_path + "ani.out"
    output:
        ani_path + "heatmap.html"
    params:
        out_dir = ani_path,
        text = fastani_text_overlay
    threads:
        cores_s
    shell:
        """
        python3 /opt/mosga/scripts/ani_visual.py -i {input} -o {params.out_dir} {params.text}
        """
