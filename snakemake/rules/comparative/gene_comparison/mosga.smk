def _get_input_comparison(wildcards):
    inputs = []
    for w in wildcards:
        if w in gbff_files:
            inputs.append(target_dir + f"/import/{w}")
        else:
            inputs.append(uploads_dir_absolute + f"{g_annotation[w][1]}/" + w + "_n")
    return inputs

rule MOSGA_get_proteins_nuc_seq:
    input:
        _get_input_comparison
    output:
        sequences_dir + "{uid}" + mosga_fna
    params:
        uid = lambda wildcards: g_annotation[wildcards.uid][1] if wildcards.uid not in gbff_files else g_annotation[wildcards.uid][1] + f"/import/DB_{wildcards.uid}"
    threads:
        cores_4
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py get_proteins_nuc_seq {params.uid} {input} -dir /opt/mosga/gui/uploads --prefix_genome --no_transcripts > {output}
        """

rule blastdb:
    input:
        expand(sequences_dir + "{uid}" + mosga_fna, uid=list(g_annotation.keys()))
    output:
        blast_dir + blastdb
    params:
        files = sequences_dir + "*" + mosga_fna,
        genes_file = mosga_path + genes,
        name = blastdb,
        out_dir = blast_dir
    threads:
        cores_4
    shell:
        """
        cat {params.files} > {params.genes_file}
        makeblastdb -dbtype nucl -in {params.genes_file} -input_type fasta -title Genes -out {output} -parse_seqids
        if [ $(find {params.out_dir} -name '{params.name}.*' | wc -l) -gt 1  ]; then touch {output}; fi
        """

rule blastn:
    input:
        blast_dir + blastdb
    output:
        blast_dir + filtered
    params:
        genes_file = mosga_path + genes,
        blastn_file = blast_dir + blastn,
        minpercident = float(mosga_minpercident)
    threads:
        cores_s
    shell:
        """
        blastn -num_threads {threads} -db {input} -query {params.genes_file} -outfmt \"6 qseqid sseqid nident pident evalue bitscore score mismatch gapopen gaps qcovs qcovhsp\" | gzip -c > {params.blastn_file}
        zcat {params.blastn_file} | awk '($4+0) >= {params.minpercident}' | sort --version-sort -k1,1 -k4,4 -k5,5r | sort --version-sort -u -k1,1 -k2,2 | gzip -c > {output}
        """

rule MOSGA_cg_genes_comp:
    input:
        blast_dir + filtered
    output:
        mosga_path + "cg_genome_similarity.svg"
    params:
        genes_file = mosga_path + genes,
        out_dir = mosga_path,
        labels = labels,
        threshold = cg_mosga_threshold
    threads:
        cores_4
    shell:
        "python3 /opt/mosga/accumulator/mosga.py cg_genes_comp \"{params.genes_file}\" \"{input}\" -i --chart \"{params.out_dir}\" --heatmap {params.threshold} -v --labels \"{params.labels}\""
