def _get_input_normal(wildcards):
    inputs = []
    for w in wildcards:
        if w in gbff_files:
            inputs.append(target_dir + f"/import/{w}")
        else:
            inputs.append(target_dir + f"/{w}")
    return inputs

rule MOSGA_import_gbff:
    input:
        target_dir + "/{file}"
    output:
        target_dir + "/import/{file}"
    params:
        in_dir = config['dir'] + "/import/",
        uploads = uploads_dir_absolute,
        log = log_dir + "/import-{file}.txt",
        dir = target_dir + "/import/",
        name = "DB_" + "{file}"
    threads:
        cores
    shell:
        """
        python3 /opt/mosga/accumulator/mosga.py import {params.in_dir} raw --file {input} -dir {params.uploads} -v --initialize-from-file -p > {params.log} && mv {params.dir}sequence {output}
        mkdir {params.dir}{params.name}/ && mv {params.dir}annotation.db {params.dir}{params.name}/annotation.db
        """
