# init output comparator file (for msa)
out_msa = ""

# MAFFT
if msa =="phylogenetics_msa_mafft":
    # cite
    try:
        cite.add(gui["comparative"]["pcas"]["content"]["msa"]["tools"]["mafft"]["publication"])
    except KeyError:
        pass

    # declare parameters
    mafft_gap_opening_penalty = config["phylogenetics"]["msa"]["mafft"]["filter"]["gap"]["opening"]["penalty"][0]
    mafft_gap_extension_penalty = config["phylogenetics"]["msa"]["mafft"]["filter"]["gap"]["extension"]["penalty"][0]
    msa_input =  config["phylogenetics"]["msa"]["mafft"]["radio"]["gc"]["filter"][0]

    # declare & update locations
    mafft_path = target_dir + "/phylogenetic/mafft/"
    out_msa = mafft_path + "genes.ph"
    include: rules_dir + "comparative/msa/prepare_msa.smk"
    include: rules_dir + "comparative/msa/mafft.smk"

# ClustalW
if msa == "phylogenetics_msa_clustalw":
    # cite
    try:
        cite.add(gui["comparative"]["pcas"]["content"]["msa"]["tools"]["clustalw"]["publication"])
    except KeyError:
        pass

    # declare parameters
    clustalw_gap_opening_penalty = config["phylogenetics"]["msa"]["clustalw"]["filter"]["gap"]["opening"]["penalty"][0]
    clustalw_gap_extension_penalty = config["phylogenetics"]["msa"]["clustalw"]["filter"]["gap"]["extension"]["penalty"][0]
    clustalw_matrix = config["phylogenetics"]["msa"]["clustalw"]["radio"]["matrix"][0]
    msa_input =  config["phylogenetics"]["msa"]["clustalw"]["radio"]["gc"]["filter"][0]

    # declare & update locations
    clustalw_path = target_dir + "/phylogenetic/clustalw/"
    out_msa = clustalw_path + "genes.ph"
    include: rules_dir + "comparative/msa/prepare_msa.smk"
    include: rules_dir + "comparative/msa/clustalw.smk"

# MUSCLE
if msa == "phylogenetics_msa_muscle":
    # cite
    try:
        cite.add(gui["comparative"]["pcas"]["content"]["msa"]["tools"]["muscle"]["publication"])
    except KeyError:
        pass

    # declare parameters
    msa_input =  config["phylogenetics"]["msa"]["muscle"]["radio"]["gc"]["filter"][0]

    # declare & update locations
    muscle_path = target_dir + "/phylogenetic/muscle/"
    out_msa = muscle_path + "genes.ph"
    include: rules_dir + "comparative/msa/prepare_msa.smk"
    include: rules_dir + "comparative/msa/muscle.smk"

# load trimming configuration
try:
    trimming = config["phylogenetics"]["trimming"]["0"]
    if trimming == "phylogenetics_trimming_none":
        comparative_outputs.append(out_msa)
    else:
        include: rules_dir + "comparative/trimming.smk"
except KeyError:
    comparative_outputs.append(out_msa) # fallback
    trimming = None
