# init output file (for trimming)
out_trimming = ""

# trimAl
if trimming == "phylogenetics_trimming_trimal":
    # cite
    try:
        cite.add(gui["comparative"]["pcas"]["content"]["trimming"]["tools"]["trimal"]["publication"])
    except KeyError:
        pass

    # declare parameters
    trimal_gapthreshold = ""
    trimal_simthreshold = ""
    trimal_cons = ""
    trimal_automated = config["phylogenetics"]["trimming"]["trimal"]["radio"]["automated"][0]
    trimal_heuristic = config["phylogenetics"]["trimming"]["trimal"]["radio"]["heuristic"][0]
    if trimal_automated != "off" or trimal_heuristic != "off":
        trimal_automated = trimal_automated if trimal_automated != "off" else ""
        trimal_heuristic = trimal_heuristic if trimal_heuristic != "off" else ""
    else:
        trimal_automated = ""
        trimal_heuristic = ""
        trimal_gapthreshold = "-gt " + config["phylogenetics"]["trimming"]["trimal"]["filter"]["gapthreshold"][0]
        trimal_simthreshold = "-st " + config["phylogenetics"]["trimming"]["trimal"]["filter"]["simthreshold"][0]
        trimal_cons = "-cons " +  config["phylogenetics"]["trimming"]["trimal"]["filter"]["cons"][0]

    # declare & update locations
    trimal_path = target_dir + "/phylogenetic/trimal/"
    out_trimming = trimal_path + "genes.clean.ph"
    include: rules_dir + "comparative/trimming/trimal.smk"

# ClipKIT
if trimming == "phylogenetics_trimming_clipkit":
    # cite
    try:
        cite.add(gui["comparative"]["pcas"]["content"]["trimming"]["tools"]["clipkit"]["publication"])
    except KeyError:
        pass

    # declare parameters
    clipkit_gapthreshold = config["phylogenetics"]["trimming"]["clipkit"]["filter"]["gapthreshold"][0]
    clipkit_mode = config["phylogenetics"]["trimming"]["clipkit"]["radio"]["modes"][0]

    # declare & update locations
    clipkit_path = target_dir + "/phylogenetic/clipkit/"
    clipkit_log = log_dir + "/clipkit.log"
    out_trimming = clipkit_path + "genes.clean.ph"
    include: rules_dir + "comparative/trimming/clipkit.smk"

# load pca configuration
try:
    phylogenetics = config["phylogenetics"]["0"]
    if phylogenetics == "phylogenetics_none":
        comparative_outputs.append(out_trimming)
    else:
        include: rules_dir + "comparative/phylogenetic.smk"
except KeyError:
    comparative_outputs.append(out_msa) # fallback
    phylogenetics = None
