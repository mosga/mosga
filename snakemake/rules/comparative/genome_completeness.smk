# init output comparator file (for msa)
out_completeness = []

# BUSCO
if "completeness_busco" in completeness:
    try: # Citation
        cite.add(gui["comparative"]["completeness"]["content"]["orthologs"]["tools"]["busco"]["publication"])
    except KeyError:
        pass

    # declare parameters
    busco_lineage = config["completeness"]["busco"]["radio"]["lineage"][0]
    busco_min_score = config["completeness"]["busco"]["filter"]["min"]["score"][0]
    busco_min_length = config["completeness"]["busco"]["filter"]["min"]["length"][0]
    busco_min_duplicates = -1 if config["completeness"]["busco"]["radio"]["mode"][0] == "single-copy" else config["completeness"]["busco"]["filter"]["min"]["duplicates"][0]

    # declare & update locations
    busco_path = target_dir + "/busco/"
    busco_lin = "/run_" + busco_lineage + "/"
    busco_tsv = busco_lin + "full_table.tsv"
    busco_comparator_log = log_dir + "/busco_comparator.txt"
    out_completeness.append(busco_path + "genes.fa")
    cite.add(publications["ggtree"])
    include: rules_dir + "comparative/genome_completeness/busco.smk"

# EukCC
if "completeness_eukcc" in completeness:
    try: # Citation
        cite.add(gui["comparative"]["completeness"]["content"]["orthologs"]["tools"]["eukcc"]["publication"])
    except KeyError:
        pass

    # declare & update locations
    eukcc_data = "/opt/mosga/tools/EukCC/eukccdb"
    eukcc_path = target_dir + "/eukcc/"
    eukcc_tsv = "/eukcc.tsv"
    out_completeness.append(eukcc_path + "genes.fa")
    cite.add(publications["pplacer"])
    cite.add(publications["ggtree"])
    include: rules_dir + "comparative/genome_completeness/eukcc.smk"

# load msa configuration
try:
    msa = config["phylogenetics"]["msa"]["0"]
    if msa == "phylogenetics_msa_none":
        comparative_outputs.append(*out_completeness)
    else:
        include: rules_dir + "comparative/msa.smk"
except KeyError:
    comparative_outputs.append(*out_completeness)
    msa = None
