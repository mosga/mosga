g_annotation = dict()   # list: [UID, genome_file]
for x in range(0, len(config["files"]["genomes"])):
    conf_file = uploads_dir_absolute + config["files"]["genomes"][x]["origin"] + "/conf.json"
    try:
        with open(conf_file) as f:
            conf = json.load(f)
            if conf["mode"] == "annotation":
                g_annotation[config["files"]["genomes"][x]["path"]] = [config["files"]["genomes"][x]["name"], config["files"]["genomes"][x]["origin"]]
    except EnvironmentError:
        pass
for i, x in enumerate(gbff_files):
    g_annotation[x] = [gbff_names[i], config["files"]["genomes"][i]["origin"]]

# MOSGA: CG Gene Comparison
if "comparison_mosga" in comparison:
    if len(g_annotation) >= 2:  # rule can only be executed if two already annotated runs are available
        # declare parameters
        mosga_minpercident = config["comparison"]["mosga"]["filter"]["minpercident"][0]

        # prepare labels for charts
        labels = ""
        for i, (k, v) in enumerate(g_annotation.items()):
            if k in gbff_files:
                labels += f"{k}={v[0]}" if i == len(g_annotation) - 1 else f"{k}={v[0]},"
            else:
                labels += f"{v[1]}={v[0]}" if i == len(g_annotation) - 1 else f"{v[1]}={v[0]},"

        # declare & update locations
        mosga_path = target_dir + "/gene_comparison/"
        sequences_dir = mosga_path + "sequences/"
        blast_dir = mosga_path + "blast/"
        mosga_fna = "_gene_sequences.fna"
        genes = "genes.fna"
        blastdb = "blastGenes"
        blastn = "blast_result.csv.gz"
        filtered = "genes_filtered.csv.gz"
        comparative_outputs.append(mosga_path + "cg_genome_similarity.svg")

        try:
            threshold_val = int(config["comparison"]["mosga"]["filter"]["minpercident"][0])
            cg_mosga_threshold = "--threshold %d" % threshold_val
        except (KeyErrory, ValueError) as e:
            cg_mosga_threshold = ""

        include: rules_dir + "comparative/gene_comparison/mosga.smk"
