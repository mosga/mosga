rule clipkit:
    input:
        out_msa
    output:
        out_trimming
    log:
        clipkit_log
    params:
        gapthreshold = clipkit_gapthreshold,
        mode = clipkit_mode
    threads:
        cores_s
    shell:
        "clipkit {input} -m {params.mode} -g {params.gapthreshold} -of phylip -o {output} >{log} 2>&1"
