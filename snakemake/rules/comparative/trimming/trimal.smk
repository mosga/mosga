rule trimal:
    input:
        out_msa
    output:
        out_trimming
    params:
        gapthreshold = trimal_gapthreshold,
        simthreshold = trimal_simthreshold,
        cons = trimal_cons,
        automated = trimal_automated,
        heuristic = trimal_heuristic
    threads:
        cores_s
    shell:
        "/opt/mosga/tools/trimAl/source/trimal -in {input} -out {output} {params.gapthreshold} {params.simthreshold} {params.cons} {params.automated} {params.heuristic}"
