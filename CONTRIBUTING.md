## Code style
### Snakemake
```
rule x:
  input:
    i
  output:
    o
  log:
    l
  params:
    p1 = pp1,
    p2 = pp2
  threads:
    t
```

### Python
Use double quotation marks if possible.

### PHP
Use single quatation marks if possible.
