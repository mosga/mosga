FROM ubuntu:22.04
MAINTAINER Roman Martin roman.martin@uni-marburg.de

# Set ENVs
ENV DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin"

# Setup MOSGA docker
RUN adduser --disabled-password --gecos '' mosga
WORKDIR /opt/
RUN chmod -R 777 /opt/
RUN mkdir /opt/mosga /opt/mosga/tools /opt/mosga/data
RUN chmod 775 /opt/mosga/ /opt/mosga/tools
RUN chmod a+rwx /opt/mosga/tools /opt/mosga/data

# Install MOSGA
COPY --chown=mosga:mosga accumulator /opt/mosga/accumulator
COPY --chown=mosga:mosga gui /opt/mosga/gui
COPY --chown=mosga:mosga snakemake /opt/mosga/snakemake
COPY --chown=mosga:mosga scripts /opt/mosga/scripts
COPY --chown=mosga:mosga scripts/docker_apache.conf gm*.gz /home/mosga/

# Install basic requirments
RUN bash /opt/mosga/scripts/install/basics_root.sh
ENV TOOLDIR="/opt/mosga/tools"

USER mosga
WORKDIR /opt/mosga/gui/
RUN composer install
RUN echo ';<?php exit(0); ?>' > /opt/mosga/gui/config.ini.php

# Prepare all tools
RUN bash /opt/mosga/scripts/install/prepare_tools.sh

ARG skip_eggnog_download
RUN if [ "x$skip_eggnog_download" = "x" ]; then bash /opt/mosga/scripts/install/eggnog.sh ; fi

ARG skip_idtaxa_download
RUN if [ "x$skip_idtaxa_download" = "x" ]; then bash /opt/mosga/scripts/install/idtaxa.sh ; fi

ARG skip_swissprot_download
RUN if [ "x$skip_swissprot_download" = "x" ]; then bash /opt/mosga/scripts/install/swissprot.sh ; fi

COPY data/*.faa /opt/mosga/tools/diamond/
COPY data/odna* /opt/mosga/data/
RUN bash /opt/mosga/scripts/install/organelles_db.sh

# Finish installations (root)
USER root
RUN bash /opt/mosga/scripts/install/tools.sh

# Build Augustus
USER mosga
WORKDIR /opt/mosga/tools/augustus/
RUN make
USER root
RUN make install

# GeneMark/BRAKER
RUN if [ -f "/home/mosga/gmes_linux_64.tar.gz" ] ; then tar -xzf /home/mosga/gmes_linux_64.tar.gz -C /opt/mosga/tools; mv /opt/mosga/tools/gmes_* /opt/mosga/tools/GeneMark ; cp -r /opt/mosga/tools/GeneMark/* /usr/local/bin/ ; else jq 'del(.annotation.genes.content.prediction.tools.braker, .annotation.genes.content.prediction.tools.genemark, .annotation.validation.content.completeness.tools.eukcc, .comparative.completeness.content.orthologs.tools.eukcc)' /opt/mosga/gui/gui-rules.json > /opt/mosga/gui/gui-rules.alt.json ;  fi

USER mosga
WORKDIR /opt/mosga/gui/
RUN composer install
RUN echo ';<?php exit(0); ?>' > /opt/mosga/gui/config.ini.php

# ProtHint
RUN if [ -f "/home/mosga/gmes_linux_64.tar.gz" ] ; then rm -rf /opt/mosga/tools/ProtHint/dependencies/GeneMarkES/ 2> /dev/null && ln -s /opt/mosga/tools/GeneMark /opt/mosga/tools/ProtHint/dependencies/GeneMarkES ; fi
RUN if [ -f "/home/mosga/gmes_linux_64.tar.gz" ] ; then cp -f /opt/mosga/tools/spaln/bin/spaln /opt/mosga/tools/ProtHint/dependencies/spaln ; fi

# Download SILVA databases
ARG skip_silva_download
RUN if [ "x$skip_silva_download" = "x" ]; then bash /opt/mosga/scripts/install/silva.sh ; fi

# Set Apache variables
User root
RUN sed -i "s/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=mosga/" /etc/apache2/envvars
RUN sed -i "s/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=mosga/" /etc/apache2/envvars
RUN cp /opt/mosga/scripts/docker_apache.conf /etc/apache2/sites-enabled/000-default.conf
ENV APACHE_RUN_USER mosga
ENV APACHE_RUN_GROUP mosga
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# Setup configuration
ARG cores
RUN if [ ! "x$cores" = "x" ]; then echo "cores = $cores" >> /opt/mosga/gui/config.ini.php ; else echo "cores = 4" >> /opt/mosga/gui/config.ini.php ; fi
ARG hostname
RUN if [ ! "x$hostname" = "x" ]; then echo "hostname = \"$hostname\"" >> /opt/mosga/gui/config.ini.php ; fi
ARG url
RUN if [ ! "x$url" = "x" ]; then echo "url = \"$url\"" >> /opt/mosga/gui/config.ini.php ; fi
ARG mail_from
RUN if [ ! "x$mail_from" = "x" ]; then echo "mail_from = \"$mail_from\"" >> /opt/mosga/gui/config.ini.php ; fi
ARG mail_notification
RUN if [ ! "x$mail_notification" = "x" ]; then echo "mail_notification = $mail_from" >> /opt/mosga/gui/config.ini.php ; fi
ARG delete_store_finished
RUN if [ ! "x$delete_store_finished" = "x" ]; then echo "delete_store_finished = $delete_store_finished" >> /opt/mosga/gui/config.ini.php ; fi
ARG delete_outdated_finished
RUN if [ ! "x$delete_outdated_finished" = "x" ]; then echo "delete_outdated_finished = $delete_outdated_finished" >> /opt/mosga/gui/config.ini.php ; fi
ARG list_jobs
RUN if [ ! "x$list_jobs" = "x" ]; then echo "list_jobs = \"$list_jobs\"" >> /opt/mosga/gui/config.ini.php ; fi
ARG ignore_sendmail
RUN if [ ! "x$ignore_sendmail" = "x" ]; then touch /opt/mosga/tools/sendmail ; fi

USER root

# Set path variables
ENV PATH=$PATH:/opt/mosga/tools/BRAKER:/opt/mosga/tools/augustus/bin:/opt/mosga/tools/augustus/scripts:/opt/mosga/tools/BRAKER/scripts:/usr/local/RepeatMasker:/opt/mosga/tools/GeneMark/gmes_petap/:/opt/mosga/tools/GeneMark/:/opt/mosga/tools/diamond:/opt/mosga/tools/perl5lib-Fasta/lib:/opt/mosga/tools/BUSCO/bin/:/opt/mosga/tools/miniconda/bin:/opt/mosga/tools/miniconda/condabin:/opt/mosga/tools/bbmap:/opt/mosga/tools/metaeuk/bin
ENV AUGUSTUS_CONFIG_PATH=/opt/mosga/tools/augustus/config
ENV GENEMARK_PATH=/opt/mosga/tools/GeneMark/
ENV BAMTOOLS_PATH=/usr/local/bin/
ENV ALIGNMENT_TOOL_PATH=/opt/mosga/tools/gth/bin/
ENV SAMTOOLS_PATH=/opt/mosga/tools/samtools/
ENV DIAMOND_PATH=/opt/mosga/tools/diamond/
ENV CDBTOOLS_PATH=/opt/mosga/tools/cdbfasta/
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8
ENV PYTHONIOENCODING=utf-8
ENV PERL5LIB=/opt/mosga/tools/perl5lib-Fasta/lib/
ENV SSUALIGNDIR=/usr/local/share/ssu-align-0.1.1

# Localize
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8
ENV PYTHONIOENCODING=utf-8

USER mosga
RUN touch /home/mosga/cron.log
RUN (crontab -l ; echo "* * * * * /bin/bash /home/mosga/env.sh && cd /opt/mosga/gui && php scheduler.php >> /home/mosga/cron.log 2>&1") | crontab

# Run unittest
WORKDIR /opt/mosga/accumulator
RUN python3 -m unittest

# Run Snakemake tests
RUN bash /opt/mosga/scripts/snakemake_tests.sh

USER root
COPY scripts/docker_wrapper.sh /root/wrapper.sh
EXPOSE 80
RUN chmod 755 /root/wrapper.sh
ENTRYPOINT ["/root/wrapper.sh"]
