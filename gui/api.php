<?php
# Copyright © 2021 Roman Martin
require_once('includes/configuration.php');
require_once('includes/api/functions.php');
require_once('includes/api/rest.php');
require_once('includes/jobs-database.php');
require_once('includes/jobs-management.php');

# Default header
header("Content-Type: text/plain; charset=utf-8");

class API {

  public function __construct() {
    $config = new MOSGAconfig();
    $this->config = $config->get_configs();

    if(!isset($_GET['method']) or !isset($_GET['version']) or !isset($_GET['action']))
      exit("Minimal required parameters are missing.");

    if($_GET['method'] != 'rest')
      exit("Unknown API method.");

    if(!is_numeric($_GET['version']))
      exit("Invalid API version number.");

    $api_name = sprintf("MOSGA_API_%s", $_GET['method']);
    $api = new $api_name($this->config, intval($_GET['version']), $_GET['action']);
  }

}

new API();
?>
