<?php
# Copyright © 2022 Roman Martin

/**
 * Set and initialize session
 */
ini_set('session.gc_maxlifetime', 43200);
session_start();

require('includes/UploadHandler.php');

/**
 * MOSGA upload handler that manages uploading chunks.
 */
class MOSGAUploadHandler extends UploadHandler {

  /**
   * Initializes the class and starts the file listning.
   * @param array $options passed to UploadHandler.
   * @param bool  $initialize true to start the class.
   * @param array $error_messages passing array of error messages.
   */
  public function __construct($options = null, bool $initialize = true, $error_messages = null) {
    parent::__construct($options, $initialize, $error_messages);
    $this->uid = $_POST['sub_id'];
    $this->file_list();
  }

  # overwrite
  protected function set_additional_file_properties($file) {
      # Nothing
  }

  /**
   * Stores recieving file chunk status to a JSON file to the upload directory.
   */
  protected function file_list() {
    $id = $_GET['param'];
    $preload = array();
    $preload['files'][$id] = array();

    $path = 'uploads/'.$this->uid.'/'.$this->uid.'.json';

    # preload existing status
    if( file_exists($path) ) {
      $preload = json_decode(file_get_contents($path), true);
    }

    if ($id != 'genomes') {
      $preload['files'][$id] = $this->response['files'][0];
    } else {
      $name = (array)$this->response['files'][0];
      $tag = hash('adler32', $name['name']);
      $preload['files'][$id][$tag] = $this->response['files'][0];
    }

    file_put_contents($path, json_encode($preload));
  }

}


if(isset($_POST['sub_id'])) {
  if(isset($_SESSION['sid']) and is_array($_SESSION['sid']) and
     isset($_SESSION['sid'][$_POST['sub_id']])) {
      $uid = $_POST['sub_id'];
  } else {
    $uid = null;
    exit(5);
  }

  $up_dir = ($uid != null) ? 'uploads/'.$uid.'/' : '/dev/null';
  $upload_handler = new MOSGAUploadHandler(array(
    'upload_dir' => $up_dir,
    'accept_file_types' => '/(.*)/',
    'access_control_allow_methods' => array('OPTIONS','HEAD', 'GET', 'POST', 'PUT', 'PATCH'),
    'delete_type' => 'will_not_be_deleted',
    )
  );
  @chmod('uploads/'.$uid, 0777);
}
