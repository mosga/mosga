<?php
# Copyright © 2022 Roman Martin
require_once('vendor/autoload.php');
require_once('includes/configuration.php');
require_once('includes/gui-builder.php');
require_once('includes/gui-settings.php');
require_once('includes/snakemake-builder.php');
require_once('includes/snakemake-gui.php');
require_once('includes/snakemake-gui-results.php');
require_once('includes/snakemake-list.php');
require_once('includes/snakemake-mails.php');
require_once('includes/snakemake-job-name.php');
require_once('includes/jobs-database.php');
require_once('includes/jobs-management.php');
require_once('includes/error-analyzer.php');
require_once('includes/modules.php');
require_once('includes/scaffold.php');

/**
 * Generates the GUI and print it out.
 */
class Gui {

  /**
   * Generates the GUI.
   * @param json_file string Path to the gui-rules.json file.
   */
  function __construct(string $json_file) {
    $config = new MOSGAconfig();
    $this->config = $config->get_configs();
    $this->modules = new Modules($config->get_modules());
    $this->mod_list = $this->modules->get_modules();
    $this->submission_templates = $this->modules->get_submissions();

    # Check and set safely mode ( invalid mode will not produces errors )
    if(isset($_GET['mode']) and ($_GET['mode'] == 'annotation' or $_GET['mode'] == 'comparative')) {
      $mode = $_GET['mode'];
      $_SESSION['mode'] = $_GET['mode'];
    } elseif( isset($_SESSION['mode']) ) {
      $mode = $_SESSION['mode'];
    } else {
      $mode = 'annotation';
    }

    $rules = $this->parser_json($json_file);
    $rules = array('mosga' => $rules[$mode]);  # select pipeline rules
    $this->jobs = new Jobs();

    # Load template
    $loader = new \Twig\Loader\FilesystemLoader('includes/templates/');
    $this->twig = new \Twig\Environment($loader, ['cache' => false ]);
    $this->template = array('view.html.twig');

    # Default parameters
    $builder = new GuiBuilder($rules);
    $show_job_list = ($this->config['list_jobs'] == 'cookie' or $this->config['list_jobs'] == 'all');
    $show_results = false;
    $job_list = [];
    $results = '';
    if ( $show_job_list ) {
      $list_object = new SnakeMake_JobList($this->jobs, $this->config);
      $job_list = $list_object->get_job_list();

      # Prepare selected jobs
      $file_list = [];
      if ( !empty($_POST['jobs']) )
      {
        $fl = $list_object->get_job_list($_POST['jobs']);
        foreach ($fl as $job) {
          foreach ($job[3] as $n) {
            array_push($file_list, [$job[0], $n]);
          }
        }
      }
    }

    $submission = false;

    ## Overwrite for specific sites
    # Check for scaffold excerpt request.
    if(isset($_GET['scaffold']) AND strlen($_GET['scaffold']) AND isset($_GET['id']) AND isset($_GET['uid']) ) {
      $scaffold_view = new ScaffoldView();
      $scaffold_view->set_var($_GET['uid'], $_GET['id'], array("scaffold" => $_GET['scaffold']));
      $this->template = $scaffold_view->get_view();

    # g:Profiler g:GOSt
    } elseif(isset($this->mod_list['gprofiler']) and isset($_GET['gprofiler']) AND isset($_GET['id']) AND isset($_GET['uid']) ) {
      $gprofiler_view = new GprofilerView();
      $gprofiler_view->set_var($_GET['uid'], $_GET['id']);
      $this->template= $gprofiler_view->get_view();

    # STRING
    } elseif(isset($this->mod_list['stringdb']) and isset($_GET['stringdb']) AND isset($_GET['id']) AND isset($_GET['uid']) ) {
      $stringdb_view = new StringDbView();
      $stringdb_view->set_var($_GET['uid'], $_GET['id']);
      $this->template= $stringdb_view->get_view();

    # Log Viewer
    } elseif(isset($this->mod_list['log_viewer']) and isset($_GET['logs']) AND isset($_GET['id']) AND isset($_GET['uid']) ) {
      $log_viewer = new LogViewer();
      $log_viewer->set_var($_GET['uid'], $_GET['id']);
      $this->template = $log_viewer->get_view();

    } else {

      # Check current state
      if(isset($_GET['status']) AND $_GET['status'] == 1 AND isset($_GET['id']) AND isset($_GET['uid'])) {
        $show_results = true;
        $this->gui = new JobStatusGUI($this->jobs, $this->config, $this->modules);
        $this->gui->generate();
        $results = $this->gui->get_output();

      # Check for submission
      } elseif(count($_POST) AND count($_FILES) AND isset($_SESSION['sid'])) {
          if(isset($_SESSION['sid']) and is_array($_SESSION['sid']) and isset($_SESSION['sid'][$_POST['sub_id']])) {
            $submission_id = $_POST['sub_id'];
            unset($_SESSION['sid'][$submission_id]);
          }

          $smk = new SnakemakeConfigBuilder($_POST, $submission_id, $this->config, $mode);
          $submission = true;
          $show_results = true;

          $uid = $smk->get_project_hash();
          $id = $this->jobs->submit_job( $uid, (isset($mode) and $mode == "comparative") ? true : false );

          if( $this->config['list_jobs'] == 'cookie') $this->set_uid_cookie($id, $uid);
          $results = array('id' => $id);

          if($id) {
            $results['position'] = $this->jobs->count_open_jobs();
            $results['path'] = 'job_'.$uid.'_'.$id.'.html';
          }

      # Something went wrong (not reachable right now by Twig)
      }

      $sub_id = $this->generate_submission_id();
      if(!isset($_SESSION['sid']) or !is_array($_SESSION['sid'])) $_SESSION['sid'] = array();
      $_SESSION['sid'][$sub_id] = true;

      # Hosts tab
      $has_host_file = file_exists('includes/templates/host.html');
      $show_server_usage = $this->config['show_server_usage'];
      $show_host_tab = ($show_server_usage or $has_host_file) ? true : false;

      # Mode switch
      if ($mode == 'annotation') {
        $mode_switch_text = "Comparative Genomics";
        $mode_switch_href = "comparative_genomics.html";
      } else {
        $mode_switch_text = "Annotation";
        $mode_switch_href = "annotation.html";
      }

      # Collects the variable for the template engine.
      $this->template[1] = array(
        'mode'               => $mode,
        'show_results'       => $show_results,
        'results'            => $results,
        'file_list'          => $file_list,
        'submission'         => $submission,
        'submission_gui'     => $builder->get_menu(),
        'show_job_list'      => $show_job_list,
        'job_list'           => $job_list,
        'version'            => $this->config['display_version'] ? $this->display_git_version() : '',
        'has_host_file'      => $has_host_file,
        'show_server_usage'  => $show_server_usage,
        'show_host_tab'      => $show_host_tab,
        'modules_submission' => $this->submission_templates,
        'mode_switch_href'   => $mode_switch_href,
        'mode_switch_text'   => $mode_switch_text,
        'submission_id'      => $sub_id,
        'public'             => $this->config['list_jobs'] == 'all',
        'motd'               => $this->motd()
      );
    }

  }

  /**
   * Outputs the render HTML website as a string.
   * @return string The whole website.
   */
  public function render() : string {
    return $this->twig->render($this->template[0], $this->template[1]);
  }

  /**
   * Generates an available project hash string ( or temporarily the submission id)
   * @return string An available project hash.
   */
  private function generate_submission_id() : string {
    $hash = substr(md5(time()-rand(10000,99999)),0,8); # submission id
    while( file_exists('uploads/' . $hash) ) {
      $hash = substr(md5(time() - (rand(10000,99999) * rand(10,99)) ), 0,8);
    }
    return $hash;
  }

  /**
   * Reads the GUI rules file.
   * @param json_file string Path to the gui-rules.json file.
   */
  private function parser_json(string $json_file) : array {
    $read_file = file_get_contents($json_file);
    $json = json_decode($read_file, true);
    return $json;
  }

  /**
   * Sets the cookie to access projects.
   * @param id int Project numerical identifier.
   * @param uid string Project hash identifier.
   */
  private function set_uid_cookie(int $id, string $uid) : bool {
    if( isset($_COOKIE['job_uids']) )
      $uids = $_COOKIE['job_uids'];
    else
      $uids = null;

    if( $uids == null ) {
      $jobs = array();
    } else {
      $jobs = unserialize($_COOKIE['job_uids'], ["allowed_classes" => false]);
      if( !count($jobs) ) $jobs = array();
    }

    $jobs[$id] = $uid;
    return setcookie("job_uids", serialize($jobs), time()+2419200);
  }

  /**
   * Catch current git version commit or tag.
   * @return array Either commit or tag and passes the values.
   */
  public function display_git_version() : array {
    $tag;
    $commit;

    @exec('git tag --points-at HEAD', $tag);
    @exec('git rev-parse --short HEAD', $commit);

    $has_tag = (is_array($tag) and count($tag)) ? true : false;
    $has_commit = (is_array($commit) and count($commit)) ? true : false;

    if( $has_tag or $has_commit ) {
      if($has_tag) {
        return array(
          'type' => 'tag',
          'url' => 'https://gitlab.com/mosga/mosga/-/blob/'.$tag[0].'/CHANGELOG.md',
          'name' => $tag[0]
        );
      } else {
        return array(
          'type' => 'commit',
          'url' => 'https://gitlab.com/mosga/mosga/-/tree/'.$commit[0],
          'name' => $commit[0]
        );
      }
    }

    return array();
  }

  /**
   * Load the message of the day content from the motd.html file if exists.
   * @return  string  The message of the day, or an empty string.
   */
  private function motd() : string {
    if(file_exists("motd.html")) {
      $cont = file_get_contents("motd.html");
      return $cont;
    }
    return '';
  }

}

# Initiate session
session_start();

# Initiate GUI class and print the template engine output
$json_path = (file_exists('gui-rules.alt.json')) ? 'gui-rules.alt.json' : 'gui-rules.json';
$gui = new Gui($json_path);
echo $gui->render();

?>
