$(document).ready(function(){
  // shows the popup and blocks the button if no checkbox of the group is selected
  $('.custom-checkbox-trigger').on('click', function() {
      let boxes = $('.custom-checkbox-group');

      if (!boxes.filter(':checked').length) {
          let box = boxes.filter(':last');
          box.popover('show');
          return false;
      }
  });

  // removes the popup when a checkbox of the group has been clicked
  $('.custom-checkbox-group').on('change', function() {
      let box = $('.custom-checkbox-group:last');
      box.popover('dispose');
  });

  // inverts the checkbox in a table by clicking on the row
  $('.record tr').on('click', function() {
      let box = $(this).find('input[type=checkbox]');
      box.prop('checked',!box.prop('checked'));
      box.trigger('change'); // trigger event
  });
});
