function set_scaffold_link() {
  var url_string = window.location.href
  var url = new URL(url_string);
  var uid = url.searchParams.get("uid");
  var id = url.searchParams.get("id");

  // catch mod rewrite addresses
  if( uid == null || id == null) {
    rewrite_addr = url_string.split("/").pop();
    ids = rewrite_addr.split("_")
    if( ids[0] == "job") {
      uid = ids[1]
      id = ids[2].split(".")[0]
    }
  }

  $('table#organelles').find('tr').each(function() {
    td = $(this).find('td:first-child').text()
    if( td.length > 0) {
      $(this).find('td:first-child').html('<a href="index.php?scaffold=' + td + '&uid=' + uid + '&id=' + id +'" target="_blank">' + td + '</a>')
    }
  });
}

$('#organelscan').on('show.bs.modal', function (e) {
    var button = $(e.relatedTarget);
    var modal = $(this);
    modal.find('.modal-body div.remote_content').load(button.data("remote"), function( response, status, xhr ) {
      set_scaffold_link();
    });
});


$('#vecscreen').on('show.bs.modal', function (e) {
    var button = $(e.relatedTarget);
    var modal = $(this);
    modal.find('.modal-body div.remote_content').load(button.data("remote"), function( response, status, xhr ) {

    });
});
