function chart_color(index) {
  if( index == 0 ) {
    return 'rgb(220, 225, 52)';
  } else if ( index == 1) {
    return 'rgb(69, 24, 84)';
  }
}

function draw_server_usage_chart(ajax_data) {
  new_labels = ajax_data['labels']

  const labels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
  ];

  new_dataset = []
  for (let i=0; i < ajax_data["datasets"].length; i++) {
    new_data = [];

    for( let k=0; k < ajax_data["data"].length; k++) {
      new_data.push(ajax_data["data"][k][i]);
    }

    set = {
      label: ajax_data['datasets'][i],
      backgroundColor: chart_color(i),
      borderColor: chart_color(i),
      data: new_data
    }
    new_dataset.push(set)
  }


  const data = {
    labels: new_labels,
    datasets: new_dataset
  };

  const config = {
    type: 'line',
    data,
    options: {}
  };

  var myChart = new Chart(
    document.getElementById('myChart'),
    config
  );
}


$.ajax({
  method: "POST",
  url: "api/rest/v100/server_usage/",
  }).done(function( data ) {
    draw_server_usage_chart(data)
  });
