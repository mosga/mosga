// Enable Buttons
function enable_all_buttons() {
	$('input[type="checkbox"]:disabled, input[type="radio"]:disabled').each(function() {
		if (!$(this).hasClass("disabled")) {
  		$(this).attr('disabled',false);
		}
  });
}

// Uncheck disabled buttons, switch to next one
function replace_checked_disabled_buttons() {
  $('input:disabled:checked').each(function (){
		if (!$(this).hasClass("disabled")) {
	    name = $(this).attr('name');
	    // Check for not selected
	    $('input[name="'+name+'"]:enabled').prop('checked', true);
	    $(this).prop('checked', false);
		}
  });
}

// Disable buttons if there are depending on other settings
function disable_buttons() {
  enable_all_buttons();

  $('input[data-description*="onlyif:"]').each(function() {
    data_description = $(this).attr('data-description').substr('onlyif:'.length)
		if (data_description.indexOf(';') !== -1) {
			data_descs = data_description.split(';');
			data_description = data_descs[0]
		}
    onlyifs = data_description.split(',');

    for(i = 0; i < onlyifs.length; i++) {
      onlyif = onlyifs[i].split(':');
      only_name = onlyif[0];
      only_value = onlyif[1];
			disable = false;
			no_operator = false;
			search = $('input[name="'+only_name+'"]:checked, input[name="'+only_name+'[]"]:checked');

			// or operators
			if( only_value.includes('||') ) {
				var only_values = only_value.split('||');
				var or_counter = 0;

				for(k = 0; k < only_values.length; k++) {
					if( search.val() == only_values[k] ) {
						or_counter++;
					}
				}

				if( !or_counter ) disable = true;

			} else {
				no_operator = true;
			}

			if( (no_operator && search.val() != only_value ) || disable ) {
				$(this).attr('disabled', true);
			}
    }

  });

	// Disable additional file buttons for taged ids
	$('input[data-description*="dis_req:"]').each(function() {
	  data_description = $(this).attr('data-description').split(';');
		for( ds = 0; ds < data_description.length; ds++) {
			// Matches with dis_req
			if( data_description[ds].indexOf("dis_req:") !== -1) {
				values = data_description[ds].substr('dis_req:'.length).split(',');
				if ( $(this).prop('checked')) {
					for ( v = 0; v < values.length; v++) {
						hide_button(values[v], true);
					}
				}
			}
		}

	});

  replace_checked_disabled_buttons();
  //reset_checkbox_button();
}

// Let's appear some buttons
function show_button(id) {
  $("label[for='"+id+"'] .btn-settings").show();
  adds = $("label[for='"+id+"'] .btn-adds");

  adds.show();
  modal_adds_target = adds.attr('data-target');
  modal = $(modal_adds_target+' input[type="file"]');
  modal.prop('aria-require', true);

  //modal.prop('required', true);
}

// Hide some
function hide_button(id, only_add=false) {
	if( !only_add ) {
		$("label[for='"+id+"'] .btn-settings").hide();
	}

  adds = $("label[for='"+id+"'] .btn-adds");
  adds.hide();
  modal_adds_target = adds.attr('data-target');
  modal = $(modal_adds_target+' input[type="file"]');
  modal.prop('aria-require', false);
  modal.prop('required', false);
  adds.popover('hide');
}

function reset_checkbox_button() {
  $('.custom-checkbox input[type="checkbox"],.custom-radio input[type="radio"]').each(function() {
    id = $(this).prop('id');
    checked = $(this).prop('checked');
    if(checked) {
      show_button(id);
    } else {
      hide_button(id);
    }
  });
}

// Set on click event
function set_click_event(formtype) {
  $('.custom-'+formtype+' input[type="'+formtype+'"]').change(function() {
    id = $(this).prop('id');
    checked = $(this).prop('checked');
    type = id.substring(0, id.lastIndexOf('_'))+'_';
    nontype = $('.custom-'+formtype+' input#'+type+'none')

		// Genes collapse
    if(checked) {
			if ( $('#genes_gene_none').prop('checked') ) {
				$('button#Genes_more').hide();
				$('button#Genes_more').text('More');
				$('.collapse_Genes').collapse('hide');
			} else if (id.indexOf("genes_gene") >= 0) {
				$('button#Genes_more').show();
			}

      show_button(id);

      if(nontype.prop('checked') && id != nontype.prop('id')) {
        nontype.prop('checked', false).parent().removeClass('active');
      }

      reset_checkbox_button(id);
    } else {
      hide_button(id);
    }

    disable_buttons();
		check_submittable();
  });
}

// Unset on
$('.custom-checkbox input.none').change(function() {
  id = $(this).prop('id');
  type = id.substring(0, id.lastIndexOf('_'))+'_';

  $('.custom-checkbox input[id^="'+type+'"]').each(function() {
    newid = $(this).prop('id');
    if(newid != type+'none') {
      $("#"+newid).prop('checked', false).parent().removeClass('active');
    }
  });
  reset_checkbox_button();

});

$('button.btn-collapse').click(function(){
    $(this).text(function(i,old){
        return old == 'More' ?  'Less' : 'More';
    });
});

$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$(function () {
  $('[data-toggle="popover"]').popover();
})

$('.modal.modal_adds').on('hidden.bs.modal', function () {
  var classes = $(this).attr('class').split(' ')
  classes.forEach(element => {
  	btn = $('.btn-adds[data-target=".'+element+'"]');
    if( btn.length ) {

			var missings = check_missings(element);
      if( missings) {
      	show = btn.popover('show');
        popover_id = show.attr('aria-describedby');
        $('#'+popover_id).addClass('popover-danger');
      } else {
        btn.popover('hide');
      }

    }
  });
});

// Counts the modal file upload fields
function check_missings( element ) {
	uploads = $('.modal.'+element+' input[type="file"]')
	missings = 0;

	uploads.each(function () {
		if( $(this).attr('data-finished') == "0") {
		 missings = missings + 1
		}
	});

	return missings;
}

$('input#set_priority').change(function() {
	if($(this).is(":checked")) {
		if ( $("ul.list-group-sortable li").length == 0 ) {

			// generate list
			$("div.unit_type").each(function() {
			  $("ul.list-group-sortable").append(
				"<li class=\"list-group-item icon arrow_both\">" +
			  	"<span class=\"txt\">" + $(this).html() + "</span></li>");
			} );

			// activate sortable
			$('ul.list-group-sortable').sortable({
			  placeholderClass: 'list-group-item'
			});
			var srtlst = []
			$("ul.list-group-sortable li").each(function() {
				lst_entry = $(this).html().replace(/<[^>]*>?/gm, '');
				srtlst.push(lst_entry)
			});
			$("#priority_list").val(srtlst)

			// On change
			$('ul.list-group-sortable').sortable().bind('sortupdate', function(e, ui) {

				var srtlst = []
				$("ul.list-group-sortable li").each(function() {
					lst_entry = $(this).html().replace(/<[^>]*>?/gm, '');
				  srtlst.push(lst_entry);
				});
				$("#priority_list").val(srtlst)

			});
		}
 } else {
	 if ( $("ul.list-group-sortable li").length != 0 ) {
		 $("ul.list-group-sortable").empty();
		 $('ul.list-group-sortable').sortable('destroy');
	 }
 }
});

reset_checkbox_button();
set_click_event('checkbox');
set_click_event('radio');
disable_buttons();
check_submittable();

$("input.dyn_slider").each(function() {
	$(this).slider();
	var id = $(this).attr("id");
	var value_text = 'span.dyn_val[data-attribute="'+ id + '"]';
	var destroy_button = 'button.dyn_distroy[data-attribute="'+ id + '"]';

	// Adapt text per change
	$("input#"+ id).on("slide", function(slideEvt) {
	 $(value_text).text(slideEvt.value);
	});

	// Allow manual input
	$(destroy_button).click(function() {
		$("input#"+ id).slider('destroy')
		$(value_text).hide()
	});

	$(value_text).text($("input#"+ id).val())
});

$('.fileupload').each(function () {
	var iid = $(this).prop("id");
		$(this).fileupload({
			maxChunkSize: 10000000, // 10 MB
			progressall: function (e, data) {
				var progress = parseInt((data.loaded / data.total) * 100, 10);
				$('.progress-bar[data-target="' + iid + '"]').css('width', progress + '%');
				$('.progress[data-target="' + iid + '"]').css('visibility', 'visible');
			},
			done: function(e, data) {
				try {
					var response = JSON.parse(data.result)
					var file_response = response.files[0];
					if( ! file_response.hasOwnProperty("error") ) {
						$('.progress-bar[data-target="' + iid + '"]').removeClass('progress-bar-animated progress-bar-striped bg-danger');
						$('.progress-bar[data-target="' + iid + '"]').addClass('bg-success');
						$('#' + iid).attr('data-finished','1');
						$('#' + iid).prop('required', false);
					} else {
						alert(file_response.error);
						$('.progress-bar[data-target="' + iid + '"]').css('width', '0%');
						$('#' + iid).attr('data-finished','0');
						$('#' + iid).prop('required', true);
					}
				} catch(e) {
					alert("Could not upload file");
				}
			}
		});
});

function update_outgroup_list() {
	var outgroups = document.getElementById("outgroups");
	if ($(this).is(':checked')){
		if (outgroups.value.length > 0) {
			outgroups.value += ("," + $(this).prop("value"));
		}
		else {
			outgroups.value = $(this).prop("value");
		}
	} else {
		var list = outgroups.value.split(',');
		list = list.filter(x => x !== $(this).prop("value"));
		outgroups.value = list.join(",");
	}
	// console.log(outgroups.value);
}

function check_submittable() {
	var selected_buttons = 0;
	var active_buttons = [];
	$(".guibuild").each(function(index) {
	  if($(this).prop("checked") && !$(this).prop("id").endsWith("_none")) {
	      active_buttons.push($(this))
	      selected_buttons += 1;
	  }
	})
	active_buttons.forEach(function(item, index, array) {
	  if(!item.prop("value").endsWith("_none") && item.parents("div.collapse").length) {
	    var div_parent = item.parents("div.collapse");
	    if(!div_parent.hasClass("show"))
	      selected_buttons -= 1;
	  }
	})
	if(selected_buttons) {
		$("#submit_job").prop("disabled", false);
	} else {
		$("#submit_job").prop("disabled", true);
	}
}

$('[id^="outgroup_"]').change(update_outgroup_list);

function strhash(str) {		// TODO: used to generate a uniform id for fileupload between calls, not very efficient
    var hash = 0;
    if (str.length == 0) {
        return hash;
    }
    for (var i = 0; i < str.length; i++) {
        var char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash;
    }
    return hash;
}

$('.fileupload.x').each(function () {
	var iid = $(this).prop("id");
		$(this).fileupload({
			maxChunkSize: 10000000, // 10 MB
			submit: function(e, data) {
				var progress_id = strhash(data.files[0].name);
				var box =
				$('<div id="' + progress_id + '" class="col" style="padding: 2.5px 5px 2.5px 5px;">' +
					'<div id="' + progress_id + '" class="border rounded bg-light">' +
					'<div style="margin: 2.5px 10px 2.5px 10px;">' + data.files[0].name + '</div>' +
					'<hr style="margin: 2.5px 10px 2.5px 10px;"/>' +
					'<div style="margin: 2.5px 10px 2.5px 10px;" class="custom-control custom-checkbox" style="top:0.35em;">' +
					'<input type="checkbox" class="custom-control-input disabled" value="' + data.files[0].name + '" id="outgroup_' + progress_id  + '">' +
					'<label class="custom-control-label" for="outgroup_' + progress_id + '">Outgroup</label></div>' +
					'<div class="progress" data-target="' + progress_id + '" style="width: 100%; height: 5px; visibility: hidden;">' +
				  '<div class="progress-bar progress-bar-striped bg-success progress-bar-animated" data-target="' + progress_id + '" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>' +
				  '</div></div></div>');
				$('div#' + "genome-files").append(box);
				// add checkbox event
				var checkbox = document.getElementById("outgroup_" + progress_id);
				checkbox.addEventListener('change', update_outgroup_list);
			},
			progress: function (e, data) {
				var progress_id = strhash(data.files[0].name);
				var progress = parseInt((data.loaded / data.total) * 100, 10);
				$('.progress-bar[data-target="' + progress_id + '"]').css('width', progress + '%');
				$('.progress[data-target="' + progress_id + '"]').css('visibility', 'visible');
			},
			done: function(e, data) {
				try {
					var progress_id = strhash(data.files[0].name);
					var response = JSON.parse(data.result)
					var file_response = response.files[0];
					if( ! file_response.hasOwnProperty("error") ) {
						$('.progress-bar[data-target="' + progress_id + '"]').removeClass('progress-bar-animated progress-bar-striped bg-danger');
						$('.progress-bar[data-target="' + progress_id + '"]').addClass('bg-success');
						$('.progress-bar[data-target="' + progress_id + '"]').attr('data-target', '');	// workaround fix for multiple files with the same name
					} else {
						alert(file_response.error);
						$('.progress-bar[data-target="' + progress_id + '"]').css('width', '0%');
						$('#' + progress_id + '.col').remove();
					}
				} catch(e) {
					alert("Could not upload file");
				}
			},
			stop: function(e) {
				$('#' + iid).attr('data-finished','1');
				$('#' + iid).prop('required', false);
			},
		});
});

$("#specie").autocomplete({
	source: function (request, response) {
		$.post("api/rest/v100/ncbi_taxonomy/", request, response);
	},
	minLength: 3,
	select: function( event, ui ) {
		$("#ncbi_tax_id").prop("value", ui.item.id)
		$("#repeats_repeatmasker_input_speciesmanual").prop("value", ui.item.value)
		$("#ncbi_taxsearch").prop("disabled", false);
	},
	open: function (e, ui) {
		var acData = $(this).data("uiAutocomplete");
		acData.menu.element.find("li").each(function () {
			var me = $(this);
			var keywords = acData.term.split(' ').join('|');
			me.html(me.text().replace(new RegExp("(" + keywords + ")", "gi"), '<span class="achi">$1</span>'));
		});
	}
});

$("#specie").change(function() {
	if (($(this).prop("value").length)) {
		// console.log($("#ncbi_tax_id").prop("value"))
	} else {
		// parseInt($("#ncbi_tax_id").prop("value"), 10)
		$("#ncbi_tax_id").prop("value", 0);
		$("#ncbi_taxsearch").prop("disabled", true);
		$("#ncbi_taxsearch").prop("checked", false);
	}
});


// Execute on submit button
$('[type="submit"]').click(function() {
	if (this.id == "submit_job")
	{
		// special cases for the comparative pipeline
		if ($("[id=comparative_submission]").length >= 1)
		{
			var container = document.getElementById("genome-files");
			if ($("#phylogenetics_raxml").prop("checked") || $("#phylogenetics_fastme").prop("checked")) {
				if (container.children.length < 4) {
					alert("Phylogenetic analysis tools (RAxML & FastME) needs at least 4 genome files!")
					return false;
				}
			} else if (container.children.length < 2) {
				alert("Comparative Genomics needs at least 2 genome files!")
				return false;
			}
		}
	}

	// Decides which modal file upload fields and lets check them
	var counter = 0;
	$('.modal.modal_adds').each(function(){
		var classes = $(this).attr('class').split(' ')
		classes.forEach(element => {
			btn = $('.btn-adds[data-target=".'+element+'"]');
			if( btn.length && btn.css('display') != 'none' ) {
				var missings = check_missings(element);
				counter = counter + missings;
				if( missings) {
					show = btn.popover('show');
					popover_id = show.attr('aria-describedby');
					$('#'+popover_id).addClass('popover-danger');
				}
			}
		});
	});
	if( counter != 0 ) {
		alert("You should upload all requested files!");
		return false;
	}

 return true;
})
