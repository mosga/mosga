<?php

/**
 * Tries to identfy the soure of potential errors and re-schedule the runs.
 */
class ErrorAnalyzer {

  /**
   * Required objects.
   */
  private $config;
  private $jobs;
  private $uid;
  private $error_sources;
  /**
   *
   */
  public function __construct(array $config, Jobs $jobs, string $uid) {
    $this->config = &$config;
    $this->jobs = &$jobs;
    $this->jobs = &$uid;

    # Snakemake rules and associated detailed log files.
    $this->error_sources = array(
      "MOSGA_initialize_genome" => array(
        "message"  => "The genome initalizer failed, are you sure you uploaded a genome file?"
      ),
      "eukcc"                   => array(
        "function" => ErrorMessages::eukcc("logs/eukcc.txt"),
      ),
      "red"                     => array("red.txt"),
      "search_vecscreen"        => array("vescreen_search.txt"),
      "MOSGA_vecscreen_analyse" => array("vecscreen.txt"),
    );

  }

  /**
   *
   */
  public function add_error_sources() {

  }

  /**
   *
   */
  public function get_error_message($error) {

  }

  /**
   *
   */
  public function analyze() {

  }

}


class ErrorMessages {

  public static function eukcc(string $path) {

  }

}

?>
