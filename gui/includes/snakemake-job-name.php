<?php

/**
 * Ensures the possibility to print or enter a job name.
*/
class JobName extends JobStatusGUI {

  /**
   * @param jobs Jobs Object that allows database interaction.
   * @param config Array Stores currenct instance configuration.
   * @param status Array Stores job specific status information.
   */
  public function __construct( Jobs $jobs, array $config, array $status) {
    $this->jobs = &$jobs;
    $this->config = &$config;
    $this->status = &$status;
  }

  /**
   * Formats the job name header and check for new incoming job names.
   * @return string HTML code with the job header with or without an input field.
  */
  protected function status_job_name(): string {

    # new job name?
    if ($this->check_register()) {
      $this->status['job_name'] = $_POST['job_name'];
    }

    $link = 'snakemake.php?status=1&uid='.$this->status['job_uid'].'&id='.$this->status['job_id'];
    $h2 = 'Job '.$this->status['job_uid'];

    # format the header
    if (isset($this->status['job_name']) and @strlen($this->status['job_name'])) {
      $h2 .= ' - '.$this->status['job_name'];
      $forms = $form = $forme = '';
    # no job name => input field
    } else {
      $forms = '<form action="#job_name" method="post">';
      $form = '
      <input type="text" id="job_name" name="job_name" placeholder="Name" value="" style="font-size:1.3rem;">
      <button id="save_job_name" type="submit" class="btn btn-sm btn-primary">Save</button>';
      $forme = '</form>';
    }
    $out = $forms.'<h2><a href="'.$link.'" target="_self">'.$h2.'</a>'.$form.'</h2>'.$forme;

    return $out;
  }

  /**
   * Checks for existing POST-Parameter with a new job name. If present, store
   * it into the database.
   * @return boolean True if name exists and is stored else false.
   */
  protected function check_register() : bool {
    if ( isset($_POST['job_name']) and strlen($_POST['job_name'])) {
        return $this->jobs->insert_job_name($this->status['job_id'], $_POST['job_name']);
    }
    return false;
  }

}

?>
