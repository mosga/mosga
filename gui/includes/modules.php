<?php

/**
 * Manages external stored modules for the GUI (in modules/*).
 */
class Modules {

  public function __construct(array $modules) {
    $this->modules = $modules;
    $this->sidebars = array();
    $this->modules_list = array();
    $this->submissions = array();

    # Load PHP source code for modules
    $this->load_modules();
  }

  /**
   * Try to load every module file that should exist according to the
   * $this->modules list.
   * The name of loaded modules will stored inside $this->modules_list().
   */
  private function load_modules() {

    # Try to include the source code, don't care about the type..
    foreach($this->modules as $mod_type => $mods) {

      if (!isset($this->sidebars[$mod_type]))
        $this->sidebars[$mod_type] = array();

      foreach($mods as $mod_value) {

        # Include the source code
        if (@include_once('includes/modules/'.$mod_value.'.php')) {
          $mod = new $mod_value;

          # Sidebar references
          $ref = $mod->get_sidebar_references();
          if(count($ref)) {
            $cnt = 1;
            foreach($ref as $m) {
              $this->sidebars[$mod_type][$mod_value . $cnt] = $m;
              $cnt += 1;
            }
          }

          # Submission templates
          $sub = $mod->get_submission_templates();
          if(count($sub)) {
            foreach($sub as $s) {
              $this->submissions[] = $s;
            }
          }

          # Add module to the loaded modules list
          $this->modules_list[$mod_value] = true;
        }
      }
    }
  }

  /**
   * Returns an array with the loaded modules as key (and True as value).
   * @return array The array $this->modules_list.
   */
  public function get_modules() : array {
    return $this->modules_list;
  }

  /**
   * Returns an array with the Twig template files that appear on the submission
   * page under "Advanced Modules". The Twig template files have to be stored
   * under "/opt/mosga/gui/includes/templates/modules/submission/".
   * @return array The array $this->submissions.
   */
  public function get_submissions() : array {
    return $this->submissions;
  }

}

abstract class ModulesSetup {

  public $sidebar = array();
  public $submission = array();

  public function get_sidebar_references() {
    return $this->sidebar;
  }

  public function get_submission_templates() {
    return $this->submission;
  }

}

interface ModuleView {

  public function set_var(string $uid, int $id, array $more = array());
  public function get_view() : array;

}

interface ModuleReference {

  public function set_var(array $vars = array());
  public function get_sidebar();

}

?>
