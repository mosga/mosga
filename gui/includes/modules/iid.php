<?php

class iid extends ModulesSetup {

  public $sidebar = array(
    array('genes/iid.csv', 'IidReference', False)
  );

  public $submission = array(
    "iid.html.twig"
  );

}

class IidReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  /**
   * @return string Returns a button link to the iid result file.
   */
  public function get_sidebar() {
    $url = 'uploads/'.$this->status['job_uid'].'/genes/iid.csv';
    return '<a type="button" class="btn btn-sm btn-success" target="_blank" href="'.$url.'">IID</a>';
  }


}
