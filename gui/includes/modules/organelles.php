<?php

class organelles extends ModulesSetup {

  public $sidebar = array(
    array('organelles.html', 'OrganellesReference', True)
  );

}

class OrganellesReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  /**
   * Builds the show button for the organelle scan analysis and the modal.
   * @return string HTML-encoded modal box.
   **/
  public function get_sidebar() {
    $path = 'uploads/'.$this->status['job_uid'].'/organelles.html';

    return array(
      'id'           => 'organelscan',
      'button_title' => 'Organelle Scan',
      'header'       => 'Organelle Scan',
      'remote'       => $path,
      'style'        => 'max-width:1000px;',
      'content'      => '
        <p class="organelles" style="text-align:justify">
        The exact identification of plastid or mitochondrial genomes is
        very tricky. To help you to identify possible organelle genomes,
        we provide you a table that gives you some indications which
        scaffolds may contain organelle scaffolds. The highest lines are
        probably organelle scaffolds. Depending on your selected tools the
        table contains more or fewer columns.
        <strong>The option for "Scan for organelles" will perform searches
        against our local plastid and mitochondrial gene database.</strong>
        Plastid scaffolds usually comprise between 4 and 25 kbp.
        While mitochondrial scaffolds cover 1 to 200 kbp for protists, 11
        to 28 kbp in animals and 19 to 1000 kbp in plants and fungi.
        We are using partially filtered data for the analysis to get as
        many indications as possible. You can verify your results by
        searching the whole scaffold against the NCBI nucleotide database.
        You can <strong>click on a scaffold and submit the scaffold
        directly to NCBI MegaBlast</strong>.
        </p><p style="text-align:left;">
        <strong>tRNA</strong>: number of tRNAs.
        <strong>rRNA</strong>: number of rRNAs.
        <strong>rRNA(p)</strong>: number of partial rRNAs (different codons).
        <strong>Mitos</strong>: number of matches against our mitochondrial gene database.
        <strong>Plastids:</strong> number of matches against our plastid gene database.
        <strong>GC dev:</strong> deviation of the GC content compared to the average.
        <strong>Len [kbp]:</strong> scaffold length in kilobase pairs.
        <strong>MD:</strong> mitochondrial gene matches per 10 kilobase pairs.
        <strong>PD:</strong> plastid gene matches per 10 kilobase pairs.
        <strong><i>Score:</i></strong> rough scoring to estimate the organelles.
        </p>
        <div class="remote_content"></div>'
      );
  }

}

?>
