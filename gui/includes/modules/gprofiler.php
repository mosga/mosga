<?php

class gprofiler extends ModulesSetup {

  public $sidebar = array(
    array('genes/gprofiler.html', 'GprofilerReference', False)
  );

  public $submission = array(
    "gprofiler.html.twig"
  );

}

class GprofilerReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  /**
   * @return string Returns a button link to the g:Profiler g:GOst table.
   */
  public function get_sidebar() : string {
      $url = 'index.php?gprofiler=1&uid='.$this->status['job_uid'].'&id='.$this->status['job_id'];
      return '<a type="button" class="btn btn-sm btn-success" target="_blank" href="'.$url.'">g:Profiler g:GOSt</a>';
  }

}

class GprofilerView implements ModuleView {

  public function set_var(string $uid, int $id, array $more = array()) {
    $this->uid = $uid;
  }

  public function get_view() : array {
    $gprofiler_file = 'uploads/'.$this->uid.'/genes/gprofiler.html';
    $table_content = (file_exists($gprofiler_file)) ? file_get_contents($gprofiler_file) : 'No g:Profiler g:GOSt results found';
    return array('modules/gprofiler.html.twig', $this->vars = array('table' => $table_content));
  }

}


?>
