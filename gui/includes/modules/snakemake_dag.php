<?php

class snakemake_dag extends ModulesSetup {

  public $sidebar = array(
    array('dag.svg', 'SnakemakeDagReference', True)
  );

}

class SnakemakeDagReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  /**
   * Return "show pipeline graph" button and modal HTML code.
   * @return string HTML code with the button and the modal.
   */
  public function get_sidebar() : array {
    $path = 'uploads/'.$this->status['job_uid'].'/dag.svg';

    return array(
      'id'           => 'pipegraph',
      'button_title' => 'Pipeline Graph',
      'header'       => 'Snakemake pipeline graph',
      'content'      => '
      <img src="'.'uploads/'.$this->status['job_uid'].'/dag.svg'.'" width="100%" /><br /><br />
      <a style="margin: 0 auto;" href="'.'uploads/'.$this->status['job_uid'].'/dag.svg'.'" target="_blank" class="btn btn-danger">
        Open graph in a new window
      </a>
      ',
      'remote'       => '',
      'style'        => '',
    );
  }

}



?>
