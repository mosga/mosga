<?php

class vecscreen extends ModulesSetup {

  public $sidebar = array(
    array('validation/vecscreen/report.html','VecScreenReference', True)
  );

}

class VecScreenReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

 /**
  * @return string Returns the pop-up button for the VecScreen results.
  */
  public function get_sidebar() {
    $path = 'uploads/'.$this->status['job_uid'].'/validation/vecscreen/report.html';

    return array(
      'id'           => 'vecscreen',
      'button_title' => 'VecScreen',
      'header'       => 'VecScreen Summary',
      'remote'       => $path,
      'style'        => 'max-width:1000px;',
      'content'      => '
      <p style="text-align:justify;">
      Vector <a href="https://www.ncbi.nlm.nih.gov/tools/vecscreen/contam/" target="_blank">contamination</a> usually occurs at the beginning or end of a sequence; therefore, different criteria are applied for terminal and internal matches. VecScreen considers a match to be terminal if it starts within 25 bases of the beginning of the query sequence or stops within 25 bases of the end of the sequence. Matches that start or stop within 25 bases of another match are also treated like terminal matches. Matches are categorized according to the expected frequency of an alignment with the same score occurring between random sequences.
      </p>
     <h5 style="text-align:left;">Legend</h5>
      <table class="table table-striped table-sm">
      <thead>
        <tr>
          <th class="tg-0lax"></th>
          <th class="tg-0lax">Terminal</th>
          <th class="tg-0lax">Internal</th>
          <th class="tg-0lax">Expect 1 random match in</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="tg-0lax">Strong</td>
          <td class="tg-0lax">&gt;=24<br></td>
          <td class="tg-0lax">&gt;=30<br></td>
          <td class="tg-0lax">1,000,000 queries of length 350 kb</td>
        </tr>
        <tr>
          <td class="tg-0lax">Moderate</td>
          <td class="tg-0lax">19-23</td>
          <td class="tg-0lax">25-29</td>
          <td class="tg-0lax">1,000 queries of length 350 kb</td>
        </tr>
        <tr>
          <td class="tg-0lax">Weak</td>
          <td class="tg-0lax">16-18</td>
          <td class="tg-0lax">23-24</td>
          <td class="tg-0lax">40 queries of length 350 kb</td>
        </tr>
      </tbody>
      </table>
      <p>
       <a style="text-align:justify;" href="https://www.ncbi.nlm.nih.gov/tools/vecscreen/about/" target="_blank">For further information please read here</a> Results are also added to the JBrowse viewer.
      </p>
      <br />
      <h5 style="text-align:left;">Results (matches according to the legend)</h5>
      <p style="text-align:justify;">
       <a target="_blank" href="uploads/'.$this->status['job_uid'].'/validation/vecscreen/matches.html">A detailed table with all the exact matches can be viewed here.</a>
      </p>
      <div class="remote_content"></div>'
     );

  }

}

?>
