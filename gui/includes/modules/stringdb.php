<?php

class stringdb extends ModulesSetup {

  public $sidebar = array(
    array('genes/stringdb.html', 'StringDbReference', False)
  );

  public $submission = array(
    "stringdb.html.twig"
  );

}

class StringDbReference implements ModuleReference {

  public function set_var(array $vars = array()) {
    $this->status = $vars['status'];
  }

  /**
   * @return string Returns a button link to the STRING DB result file.
   */
  public function get_sidebar() {
    $url = 'index.php?stringdb=1&uid='.$this->status['job_uid'].'&id='.$this->status['job_id'];
    return '<a type="button" class="btn btn-sm btn-success" target="_blank" href="'.$url.'">STRING DB</a>';
  }

}

class StringDbView implements ModuleView {

  public function set_var(string $uid, int $id, array $more = array()) {
    $this->uid = $uid;
  }

  public function get_view() : array {
    $stringdb_file = 'uploads/'.$_GET['uid'].'/genes/stringdb.html';
    $table_content = (file_exists($stringdb_file)) ? file_get_contents($stringdb_file) : 'No STRING DB results found';
    return array('modules/stringdb.html.twig', $this->vars = array('table' => $table_content));
  }

}

?>
