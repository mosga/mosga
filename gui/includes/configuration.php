<?php

/**
 * Set session configuration.
 */
ini_set('session.gc_maxlifetime', 43200);

/**
 * Manages the basic configuration that could be overwritten by a config file.
 */
class MOSGAconfig {

  /**
   * Default configuration.
   */
  private $config = array(
    'cores'                    => 8,
    'delete_store_finished'    => 5,
    'delete_outdated_finished' => (60*60*24*14), # 14 days
    'list_jobs'                => 'cookie',
    'display_version'          => true,
    'mail_notification'        => true,
    'show_server_usage'        => false
  );

  /**
   * Default modules.
   */
  private $modules = array(
    'general'   => array(
      'log_viewer',
      'snakemake_dag'
    ),
    'annotation' => array(
      'organelles',
      'vecscreen',
      'cpgiscan',
      'newcpgreport',
      'spliceator',
      'gprofiler',
      'stringdb',
      'iid'
    ),
    'comparative' => array(
    )
  );

  /**
   * Initiates the configuration initalization and overwrites the configuration.
   * @param string  $cfg_file File that could overwrite the default config.
   */
  public function __construct(string $cfg_file = 'config.ini.php') {
    # Load config file if present and merge it with the defaults parameters
    if(file_exists($cfg_file))
      $this->config = array_merge($this->config, @parse_ini_file($cfg_file, false, INI_SCANNER_NORMAL));
  }

  /**
   * Returns the whole configuration as an array.
   * @return  array   the final configuration.
   */
  public function get_configs() : array {
    return $this->config;
  }

  public function get_modules() : array {
    return $this->modules;
  }

  /**
   * Returns a single configuration if it is existing otherwise returns null.
   * @param string  The name of the desired configuration entry.
   *
   * @return  null  Returns the configuration value or null.
   */
  public function get_config(string $key) {
    if( isset($this->config[$key]) ) {
      return $this->config[$key];
    }

    return null;
  }

}

?>
