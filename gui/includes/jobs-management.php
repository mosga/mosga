<?php

class Jobs {

  private $db;

  public function __construct() {
    $this->db = new JobsDatabase('/opt/mosga/gui/jobs.db');
  }

  public function submit_job(string $uid, bool $comparative = false) : int {
    $this->db->insert_job_request($uid, $comparative);
    $last_id = $this->db->get_last_id();
    return ($last_id) ? $last_id : 0;
  }

  public function count_open_jobs() : int {
    return $this->db->count_open_jobs();
  }

  public function get_queue_position(int $id) : int {
    return $this->db->get_queue_position($id);
  }

  public function get_status(int $id, string $uid) : array {
    return $this->db->get_entry($id, $uid);
  }

  public function get_job_id_from_uid(string $uid) : int {
    return $this->db->get_job_id_from_uid($uid);
  }

  public function get_job_uid_from_id(int $id) : string {
    return $this->db->get_job_uid_from_id($id);
  }

  public function get_open_jobs() : array {
    return $this->db->get_open_jobs();
  }

  public function get_active_jobs() : array {
    return $this->db->get_active_jobs();
  }

  public function get_average_runtime(int $limit = 100) : int {
    return $this->db->average_runtime($limit);
  }

  public function get_finished_jobs() : array {
    return $this->db->get_finished_jobs();
  }

  public function get_all_jobs(bool $only_not_deleted = true) {
    return $this->db->get_all_jobs($only_not_deleted);
  }

  public function search_mail(string $mail) : int {
    return $this->db->search_mail($mail);
  }

  public function insert_mail(string $mail) : int {
    return $this->db->insert_mail($mail);
  }

  public function get_user_mail(int $id) : string {
    return $this->db->get_user_mail($id);
  }

  public function notfications() : array {
    return $this->db->notfications();
  }

  public function insert_job_name(int $id, string $name) : bool {
    return $this->db->insert_job_name($id, $name);
  }

  public function update_job(int $id, array $pairs) : bool {

    foreach($pairs as $key => $value) {
      $update = $this->db->update_job($id, $key, $value);
      if(!$update) return false;
    }

    return true;
  }

  public function perform_deletion(int $id, string $uid) : bool {
    $path = 'uploads/'.$uid;
    $o = '';
    $r = null;

    if( $this->has_symlink_target($uid) )
      exec('\rm -r -f '.$path, $o, $r);

    $jbrowse_uid = 'jbrowse/mosga/'.$uid;
    if( file_exists('jbrowse/mosga/'.$uid) ) {
      @unlink($jbrowse_uid);
    }

    if($r !== null and $r === 0)
      return true;

    return false;
  }

  private function has_symlink_target(string $uid) : bool {
    $uploaded_files = array();
    if(!file_exists('uploads/'.$uid)) return true;

    $del_dir = scandir('uploads/'.$uid);
    foreach($del_dir as $del_file) {
      if($del_file == '.'
        or $del_file == '..'
        or is_dir('uploads/'.$uid.'/'.$del_file)
        or strlen($del_file) != 24
        or is_link('uploads/'.$uid.'/'.$del_file)
        )
        continue;
      $uploaded_files[] = $del_file;
    }

    if(count($uploaded_files)) {
      # Check for symlinks to that file
      foreach($uploaded_files as $ofile) {
        $symfiles = $this->find_symlink_ref($ofile, $uid);
        $rewrite = $this->rewrite_symlinks($symfiles, $ofile, $uid);
        return $rewrite;
      }
    }

    return true;
  }

  /**
   * Select a new location for a file in a uid that should be deleted and
   * replace all symlinks to that new location.
   * @param uids array Contains the uid with symlink to the file in the uid dir.
   * @param file string The file name.
   * @param uid string The original uid
   */
  private function rewrite_symlinks(array $uids, string $file, string $uid) {
    if(!count($uids)) return true;
    $moved = false;
    $processed = 0;
    $new_path = '';
    $cnt_uids = count($uids);

    while(count($uids)) {
      $link = array_pop($uids);

      $symlink_path = 'uploads/'.$link.'/'.$file;
      $remove_sym_link = unlink($symlink_path);
      if( $remove_sym_link ) {
        # Move file to old sym link path
        if(!$moved) {
          $new_path = $link;
          rename('uploads/'.$uid.'/'.$file, $symlink_path);
          $moved = true;
        # Set symlinks
        } else {
          symlink('../'.$new_path.'/'.$file, $symlink_path);
        }
        $processed++;
      }
    }

    return ($processed == $cnt_uids);
  }

  /**
   * Searches for symlinks that refer to target file in the target uid
   * @param target_file string The target file name.
   * @param target_uid string The target uid (directory).
   * @return array A list of matching symlink to the given target.
   */
  private function find_symlink_ref(string $target_file, string $target_uid) : array {
    $found = array();
    if( !file_exists('uploads/') ) return $found;

    foreach(scandir('uploads/') as $uid) {
      if ($uid == '.' or $uid == '..' or !is_dir('uploads/'.$uid) or $uid == $target_uid) continue;

      $potential_file = 'uploads/'.$uid.'/'.$target_file;
      if( file_exists($potential_file) and is_link($potential_file)) {
        $sym_link_aim = readlink($potential_file);
        if( $sym_link_aim !== false and substr($sym_link_aim,3,8) == $target_uid)
          $found[] = $uid;
      }
    }

    return $found;
  }

}

/**
 * Manages as a pseudo enumeration the possible job statuses.
 */
abstract class JobStatus {

  const Unknown = 0;
  const Queued = 1;
  const Running = 2;
  const Finished = 3;
  const Failed = 4;
  const Deleted = 5;
  const ReQueued = 6;

  /**
   * Decides the job status corresponding the job values.
   * @param job array The information about this job.
   * @return int The valid job status as integer.
  */
  public static function get_status(array $job) : int {
    if($job['job_deleted'] == 1) {
      return JobStatus::Deleted;
    } elseif($job['job_start'] == null and !$job['job_reruns']) {
      return JobStatus::Queued;
    } elseif($job['job_start'] == null and $job['job_reruns']) {
      return JobStatus::ReQueued;
    } elseif($job['job_start'] != null and $job['job_end'] == null) {
      return JobStatus::Running;
    } elseif($job['job_end'] != null and $job['job_canceled']) {
      return JobStatus::Failed;
    } elseif($job['job_end'] != null and !$job['job_canceled']) {
      return JobStatus::Finished;
    } else return JobStatus::Unknown;
  }

  /**
   * Returns the status name for a specific status integer.
   * @param $status int The status integer.
   * @return string The current status as string.
  */
  public static function get_status_name(int $status) : string {
    switch($status) {
      case JobStatus::Unknown;  return 'Unknown';   break;
      case JobStatus::Queued;   return 'Queued';    break;
      case JobStatus::Running;  return 'Running';   break;
      case JobStatus::Finished; return 'Finished';  break;
      case JobStatus::Failed;   return 'Failed';    break;
      case JobStatus::Deleted;  return 'Deleted';   break;
      case JobStatus::ReQueued; return 'Re-queued'; break;
    }

    return 'Unknown';
  }
}

?>
