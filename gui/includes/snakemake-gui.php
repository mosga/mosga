<?php

/**
 * Basic functions for the job status page, and passes the results.
 */
class JobStatusGUI {

  /**
   * Default parameters, like the data format.
   */
  public $date_format = 'Y/d/m H:i';

  /**
   * Initialize and starts the GUI processing.
   * @param array $config the current configuration.
   * @param Modules the modules object.
   */
  public function __construct($jobs, array $config, Modules $modules) {
    $this->jobs = &$jobs;
    $this->config = &$config;
    $this->status = $this->jobs->get_status(intval($_GET['id']), $_GET['uid']);
    $this->results = new JobStatusGUI_Results($this->jobs, $this->status);
    $this->result = array('not_found' => true);
    $this->modules = $modules;
  }

  /**
   * Collects all ressources for the basic Job Details page view.
   */
  public function generate() {
    if(!count($this->status) ) return null;

    $this->job_name = new JobName($this->jobs, $this->config, $this->status);
    $this->project_dir = 'uploads/'.$this->status['job_uid'].'/';

    # formats and calcualtes the at least available until date
    $this->stored_until = (isset($this->config['delete_outdated_finished']) and $this->config['delete_outdated_finished']);
    $this->stored_date = ($this->stored_until) ? date($this->date_format,$this->config['delete_outdated_finished'] + $this->status['job_end']) : '';

    # Get results values dependind off the job
    if (isset($this->status['job_comparative']) and $this->status['job_comparative']) {
      $this->mode = "comparative";
    } else {
      $this->mode = "annotation";
    }

    $mode = $this->get_mode();
    $results = $this->$mode();

    # Merge modules depending on the mode
    if( isset($this->modules->sidebars[$this->mode]) ) {
    $sidebar_modules = array_merge(
      $this->modules->sidebars[$this->mode],
      $this->modules->sidebars['general']);
    } else {
      $sidebar_modules = $this->modules->sidebars['general'];
    }

    # Iterate over each sidebar button class
    foreach($sidebar_modules as $sname => $sentry) {
      if ( ( is_string($sentry[0]) and file_exists('uploads/'.$this->status['job_uid'].'/'.$sentry[0]) ) or $sentry[0] === true) {
        $ref = new $sentry[1];
        $ref->set_var(array('status' => $this->status));
        $results['sidebar'][$sname] = array($ref->get_sidebar(), $sentry[2]);
      }
    }

    $this->result = array(
      'not_found' => false,
      'title'     => $this->job_name->status_job_name(),
      'status'    => $this->status_bar($this->status),
      'result'    => $results
    );

    # set mail notification
    if( isset($this->config['mail_notification']) and $this->config['mail_notification'] ) {
       $this->result['mail'] = $this->status_mail_notification();
    }
  }

  /**
   * Returns the panelled status bar.
   * @return  array  The status bar content
   */
  private function status_bar() : array {
    $queue_position = $this->jobs->get_queue_position($this->status['job_id']);
    if( $queue_position == 0) $queue_position = 1;
    $avg_runtime_limit = 20;
    $avg_runtime_db = $this->jobs->get_average_runtime($avg_runtime_limit);
    $avg_runtime_db_query = $avg_runtime_db * $queue_position;
    if( $avg_runtime_db_query == 0) $avg_runtime_db = 7200;
    $avg_runtime = $this->get_duration($avg_runtime_db_query);
    $runtime_btn = '
    <button type="button" style="padding-top:0;padding-bottom:0;" class="btn btn-info btn-sm" data-container="body" data-toggle="popover" data-placement="right" data-content="Current average execution time is ~'.$this->get_duration($avg_runtime_db).' per job. The execution time estimation is imprecise since it only considers average execution time of the last '.$avg_runtime_limit.' jobs, but some jobs require two days, while others are done within 5 minutes." data-original-title="" title="">?</button>';

    $current_status = JobStatus::get_status($this->status);
    if($current_status == JobStatus::Queued or $current_status == JobStatus::ReQueued) {
      $p1 = array('Status', ($current_status == JobStatus::Queued) ? 'Queued' : 'Re-queued' );
      $p2 = array('Queue position', $queue_position . ' of '. count($this->jobs->get_open_jobs()));
    } elseif($current_status == JobStatus::Running or $current_status == JobStatus::Failed) {
      $p1 = array('Status', ($current_status == JobStatus::Running) ? 'Running' : 'Failed');
      $p2 = array('Start time', date($this->date_format, $this->status['job_start']) );
    } elseif($current_status == JobStatus::Finished) {
      $p1 = array('Status', 'Finished');
      $p2 = array('Finished at', date($this->date_format, $this->status['job_end']) );
    }

    if($current_status == JobStatus::Queued or $current_status == JobStatus::ReQueued or $current_status == JobStatus::Running) {
      $p3 = array('Aprox. finished in ', $avg_runtime . ' ' . $runtime_btn );
    } else {
      $p3 = array('Finished in ', $this->get_duration($this->status['job_end']-$this->status['job_start']) . ' ' .$runtime_btn );
    }

    return array(
      'g1c1' => $p1[0], 'g1c2' => $p1[1],
      'g2c1' => $p2[0], 'g2c2' => $p2[1],
      'g3c1' => $p3[0], 'g3c2' => $p3[1],
    );
  }

  /**
   * Returns an array with content about the mail notification for the status bar.
   * @return  array  An array contaning all relevant information about the mail notification.
   */
  private function status_mail_notification() : array {
    $this->mail = new JobMailNotification($this->jobs, $this->config, $this->status);

    # Update status if mail was registered
    if( $this->mail->check_register() )
      $this->status = $this->jobs->get_status($this->status['job_id'], $this->status['job_uid']);

    # Restore last mail
    $last_mail = (isset($_COOKIE['last_email']) and filter_var($_COOKIE['last_email'], FILTER_VALIDATE_EMAIL)) ? $_COOKIE['last_email'] : '';

    # Not set notification but job is already finished
    if( $this->status['job_notification'] == 0 and $this->status['job_end'] != null )
      return array('invite' => false, 'overview' => false);
    # Not set notification
    elseif( $this->status['job_notification'] == 0 )
      return array('invite' => true, 'overview' => false, 'values' => array('mail' => $last_mail));
    # Notification was set
    else
      return array('invite' => false, 'overview' => true, 'values' => $this->mail->overview($this->status['job_user']));
  }

  /**
   * Prepares all relevant results for the annotation result page.
   * @return  array  all results information required for the GUI.
   */
  private function annotation() : array {
    # Parameters for the template engine
    $result = array(
      'mode'         => 'annotation',
      'path'         => $this->project_dir,
      'finished'     => ($this->status['job_end']) ? true : false,
      'stored_until' => $this->stored_until,
      'stored_date'  => $this->stored_date,
      'canceled'     => ($this->status['job_canceled'] != 0) ? true : false,
      'jbrowse'      => true,# file_exists($this->project_dir.'jbrowse2/config.json'),
      'jbrowse_url'  => 'jbrowse2/?config=mosga/'.$this->status['job_uid'].'/config.json',
      'files'        => array(),
      'predictions'  => array(),
      'sidebar'      => array(),
      'error_analysis'=> ($this->status['job_canceled']) ? $this->get_error_analysis($this->status['job_uid']) : ''
    );

    # Static files that should be linked
    $files_list = array(
      array('logs/run.txt','Snakemake log','warning'),
      array('logs/tax-summary.txt', 'Taxonomy Search', 'info'),
      array('logs/tbl2asn-discrepancy.txt','Discrepancy Report','primary'),
      array('logs/tbl2asn-error-summary.txt','Error Summary','warning'),
      array('logs/tbl2asn-validation.txt','Validation File','success'),
      array('annotation.db','Annotation database','success'),
      array('conf.json','Snakemake configuration','info'),

      # backwards-compatbility for user-interface
      array('run.log','Snakemake log','warning'),
      array('tax-summary.txt', 'Taxonomy Search', 'info'),
      array('genome.val','Validation File','success'),
      array('genome-validation.txt','Validation File','success'),
      array('errorsummary.val','Error Summary','warning'),
      array('genome-error-summary.txt','Error Summary','warning'),
      array('genome-discrepancy.txt','Discrepancy Report','primary'),
      array('report.txt','Discrepancy Report','primary'),
      array('what-to-cite.txt', 'What to cite', 'info'),
    );
    # Buttons under "Details"
    $result['files'] = $this->check_existence_and_remain($files_list, $this->project_dir);

    # Buttons under "Details / Prediction Outputs"
    $result['predictions'] = $this->check_existence_and_remain(
      $this->results->get_predictions_result_links(), $this->project_dir);

    $citation_path = $this->project_dir.'what-to-cite.txt';
    $result['citations'] = (file_exists($citation_path)) ? file($citation_path) : array();

    $result['sqn'] = (file_exists($this->project_dir.'genome.sqn'));
    $result['tbl'] = (file_exists($this->project_dir.'genome.tbl'));
    $result['tbl_alt'] = (file_exists($this->project_dir.'genome-table.txt'));

    $result['has_val'] = False;
    $result['val'] = array(
      'busco'     => file_exists($this->project_dir.'validation/busco/busco.svg'),
      'eukcc'     => file_exists($this->project_dir.'validation/eukcc/eukcc.svg'),
      'blobtools' => file_exists($this->project_dir.'validation/blobtools/plot.svg')
    );
    foreach($result['val'] as $v) {
      if($v) {
        $result['has_val'] = True;
        break;
      }
    }

    # Check if a summary exists
    if( file_exists($this->project_dir.'summary.html') ) {
      if( filesize($this->project_dir.'summary.html') <= 1024 * 1024 * 4 ) {
        $result['summary'] = file_get_contents($this->project_dir.'summary.html');
      } else {
        $result['summary'] = '<strong>The summary file exceeds the memory limit; <a href="'.$this->project_dir.'summary.html" target="_blank">please open it manually here.</a></strong>';
      }
    } else {
      $resul['summary'] = '';
    }

    # Check if an overall summary exists
    $result['overall_summary'] = (file_exists($this->project_dir.'overall_summary.html')) ? file_get_contents($this->project_dir.'overall_summary.html') : '';

    # Taxonomy summary
    $result['tax-summary'] = (file_exists($this->project_dir.'tax-summary.html')) ? file_get_contents($this->project_dir.'tax-summary.html') : '';

    return $result;
  }

  /**
   * Prepares all relevant results for the comparative genomics result page.
   * @return  array  all results information required for the GUI.
   */
  private function comparative() {
    # Parameters for the template engine
    $result = array(
      'mode'         => 'comparative',
      'path'         => $this->project_dir,
      'finished'     => ($this->status['job_end']) ? true : false,
      'stored_until' => $this->stored_until,
      'stored_date'  => $this->stored_date,
      'canceled'     => ($this->status['job_canceled'] != 0) ? true : false,
      'files'        => array(),
      'sidebar'      => array(),
      'error_analysis'=> ($this->status['job_canceled']) ? $this->get_error_analysis($this->status['job_uid']) : ''
    );

    # Static files that should be linked
    $files_list = array(
      array('conf.json','Snakemake configuration','info'),
      array('run.log','Snakemake log','warning'),
      array('logs/run.log','Snakemake log','warning'),
      array('what-to-cite.txt', 'What to cite', 'info')
    );
    # Buttons under "Details"
    $result['files'] = $this->check_existence_and_remain($files_list, $this->project_dir);

    # Buttons under "Details / Prediction Outputs"
    $result['predictions'] = $this->check_existence_and_remain(
      $this->results->get_predictions_results_by_rules('comparative'), $this->project_dir);

    # Add citations under "Details"
    $citation_path = $this->project_dir.'what-to-cite.txt';
    $result['citations'] = (file_exists($citation_path)) ? file($citation_path) : array();

    # Check for a gene comparison result
    $result['gc']['out'] = (file_exists($this->project_dir.'gene_comparison/cg_gene_matches.html')) ? file_get_contents($this->project_dir.'gene_comparison/cg_gene_matches.html') : '';
    $result['gc']['out_raw'] = (file_exists($this->project_dir.'gene_comparison/cg_gene_matches.txt'));
    $result['gc']['similarity'] = (file_exists($this->project_dir.'gene_comparison/cg_genome_similarity.svg'));

    # Check for a average nucleotide identity result
    $result['ani']['out'] = $this->results->build_table($this->project_dir.'ani/ani.out', 'ani_table', ['Query genome', 'Reference genome', 'ANI value', 'Bidirectional fragments', 'Total fragments']);
    $result['ani']['heatmap'] = file_exists($this->project_dir.'ani/heatmap.svg');

    $result['eukcc']['completeness'] = file_exists($this->project_dir.'eukcc/scatter_plot.svg');
    $result['busco']['completeness'] = file_exists($this->project_dir.'busco/bar_chart.svg');
    $result['blobtools']['plot'] = file_exists($this->project_dir.'blobtools/plot.svg');
    $result['blobtools']['cvg'] = file_exists($this->project_dir.'blobtools/read_coverage.svg');

    # Check for phylogenetic results
    $result['phy_tree_nwk'] = (file_exists($this->project_dir.'phylogenetic/phy_tree.nwk'));
    $result['phy_tree_svg'] = (file_exists($this->project_dir.'phylogenetic/phy_tree.svg'));

    return $result;
  }

  /**
   * Checks if a given paths from an array exists in the a project directory; if not remove from array.
   * @param array  $bucket  The 2D-array with the paths as first element.
   * @param string  $project_path The path to the project directory.
   */
  private function check_existence_and_remain(array $bucket, string $project_path) : array {
    $arr = array();
    if (count($bucket)) {
      foreach($bucket as $file) {
        if( file_exists($project_path.$file[0])) {
          $arr[] = $file;
        } elseif( file_exists($project_path.$file[0] . '.gz') ) {
          $file[0] .= '.gz';
          $arr[] = $file;
        }
      }
    }
    return $arr;
  }

  /**
   *
   * Not finished, should try to interpretate the aborted job execution.
   * @param string  $uid  The unique project identifier.
   * @return  string  Returns potential reseason for the job abortion.
   */
  protected function get_error_analysis(string $uid) : string {
    $err = new ErrorAnalyzer($this->config, $this->jobs, $this->status['job_uid']);
    return "";
  }

  /**
   * Returns a formated duration time (3600 -> 1 h; 3660 -> 1 h 1 min).
   * @param int $time The time in seconds.
   * @param string  The formatted time.
   */
  private function get_duration(int $time) : string {
    $days = floor( $time / (24*60*60) );
    $hours = floor( ($time - ($days*24*60*60)) / (60*60) );
    $minutes = floor( ($time - ($days*24*60*60)-($hours*60*60)) / 60 );
    if($days)
      return $days . ' d ' . $hours . ' h ' . $minutes . ' min';
    elseif($hours)
      return $hours . ' h ' . $minutes . ' min';
    return $minutes . ' min';
  }

  public function get_output() : array {
    return $this->result;
  }

  public function get_mode() : string {
    return $this->mode;
  }

}

?>
