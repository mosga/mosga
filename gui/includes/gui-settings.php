<?php

class GuiBuilderSettings {

  function settings(array $settings, string $prefix) : array {
    $entries = array();
    $html = '';

    foreach($settings as $settingname => $setting) {
      if(!isset($setting['type'])) continue;

      $taxonomy = false;
      if(isset($setting['taxonomy']) and $setting['taxonomy']) {
        $class = "taxonomy";
        $taxonomy = true;
      } else {
        $class = "";
      }

      $entry = array (
        'title' => $setting['title'],
        'type'  => $setting['type']
      );

      switch($setting['type']) {
        case 'radio':
        case 'select':
        case 'input':
        case 'range_slider':

          # overwrite radio to select
          if( $setting['type'] == 'radio'
            and (isset($setting['values'])
            and isset($setting['values'][0])
            and is_array($setting['values'][0]))
            and count($setting['values'][0]) > 10 )
              $entry['type'] = 'select';

          # Add white-spaces for selects
          if( $entry['type'] == 'select' && count($setting['values']) && isset($setting['values'][0]) && count($setting['values'][0]) ) {
            foreach($setting['values'][0] as &$v) {
              if( isset($v['title']) and strlen($v['title']) )
              $v['title'] = str_replace(' ', '&nbsp;', $v['title']);
            }
          }

          $entry['para'] = array(
            'name'    => $settingname,
            'setting' => $setting,
            'prefix'  => $prefix,
            'class'   => $class
          );

        break;
        default:
          $entry['type'] = 'none';
        break;
      }

      $entries[] = $entry;
    }
    return $entries;
  }

  public function additional_files(array $adds, string $prefix) : array {
    $files = array();
    foreach( $adds[0] as $name => $add) {
      $fileid = $prefix.'_'.$name;
      $files[] = array(
        'title' => $add['title'],
        'name'  => $fileid,
        'id'    => $fileid,
        'type'  => $add['type'],
        'label' => $fileid,
        'desc'  => $add['description'],
      );
    }
    return $files;
  }

}
?>
