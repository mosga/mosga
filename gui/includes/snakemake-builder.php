<?php

/**
 * Generates the Snakemake configuration file for each submission.
 */
class SnakemakeConfigBuilder {

  public $bucket = array();
  public $file_bucket = array();
  private $config_ini;

  /**
   * Initialize the Snakemake configuration generation.
   * @param array  $settings  The project data ($_POST)
   * @param string $uid       The current unique project identifier.
   * @param array  $config    The system wide configuration.
   * @param string $mode      Differentiate between annotation and comparative genomics.
   */
  function __construct(array $settings, string $uid, array $config, string $mode) {
    # remove illegal characters
    $illegal_chars = array(';', '|', '&', '"', '\'', '>', '<', '{', '}',',');
    #$settings = $this->stripall($illegal_chars, '', $settings);
    array_walk_recursive($settings , function (&$value) {$value = str_replace(array(';', '|', '&', '"', '\'', '>', '<', '{', '}',','), '', $value); });
    #$settings = str_replace($illegal_chars, '', $settings);

    $this->mode = $mode;
    $this->config_ini = $config;
    $this->project_hash = $uid;

    if( $mode == 'comparative' ) {  # Comparative
      $settings['outgroups'] = preg_split('/,/', $settings['outgroups'], -1, PREG_SPLIT_NO_EMPTY); # format outgroups
      $upload = [];
      if ( is_dir('uploads/'.$this->project_hash) )
      {
        $file_list = $this->get_files_list();
        $upload = $this->rename_files($file_list);
      } else {
        @mkdir('uploads/'.$this->project_hash);
        @chmod('uploads/'.$this->project_hash, 0777);
      }
      if ( !empty($settings['files']) )
      {
        $upload = $this->copy_files_list($upload, $settings['files']);
      }
      $this->file_bucket = $upload;
    } else {    # Annotation
      $file_list = $this->get_files_list();
      $upload = $this->rename_files($file_list);
      $this->collect_in_bucket($this->file_bucket, $upload);

      # Check GenBank submission details
      $this->prepare_genbank($settings);
    }

    # TODO: Re-check unsetting
    unset($settings['files'], $settings['specie'], $settings['strain'], $settings['prefix'], $settings['loc_prefix']);

    $this->collect_in_bucket($this->bucket, $settings);
    $config = $this->construct_config();
  }

  /**
   * Rename the files according the hash sums.
   * @param array $files  A list of files.
   * @return  array a list with renamed files.
   */
  private function rename_files(array $files) : array {
    $file_list = array();

    foreach($files as $file_name => $file) {
      if (is_array(array_values($file)[0]))
      {
        foreach ($file as $f) {
          if ( isset($f['error']) and $f['error'] ) continue;
          $file_list[$file_name][] = $this->rename_file($f);
        }
      } else {
        if ( isset($file['error']) and $file['error'] ) continue;
        $file_list[$file_name] = $this->rename_file($file);
      }
    }

    return $file_list;
  }

  /**
   * Rename each file.
   * Optionally, duplicated files were removed and linked.
   * @param array $file Complete file name list.
   * @param array List of the new file names.
   */
  private function rename_file(array $file) : array {
    $old_path = 'uploads/'.$this->project_hash.'/'.$file['name'];
    $hash_sum = substr(md5_file($old_path), 8);
    $file['path'] = $hash_sum;
    rename($old_path, 'uploads/'.$this->project_hash . '/' . $file['path']);
    $this->check_duplicated_file($hash_sum, $this->project_hash);
    return $file;
  }


  private function get_files_list() : array {
    $file_list = array();

    $path = 'uploads/'.$this->project_hash.'/'.$this->project_hash.'.json';
    if( file_exists($path) ) {
      $files = file_get_contents($path);
      $json = json_decode($files, true);
      foreach($json['files'] as $name => $entry) {
        if (is_array(array_values($entry)[0])) {
          foreach ($entry as $e) {
            unset($e['url']);
            if( file_exists('uploads/'.$this->project_hash.'/'.$e['name']) )
            {
              $e['origin'] = $this->project_hash;
              $file_list[$name][] = $e;
            }
          }
        } else {
          unset($entry['url']);
          if( file_exists('uploads/'.$this->project_hash.'/'.$entry['name']) ) {
            $entry['origin'] = $this->project_hash;
            $file_list[$name] = $entry;
          }
        }
      }
    }

    return $file_list;
  }

  private function copy_files_list(array $file_list, array $files) : array {
    foreach($files as $job_file) {
      $file = preg_split('/\s+/', $job_file, 2, PREG_SPLIT_NO_EMPTY);

      $conf = 'uploads/'.$file[0].'/conf.json';
      if ( file_exists($conf) ) {
        # copy json settings
        $json = json_decode(file_get_contents($conf), true);
        if ( isset($json['files']['genome']) )
          $f = $json['files']['genome'][0];
        else
        {
          $f = array_filter($json['files']['genomes'], function($e) use($file){ return $e['name'] == $file[1]; });
          $f = array_values($f)[0];
        }
        $file_list['genomes'][] = $f;
        # copy file
        copy('uploads/'.$file[0].'/'.$f['path'], 'uploads/'.$this->project_hash.'/'.$f['path']);
      }
    }
    return $file_list;
  }


  /**
   * Checks out if a file is twice uploaded, removes the new file and symlink the old to it.
   * @param string  $file The filename of the new file.
   * @param string  $uid  The new unique project hash identifier.
   */
  public function check_duplicated_file(string $file, string $uid) : bool {
    $original_file = 'uploads/'.$uid.'/'.$file;
    # Remember the original file size (avoid hash collisions)
    $size = filesize($original_file);
    $dirs = scandir('uploads/');
    $found = false;
    if (!count($dirs)) return false;

    # Iteratve over each directory and checks if the file exists already.
    foreach($dirs as $dir) {
      if($dir == '.' or $dir == '..' or $dir == '.htaccess' or $dir == $uid) continue;
      $new_file = 'uploads/'.$dir.'/'.$file;

      if( file_exists($new_file) and !is_link($new_file) and $size == filesize($new_file) ) {
        $found = true;
        break;
      }
    }

    # Stop if not file appears twice
    if( !$found ) return false;

    # Renames the original file (add suffix _tmp)
    rename($original_file, $original_file.'_tmp');

    # Try to create a symbolic link
    $sym = symlink('../'.$dir.'/'.$file, $original_file);

    # If symbolic link has be created, remove the renamed original file.
    if( $sym ) {
      unlink($original_file.'_tmp');
    # catch the missed symlink creation and rename the orignal file back again.
    } else {
      rename($original_file.'_tmp', $original_file);
    }

    return true;
  }

  /**
   * Overwrite GenBank relevant entries and filters them.
   * @param array &$settings  A reference to the current settings.
   */
  private function prepare_genbank(array &$settings) {
    $shell_filter = array('&', '|', '"', ';', '>', '<','-f', '-r');
    $genbank = array();
    $genbank['specie'] = $this->check_post_existence($settings, 'specie','',$shell_filter);
    $genbank['strain'] = $this->check_post_existence($settings, 'strain','',$shell_filter);
    $genbank['prefix'] = $this->check_post_existence($settings, 'prefix','',  array('&', '"', ';', '>', '<','-f', '-r') );
    $genbank['loc_prefix'] = $this->check_post_existence($settings, 'loc_prefix','',$shell_filter);
    $genbank['polyN'] = $this->check_post_existence($settings, 'polyN','',$shell_filter);
    $genbank['linkage'] = $this->check_post_existence($settings, 'linkage','',$shell_filter);
    $this->genbank = $genbank;
  }

  /**
   * Checks if a post entry exists or not.
   * @param array   &$s       Array with all settings.
   * @param string  $name     The key that should be inside of the settings.
   * @param string  $default  A default return value.
   * @param array   $filter   is not null, filters were applied.
   * @return  string  Returns the found value or the default argument.
   */
  private function check_post_existence(array &$s, string $name = null, string $default = null, array $filter = null) {
    $c = 0;
    if ( (isset($s[$name]) and strlen($s[$name])) ) {

      if ($filter != null)
        str_replace($filter, '', $s[$name], $c);

      if ($c) {
        unset($s[$name]);
        return $default;
      }

      $t = sprintf('%s',$s[$name]);
      unset($s[$name]);
      return $t;
    } else {
      return $default;
    }
  }

  private function collect_in_bucket(array &$bucket, &$values) {
    foreach($values as $value_name => $value) {
      $tmp = &$bucket;
      $names = explode('_',$value_name);
      foreach($names as $name) {
        if($name == 'slider' OR $name == 'input')
          $name = 'filter';
        $tmp = &$tmp[$name];
      }
        $tmp[] = $value;
    }
  }

  /**
   * Removes basically already "disabled" options as far as possible from the tree.
   * @param array     The whole config values.
   * @return  array   Cleaned config values.
   */
  private function clean_config(array $config) : array {
    $to_remove = array();
    foreach($config as $k => $c) {
      if( is_array($config[$k]) ) {
        foreach($config[$k] as $tk => $tc) {
          if(is_numeric($tk) and is_array($tc)) {
            foreach($tc as $n => $s) {
              if(substr_count($s, '_none'))
                $to_remove[] = $k;
            }
          } elseif(is_array($tc)) {
            foreach($tc as $n => $s) {
              if(!is_array($s) && substr_count($s, '_none')) {
                $to_remove[] = $k;
              } elseif(is_array($s)) {
                foreach($s as $sk => $sv) {
                  if(!is_array($sv) && substr_count($sv, '_none')) {
                    if(isset($config[$k][$tk]))
                      unset($config[$k][$tk]);
                  }
                }
              }
            }
          }
        }
      }
    }

    foreach($to_remove as $remove_key) {
      unset($config[$remove_key]);
      if(isset($config['files'][$remove_key]))
        echo $remove_key;
        unset($config['files'][$remove_key]);
    }

    return $config;
  }

  /**
   * Try to identify the current git version.
   * @return  string  The current git hash or an empty string.
   */
  private function get_git_version() : string {
    if(!is_dir('../.git')) return '';
    
    $cmd = 'cd .. && git log -p -1 | head -n 1 | awk \'{print $2}\'';
    $exit = 0;
    $output = '';
    exec($cmd, $output, $exit);

    # Positive result
    if(is_array($output) and count($output))
      return $output[0];

    return '';
  }

  public function construct_config() : bool {
    $config = $this->bucket;
    $config['mode'] = $this->mode;
    $config['files'] = $this->file_bucket;
    $config_path = 'uploads/'.$this->project_hash.'/conf.json';

    $this->config = $this->clean_config($config);
    $this->config['dir'] = $this->project_hash;
    $this->config['genbank'] = $this->genbank;
    if(isset($this->config['priority'])) {
      $this->config['priority_list'] = $this->config['priority']['list'][0];
    } else {
      $this->config['priority_list'] = '';
    }

    /* Clean up */
    unset($this->config['priority']);

    # Add git repo versioning
    $this->config['git_commit'] = $this->get_git_version();
    $this->config['cores'] = $this->config_ini['cores'];

    $fp = fopen($config_path,'w');
    fwrite($fp, json_encode($this->config, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT));
    fclose($fp);

    return (file_exists($config_path));
  }

  public function get_config() : array {
    return $this->config;
  }

  public function get_project_hash() : string {
    return $this->project_hash;
  }




}

?>
