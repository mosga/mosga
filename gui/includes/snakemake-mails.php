<?php

class JobMailNotification extends JobStatusGUI {

  public function __construct( $jobs, array $config, array $status) {
    $this->jobs = &$jobs;
    $this->config = &$config;
    $this->status = &$status;
  }

  public function check_register() : bool {

    # Check for valid email address
    if( isset($_POST['mail_notify']) and filter_var($_POST['mail_notify'], FILTER_VALIDATE_EMAIL) ) {
      $mail = $_POST['mail_notify'];

      # check if email adress is already known
      $mail_id = $this->jobs->search_mail($mail);
      if( $mail_id == 0 )
        $mail_id = $this->jobs->insert_mail($mail);

      if( $mail_id == 0) return false;

      # Store mail address in a session
      $servername = (isset($_SERVER['SERVER_NAME']) and strlen($_SERVER['SERVER_NAME'])) ? $_SERVER['SERVER_NAME'] : null;
      $http_host = (isset($_SERVER['HTTP_HOST']) and strlen($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : null;
      if( $servername != null )
        setcookie('last_email', $mail, time()+43200, '', $servername, false);
      elseif( $http_host != null )
        setcookie('last_email', $mail, time()+43200, '', $http_host, false);

      $this->jobs->update_job($this->status['job_id'],array(
        'job_user' => $mail_id,
        'job_notification' => 1,
      ));

      return true;
    }

    return false;
  }

  public function overview(int $user_id) : array {
    $notified = ($this->status['job_notifed'] != 0) ? date($this->date_format, $this->status['job_notifed']) : 'Not notified yet.';
    return array('mail' => $this->jobs->get_user_mail($user_id), 'status' => $notified);
  }

}

?>
