<?php

/**
 * Generates and formats the job list.
 */
class SnakeMake_JobList extends JobStatusGUI {

  /**
   * Inherits jobs instance and configuration.
   * @param jobs Jobs Object with database functions.
   * @param config array Configuration array for this instance.
   */
  public function __construct(Jobs $jobs, array $config) {
    $this->jobs = $jobs;
    $this->config = $config;
  }

  /**
   * Reads the job depended on the Snakemake configuration file and returns the
   * json configuration.
   * @param uid string The unique job identification to check for.
   * @return string The original json configuration as array.
  */
  private function get_genome_configuration(string $uid) : array {
    $filename = 'uploads/'.$uid.'/conf.json';
    if ( file_exists($filename) ) {
      $json = json_decode(file_get_contents($filename), true);
      if( $json == null) return array();
      return $json;
    }
    return array();
  }

  /**
   * Reads the original json configuration and returns the
   * genome assembly file name as array.
   * @param json string The original json configuration as array.
   * @return array The original fasta file names.
  */
  private function get_genome_filenames(array $json) : array {
    if( isset($json['files']['genome'][0]['name']) ) return [$json['files']['genome'][0]['name']];
    elseif( isset($json['files']['genomes']) ) {
      return array_map(function($e) { return $e['name']; }, $json['files']['genomes']);
    }
    return [];
  }

  /**
   * Builds, filters and formats the complete job list.
   * @param only_not_deleted bool List deleted jobs.
   * @param filter_by_cookie bool Decide whether the job list will be filtered
   * according to a cookie or not.
   * @return array Return the formated job list.
  */
  private function build_job_list(bool $only_not_deleted = true, bool $filter_by_cookie = false) : array {
    $data = array();
    $jobs = $this->jobs->get_all_jobs($only_not_deleted);
    $passwd = ((isset($this->config['passwd']) and isset($_GET['passwd'])) and $this->config['passwd'] == $_GET['passwd']) ? true : false;

    if( $filter_by_cookie and !$passwd and isset($_COOKIE['job_uids']) ) {
      $uids = $_COOKIE['job_uids'];
      if( $uids == null ) return array();
      $uids = unserialize($_COOKIE['job_uids'], ["allowed_classes" => false]);
    }

    foreach($jobs as $job) {
      # Add the job list to an array and filter the list directly
      if( (!$filter_by_cookie xor $filter_by_cookie and isset($uids[$job['job_id']]) and $uids[$job['job_id']] == $job['job_uid']) or $passwd )
      {
        $json = $this->get_genome_configuration($job['job_uid']);
        # fallback
        $json['mode'] = ( !isset($json['mode']) ) ? 'annotation' : $json['mode'];
        $data[] = array(
          $job['job_uid'],
          $job['job_id'],
          (isset($job['job_name']) ? $job['job_name'] : ''),
          ($this->get_genome_filenames($json)),
          date($this->date_format, $job['job_request_time']),
          ($job['job_start'] != null) ? date($this->date_format, $job['job_start']) : '-',
          ($job['job_end'] != null) ? date($this->date_format, $job['job_end']) : '-',
          ucfirst($json['mode']),
          JobStatus::get_status_name(JobStatus::get_status($job))
        );
      }
    }

    return $data;
  }

   /**
    * Executes the table building and return the whole job table as array.
    * @param uids List of jobs to be filtered by. (optional)
    * @return array The complete job table (UID/ID/name/file names/submission date/start date/end date/status).
    */
   public function get_job_list(array $uids = []) : array {
     $only_not_deleted = (! (isset($_GET['showall']) and $_GET['showall'] == 1)) ? true : false;
     $only_cookie = $this->config['list_jobs'] == 'cookie';
     $job_list = $this->build_job_list( $only_not_deleted, $only_cookie );

     if ( empty($uids) ) return $job_list;
     else
     {      # TODO: filter the entries directly, do not get them all out of database
       return array_filter($job_list, function($e) use($uids){ return in_array($e[0], $uids); });
     }
     return array();
   }
}

?>
