<?php

/**
 * This class is responsible for the output of the results page.
 * It resolves the results file link buttons and the links in the sidebar.
 */
class JobStatusGUI_Results extends JobStatusGUI {

  /**
   * Inherits the jobs and states.
   * @param jobs Jobs Object with the function to access to the database.
   * @param status array Contains status information about a specific job.
   * @var prediction_counter int Amount of displayed and found prediction outputs.
   */
  public function __construct(Jobs $jobs, array $status) {
    $this->jobs = &$jobs;
    $this->status = &$status;
    $this->prediction_counter = 0;
  }

  /**
   * Parses the given json_rules according to our guideline and collects presented output file linkages.
   * @param json_rule string Contains the file path to the json gui-rules.
   * @param json_key string Declares if and which array key should be used for parsing.
   * @return array A dictionary with all known output files from the prediction tools
   */
  protected function collect_outputs_by_rules(string $json_rule, string $json_key = null) : array {
    $output_files = array();

    if(file_exists($json_rule)) {
      $json_string = file_get_contents($json_rule);
      $json = json_decode($json_string, true);

      if( $json_key != null and isset($json[$json_key]) ) {
        $json = $json[$json_key];
      }

      # $json[mosga][]
      foreach($json as $type) {

        if(!isset($type['content'])) continue;

        foreach($type as $list) {
          if( !is_array($list) or !count($list) ) continue;
          # $json[mosga][][content][][]
          foreach($list as $tool_list) {
            if(!isset($tool_list['tools'])) continue;

            # $json[mosga][][content][][][tools][]
            foreach($tool_list['tools'] as $name => $tool) {

              if( isset($tool['output']) and count($tool['output'])) {
                $output_files[$name] = array(
                  'output' => $tool['output'],
                  'type'   => $type_title = (isset($type['title'])) ? $type['title'] : null,
                  'title'  => (isset($tool['title'])) ? $tool['title'] : null
                );
              }
            }
          }
        }
      }
    }

    return $output_files;
  }

  /**
   * Parses the array of given prediction output file informations into HTML.
   * @param json_key string Declares if and which array key should be used for parsing.
   * @return string The HTML output.
   */
  protected function get_predictions_results_by_rules(string $json_key = null) : array {
    $bucket = array();
    $outputs = $this->collect_outputs_by_rules('gui-rules.json', $json_key);

    # iterate over all tools
    foreach( $outputs as $tool_name => $tool ) {

      # iterate over all output files from each tool
      foreach( $tool['output'] as $output_name => $output) {
        if(!isset($output['file'])) continue;
        $bucket[] = array(
          $output['file'],
          (isset($output['title'])) ? ((substr($output['title'], 0, 1) === '~') ? substr($output['title'], 1) : $tool['title'] . ' ' . $output['title']) : $output['file'],
          (isset($output['class'])) ? $output['class'] : 'secondary btn-sm',
        );
      }
    }

    return $bucket;
  }

  /**
   * Combines hard-coded results buttons with dynamically generated buttons.
   * @return string HTML code with all "predictou outputs"-buttons.
   */
  protected function get_predictions_result_links() : array {
    # Results output
    $static_files = array(
      array('genome.gff','Genome Annotation GFF','success btn-sm', $this->prediction_counter),
      array('writer.log','Feature Table Writing','secondary btn-sm', $this->prediction_counter),
      array('organelles.txt','Organelle Scan','secondary btn-sm', $this->prediction_counter),
      array('genes/mitos.txt','Mitos','secondary btn-sm', $this->prediction_counter),
      array('genes/plastids.txt','Plastids','secondary btn-sm', $this->prediction_counter),
    );
    $outputs = array_merge($static_files, $this->get_predictions_results_by_rules('annotation'));

    return $outputs;
  }

  /**
  * Parses a table out of a csv file, e.g. ANI results.
  * @param path Path to the file which should be parsed into a HTML table.
  * @param id ID which should be used for the table.
  * @param header Header which should be used for the table.
  * @return string Returns the HTML-table as string.
  */
  protected function build_table($path, $id, $header) : string {
    if (file_exists($path)) {
      $t = '<table id="'.$id.'" class="table table-striped table-sm">';
      $t .= '<thead><tr>';
      foreach($header as $h) {
        $t .= '<th>'.$h.'</th>';
      }
      $t .= '</tr></thead><tbody>';
      if ($file = fopen($path, "r")) {
        while(!feof($file)) {
          $line = fgets($file);
          if (strlen($line) > 0) {
            $td = explode(',', $line);
            $t .= '<tr>';
            foreach($td as $d) {
              $t .= '<td>'.$d.'</td>';
            }
            $t .= '</tr>';
          }
      }
      fclose($file);
      }
      $t .= '</tbody></table>';
      return $t;
    }
    return '';
  }

  /**
   * @return int Returns the current number of the prediction counter.
   */
  protected function get_prediction_counter() : int {
    return $this->prediction_counter;
  }

}

?>
