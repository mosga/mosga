<?php
# Copyright © 2022 Roman Martin
require_once('includes/configuration.php');
require_once('includes/jobs-database.php');
require_once('includes/jobs-management.php');

/**
 * Checks for queued jobs, starts, stops and deletes finished jobs.
 */
class Scheduler {

  /**
   * @var jobs Jobs An Instance for the database job interface.
   * @var slots int The number of how many jobs can simultaneously run.
   * TODO: Snakemake locking bug allows only to run concurrently one job.
   */
  private $jobs;
  private $slots = 1;

  /**
   * Initiates the class by reading the configuration file if available. Checks
   * for running and finished jobs and starts Snakemake execution on open jobs.
   */
  public function __construct() {
    $config = new MOSGAconfig();
    $this->config = $config->get_configs();
    $this->jobs = new Jobs();

    # Check for queued jobs #
    $running_jobs = $this->check_running_jobs();

    # Check for waiting jobs
    $open_jobs = $this->jobs->get_open_jobs();

    if( count($open_jobs) ) {
      if($this->slots - count($running_jobs) > 0) {
        $this->start_job($open_jobs[0]['job_id'], $open_jobs[0]['job_uid']);
      }
    }

    # Delete outdated annotations
    $this->delete_old_jobs();

    # Check for incomplete uploads
    $this->check_incomplete_uploads();

    # Send notifications
    if( $this->config['mail_notification'] ) $this->notify();
  }

  /**
   * Checks for open jobs.
   * @return array List of all active jobs with their attributes and values.
   */
  private function check_running_jobs() : array {
    return $this->jobs->get_active_jobs();
  }

  /**
   * Stats the job execution.
   * @param id int Job identifier.
   * @param uid string Job uniqu identifier (shorted hash)
   */
  private function start_job(int $id, string $uid) {
    $this->jobs->get_status($id, $uid);

    # insert job_start
    $this->jobs->update_job($id, array('job_start' => time()));

    # check for existing log directory
    if (!file_exists("uploads/$uid/logs")) {
      $this->create_log_dir($uid);
    }

    # exit code != 0 => cancel
    if( $this->dry_run($uid) ) {
      $this->jobs->update_job($id, array(
        'job_end' => time(),
        'job_canceled' => 1,
      ));

    } else {
      $this->jobs->update_job($id, array('job_dry' => 1));
      $this->draw_dag($uid);

      # Snakemake execution
      $snakemake_run = $this->run_snakemake($uid);

      if($snakemake_run) {
        # Exit code != 0
        $this->jobs->update_job($id, array(
          'job_end' => time(),
          'job_canceled' => 1,
        ));
      } else {
        # Runs without issues
        $this->jobs->update_job($id, array('job_end' => time()));
      }
    }

  }

  /**
   * Checks if jobs have to be deleted and performs the deletion.
   */
  private function delete_old_jobs() {
    $finished_jobs = $this->jobs->get_finished_jobs();
    $to_delete = array();

    if( !count($finished_jobs)) return false;

    # Delete if jobs were to old and preserve the last x jobs
    if( $this->config['delete_store_finished'] != 0 and $this->config['delete_outdated_finished'] != 0) {
      $too_old = $this->too_old_jobs($finished_jobs);

      $diff = count($too_old) - $this->config['delete_store_finished'];
      if($diff) {
        $too_old_rev = array_reverse($too_old);
        for($c = $diff; $c >= 0; $c--) {
          $to_delete[] = $too_old_rev[$c];
        }

      }

    } elseif( $this->config['delete_outdated_finished'] != 0) {
      $to_delete = $this->too_old_jobs($finished_jobs);
    } elseif( $this->config['delete_store_finished'] != 0 ) {
      $to_delete = $this->too_much_jobs($finished_jobs);
    }

    if( ! count($to_delete) ) return false;

    # Perform the actual deletion
    foreach( $to_delete as $j ) {
      $uid = $j[1];
      $id = $j[0];

      if(is_dir('uploads/'.$uid)) {
        echo 'Delete: '.$uid;
        $deletion = $this->jobs->perform_deletion($id, $uid);

        if($deletion) {
          $this->jobs->update_job($id, array('job_deleted' => 1));
          echo '- successfull deleted'."\r\n";
        } else {
          echo '- could not be deleted'."\r\n";
        }

      } else {
        echo 'Directory '. $uid .' does not exists'."\r\n";
        $this->jobs->update_job($id, array('job_deleted' => 1));
      }
    }

    return true;
  }

  /**
   * Counts have many jobs have to be preserved.
   * @param jobs array The list of not deleted jobs.
   * @return array A list of filtered jobs.
   */
  private function too_much_jobs(array $jobs) : array {
    $n = array();
    $c = count($jobs);
    for( $i = 0; $i < count($jobs); $i++ ) {
      if( $c > $this->config['delete_store_finished'] )
        $n[] = array($jobs[$i]['job_id'],$jobs[$i]['job_uid']);
      $c--;
    }
    return $n;
  }

  /**
   * Checks whetver a job is too old.
   * @return array A list of filtered jobs.
   */
  private function too_old_jobs(array $jobs) : array {
    $n = array();
    # Remove old jobs
    foreach( $jobs as $job ) {
      if( $job['job_end'] < time() - $this->config['delete_outdated_finished'] ) {
        $n[] = array($job['job_id'],$job['job_uid']);
      }
    }
    return $n;
  }

  /**
   * Notifes the user about a finished job.
   * @return int The number of notified user.
   */
  private function notify() : int {
    $notified = 0;

    $notifies = $this->jobs->notfications();
    if( !count($notifies) ) return $notified;

    $mail_template = file_get_contents('includes/mail-notifications.html');
    $hostname = (isset($this->config['hostname'])) ? $this->config['hostname'] : gethostname();
    if($hostname === false) $hostname = 'localhost';
    $url = (isset($this->config['url'])) ? $this->config['url'] : $hostname;
    $from = (isset($this->config['mail_from'])) ? $this->config['mail_from'] : 'mosga@'.$hostname;

    $headers = "From: $from\n";
    $headers .= "Content-Type: text/plain; charset=UTF-8";

    foreach($notifies as $n) {
      # let's decide the protocol (http/https) by the browser
      $mail_url = $url .'/snakemake.php?status=1&uid='.$n['job_uid'].'&id='.$n['job_id'];
      $subject = 'Your MOSGA job '.$n['job_uid'].' is finished.';
      $msg = str_replace('{$url$}',$mail_url, $mail_template);

      if( @mail( $n['mail_address'], $subject, $msg, $headers ) === true ) {
        $this->jobs->update_job( $n['job_id'], array('job_notifed' => time()) );
        echo 'Send mail to '.$n['mail_address']. '. id: '. $n['job_id'] . "\r\n";
        $notified++;
      } else {
        echo 'Could not send mail to '.$n['mail_address'] . "\r\n";
      }

    }

    return $notified;
  }

  /**
   * Creates log directory.
   * @param uid string The unique job identifier.
   * @return int The exit code.
   */
  private function create_log_dir(string $uid) : int {
    $mkdir_log_cmd = 'mkdir uploads/'.$uid.'/logs/ > /dev/null 2>&1';
    $mkdir_log_exit = 0;
    $mkdir_log_out = '';
    $mkdir_log = exec($mkdir_log_cmd, $mkdir_log_out, $mkdir_log_exit);
    return $mkdir_log_exit;
  }

  /**
   * Starts a Snakemake dry run.
   * @param uid string The unique job identifier.
   * @return int The exit code.
   */
  private function dry_run(string $uid) : int {
    $dry_run_cmd = 'cd ../snakemake && snakemake -n --configfile ../gui/uploads/'.$uid.'/conf.json > ../gui/uploads/'.$uid.'/logs/dry_run.txt 2>&1';
    $dry_run_exit = 0;
    $dry_run_output = '';
    $dry_run = exec($dry_run_cmd, $dry_run_output, $dry_run_exit);
    $this->remove_snakemake_tmp();
    return $dry_run_exit;
  }

  /**
   * Starts a Snakemake pipeline graph creation.
   * @param uid string The unique job identifier.
   * @return int The exit code.
   */
  private function draw_dag(string $uid) : int {
    $draw_dag_cmd = 'cd ../snakemake && snakemake -p --configfile ../gui/uploads/'.$uid.'/conf.json --dag | dot -Tsvg > ../gui/uploads/'.$uid.'/dag.svg';
    $draw_dag_exit = 0;
    $draw_dag_output = '';
    $draw_dag = exec($draw_dag_cmd, $draw_dag_output, $draw_dag_exit);
    $this->remove_snakemake_tmp();
    return $draw_dag_exit;
  }

  /**
   * Starts a actual Snakemake run.
   * @param uid string The unique job identifier.
   * @return int The exit code.
   */
  private function run_snakemake(string $uid) : int {
    $smk_run_cmd = 'cd ../snakemake && PATH=$PATH:/opt/mosga/tools/miniconda/bin/ snakemake -p --cores '.$this->config['cores'].' --configfile ../gui/uploads/'.$uid.'/conf.json --conda-frontend conda --use-conda > ../gui/uploads/'.$uid.'/logs/run.txt 2>&1';
    $smk_run_exit = 0;
    $smk_run_output = '';
    $smk_run = exec($smk_run_cmd, $smk_run_output, $smk_run_exit);
    $this->remove_snakemake_tmp();
    return $smk_run_exit;
  }

  /**
   * Removes files from .snakemake directory if it exists.
   * @return int  the exit code of the deletion.
   */
  private function remove_snakemake_tmp() : int {
    $smk_rm_cmd = 'rm -r ../snakemake/.snakemake/locks > /dev/null 2>&1 && rm -r ../snakemake/.snakemake/log > /dev/null 2>&1';
    $smk_rm_exit = 0;
    $smk_rm_out = '';
    $smk_rm = exec($smk_rm_cmd, $smk_rm_out, $smk_rm_exit);
    return $smk_rm_exit;
  }

  /**
   * Check for incomplete uploads and delete interrupted upload directories.
   * @return bool true if not failed.
   */
  private function check_incomplete_uploads() : bool {
    $upload_dir = 'uploads/';
    if( !file_exists($upload_dir)) return false;

    $files = scandir($upload_dir);
    if( !count($files) ) return false;

    foreach($files as $file) {
      if( is_dir($upload_dir . $file) ) {
        if( $file == '.' or $file == '..') continue;
        $mod_time = filemtime($upload_dir . $file);

        if( time()-$mod_time > 3600*24*2 and !file_exists($upload_dir.$file."/conf.json")) {
          echo 'Delete incomplete upload directory: '.$file."\r\n";
          $this->rrmdir($upload_dir.$file.'/');
        }
      }
    }

    return true;
  }

  /**
   * Removes a directory recursively.
   * @param string  $dir the path to the directory.
   * @return  bool  true if not failed.
   */
  private function rrmdir(string $dir) {
    if (is_dir($dir)) {
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
            $this->rrmdir($dir. DIRECTORY_SEPARATOR .$object);
          else
            unlink($dir. DIRECTORY_SEPARATOR .$object);
        }
      }
      return rmdir($dir);
    }
  }

}

$scheduler = new Scheduler();

?>
